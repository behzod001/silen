package uz.uzkassa.silen.exceptions;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class FieldErrorVM implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String objectName;
    private final String field;
    private final String message;

    public FieldErrorVM(String dto, String field, String message) {
        this.objectName = dto;
        this.field = field;
        this.message = message;
    }
}

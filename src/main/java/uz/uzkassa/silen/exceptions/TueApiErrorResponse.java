package uz.uzkassa.silen.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TueApiErrorResponse {
    private String error_message;

    @Override
    public String toString() {
        return "Ошибка от TrueApi!\n" + error_message;
    }
}

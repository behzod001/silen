package uz.uzkassa.silen.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.config.ApplicationProperties;
import uz.uzkassa.silen.security.CustomUser;

import javax.crypto.SecretKey;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class TokenProvider implements InitializingBean {
    private static final String AUTHORITIES_KEY = "auth";
    private long tokenValidityInMilliseconds;
    private final ApplicationProperties applicationProperties;
    private SecretKey key;

    @Override
    public void afterPropertiesSet() {
        log.debug("Using a Base64-encoded JWT secret key");
        this.tokenValidityInMilliseconds = 1000 * applicationProperties.getTokenValidityInSeconds();
        this.key = Keys.hmacShaKeyFor(Base64.getDecoder().decode(applicationProperties.getJwtSecretKey()));
    }

    public String createToken(Authentication authentication) {
        String authorities = authentication.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.joining(","));

        String userId = null;
        String organizationId = null;
        String organizationTin = null;

        if (authentication.getPrincipal() instanceof CustomUser customUser) {
            userId = customUser.getUserId();
            organizationId = customUser.getOrganizationId();
            organizationTin = customUser.getOrganizationTin();
        }

        long now = (new Date()).getTime();
        Date validity = new Date(now + this.tokenValidityInMilliseconds);
        return Jwts.builder()
                .setSubject(authentication.getName())
                .claim(AUTHORITIES_KEY, authorities)
                .claim("userId", userId)
                .claim("organizationId", organizationId)
                .claim("organizationTin", organizationTin)
                .claim("sessionId", System.currentTimeMillis())
                .signWith(key, SignatureAlgorithm.HS512)
                .setExpiration(validity)
                .compact();
    }

    public Authentication getAuthentication(String token) {
        Claims claims = Jwts.parser()
                .verifyWith(key)
                .build()
                .parseSignedClaims(token)
                .getPayload();

        Collection<? extends GrantedAuthority> authorities =
                Arrays.stream(claims.get(AUTHORITIES_KEY).toString().split(","))
                        .map(SimpleGrantedAuthority::new)
                        .collect(Collectors.toList());
        String userId = (String) claims.getOrDefault("userId", null);
        String organizationId = (String) claims.getOrDefault("organizationId", null);
        String organizationTin = (String) claims.getOrDefault("organizationTin", null);
        Long sessionId = (Long) claims.getOrDefault("sessionId", null);

        CustomUser principal = new CustomUser(claims.getSubject(), "", authorities, userId, organizationId, organizationTin, sessionId);

        return new UsernamePasswordAuthenticationToken(principal, token, authorities);
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().verifyWith(key).build().parseSignedClaims(authToken);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            log.info("Invalid JWT token.");
            log.trace("Invalid JWT token trace.", e);
        }
        return false;
    }
}

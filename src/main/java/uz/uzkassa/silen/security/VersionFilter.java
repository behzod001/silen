package uz.uzkassa.silen.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
@RequiredArgsConstructor
public class VersionFilter extends OncePerRequestFilter {
    @Value("${application.swagger.url}")
    String swaggerHost;

    @Value("${application.swagger.protocols}")
    String swaggerProtocol;

    @Autowired
    @Qualifier("handlerExceptionResolver")
    private HandlerExceptionResolver resolver;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        filterChain.doFilter(httpServletRequest, httpServletResponse);
        /*try {
            final String referer = HeaderUtils.getCurrentHeaderByKey("Referer");
            if (referer != null && referer.contains(swaggerHost)) {
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            }
            final String appVersion = HeaderUtils.getCurrentHeaderByKey("App_version");
            log.info("APP_VERSION {}", appVersion);
            if (StringUtils.isBlank(appVersion) || !appVersion.equalsIgnoreCase("1.5.1")) {
                throw new VersionMismatchException();
            }
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        } catch (VersionMismatchException | ExpiredJwtException | UsernameNotFoundException |
                 UserNotActivatedException ex) {
            resolver.resolveException(httpServletRequest, httpServletResponse, null, ex);
        }*/
    }
}

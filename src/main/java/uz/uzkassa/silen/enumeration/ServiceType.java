package uz.uzkassa.silen.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import uz.uzkassa.silen.dto.SelectItem;

import java.util.Arrays;
import java.util.List;

@Getter
@AllArgsConstructor
public enum ServiceType {
    E_INVOICE("СФ"),
    PRIMARY_AGGREGATION("Основной пакет"),
    PRIMARY_MARK("Основной пакет"),
    ADDITIONAL_AGGREGATION("Дополнительный пакет"),
    ADDITIONAL_MARK("Дополнительный пакет"),
    FREE_TRIAL("Пробный период");

    private final String nameRu;

    public SelectItem toSelectItem() {
        return new SelectItem(this.name(), this.getNameRu());
    }

    public static List<String> countableBillingPermissions() {
        return Arrays.asList(ServiceType.E_INVOICE.name(), ServiceType.PRIMARY_AGGREGATION.name(),
            ServiceType.ADDITIONAL_AGGREGATION.name(), ServiceType.PRIMARY_MARK.name(), ServiceType.ADDITIONAL_MARK.name());
    }

    public static List<SelectItem> givePermissionList() {
        return Arrays.asList(ServiceType.PRIMARY_AGGREGATION.toSelectItem(),
            ServiceType.ADDITIONAL_AGGREGATION.toSelectItem(),
            ServiceType.PRIMARY_MARK.toSelectItem(),
            ServiceType.ADDITIONAL_MARK.toSelectItem());
    }


}

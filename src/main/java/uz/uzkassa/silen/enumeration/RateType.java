package uz.uzkassa.silen.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import uz.uzkassa.silen.dto.SelectItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 5/8/2023 17:58
 */
@Getter
@AllArgsConstructor
public enum RateType {
    ZERO(0, "Нулевой"),
    STANDARD(1, "Стандартный");

    private final int code;
    private final String nameRu;

    public static List<SelectItem> getAll() {
        List<SelectItem> items = new ArrayList<>(RateType.values().length);
        for (RateType item : RateType.values()) {
            items.add(item.toSelectItem());
        }
        return items;
    }

    public SelectItem toSelectItem() {
        return new SelectItem(this.name(), this.getNameRu());
    }

}

package uz.uzkassa.silen.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 9/8/2023 16:20
 */
@Getter
@AllArgsConstructor
public enum TransferType implements Serializable {
    INCOME("INCOME-"),
    RETURN("RETURN-");

    final String number;
}

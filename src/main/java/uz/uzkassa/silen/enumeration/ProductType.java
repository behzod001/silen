package uz.uzkassa.silen.enumeration;

import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;
import uz.uzkassa.silen.dto.SelectItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

//@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@Getter
public enum ProductType implements Serializable {
    Alcohol("Спирт этиловый пищевой", 1000, ProductGroup.Alcohol, 13),
//    AlcoholBlock("Спирт этиловый пищевой, блоки", 1000, ProductGroup.Alcohol, 17),
    AlcoholTechnical("Спирт этиловый технический", 1001, ProductGroup.Alcohol, 13),
    Ethereum("Эфир (ЭАФ)", 1002, ProductGroup.Alcohol, 13),
    CognacAlcohol("Коньячный спирт", 2000, ProductGroup.Alcohol, 13),
    Cognac("Коньяк", 3000, ProductGroup.Alcohol, 13),
    Vodka("Водка", 4000, ProductGroup.Alcohol, 13),
    VodkaBlock("Водка, блоки", 4001, ProductGroup.Alcohol, 17),
    Wine("Вина", 5000, ProductGroup.Alcohol, 13),
    Champagne("Шампанское вино", 5001, ProductGroup.Alcohol, 13),
    Liquor("Ликёры", 6000, ProductGroup.Alcohol, 13),
    LiquorVodka("Ликёр-водка", 6001, ProductGroup.Alcohol, 13),
    WineMaterial("Винматериалы", 7000, ProductGroup.Alcohol, 13),
    NaturalWine("Натуральные вина", 8000, ProductGroup.Alcohol, 13),
    Beer("Пиво", 9000, ProductGroup.Beer, 18),
    BeerBlock("Пиво, блоки", 9001, ProductGroup.Beer, 19),
    Other("Другие", 10000, ProductGroup.Alcohol, 13),
    Tincture("Настойка", 10001, ProductGroup.Alcohol, 13),
    Cigarette("Сигареты, пачки", 11000, ProductGroup.Tobacco, 4),
    CigaretteBlock("Сигареты, блоки", 11001, ProductGroup.Tobacco, 3),
    WaterAndSoftDrinks("Вода и прохладительные напитки", 12000, ProductGroup.Water, 16),
    Medicines("Лекарственные средства", 13000, ProductGroup.Pharma, 5),
    Electronics("Бытовая техника", 14000, ProductGroup.Appliances, 24);

    private final String nameRu;
    private final Integer code;
    private final Integer templateId;
    private final ProductGroup productGroup;

    ProductType(String nameRu, Integer code, ProductGroup productGroup, Integer templateId) {
        this.nameRu = nameRu;
        this.code = code;
        this.templateId = templateId;
        this.productGroup = productGroup;
    }

    public static ProductType fromCode(Integer code) {
        for (ProductType productType : ProductType.values()) {
            if (productType.getCode().equals(code)) {
                return productType;
            }
        }
        return null;
    }

    public static ProductType fromName(String name) {
        for (ProductType productType : ProductType.values()) {
            if (productType.getNameRu().equals(name)) {
                return productType;
            }
        }
        return null;
    }

    public static Set<ProductType> byOrganizationTypes(EnumSet<OrganizationType> organizationTypes) {
        if (CollectionUtils.isEmpty(organizationTypes)) {
            return EnumSet.noneOf(ProductType.class);
        }
        EnumSet<ProductType> types = EnumSet.noneOf(ProductType.class);
        for (OrganizationType organizationType : organizationTypes) {
            if (EnumSet.of(OrganizationType.Alcohol, OrganizationType.Delivery, OrganizationType.Other).contains(organizationType)) {
                types.add(Alcohol);
                types.add(AlcoholTechnical);
                types.add(Ethereum);
            } else if (EnumSet.of(OrganizationType.Vine, OrganizationType.Wholesale, OrganizationType.AlcoholDistribution, OrganizationType.AlcoholRetail).contains(organizationType)) {
                types.add(Cognac);
                types.add(Vodka);
                types.add(VodkaBlock);
                types.add(Wine);
                types.add(Liquor);
                types.add(WineMaterial);
                types.add(NaturalWine);
            } else if (EnumSet.of(OrganizationType.Beer, OrganizationType.DistributorOfBeer).contains(organizationType)) {
                types.add(Beer);
                types.add(BeerBlock);
            } else if (EnumSet.of(OrganizationType.Tobacco, OrganizationType.TobaccoDistribution, OrganizationType.TobaccoRetail).contains(organizationType)) {
                types.add(Cigarette);
                types.add(CigaretteBlock);
            } else if (EnumSet.of(OrganizationType.MedicinesManufacturer, OrganizationType.DistributorOfMedicines).contains(organizationType)) {
                types.add(Medicines);
            } else if (EnumSet.of(OrganizationType.WaterAndBeverageManufacturer, OrganizationType.DistributorOfWaterAndBeverage).contains(organizationType)) {
                types.add(WaterAndSoftDrinks);
            } else if (EnumSet.of(OrganizationType.Electronics, OrganizationType.DistributorOfElectronics).contains(organizationType)) {
                types.add(Electronics);
            }
        }
        return types;
    }

    public static List<SelectItem> getAll() {
        List<SelectItem> items = new ArrayList<>(ProductType.values().length);
        for (ProductType item : ProductType.values()) {
            items.add(item.toSelectItem());
        }
        return items;
    }

    public SelectItem toSelectItem() {
        return new SelectItem(this.name(), this.getNameRu());
    }
}

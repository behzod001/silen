package uz.uzkassa.silen.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import uz.uzkassa.silen.dto.SelectItem;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * Created by: Azazello
 * Date: 11/23/2019 10:11 PM
 */

@Getter
@AllArgsConstructor
public enum AggregationStatus {
    DRAFT("Создано"),
    ERROR("Ошибка"),
    EMPTY("Пустой"),
    CREATED("Агрегировано"),
    DISAGGREGATED("Дизагрегировано"),
    UPDATED("Редактировано"),
    UTILIZED("Утилизировано"),
    RETURNED("Возвращен"),
    SHIPPED("Отгружено"),
    IN_PROGRESS("В процессе"),
    DROPOUT("Списано");

    private final String text;

    public static AggregationStatus fromString(String code) {
        if (code != null) {
            for (AggregationStatus item : AggregationStatus.values()) {
                if (code.equalsIgnoreCase(item.getText()) || code.equals(item.name())) {
                    return item;
                }
            }
        }
        return null;
    }

    public static List<SelectItem> getAll() {
        List<SelectItem> items = new ArrayList<>(AggregationStatus.values().length - 1);
        for (AggregationStatus item : AggregationStatus.values()) {
            if (AggregationStatus.EMPTY != item) {
                items.add(item.toSelectItem());
            }
        }
        return items;
    }

    public static List<SelectItem> getList() {
        List<SelectItem> items = new ArrayList<>(3);
        items.add(DRAFT.toSelectItem());
        items.add(CREATED.toSelectItem());
        items.add(ERROR.toSelectItem());
        items.add(UPDATED.toSelectItem());
        items.add(IN_PROGRESS.toSelectItem());
        return items;
    }

    public boolean checkStatusContainNotAbleToShip() {
        return EnumSet.of(
//                AggregationStatus.DRAFT,
                AggregationStatus.ERROR,
                AggregationStatus.UPDATED,
                AggregationStatus.UTILIZED,
                AggregationStatus.SHIPPED,
                AggregationStatus.DISAGGREGATED,
                AggregationStatus.DROPOUT,
                AggregationStatus.IN_PROGRESS
            )
            .contains(this);
    }

    public boolean checkingIfAggregationNotAbleToAggregate() {
        return EnumSet.complementOf(EnumSet.of(AggregationStatus.CREATED, AggregationStatus.RETURNED)).contains(this);
    }

    public boolean checkingIfAggregationNotAbleToUtilize() {
        return EnumSet.complementOf(EnumSet.of(AggregationStatus.CREATED, AggregationStatus.RETURNED, AggregationStatus.ERROR, AggregationStatus.IN_PROGRESS, AggregationStatus.UPDATED)).contains(this);
    }

    public boolean checkingIfAggregationNotAbleToUpdate() {
        return EnumSet.complementOf(EnumSet.of(AggregationStatus.CREATED, AggregationStatus.RETURNED, AggregationStatus.IN_PROGRESS, AggregationStatus.UPDATED, AggregationStatus.ERROR)).contains(this);
    }

    public boolean checkingIfBlockNotAbleToAggregate() {
        return EnumSet.complementOf(EnumSet.of(AggregationStatus.CREATED, AggregationStatus.RETURNED)).contains(this);
    }

    public boolean checkingIfMarkNotAbleToAggregate(boolean isBlock) {
        if (isBlock)
            return EnumSet.complementOf(EnumSet.of(AggregationStatus.CREATED, AggregationStatus.RETURNED)).contains(this);
        else
            return EnumSet.complementOf(EnumSet.of(AggregationStatus.DRAFT, AggregationStatus.RETURNED)).contains(this);
    }

    public boolean checkingIfMarkNotAbleToAggregate() {
        return EnumSet.complementOf(EnumSet.of(AggregationStatus.DRAFT, AggregationStatus.RETURNED))
            .contains(this);
    }

    public SelectItem toSelectItem() {
        return new SelectItem(this.name(), this.getText());
    }

    @JsonCreator
    public static AggregationStatus forValue(String value) {
        return fromString(value);
    }

    @JsonValue
    public String toValue() {
        return this.name();
    }

    @Override
    public String toString() {
        return this.name();
    }
}

package uz.uzkassa.silen.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import uz.uzkassa.silen.security.SecurityUtils;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 8/29/2023 10:42
 */
@Getter
@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ExportReportType implements Serializable {
    AGGREGATION_BY_PRODUCT {
        @Override
        public String getName(String language) {
            return switch (language) {
                case "uz" -> "Tovar bo'yicha aggregatsiya";
                case "cyrillic" -> "Товар бўйича аггрегация";
                default -> "Агрегация по продуктам";
            };
        }
    },

    MARK_KM {
        @Override
        public String getName(String language) {
            return switch (language) {
                case "uz" -> "Sotuv belgilari";
                case "cyrillic" -> "Сотув белгилари";
                default -> "Коды маркировок";
            };
        }
    },
    MARK_KA {
        @Override
        public String getName(String language) {
            return switch (language) {
                case "uz" -> "Sotuv belgilari";
                case "cyrillic" -> "Сотув белгилари";
                default -> "Коды маркировок";
            };
        }
    };

    private final String name;
    private final String code;

    ExportReportType() {
        this.code = this.name();
        this.name = this.getName();
    }

    //
    public abstract String getName(String language);

    public String getName() {
        return getName(SecurityUtils.getCurrentLocale());
    }

    @JsonCreator
    @SuppressWarnings("unused")
    public static ExportReportType forValue(Map<String, String> value) {
        return ExportReportType.valueOf(value.get("code"));
    }
}

package uz.uzkassa.silen.rest;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.uzkassa.silen.dto.SelectItem;
import uz.uzkassa.silen.dto.filter.AggregationFilter;
import uz.uzkassa.silen.dto.marking.*;
import uz.uzkassa.silen.dto.mq.WarehousePayload;
import uz.uzkassa.silen.dto.redis.RedisLastAggregationMarksDTO;
import uz.uzkassa.silen.enumeration.*;
import uz.uzkassa.silen.handler.CsvAggregationExportHandler;
import uz.uzkassa.silen.handler.ExcelAggregationExportHandler;
import uz.uzkassa.silen.security.SecurityUtils;
import uz.uzkassa.silen.service.AggregationService;
import uz.uzkassa.silen.service.ExportReportService;
import uz.uzkassa.silen.service.ValidationService;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

/**
 * REST controller for managing {@link uz.uzkassa.silen.domain.Aggregation}.
 */
@RestController
@RequestMapping("/api/aggregation")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AggregationResource {
    ExcelAggregationExportHandler excelAggregationHandler;
    CsvAggregationExportHandler csvHandler;

    AggregationService aggregationService;
    ValidationService validationService;
    ExportReportService exportReportService;

    @PostMapping
    public ResponseEntity<String> create(@RequestBody @Valid AggregationCreateDTO dto) {
        return ResponseEntity.ok(aggregationService.create(dto, false, null));
    }

    @PutMapping
    public ResponseEntity<String> update(@RequestParam("id") String id, @RequestBody @Valid AggregationUpdateDTO aggregationUpdateDTO) {
        return ResponseEntity.ok(aggregationService.update(id, aggregationUpdateDTO));
    }

    @GetMapping
    public ResponseEntity<Page<AggregationListDTO>> findAllByFilter(@ModelAttribute AggregationFilter filter) {
        filter.setAggregationStatusList(Arrays.asList(AggregationStatus.CREATED, AggregationStatus.IN_PROGRESS, AggregationStatus.UPDATED, AggregationStatus.ERROR, AggregationStatus.DRAFT));
        return ResponseEntity.ok(aggregationService.findAllByFilter(filter));
    }

    @GetMapping("/{id}")
    public ResponseEntity<AggregationDTO> get(@PathVariable String id) {
        return ResponseEntity.ok(aggregationService.findOne(id));
    }

    @GetMapping("/package-types")
    public ResponseEntity<List<SelectItem>> getPackageTypes() {
        return ResponseEntity.ok(Arrays.asList(PackageType.BOX.toSelectItem(), PackageType.PALLET.toSelectItem()));
    }

    @GetMapping("/types")
    public ResponseEntity<List<SelectItem>> getAggregationTypes() {
        return ResponseEntity.ok(AggregationType.getAll());
    }

    @GetMapping("/statuses")
    public ResponseEntity<List<SelectItem>> getStatuses() {
        return ResponseEntity.ok(AggregationStatus.getList());
    }

    @GetMapping("/stats")
    public ResponseEntity<AggregationStatsDTO> getStats(@ModelAttribute AggregationFilter filter) {
        filter.setAggregationStatusList(Arrays.asList(AggregationStatus.CREATED, AggregationStatus.IN_PROGRESS, AggregationStatus.UPDATED, AggregationStatus.ERROR, AggregationStatus.DRAFT));
        return ResponseEntity.ok(aggregationService.getStats(filter));
    }

    @GetMapping(value = "/export_csv_old")
    public void generateCsv(@ModelAttribute AggregationFilter filter, HttpServletResponse response) throws IOException {

        if (filter.getFileType().equals(FileType.EXCEL)) {
            response.setContentType(FileType.EXCEL.getContentType());
            response.setHeader("Content-Disposition", "attachment; filename='Aggregation_KA'" + FileType.CSV.getExtension());
            excelAggregationHandler.writeToStream(filter, response.getOutputStream());
        } else {
            response.setContentType(FileType.CSV.getContentType());
            response.setHeader("Content-Disposition", "attachment; filename='Aggregation_KA'" + FileType.CSV.getExtension());
            csvHandler.writeToStream(filter, response.getOutputStream());
        }
    }

    @PutMapping("/validate")
    public ResponseEntity<String> validate(@RequestBody @Valid ValidationDTO dto) {
        return ResponseEntity.ok(validationService.validate(dto));
    }

    @PutMapping("/check-code")
    public ResponseEntity<RedisLastAggregationMarksDTO> checkCode(@RequestBody @Valid CodeDTO dto) {
        return ResponseEntity.ok(validationService.checkCode(dto));
    }

    @PutMapping("/clear-code")
    public ResponseEntity<Void> clearCode(@RequestBody @Valid CodeDTO dto) {
        validationService.clearCode(dto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/clear-all")
    @Secured({"ROLE_SUPERADMIN", "ROLE_ADMIN"})
    public ResponseEntity<Void> clearAll() {
        validationService.clearAll();
        return ResponseEntity.ok().build();
    }

    @GetMapping("/get-codes/{id}")
    public ResponseEntity<Set<String>> getCodes(@PathVariable String id, @RequestParam("unit") PackageType unit, @RequestParam("child") boolean child) {
        return ResponseEntity.ok(validationService.getCodes(id, unit, child));
    }

    @GetMapping("/get-session-codes/{id}")
    public ResponseEntity<List<String>> getCodes(@PathVariable String id, @RequestParam("sessionId") Long sessionId) {
        return ResponseEntity.ok(validationService.getSessionCodes(id, sessionId));
    }

    @PutMapping("/send/{id}")
    public ResponseEntity<Void> send(@PathVariable String id) {
        aggregationService.resendAggregation(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/print")
    public ResponseEntity<List<String>> getAllForPrint(@ModelAttribute AggregationFilter filter) {
        return ResponseEntity.ok(aggregationService.print(filter));
    }

    @PutMapping("/status-error/{id}")
    public ResponseEntity<Void> setAggregationStatusToError(@PathVariable("id") String id) {
        aggregationService.setAggregationStatus(id, AggregationStatus.ERROR);
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/validate/{fileType}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Map<String, String>> readFromFileAndValidate(@PathVariable("fileType") FileType fileType, @RequestParam("file") MultipartFile file) {
        return ResponseEntity.ok(validationService.readFromFileAndValidate(fileType, file));
    }

    @GetMapping(value = "/file-types")
    public ResponseEntity<EnumSet<FileType>> fileType() {
        return ResponseEntity.ok(EnumSet.allOf(FileType.class));
    }

    @PostMapping("/batch/send")
    public ResponseEntity<Map<String, String>> batchSend(@RequestBody List<String> aggregationIds) {
        return ResponseEntity.ok(aggregationService.batchSending(aggregationIds));
    }

    @GetMapping("/export")
    public ResponseEntity<Void> getAggregationReportCsv(AggregationFilter filter) {
        if (filter.getFrom() == null && filter.getTo() == null) {
            filter.setTo(LocalDateTime.now());
            filter.setFrom(filter.getTo().minusMonths(1));
        } else if (filter.getFrom() != null && filter.getTo() == null) {
            filter.setTo(filter.getFrom().plusMonths(1));
        } else if (filter.getTo() != null && filter.getFrom() == null) {
            filter.setFrom(filter.getTo().minusMonths(1));
        }
        String organizationId = Optional.ofNullable(filter.getOrganizationId()).orElse(SecurityUtils.getCurrentOrganizationId());
        filter.setOrganizationId(organizationId);
        exportReportService.export(filter, ExportReportType.AGGREGATION_BY_PRODUCT, filter.getFileType(), "Aggregation_Report");
        return ResponseEntity.ok().build();
    }

    @PutMapping("/by-report/id")
    public ResponseEntity<Void> fillAggregateByReportId(@RequestBody Set<String> reports) {
        aggregationService.aggregateByReportId(reports);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/check/{code}")
    public ResponseEntity<Void> checkCode(@PathVariable String code) {
        aggregationService.checkAndUtilise(code);
        return ResponseEntity.ok().build();
    }


    @PostMapping("/test/kafka")
    public ResponseEntity<Void> testKafkaProducer(@RequestBody WarehousePayload warehousePayload) {
        aggregationService.testKafkaProducer(warehousePayload);
        return ResponseEntity.ok().build();
    }
}

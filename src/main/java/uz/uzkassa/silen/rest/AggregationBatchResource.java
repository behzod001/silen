package uz.uzkassa.silen.rest;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.uzkassa.silen.dto.filter.BatchFilter;
import uz.uzkassa.silen.dto.marking.BatchAggregationDTO;
import uz.uzkassa.silen.dto.marking.BatchAggregationListDTO;
import uz.uzkassa.silen.service.BatchService;

import jakarta.validation.Valid;

/**
 * REST controller for managing {@link uz.uzkassa.silen.domain.BatchAggregation}.
 */
@RestController
@RequestMapping("/api/aggregation/batch")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AggregationBatchResource {
    BatchService batchService;

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody @Valid BatchAggregationDTO batchAggregationDTO) {
        batchService.save(batchAggregationDTO);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<Page<BatchAggregationListDTO>> getAll(@ModelAttribute BatchFilter batchFilter) {
        return ResponseEntity.ok(batchService.findAll(batchFilter));
    }

    @GetMapping("/stats")
    public ResponseEntity<Long> stats(@ModelAttribute BatchFilter batchFilter) {
        return ResponseEntity.ok(batchService.getStats(batchFilter));
    }

    @GetMapping("/{id}")
    public ResponseEntity<BatchAggregationListDTO> get(@PathVariable String id) {
        return ResponseEntity.ok(batchService.findOne(id));
    }
}

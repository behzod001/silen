package uz.uzkassa.silen.rest;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.uzkassa.silen.dto.*;
import uz.uzkassa.silen.dto.filter.OrganizationFilter;
import uz.uzkassa.silen.dto.invoice.TINAbstractDTO;
import uz.uzkassa.silen.enumeration.OrganizationType;
import uz.uzkassa.silen.enumeration.ProductGroup;
import uz.uzkassa.silen.service.OrganizationService;

import jakarta.validation.Valid;
import java.util.List;

/**
 * REST controller for managing {@link uz.uzkassa.silen.domain.Organization}.
 */
@RestController
@RequestMapping("/api/organization")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class OrganizationResource {
    OrganizationService organizationService;

    @PostMapping
    public ResponseEntity<OrganizationDTO> create(@RequestBody @Valid OrganizationDTO organizationDTO) {
        return ResponseEntity.ok(organizationService.save(organizationDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<OrganizationDTO> update(@PathVariable("id") String id, @RequestBody @Valid OrganizationDTO organizationDTO) {
        return ResponseEntity.ok(organizationService.update(id, organizationDTO));
    }

    @GetMapping
    public ResponseEntity<Page<OrganizationListDTO>> getAll(@ModelAttribute OrganizationFilter filter) {
        return ResponseEntity.ok(organizationService.findAllByFilter(filter));
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrganizationDTO> get(@PathVariable String id) {
        return ResponseEntity.ok(organizationService.findOne(id));
    }

    @GetMapping("/byTin/{tin}")
    public ResponseEntity<TINAbstractDTO> getByTin(@PathVariable("tin") String tin) {
        return ResponseEntity.ok(organizationService.getCompanyByTin(tin));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id) {
        organizationService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/items")
    public ResponseEntity<List<SelectItem>> getItems(@ModelAttribute OrganizationFilter filter) {
        return ResponseEntity.ok(organizationService.getItems(filter));
    }

    @GetMapping("/types")
    public ResponseEntity<List<SelectItem>> getTypes() {
        return ResponseEntity.ok(OrganizationType.getAll());
    }

    @GetMapping("/product-groups")
    public ResponseEntity<List<SelectItem>> getProductGroups() {
        return ResponseEntity.ok(ProductGroup.getAll());
    }

    @GetMapping("/billing-permissions")
    public ResponseEntity<List<OrganizationPermissionDTO>> getBillingPermissions(@RequestParam(name = "organizationId", required = false) String organizationId) {
        return ResponseEntity.ok(organizationService.getBillingPermissions(organizationId));
    }

}

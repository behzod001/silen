package uz.uzkassa.silen.rest.integration.billing;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.uzkassa.silen.integration.billing.dto.agreement.AgreementManagerRequestDTO;
import uz.uzkassa.silen.integration.billing.dto.customerpublicoffer.CustomerPublicOfferCtoIdRequestDto;
import uz.uzkassa.silen.integration.billing.dto.customerpublicoffer.CustomerPublicOfferResponseDto;
import uz.uzkassa.silen.integration.billing.BillingClient;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/admin/billing-services")
public class BillingAdminResource {

    private final BillingClient billingClient;

    @GetMapping("/agreements/{id}")
    public Object getBillingAgreementsForAdmin(@PathVariable("id") Long id) {
        return billingClient.getAgreementById(id);
    }

    @PostMapping("/customer-public-offer/create-by-tin")
    public ResponseEntity<CustomerPublicOfferResponseDto> createCustomerOffer(@Valid @RequestBody CustomerPublicOfferCtoIdRequestDto dto) {
        return ResponseEntity.ok(billingClient.createCustomerOffer(dto));
    }

    @PostMapping("/agreements/agreements-manager-update")
    public ResponseEntity<Object> updateAgreementManager(@RequestBody AgreementManagerRequestDTO dto) {
        return ResponseEntity.ok(billingClient.updateAgreementManager(dto));
    }

    @GetMapping("/balance/tin/{tin}")
    public ResponseEntity<Object> getBalanceByCtoId(@PathVariable @NotEmpty @NotNull String tin) {
        return ResponseEntity.ok(billingClient.getBalanceByTin(tin));
    }

    @GetMapping("/customer-public-offer/tin/{tin}")
    public ResponseEntity<CustomerPublicOfferResponseDto> getCustomerOfferByTin(@PathVariable @NotEmpty @NotNull String tin) {
        return ResponseEntity.ok(billingClient.getCustomerOfferByTin(tin));
    }
}

package uz.uzkassa.silen.rest.integration._1c;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.uzkassa.silen.dto.SelectItem;
import uz.uzkassa.silen.dto.filter.StoreFilter;
import uz.uzkassa.silen.enumeration.StoreStatus;
import uz.uzkassa.silen.integration.onec.Order1CDTO;
import uz.uzkassa.silen.service.Order1CService;
import uz.uzkassa.silen.service.StoreService;

import jakarta.validation.Valid;
import java.util.List;

/**
 * REST controller for managing {@link uz.uzkassa.silen.domain.Order}.
 */
@Slf4j
@RestController
@RequestMapping("/api/1c/order")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class Order1CResource {
    Order1CService order1CService;
    StoreService storeService;

    @PostMapping
    public ResponseEntity<Order1CDTO> create(@RequestBody @Valid Order1CDTO order1CDTO) {
        log.info("1C Order JSON :{}", order1CDTO);
        return ResponseEntity.ok(order1CService.save(order1CDTO));
    }

    @GetMapping("/warehouse")
    public ResponseEntity<List<SelectItem>> getItems() {
        log.debug("REST request to get all Store");
        StoreFilter filter = new StoreFilter();
        filter.setStatus(StoreStatus.ACTIVE);
        return ResponseEntity.ok(storeService.getItems(filter));
    }
}

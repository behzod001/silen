package uz.uzkassa.silen.rest;

import jakarta.servlet.http.HttpServletResponse;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.uzkassa.silen.dto.filter.MongoFilter;
import uz.uzkassa.silen.dto.marking.AggregationCreateDTO;
import uz.uzkassa.silen.dto.marking.BlockUpdateDTO;
import uz.uzkassa.silen.dto.marking.ValidationDTO;
import uz.uzkassa.silen.dto.mongo.BlockDTO;
import uz.uzkassa.silen.handler.CsvMarkExportHandler;
import uz.uzkassa.silen.service.BlockService;
import uz.uzkassa.silen.service.ValidationService;

import jakarta.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * REST controller for managing {@link uz.uzkassa.silen.domain.Aggregation}.
 */
@RestController
@RequestMapping("/api/aggregation/block")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AggregationBlockResource {
    BlockService blockService;
    CsvMarkExportHandler blockCsvHandler;
    ValidationService validationService;

    @PostMapping
    public ResponseEntity<String> create(@RequestBody @Valid AggregationCreateDTO aggregationCreateDTO) {
        return ResponseEntity.ok(blockService.create(aggregationCreateDTO,false,null));
    }

    @PutMapping
    public ResponseEntity<String> update(@RequestParam("id") String id, @RequestBody @Valid BlockUpdateDTO blockUpdateDTO) {
        return ResponseEntity.ok(blockService.update(id, blockUpdateDTO));
    }

    @PutMapping("/validate")
    public ResponseEntity<String> validate(@RequestBody @Valid ValidationDTO dto) {
        return ResponseEntity.ok(validationService.validateBlock(dto));
    }

    @GetMapping
    public ResponseEntity<Page<BlockDTO>> getAll(@ModelAttribute MongoFilter filter) {
        return ResponseEntity.ok(blockService.findAllAggregationBlock(filter));
    }

    @GetMapping("/stats")
    public ResponseEntity<BigDecimal> getStats(@ModelAttribute MongoFilter filter) {
        return ResponseEntity.ok(blockService.findAllAggregationBlockStats(filter));
    }

    @GetMapping("/{id}")
    public ResponseEntity<BlockDTO> get(@PathVariable("id") String id) {
        return ResponseEntity.ok(blockService.findBlockWithMarks(id));
    }

    @GetMapping(value = "/export_csv")
    public ResponseEntity<Void> generateCsv(@ModelAttribute MongoFilter filter, HttpServletResponse response) throws IOException {
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=scheme.xlsx");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");
        blockCsvHandler.writeToStream(filter, response.getOutputStream());
        return ResponseEntity.ok().build();
    }

    @PutMapping("/send/{id}")
    public ResponseEntity<Void> send(@PathVariable String id) {
        blockService.sendAggregationBlock(id);
        return ResponseEntity.ok().build();
    }
}

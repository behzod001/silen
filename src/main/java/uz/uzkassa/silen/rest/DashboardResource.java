package uz.uzkassa.silen.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.uzkassa.silen.dto.Result;
import uz.uzkassa.silen.dto.SelectItem;
import uz.uzkassa.silen.dto.dashboard.OrganizationsDTO;
import uz.uzkassa.silen.dto.dashboard.StatsDTO;
import uz.uzkassa.silen.dto.filter.DailyReportFilter;
import uz.uzkassa.silen.enumeration.FilterMode;
import uz.uzkassa.silen.enumeration.OrganizationType;
import uz.uzkassa.silen.service.DashboardService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/dashboard")
@RequiredArgsConstructor
public class DashboardResource {
    private final DashboardService dashboardService;

    @GetMapping("/stats")
    public ResponseEntity<Result<List<StatsDTO>>> getStats(@ModelAttribute DailyReportFilter filter) {
        return ResponseEntity.ok(Result.success(new ArrayList<>()));
    }

    @GetMapping("/filter-modes")
    public ResponseEntity<List<SelectItem>> getFilterModes() {
        return ResponseEntity.ok(FilterMode.getAll());
    }

    @GetMapping("/organization-types")
    public ResponseEntity<Result<List<SelectItem>>> getTypes() {
        return ResponseEntity.ok(Result.success(OrganizationType.getForFilter()));
    }

    @GetMapping("/organizations")
    public ResponseEntity<Result<List<OrganizationsDTO>>> getOrganizations() {
        return ResponseEntity.ok(Result.success(dashboardService.getOrganizations()));
    }
}

package uz.uzkassa.silen.rest;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.uzkassa.silen.dto.Result;
import uz.uzkassa.silen.dto.invoice.FacturaNewDTO;
import uz.uzkassa.silen.dto.invoice.WebhookSignDTO;
import uz.uzkassa.silen.enumeration.Status;
import uz.uzkassa.silen.service.InvoiceHookService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/invoice/webhook")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class InvoiceWebHookResource {
    InvoiceHookService invoiceHookService;

    @PostMapping("/updated")
    public ResponseEntity<Result<Void>> onReceive(@Valid @RequestBody final FacturaNewDTO dto) {
        invoiceHookService.onUpdate(dto);
        return ResponseEntity.ok(Result.success());
    }

    @PostMapping("/received")
    public ResponseEntity<Result<Void>> onReceive(@Valid @RequestBody final WebhookSignDTO signature) {
        invoiceHookService.onReceive(signature.getSign());
        return ResponseEntity.ok(Result.success());
    }

    @PostMapping("/accepted")
    public ResponseEntity<Result<Void>> onAccept(@Valid @RequestBody final WebhookSignDTO signature) {
        invoiceHookService.onAcceptReject(signature.getSign(), Status.ACCEPTED);
        return ResponseEntity.ok(Result.success());
    }

    @PostMapping("/agent_accepted")
    public ResponseEntity<Result<Void>> onAgentAccept(@Valid @RequestBody final WebhookSignDTO signature) {
        invoiceHookService.onAcceptReject(signature.getSign(), Status.AGENT_ACCEPTED);
        return ResponseEntity.ok(Result.success());
    }

    @PostMapping("/rejected")
    public ResponseEntity<Result<Void>> onRejected(@Valid @RequestBody final WebhookSignDTO signature) {
        invoiceHookService.onAcceptReject(signature.getSign(), Status.REJECTED);
        return ResponseEntity.ok(Result.success());
    }

    @PostMapping("/cancelled")
    public ResponseEntity<Result<Void>> onCancelled(@Valid @RequestBody final WebhookSignDTO signature) {
        invoiceHookService.onAcceptReject(signature.getSign(), Status.CANCELLED);
        return ResponseEntity.ok(Result.success());
    }
}

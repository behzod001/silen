package uz.uzkassa.silen.rest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.uzkassa.silen.dto.CheckCisesDTO;
import uz.uzkassa.silen.dto.eimzo.Pkcs7Dto;
import uz.uzkassa.silen.integration.EimzoClient;
import uz.uzkassa.silen.integration.TrueApi;
import uz.uzkassa.silen.integration.asilbelgi.AuthKey;
import uz.uzkassa.silen.integration.billing.dto.CisInfoResponse;
import uz.uzkassa.silen.integration.asilbelgi.TrueApiRequest;
import uz.uzkassa.silen.service.MarkService;
import uz.uzkassa.silen.service.OrganizationService;
import uz.uzkassa.silen.exceptions.BadRequestException;

import jakarta.validation.Valid;
import java.util.Set;

@Slf4j
@RestController
@RequestMapping("/api/true")
@RequiredArgsConstructor
public class TrueApiResource {
    private final TrueApi trueApi;
    private final EimzoClient eimzoClient;
    private final OrganizationService organizationService;
    private final MarkService markService;

    @GetMapping("/auth-key")
    public ResponseEntity<AuthKey> getGuid() {
        return ResponseEntity.ok(trueApi.getAuthKey());
    }

    @PostMapping("/sign-in")
    public ResponseEntity<String> signIn(@RequestBody @Valid final AuthKey key) {
//        String token = trueApi.signIn(key);
//        organizationService.updateTrueToken(token);
//        return ResponseEntity.ok(token);
        final Pkcs7Dto pkcs7Dto = eimzoClient.frontTimestamp(key.getData());
        if (pkcs7Dto.isSuccess()) {
            key.setData(pkcs7Dto.getPkcs7b64());
            final String token = trueApi.signIn(key);
            organizationService.updateTrueToken(token);
            return ResponseEntity.ok(token);
        } else {
            log.error("JSON: {}", pkcs7Dto);
            throw new BadRequestException("Not verified, reason: " + pkcs7Dto.getMessage());
        }

    }

    @PutMapping("/update-token")
    public ResponseEntity<String> updateTrueToken(@RequestBody final AuthKey data) {
        organizationService.updateTrueToken(data.getUuid(), data.getData());
        return ResponseEntity.ok(data.getUuid());
    }

    @PostMapping("/cises-info")
    public ResponseEntity<CisInfoResponse[]> cisesInfo(@RequestBody final CheckCisesDTO request) {
        return ResponseEntity.ok(markService.cisesInfo(request));
    }

    @GetMapping("/aggregated-list")
    public ResponseEntity<Set<String>> aggregatedList(@ModelAttribute final TrueApiRequest request) {
        final Set<String> data = markService.aggregatedList(request);
        return ResponseEntity.ok(data);
    }
}

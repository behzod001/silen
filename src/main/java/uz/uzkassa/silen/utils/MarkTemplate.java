package uz.uzkassa.silen.utils;

import org.springframework.stereotype.Component;

public final class MarkTemplate {

    public static boolean isMark(final String code) {
        if (alcoholMatcher(code)) {
            return true;
        }
        if (alcoholBlockMatcher(code)) {
            return true;
        }
        if (pharmaMatcher(code)) {
            return true;
        }
        if (tobaccoBlockMatcher(code)) {
            return true;
        }
        if (tobaccoMatcher(code)) {
            return true;
        }
        if (waterMatcher(code)) {
            return true;
        }
        if (waterBlockMatcher(code)) {
            return true;
        }
        return appliancesMatcher(code);
    }

    public static boolean isBlock(final String code) {
        if (alcoholBlockMatcher(code)) {
            return true;
        }
        if (tobaccoBlockMatcher(code)) {
            return true;
        }
        return waterBlockMatcher(code);
    }

    /**
     * Сигареты, блоки
     * 01 + 0 + gtin(13 chars) + 21 + serial (7 chars) +  + 93 + serial(8 chars)
     * printCode
     * 010487021676250621e;x6kSB93elFTQlFL
     * code
     * 010487021676250621e;x6kSB
     *
     * @param code {@link uz.uzkassa.silen.domain.mongo.Mark}
     * @return boolean
     */
    public static boolean tobaccoBlockMatcher(String code) {
        return code.matches("^010([0-9]{13})21.{7}[↔\u001D\u001D]?93.{8}$");
    }

    /**
     * Сигареты, пачки
     * 0 + gtin(13 chars) + serial (15 chars)
     * 04600818000061daaA!ETelZqRTQx
     *
     * @param code
     * @return
     */
    public static boolean tobaccoMatcher(String code) {
        return code.matches("^0[0-9]{13}.{15}$");
    }

    /**
     * Лекарственные средства
     * printCode
     * 01 + 0 + gtin(13 chars) + 21 + serial (13 chars) +  + 91 + serial(4 chars) +  + 92 + serial(44 chars)
     * 0104870046198488213z+S-i3z+S-i:91reSA92XBjhasbxahjs5rt68sxasxsxsczxcXsdc#$%^&*(12))
     * code
     * 0104870046198488213z+S-i3z+S-i:
     *
     * @param code
     * @return
     */
    public static boolean pharmaMatcher(String code) {
        return code.matches("^010([0-9]{13})21(.{13})[↔\u001D\u001D]?91.{4}[↔\u001D\u001D]?92.{44}$");
    }

    /**
     * Бытовая техника
     * <p>
     * 01 + 0 + gtin(13 chars) + 21 + serial (20 chars) +  + 91 + serial(4 chars) +  + 92 + serial(44 chars)
     * printCode
     * 010481115902075921SqTZpejcSJTqcqn.;ly.91UZF092N0hCUvcXwFFKf1Bftt4+1LYhRfo4bHn1uTs/N7YUY0A=
     * code
     * 0107623900780341210(Ho0PAJZt5U1(yHYZ78
     *
     * @param code
     * @return
     */
    public static boolean appliancesMatcher(String code) {
        return code.matches("^010([0-9]{13})21(.{20})[↔\u001D\u001D]?91.{4}[↔\u001D\u001D]?92.{44}$");
    }

    /**
     * Алкогольная продукция
     * 01 + 0 + gtin(13 chars) + 21 + serial (7 chars) +  + 93 + serial(4 chars)
     * printCode
     * 010306351564123321N,p?OBM93UVJa
     * code
     * 010536756723004821RFBakfs
     * "^(01)0([0-9]{13})21(.{13})[↔]93(.{4})"
     *
     * @param code
     * @return
     */
    public static boolean alcoholMatcher(final String code) {
        return code.matches("^010([0-9]{13})21(.{7})[↔\u001D\u001D]?[\u001d\u001d]?93.{4}$");
    }

    /**
     * Алкогольная продукция
     * 01 + 0 + gtin(13 chars) + 21 + serial (13 chars) +  + 93 + serial(4 chars)
     * printCode
     * 010460351400145521WQCRDpN7gte9793eFhh
     * code
     * 010460351400145521WQCRDpN7gte97
     *
     * @param code
     * @return
     */
    public static boolean alcoholBlockMatcher(final String code) {
        return code.matches("^010[0-9]{13}21(.{13})[↔\u001D\u001D]?[\u001d\u001d]?93.{4}$");
    }

    /**
     * Вода и прохладительные напитки
     * 01 + 0 + gtin(13 chars) + 21 + serial (13 chars) +  + 93 + serial(4 chars)
     * printCode
     * 010306351564124021rs.),TuJ,BlfL93RkEw
     * code
     * 010306351564124021rs.),TuJ,BlfL
     *
     * @param code
     * @return
     */
    public static boolean waterMatcher(final String code) {
        return code.matches("^010([0-9]{13})21(.{13})[↔\u001D\u001D]?[\u001d\u001d]?93.{4}$");
    }

    /**
     * Вода и прохладительные напитки
     * 01 + 0 + gtin(13 chars) + 21 + serial (13 chars) +  + 93 + serial(4 chars)
     * printCode
     * 010460717457789321aHQeKsdLWbSaX93VWhq
     * code
     * 010460717457789321aHQeKsdLWbSaX
     *
     * @param code
     * @return
     */
    public static boolean waterBlockMatcher(final String code) {
        return code.matches("^010([0-9]{13})21(.{13})[↔\u001D\u001D]?[\u001d\u001d]?93.{4}$");
    }

    public static boolean isAggregation(final String code) {
        return code.matches("^[(]?[0]?[0]?[)]?[0-9]{1}.{17}$");
    }

    public static boolean isAggregationPallet(final String code) {
        return code.matches("^[(]?[0]?[0]?[)]?[1]{1}.{17}$");
    }
}


package uz.uzkassa.silen.utils;


public interface MarkingConstants {
    String ALCOHOL = "05367567230048";
    String TOBACCO = "05367567230024";
    String BEER = "07501064193972";
    String WATER = "07623900405985";
    String FARM = "07623900780396";
    String TECHNICAL = "07623900780341";
}

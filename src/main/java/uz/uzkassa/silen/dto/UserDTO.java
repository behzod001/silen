package uz.uzkassa.silen.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import uz.uzkassa.silen.enumeration.OrganizationType;
import uz.uzkassa.silen.enumeration.UserRole;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * A DTO representing a user, with his authorities.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDTO {

    protected String id;

    @Size(min = 1, max = 15)
    protected String pinfl;

    @Size(min = 1, max = 50)
    protected String login;

    @Size(min = 4, max = 100)
    protected String password;

    @Size(min = 4, max = 100)
    protected String passwordNew;

    @Size(max = 50)
    protected String firstName;

    @Size(max = 50)
    protected String lastName;

    protected boolean activated;

    protected LocalDateTime createdDate;

    private String organizationId;

    private String organizationTin;

    private String organizationName;

    private Set<OrganizationType> organizationTypes;

    @NotNull
    protected UserRole role = UserRole.USER;

    protected String roleName;

    String phone;

    Integer bonus;

    public UserDTO(String id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}

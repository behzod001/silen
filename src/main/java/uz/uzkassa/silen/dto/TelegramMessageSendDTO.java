package uz.uzkassa.silen.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class TelegramMessageSendDTO implements Serializable {

    private static final long serialVersionUID = 133898928L;

    @JsonProperty("changeId")
    private String changeId;
    @JsonProperty("message")
    private String message;
}

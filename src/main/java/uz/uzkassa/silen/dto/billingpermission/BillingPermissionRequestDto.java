package uz.uzkassa.silen.dto.billingpermission;

import lombok.Data;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class BillingPermissionRequestDto {

    Integer unitCount;

    BigDecimal amount;

    LocalDateTime startDate;

    LocalDateTime endDate;

    @NotEmpty
    @NotNull
    String tin;

    @NotNull
    String serviceType;

    String pinfl;

    String noteFromExternalSystem;
}

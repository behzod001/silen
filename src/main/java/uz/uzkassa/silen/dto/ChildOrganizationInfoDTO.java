package uz.uzkassa.silen.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChildOrganizationInfoDTO {
    String id;
    String tin;
    String name;

    public ChildOrganizationInfoDTO() {
    }
}

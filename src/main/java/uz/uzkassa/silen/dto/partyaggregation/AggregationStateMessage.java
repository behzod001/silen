package uz.uzkassa.silen.dto.partyaggregation;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@ToString
@Getter
@Setter
public class AggregationStateMessage implements Serializable {

    String code;
    String status;
    String statusName;

    public AggregationStateMessage() {
    }

    public AggregationStateMessage(String code, String status, String statusName) {
        this.code = code;
        this.status = status;
        this.statusName = statusName;
    }
}

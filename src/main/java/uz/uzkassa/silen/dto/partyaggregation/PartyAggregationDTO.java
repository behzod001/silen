package uz.uzkassa.silen.dto.partyaggregation;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import uz.uzkassa.silen.dto.BaseUuidDTO;
import uz.uzkassa.silen.dto.ProductCommonDTO;
import uz.uzkassa.silen.dto.SelectItem;
import uz.uzkassa.silen.enumeration.PartyAggregationStatus;

import jakarta.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString
public class PartyAggregationDTO extends BaseUuidDTO {

    String number;

    @Size(min = 1)
    Set<String> parties;

    String productId;

    String orderId;

    ProductCommonDTO product;

    Integer capacity;

    Integer quantity;

    String serialId;

    SelectItem serial;

    String storeId;

    String description;

    String gcp;

    PartyAggregationStatus status;

    String statusName;

    LocalDate productionDate = LocalDate.now();

    LocalDate expirationDate;

    boolean isOffline;

    List<AggregationStateMessage> codes;
}

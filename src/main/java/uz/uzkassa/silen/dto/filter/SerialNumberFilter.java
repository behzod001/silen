package uz.uzkassa.silen.dto.filter;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import uz.uzkassa.silen.enumeration.SerialNumberStatus;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 18.11.2022 19:07
 */
@Getter
@Setter
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SerialNumberFilter extends Filter {

    String productId;

    SerialNumberStatus status;

    String serialNumber;
}

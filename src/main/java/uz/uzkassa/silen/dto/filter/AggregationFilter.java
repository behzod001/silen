package uz.uzkassa.silen.dto.filter;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import uz.uzkassa.silen.enumeration.AggregationStatus;
import uz.uzkassa.silen.enumeration.AggregationType;
import uz.uzkassa.silen.enumeration.FileType;
import uz.uzkassa.silen.enumeration.PackageType;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AggregationFilter extends Filter implements Serializable {

    String productId;

    String batchId;

    String serialId;

    AggregationStatus aggregationStatus;

    @Schema(hidden = true)
    List<AggregationStatus> aggregationStatusList;

    PackageType unit;

    String login;

    FileType fileType = FileType.CSV;

    AggregationType type;

    String partyAggregationId;

}

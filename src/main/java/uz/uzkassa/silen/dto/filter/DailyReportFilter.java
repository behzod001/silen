package uz.uzkassa.silen.dto.filter;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import org.apache.commons.collections4.CollectionUtils;
import uz.uzkassa.silen.enumeration.FilterMode;
import uz.uzkassa.silen.enumeration.OrganizationType;
import uz.uzkassa.silen.enumeration.ProductType;

import java.time.LocalDate;
import java.util.EnumSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.Collections;

@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DailyReportFilter extends Filter {
    String deviceId;

    String lineId;

    FilterMode filterMode = FilterMode.Production;

    ProductType productType;

    public FilterMode getFilterMode() {
        if (filterMode == null) {
            filterMode = FilterMode.Production;
        }
        return filterMode;
    }

    private OrganizationType organizationType;

    private EnumSet<OrganizationType> organizationTypes;

    public boolean isTodayIncluded() {
        if (getTo() == null) {
            return true;
        }
        return getTo().isAfter(LocalDate.now().atStartOfDay());
    }

    public EnumSet<OrganizationType> getOrganizationTypes() {
        if (this.organizationTypes == null && this.organizationType != null) {
            this.organizationTypes = EnumSet.of(this.organizationType);
        }
        return organizationTypes;
    }

    public Set<String> getOrganizationTypeStrings() {
        if (CollectionUtils.isNotEmpty(getOrganizationTypes())) {
            return getOrganizationTypes().stream().map(OrganizationType::name).collect(Collectors.toSet());
        } else if (getOrganizationType() != null) {
            return Collections.singleton(getOrganizationType().name());
        }
        return Collections.singleton(OrganizationType.Alcohol.name());
    }
}

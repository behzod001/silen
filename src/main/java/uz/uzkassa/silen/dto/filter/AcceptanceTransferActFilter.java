package uz.uzkassa.silen.dto.filter;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import uz.uzkassa.silen.enumeration.ActStatus;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 5/25/2023 12:42
 */
@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AcceptanceTransferActFilter extends Filter {

    String customerId;

    String customerTin;

    String supplierTin;

    Integer year;

    ActStatus status;
}

package uz.uzkassa.silen.dto.filter;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import uz.uzkassa.silen.enumeration.DropoutReason;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 31.10.2022 18:44
 */
@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DropoutFilter extends Filter {

    DropoutReason dropoutReason;

    String productId;
}

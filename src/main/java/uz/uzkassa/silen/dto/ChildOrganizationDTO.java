package uz.uzkassa.silen.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class ChildOrganizationDTO {
    @NotNull
    private String parentId;

    @NotNull
    private String childId;
}

package uz.uzkassa.silen.dto.mongo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import jakarta.persistence.Id;
import java.time.LocalDateTime;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 9/19/2023 11:17
 */
@ToString
@Getter
@Setter
@Document(collection = "store_in_aggregation_mark")
public class StoreInAggregation {
    @Id
    private String id;
    @Indexed
    private String parentCode;
    private LocalDateTime createdDate;
    @Indexed
    private String storeInId;
    private int quantity;
}

package uz.uzkassa.silen.dto.mongo;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import uz.uzkassa.silen.enumeration.AggregationStatus;
import uz.uzkassa.silen.enumeration.MarkStatus;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * A DTO for the {@link uz.uzkassa.silen.domain.mongo.Mark} entity.
 */
@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MarkDTO implements Serializable {
    String id;
    String code;
    String printCode;
    String orderProductId;
    String partyId;
    String productId;
    String productName;
    String parentCode;
    boolean used;
    String organizationId;
    LocalDateTime createdDate;
    AggregationStatus status;
    String statusName;
    MarkStatus turonStatus;
    String turonStatusName;

    private String gtin;
    private String serialNumber;
    private String packUnit;
    private String other;
}

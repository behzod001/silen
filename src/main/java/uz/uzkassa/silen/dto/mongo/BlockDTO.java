package uz.uzkassa.silen.dto.mongo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import uz.uzkassa.silen.enumeration.MarkStatus;
import uz.uzkassa.silen.enumeration.PackageType;

import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * A DTO for the {@link uz.uzkassa.silen.domain.mongo.Mark} entity.
 */
@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BlockDTO extends MarkDTO {

    String barcode;

    String aggregationCode;

    LocalDateTime aggregationDate;

    String statusName;

    MarkStatus markStatus;

    Integer quantity;

    String login;

    Set<String> marks;

    Set<String> cleanMarks = new LinkedHashSet<>();

    String childProductId;

    String childProductName;

    String childBarcode;

    String unitName;

    String turonAggregationId;

    PackageType unit;

    String serialNumber;

    String shipmentId;

    String shipmentNumber;

    Long invoiceId;

    String invoiceNumber;

}

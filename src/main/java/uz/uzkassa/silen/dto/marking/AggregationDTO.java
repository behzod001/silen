package uz.uzkassa.silen.dto.marking;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lombok.experimental.FieldDefaults;
import uz.uzkassa.silen.dto.SelectItem;
import uz.uzkassa.silen.dto.mongo.MarkDTO;
import uz.uzkassa.silen.enumeration.AggregationStatus;
import uz.uzkassa.silen.enumeration.AggregationType;
import uz.uzkassa.silen.enumeration.PackageType;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * A DTO for the {@link uz.uzkassa.silen.domain.Aggregation} entity.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class AggregationDTO implements Serializable {

    String id;

    String productName;

    String productId;

    Integer quantity;

    String printCode;

    PackageType unit;

    String unitName;

    String serialId;

    SelectItem serial;

    @NotNull(message = "Коды маркировки отсутствуют.")
    Set<String> marks = new LinkedHashSet<>();

    Set<String> cleanMarks = new LinkedHashSet<>();

    Set<MarkDTO> markDTOs = new LinkedHashSet<>();

    Set<String> dropoutMarks = new LinkedHashSet<>();

    String description;

    String organizationName;

    AggregationStatus status;

    String statusName;

    String gcp;

    String barcode;

    String turonAggregationId;

    String shipmentId;

    String shipmentNumber;

    Long invoiceId;

    String invoiceNumber;

    LocalDateTime aggregationDate;

    String utilisationId;

    LocalDateTime utilisationTime;

    LocalDateTime productionDate;

    LocalDateTime expirationDate;

    AggregationType type;

    String typeName;

    SelectItem partyAggregation;

    String errorMessage;
}

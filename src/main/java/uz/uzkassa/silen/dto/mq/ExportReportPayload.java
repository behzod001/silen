package uz.uzkassa.silen.dto.mq;

import lombok.*;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 8/29/2023 16:45
 */
@ToString
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class ExportReportPayload {

    Object filter;

    Long exportReportId;
}

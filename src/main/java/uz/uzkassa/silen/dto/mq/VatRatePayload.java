package uz.uzkassa.silen.dto.mq;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 04.01.2023 11:56
 */
@Getter
@Setter
@ToString
public class VatRatePayload {
    Long id;
    String code;
    BigDecimal oldAmount;
    BigDecimal newAmount;
}

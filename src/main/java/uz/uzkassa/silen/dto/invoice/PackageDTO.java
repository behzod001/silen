package uz.uzkassa.silen.dto.invoice;

import lombok.*;
import uz.uzkassa.silen.security.SecurityUtils;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PackageDTO implements Serializable {
    private String code;
    private String nameRu;
    private String nameUz;

    public String getName(){
        final String locale = SecurityUtils.getCurrentLocale();
        String name = "";
        switch (locale) {
            case "uz":
                name = this.getNameUz();
                break;
            default:
                name = this.getNameRu();
                break;
        }
        if (name == null || name.trim().length() == 0) {
            name = this.getNameRu();
        }
        return name;
    }
}

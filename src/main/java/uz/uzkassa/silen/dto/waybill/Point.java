package uz.uzkassa.silen.dto.waybill;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Point implements Serializable {

    @JsonProperty("RegionId")
    Integer regionId;
    @JsonProperty("RegionName")
    String regionName;

    @JsonProperty("DistrictCode")
    Integer districtCode;
    @JsonProperty("DistrictName")
    String districtName;

    @JsonProperty("MahallaId")
    Integer mahallaId;
    @JsonProperty("MahallaName")
    String mahallaName;
    @JsonProperty("Address")
    String address;
    @JsonProperty("Longitude")
    Double longitude;
    @JsonProperty("Latitude")
    Double latitude;

    @JsonSetter("setregionid")
    public void setregionidLowerCase(Integer regionId) {
        this.regionId = regionId;
    }

    @JsonSetter("setregionname")
    public void setregionnameLowerCase(String regionName) {
        this.regionName = regionName;
    }

    @JsonSetter("setdistrictcode")
    public void setdistrictcodeLowerCase(Integer districtCode) {
        this.districtCode = districtCode;
    }

    @JsonSetter("setdistrictname")
    public void setdistrictnameLowerCase(String districtName) {
        this.districtName = districtName;
    }

    @JsonSetter("setmahallaname")
    public void setmahallanameLowerCase(String mahallaName) {
        this.mahallaName = mahallaName;
    }

    @JsonSetter("setaddress")
    public void setaddressLowerCase(String address) {
        this.address = address;
    }

    @JsonSetter("setmahallaid")
    public void setmahallaidLowerCase(Integer mahallaId) {
        this.mahallaId = mahallaId;
    }

    @JsonSetter("setlongitude")
    public void setlongitudeLowerCase(Double longitude) {
        this.longitude = longitude;
    }

    @JsonSetter("setlatitude")
    public void setlatitudeLowerCase(Double latitude) {
        this.latitude = latitude;
    }
}


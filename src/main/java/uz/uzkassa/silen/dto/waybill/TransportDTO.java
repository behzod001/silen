package uz.uzkassa.silen.dto.waybill;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import uz.uzkassa.silen.enumeration.TrailerType;
import uz.uzkassa.silen.enumeration.TransportType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransportDTO implements Serializable {
    private String id;
    @JsonProperty("Driver")
    private PersonDTO driver;

    @JsonProperty("Truck")
    private TruckDTO truckDTO;

    @JsonProperty("Trailer")
    private TruckDTO trailer;

    @JsonProperty("Carriages")
    private List<TruckDTO> carriages;

    @JsonProperty("ProductGroups")
    private List<ProductGroupDTO> productGroups = new ArrayList<>();

    private TrailerType trailerType;
    private TransportType transportType;
    @JsonSetter("driver")
    public void setdriverLowerCase(PersonDTO driver) {
        this.driver = driver;
    }

    @JsonSetter("truck")
    public void settruckLowerCase(TruckDTO truckDTO) {
        this.truckDTO = truckDTO;
    }

    @JsonSetter("trailer")
    public void settrailerLowerCase(TruckDTO trailer) {
        this.trailer = trailer;
    }

    @JsonSetter("carriages")
    public void setcarriagesLowerCase(List<TruckDTO> carriages) {
        this.carriages = carriages;
    }

    @JsonSetter("productgroups")
    public void setproductgroupsLowerCase(List<ProductGroupDTO> productgroups) {
        this.productGroups = productgroups;
    }
}

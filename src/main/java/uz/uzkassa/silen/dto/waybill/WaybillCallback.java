package uz.uzkassa.silen.dto.waybill;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDate;


@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class WaybillCallback implements Serializable {

    @JsonProperty("WaybillLocalId")
    private String waybillLocalId;

    @JsonProperty("CurrentStateId")
    private Integer currentStateId;

    @JsonProperty("CurrentStateName")
    private String currentStateName;

    @JsonProperty("WaybillNo")
    private String waybillNo;
    @JsonProperty("WaybillDate")
    private LocalDate waybillDate;

    @JsonProperty("ContractNo")
    private String contractNo;

    @JsonProperty("ContractDate")
    private LocalDate contractDate;

    @JsonProperty("ConsignorTinOrPinfl")
    private String consignorTinOrPinfl;

    @JsonProperty("ConsignorName")
    private String consignorName;

    @JsonProperty("ConsignorBranchCode")
    private String consignorBranchCode;

    @JsonProperty("ConsignorBranchName")
    private String consignorBranchName;

    @JsonProperty("ConsigneeTinOrPinfl")
    private String consigneeTinOrPinfl;


    @JsonProperty("ConsigneeName")
    private String consigneeName;

    @JsonProperty("ConsigneeBranchCode")
    private String consigneeBranchCode;

    @JsonProperty("ConsigneeBranchName")
    private String consigneeBranchName;

    @JsonProperty("ResponsiblePersonPinfl")
    private String responsiblePersonPinfl;

    @JsonProperty("ResponsiblePersonFullName")
    private String responsiblePersonFullName;


    @JsonSetter("waybilllocalid")
    public void setwaybilllocalidLower(String waybilllocalid) {
        this.waybillLocalId = waybilllocalid;
    }

    @JsonSetter("currentstateid")
    public void setcurrentstateidLower(Integer currentstateid) {
        this.currentStateId = currentstateid;
    }

    @JsonSetter("currentstatename")
    public void setcurrentstatenameLower(String currentstatename) {
        this.currentStateName = currentstatename;
    }

    @JsonSetter("waybillno")
    public void setwaybillnoLower(String waybillno) {
        this.waybillNo = waybillno;
    }

    @JsonSetter("waybilldate")
    public void setwaybilldateLower(LocalDate waybilldate) {
        this.waybillDate = waybilldate;
    }

    @JsonSetter("contractno")
    public void setcontractnoLower(String contractno) {
        this.contractNo = contractno;
    }

    @JsonSetter("contractdate")
    public void setcontractdateLower(LocalDate contractdate) {
        this.contractDate = contractdate;
    }

    @JsonSetter("consignortinorpinfl")
    public void setconsignortinorpinflLower(String consignortinorpinfl) {
        this.consignorTinOrPinfl = consignortinorpinfl;
    }

    @JsonSetter("consignorname")
    public void setconsignornameLower(String consignorname) {
        this.consignorName = consignorname;
    }

    @JsonSetter("consignorbranchcode")
    public void setconsignorbranchcodeLower(String consignorbranchcode) {
        this.consignorBranchCode = consignorbranchcode;
    }

    @JsonSetter("consignorbranchname")
    public void setconsignorbranchnameLower(String consignorbranchname) {
        this.consignorBranchName = consignorbranchname;
    }

    @JsonSetter("consigneetinorpinfl")
    public void setconsigneetinorpinflLower(String consigneetinorpinfl) {
        this.consigneeTinOrPinfl = consigneetinorpinfl;
    }

    @JsonSetter("consigneename")
    public void setconsigneenameLower(String consigneename) {
        this.consigneeName = consigneename;
    }

    @JsonSetter("consigneebranchcode")
    public void setconsigneebranchcodeLower(String consigneebranchcode) {
        this.consigneeBranchCode = consigneebranchcode;
    }

    @JsonSetter("consigneebranchname")
    public void setconsigneebranchnameLower(String consigneebranchname) {
        this.consigneeBranchName = consigneebranchname;
    }

    @JsonSetter("responsiblepersonpinfl")
    public void setresponsiblepersonpinflLower(String responsiblepersonpinfl) {
        this.responsiblePersonPinfl = responsiblepersonpinfl;
    }

    @JsonSetter("responsiblepersonfullname")
    public void setresponsiblepersonfullnameLower(String responsiblepersonfullname) {
        this.responsiblePersonFullName = responsiblepersonfullname;
    }
}

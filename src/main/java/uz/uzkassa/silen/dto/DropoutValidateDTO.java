package uz.uzkassa.silen.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 5/5/2023 19:14
 */
@Getter
@Setter
@NoArgsConstructor
public class DropoutValidateDTO {

    @NonNull
    String printCode;
}

package uz.uzkassa.silen.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import uz.uzkassa.silen.enumeration.PackageType;

import jakarta.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 9/23/2023 15:36
 */
@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StoreInMarkDTO implements Serializable {

    String id;

    PackageType unit;

    String code;

    @NotNull(message = "Код не может быть пустым")
    String printCode;

    Long itemId;

    @NotNull(message = "Возврат ид не может быть пустым")
    String storeInId;
}

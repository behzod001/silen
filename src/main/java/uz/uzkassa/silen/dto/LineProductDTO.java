package uz.uzkassa.silen.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the {@link uz.uzkassa.silen.domain.Line} entity.
 */
@Getter
@Setter
@ToString
public class LineProductDTO implements Serializable {
    @NotNull
    private String lineId;
    @NotEmpty
    private Set<String> productIds = new HashSet<>();
}

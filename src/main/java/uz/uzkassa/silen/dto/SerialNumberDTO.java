package uz.uzkassa.silen.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 18.11.2022 18:46
 */
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SerialNumberDTO {

    String serialNumber;

    String description;

    String productId;

    Integer startQuantity;

    LocalDate productionDate;
}

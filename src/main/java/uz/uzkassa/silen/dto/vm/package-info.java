/**
 * View Models used by Spring MVC REST controllers.
 */
package uz.uzkassa.silen.dto.vm;

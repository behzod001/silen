package uz.uzkassa.silen.dto.vm;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by: Azazello
 * Date: 12/9/2019 11:13 PM
 */

@Getter
@Setter
public class SignVM implements Serializable {
    private String sign;
}

package uz.uzkassa.silen.dto.act;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 5/25/2023 12:44
 */
@Getter
@Setter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AcceptanceTransferActSilenDTO {

    String actNumber;

    LocalDate actDate;

    List<AcceptanceTransferActProductSilenDTO> products;

    String description;

    Long customerId;

    String contractId;

    BigDecimal comission;

    BigDecimal totalCost;

    BigDecimal totalDiscount;

    BigDecimal totalPayment;
}

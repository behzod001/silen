package uz.uzkassa.silen.dto.act;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 5/30/2023 12:44
 */
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ActProductItemDTO implements Serializable {
    String id;

    String name;

    String barcode;

    String packageName;
}

package uz.uzkassa.silen.transaction;

import java.util.concurrent.Executor;

public interface AfterCommitExecutor extends Executor {
}

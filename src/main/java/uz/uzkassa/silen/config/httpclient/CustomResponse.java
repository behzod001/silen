package uz.uzkassa.silen.config.httpclient;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@NoArgsConstructor
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CustomResponse {

    int code;

    String content;

    String errorMessage;
}

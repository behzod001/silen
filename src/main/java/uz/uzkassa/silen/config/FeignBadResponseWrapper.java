package uz.uzkassa.silen.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpHeaders;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class FeignBadResponseWrapper extends RuntimeException {
    private final int status;
    private final HttpHeaders headers;
    private final String body;
    private final Map<String, String> content;

    public FeignBadResponseWrapper(int status, HttpHeaders headers, String body) {
        super("Bad request");
        this.status = status;
        this.headers = headers;
        this.body = body;
        this.content = new HashMap<>();
    }

    public FeignBadResponseWrapper(int status, HttpHeaders headers, Map<String, String> content) {
        super("Bad request");
        this.status = status;
        this.headers = headers;
        this.content = content;
        this.body = "";
    }
}

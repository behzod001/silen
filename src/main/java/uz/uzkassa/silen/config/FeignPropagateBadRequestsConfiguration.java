package uz.uzkassa.silen.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class FeignPropagateBadRequestsConfiguration extends FeignConfiguration {
    @Bean
    public ErrorDecoder errorDecoder() {
        return (methodKey, response) -> {
            int status = response.status();
            Map body = getResponseBodyAsString(response.body());
            HttpHeaders httpHeaders = new HttpHeaders();
            response.headers().forEach((k, v) -> httpHeaders.add("feign-" + k, StringUtils.join(v, ",")));
            return new FeignBadResponseWrapper(status, httpHeaders, body);
        };
    }

    private Map getResponseBodyAsString(final Response.Body body) {
        Map result = new HashMap();
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonStr = IOUtils.toString(body.asReader(StandardCharsets.UTF_8));
            System.out.println(jsonStr);
            result = mapper.readValue(jsonStr, Map.class);
        } catch (final IOException e) {
            System.out.println("ERROR: exception on parsing feign response");
        }
        return result;
    }
}

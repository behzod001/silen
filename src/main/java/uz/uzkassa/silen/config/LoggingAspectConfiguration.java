package uz.uzkassa.silen.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.Environment;
import uz.uzkassa.silen.aop.logging.LoggingAspect;

@Configuration
@EnableAspectJAutoProxy
public class LoggingAspectConfiguration {

    @Bean
//    @Profile(JHipsterConstants.SPRING_PROFILE_PRODUCTION)
    public LoggingAspect loggingAspect(Environment env) {
        return new LoggingAspect(env);
    }
}

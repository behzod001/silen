package uz.uzkassa.silen.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import uz.uzkassa.silen.domain.Organization;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.domain.redis.RedisLastAggregationMarks;
import uz.uzkassa.silen.dto.marking.ValidationBatchDTO;
import uz.uzkassa.silen.enumeration.ProductGroup;
import uz.uzkassa.silen.integration.TrueApi;
import uz.uzkassa.silen.integration.asilbelgi.CisInfo;
import uz.uzkassa.silen.integration.billing.dto.CisInfoResponse;
import uz.uzkassa.silen.repository.mongo.MarkRepository;
import uz.uzkassa.silen.repository.redis.RedisLastAggregationMarksRepository;
import uz.uzkassa.silen.utils.Utils;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class StationService extends BaseService {

    private final RedisLastAggregationMarksRepository redisLastAggregationMarksRepository;
    private final TrueApi trueApi;
    private final MarkRepository markRepository;


    public Map<String, String> validateBatch(ValidationBatchDTO validationBatchDTO) {
        final Organization currentOrganization = getCurrentOrganization();
        if (validationBatchDTO.isTrueCheck() && StringUtils.isEmpty(currentOrganization.getTrueToken())) {
            throw badRequest("Отсутствует токен организации").get();
        }
        Map<String, String> response = new HashMap<>();
        Map<String, String> validCodes = new HashMap<>();
        for (String printCode : validationBatchDTO.getCodes()) {
            final String code = Utils.clearPrintCode(printCode);
            if (!Utils.validateCode(printCode, validationBatchDTO.getUnit())) {
                response.put(printCode, "Код не соответствует стандартному формату");
                continue;
            }
            if (Utils.isMark(printCode) && Utils.checkIsEqualBarcodes(validationBatchDTO.getBarcode(), printCode)) {
                response.put(printCode, "Ошибка! Несоответствие штрихкода:\nKод: " + printCode);
                continue;
            }

            if (redisLastAggregationMarksRepository.existsById(code)) {
                response.put(printCode, "Код уже использован другим оператором");
                continue;
            }
            validCodes.put(code, printCode);
        }
        List<Mark> allByCodeIn = markRepository.findAllByCodeIn(validCodes.keySet());
        for (Mark mark : allByCodeIn) {
            if (mark.getParentCode() != null) {
                response.put(mark.getPrintCode(), "Ошибка! Код состоит в агрегации. КА: " + mark.getParentCode());
                validCodes.remove(mark.getCode());
                continue;
            } else if (mark.isUsed() || mark.getStatus().checkingIfMarkNotAbleToAggregate(mark.isBlock())) {
                response.put(mark.getPrintCode(), "Код не можете агрегировать. \nКод статус '" + mark.getStatus().getText() + "'");
                validCodes.remove(mark.getCode());
                continue;
            }
            if (!validationBatchDTO.isTrueCheck()) {
                response.put(mark.getPrintCode(), "");
                validCodes.remove(mark.getCode());
            }
        }
        if (validationBatchDTO.isTrueCheck()) {
            if (CollectionUtils.isNotEmpty(validCodes.keySet())) {
                final ProductGroup productGroup = currentOrganization.getProductGroup();
                final CisInfoResponse[] cisInfoResponses = trueApi.cisesInfo(currentOrganization.getTin(), currentOrganization.getTrueToken(), validCodes.keySet());
                for (CisInfoResponse cisInfoResponse : cisInfoResponses) {
                    final CisInfo cisInfo = cisInfoResponse.getCisInfo();
                    if (cisInfo != null) {
                        if (cisInfo.getStatus() != null) {
                            if (!productGroup.checkIsCanAggregateCodeByStatus(cisInfo.getStatus())) {
                                final String errorMessage = cisInfo.getStatus() != null ? "Ошибка агрегации! Статус Код : " + cisInfo.getStatus().getText() : "Ошибка агрегации! Статус Код : NULL";
                                response.put(cisInfo.getRequestedCis(), errorMessage);
                                validCodes.remove(cisInfo.getCis());
                            } else {
                                response.put(cisInfo.getRequestedCis(), "Код маркировки отсутствует в системе Турона!");
                                validCodes.remove(cisInfo.getCis());
                            }
                        }
                    }
                }
                if (CollectionUtils.isNotEmpty(validCodes.keySet())) {
                    for (String validCode : validCodes.values()) {
                        response.put(validCode, "Код маркировки отсутствует в системе Турона!");
                    }
                }
            }
        }

        validCodes.forEach((key, value) -> {
            redisLastAggregationMarksRepository.save(RedisLastAggregationMarks.builder()
                .id(key)
                .productId(validationBatchDTO.getProductId())
                .userId(getCurrentUserId())
                .sessionId(getCurrentSessionId())
                .printCode(value)
                .scanTime(LocalDateTime.now())
                .userLogin(getCurrentUserLogin())
                .build());
        });

        return response;
    }


}

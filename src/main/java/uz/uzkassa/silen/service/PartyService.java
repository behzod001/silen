package uz.uzkassa.silen.service;

import com.opencsv.CSVReader;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import uz.uzkassa.silen.domain.*;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.dto.SelectItem;
import uz.uzkassa.silen.dto.filter.MongoFilter;
import uz.uzkassa.silen.dto.marking.PartyDTO;
import uz.uzkassa.silen.dto.marking.PartyMergeDTO;
import uz.uzkassa.silen.dto.marking.PartyUtilisationDTO;
import uz.uzkassa.silen.dto.mq.Payload;
import uz.uzkassa.silen.enumeration.*;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.integration.TuronClient;
import uz.uzkassa.silen.integration.turon.GetMarksResponseDTO;
import uz.uzkassa.silen.integration.turon.UtilisationRequestDTO;
import uz.uzkassa.silen.integration.turon.UtilisationResponseDTO;
import uz.uzkassa.silen.repository.OrderProductRepository;
import uz.uzkassa.silen.repository.PartyRepository;
import uz.uzkassa.silen.repository.mongo.MarkRepository;
import uz.uzkassa.silen.utils.Utils;

import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Mark}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class PartyService extends BaseService {
    private final OrderProductRepository orderProductRepository;
    private final PartyRepository partyRepository;
    private final MarkRepository markRepository;
    private final TuronClient turonClient;

    public Page<PartyDTO> findAll(MongoFilter filter) {
        return partyRepository.findAllByFilter(filter).map(Party::toDto);
    }

    public List<SelectItem> getItems(MongoFilter filter) {
        return partyRepository.findAllByFilter(filter)
            .map(Party::toSelectItem)
            .getContent();
    }

    @Transactional
    public void save(final PartyDTO partyDTO) {
        final String orderProductId = partyDTO.getOrderProductId();

        final OrderProduct orderProduct = orderProductRepository.findById(orderProductId)
            .orElseThrow(notFound());
        if (!BufferStatus.ACTIVE.equals(orderProduct.getBufferStatus())) {
            throw new BadRequestException(Optional.ofNullable(orderProduct.getBufferStatus()).map(BufferStatus::getNameRu).orElse("Буфер не готов"));
        } else if (partyRepository.existsByOrderProductIdAndJobStatus(orderProductId, AggregationStatus.IN_PROGRESS)) {
            throw new BadRequestException("Невозможно создать партию. В процессе есть партия");
        }

        final int quantity = partyDTO.getQuantity();
        if (quantity > orderProduct.getRemaining()) {
            throw new BadRequestException("Количество превышает лимит");
        }

        final Organization organization = orderProduct.getOrder().getOrganization();
        if (StringUtils.isAnyEmpty(organization.getOmsId(), organization.getTuronToken())) {
            throw badRequest("В данной организации отсутствует OMS_ID или TOKEN").get();
        }

        Party party = new Party();
        party.setOrganizationId(organization.getId());
        party.setOrderProductId(orderProductId);
        party.setQuantity(quantity);
        party.setDescription(partyDTO.getDescription());
        party.setJobStatus(AggregationStatus.IN_PROGRESS);
        party = partyRepository.saveAndFlush(party);

        final String partyId = party.getId();
        afterCommitExecutor.execute(() -> rabbitMqProducer.sendPartyMarks(new Payload(partyId)));
    }

    @Transactional
    public void partyMarks(final Payload payload) {
        final Party party = partyRepository.findById(payload.getId())
            .orElseThrow(notFound());
        final int quantity = party.getQuantity();
        final OrderProduct orderProduct = party.getOrderProduct();
        final Order order = orderProduct.getOrder();
        final Organization organization = orderProduct.getOrder().getOrganization();
        final Product product = orderProduct.getProduct();

        final String extension = product.getType().getProductGroup().getExtension();
        final String barcode = Utils.fillProductBarcode(product.getBarcode());
        final String lastBlockId = Optional.ofNullable(orderProduct.getLastBlockId()).orElse("0");
        final boolean isBlock = CisType.GROUP.equals(orderProduct.getCisType());

        try {
            final GetMarksResponseDTO marksResponseDTO = turonClient.getMarks(extension, order.getTuronId(), lastBlockId, barcode, quantity, organization.getTuronToken(), organization.getOmsId());
            saveMarks(marksResponseDTO.getCodes(), party.getOrderProductId(), orderProduct.getProductId(), product.getShortName(), party.getId(), isBlock, organization.getId());

            party.setBlockId(marksResponseDTO.getBlockId());
            party.setJobStatus(AggregationStatus.DRAFT);
            partyRepository.save(party);

            orderProduct.setReceived(orderProduct.getReceived() + quantity);
            orderProduct.setRemaining(orderProduct.getRemaining() - quantity);
            orderProduct.setLastBlockId(marksResponseDTO.getBlockId());
            if (orderProduct.getRemaining() == 0) {
                orderProduct.setBufferStatus(BufferStatus.CLOSED);
            }
            orderProductRepository.save(orderProduct);
        } catch (Exception e) {
            party.setJobStatus(AggregationStatus.ERROR);
            party.setErrorMessage(e.getMessage());
            partyRepository.save(party);
        }
    }

    private void saveMarks(final Set<String> codes, final String orderProductId, final String productId,
                           final String productName, final String partyId, final boolean isBlock, final String organizationId) {
        for (final String printCode : codes) {
            final String code = Utils.clearPrintCode(printCode);
            if (!markRepository.existsByCode(code)) {
                Mark mark = new Mark();
                mark.setPrintCode(printCode);
                mark.setCode(code);
                mark.setOrderProductId(orderProductId);
                mark.setProductId(productId);
                mark.setProductName(productName);
                mark.setPartyId(partyId);
                mark.setCreatedDate(LocalDateTime.now());
                mark.setOrganizationId(organizationId);
                mark.setStatus(AggregationStatus.DRAFT);
                mark.setBlock(isBlock);
                mark.setQuantity(isBlock ? 0 : 1);
                markRepository.save(mark);
            }
        }
    }

    @Transactional
    public PartyStatus apply(String partyId, PartyUtilisationDTO partyUtilisationDTO) {
        Party party = partyRepository.findById(partyId).orElseThrow(notFound("Заказ не найден"));
        if (party.getQuantity() > 30000) {
            throw badRequest("Reached out of limit 30000").get();
        }
        if (PartyStatus.APPLIED.equals(party.getStatus())) throw badRequest("Заказ уже нанесен").get();
        ProductType productType = party.getOrderProduct().getProduct().getType();

        if (EnumSet.of(ProductGroup.Beer, ProductGroup.Alcohol).contains(productType.getProductGroup())) {
            if (partyUtilisationDTO.getProductionDate() == null) {
                throw this.badRequest("productionDate обязателен для ТГ " + productType.getProductGroup().getNameRu()).get();
            }
        } else if (ProductGroup.Pharma.equals(productType.getProductGroup())) {
            if (partyUtilisationDTO.getProductionDate() == null) {
                throw this.badRequest("productionDate обязателен для ТГ " + productType.getProductGroup().getNameRu()).get();
            }
            if (partyUtilisationDTO.getExpirationDate() == null) {
                throw this.badRequest("expirationDate обязателен для ТГ " + productType.getProductGroup().getNameRu()).get();
            }
            if (partyUtilisationDTO.getSeriesNumber() == null) {
                throw this.badRequest("serialNumber обязателен для ТГ " + productType.getProductGroup().getNameRu()).get();
            }
        }
        Organization organization = getCurrentOrganizationAndValidateOmsIdAndTuronToken();

        List<Mark> allByPartyId = markRepository.findAllByPartyId(partyId);
        Set<String> sntins = allByPartyId.stream().map(Mark::getPrintCode).collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(sntins)) {
            throw badRequest("Массив строк отсутствует").get();
        }
        UtilisationRequestDTO utilisationRequestDTO = new UtilisationRequestDTO();
        utilisationRequestDTO.setSntins(sntins);
        utilisationRequestDTO.setUsageType(Optional.ofNullable(partyUtilisationDTO.getUsageType()).orElse(UsageType.USED_FOR_PRODUCTION));
        utilisationRequestDTO.setProductionDate(partyUtilisationDTO.getProductionDate());
        utilisationRequestDTO.setExpirationDate(partyUtilisationDTO.getExpirationDate());
        utilisationRequestDTO.setSeriesNumber(partyUtilisationDTO.getSeriesNumber());
        utilisationRequestDTO.setProductionLineId(partyId);

        UtilisationResponseDTO utilisationResponseDTO = turonClient.apply(organization.getTuronToken(), productType.getProductGroup().getExtension(), organization.getOmsId(), utilisationRequestDTO);
        log.debug("UtilisationDTO {}", utilisationResponseDTO);
        if (utilisationResponseDTO.getReportId() != null) {
            party.setUsageType(utilisationRequestDTO.getUsageType());
            party.setReportId(utilisationResponseDTO.getReportId());
            party.setReportTime(LocalDateTime.now());
            party.setStatus(PartyStatus.APPLIED);
            partyRepository.save(party);

            afterCommitExecutor.execute(() -> rabbitMqProducer.sendPartyMarksSetApplied(new Payload(partyId)));
            return party.getStatus();
        } else {
            throw badRequest("Што то пошло не так попробуйте ещё раз").get();
        }
    }

    public void sendPartyMarksSetApplied(Payload payload) {
        long startTime = System.currentTimeMillis();
        List<Mark> allByPartyId = markRepository.findAllByPartyId(payload.getId());
        List<Mark> batchUpdate = allByPartyId.stream()
            .peek(mark -> {
                mark.setStatus(AggregationStatus.DRAFT);
                mark.setTuronStatus(MarkStatus.APPLIED);
            }).collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(batchUpdate)) {
            markRepository.saveAll(batchUpdate);
        }
        log.info("By party {} marks applied timing: {}", payload.getId(), System.currentTimeMillis() - startTime);
    }

    @Transactional
    public void uploadParty(String orderProductId, int quantity, MultipartFile multipartFile) {
        OrderProduct orderProduct = orderProductRepository.findById(orderProductId).orElseThrow(notFound());
        final boolean isBlock = CisType.GROUP.equals(orderProduct.getCisType());
        if (!BufferStatus.ACTIVE.equals(orderProduct.getBufferStatus())) {
            throw new BadRequestException(orderProduct.getBufferStatus() != null ? orderProduct.getBufferStatus().getNameRu() : "Буфер не готов");
        }
        if (quantity > orderProduct.getRemaining()) {
            throw new BadRequestException("Количество превышает лимит");
        }
        final Organization organization = getCurrentOrganization();
        if (StringUtils.isAnyEmpty(organization.getOmsId(), organization.getTuronToken())) {
            throw badRequest("В данной организации отсутствует OMS_ID или TOKEN").get();
        }
        Set<String> marks = readFromFile(multipartFile);
        Party party = new Party();
        party.setOrganizationId(organization.getId());
        party.setOrderProductId(orderProductId);
        party.setQuantity(quantity);
        party.setBlockId(orderProductId);
        partyRepository.save(party);

        saveMarks(marks, orderProductId, orderProduct.getProductId(), orderProduct.getProduct().getShortName(), party.getId(), isBlock, organization.getId());

        orderProduct.setReceived(marks.size() + quantity);
        orderProduct.setRemaining(orderProduct.getRemaining() - quantity);
        orderProduct.setLastBlockId(orderProductId);
        if (orderProduct.getRemaining() == 0) {
            orderProduct.setBufferStatus(BufferStatus.CLOSED);
        }
        orderProductRepository.save(orderProduct);
    }

    private Set<String> readFromFile(MultipartFile file) {
        Set<String> codes = new LinkedHashSet<>();
        try {
            CSVReader csvReader = new CSVReader(new InputStreamReader(file.getInputStream()));
            String[] values;
            while ((values = csvReader.readNext()) != null) {
                codes.add(values[0]);
            }
        } catch (Exception e) {
            log.error("On time reading something went wrong", e);
            throw badRequest("Time out").get();
        }
        return codes;
    }

    public void merge(final PartyMergeDTO dto) {
        final List<Mark> fromParty = markRepository.findAllByPartyId(dto.getFromId());
        final List<Mark> toParty = markRepository.findAllByPartyId(dto.getToId());
        final List<Mark> toDelete = new ArrayList<>();
        final List<Mark> toUpdate = new ArrayList<>();
        final Set<String> existingCodes = toParty.stream().map(Mark::getCode).collect(Collectors.toSet());
        final String toPartyId = dto.getToId();
        for (Mark mark : fromParty) {
            if (existingCodes.contains(mark.getCode())) {
                toDelete.add(mark);
            } else {
                mark.setPartyId(toPartyId);
                toUpdate.add(mark);
            }
        }
        if (CollectionUtils.isNotEmpty(toUpdate)) {
            markRepository.saveAll(toUpdate);
        }
        if (CollectionUtils.isNotEmpty(toDelete)) {
            markRepository.deleteAll(toDelete);
        }
    }

    public Map<String, Long> checkParties(List<String> partyIds) {
        Map<String, Long> responseMap = new HashMap<>();
        for (String partyId : partyIds) {
            responseMap.put(partyId, markRepository.countMarksByPartyIdAndUsedIsFalse(partyId));
        }
        return responseMap;
    }
}

package uz.uzkassa.silen.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.uzkassa.silen.domain.Aggregation;
import uz.uzkassa.silen.domain.AggregationHistory;
import uz.uzkassa.silen.domain.Dropout;
import uz.uzkassa.silen.domain.Organization;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.dto.*;
import uz.uzkassa.silen.dto.filter.DropoutFilter;
import uz.uzkassa.silen.dto.filter.MongoFilter;
import uz.uzkassa.silen.dto.mq.WarehousePayload;
import uz.uzkassa.silen.enumeration.AggregationStatus;
import uz.uzkassa.silen.enumeration.CodeType;
import uz.uzkassa.silen.enumeration.DropoutReason;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.integration.TrueApi;
import uz.uzkassa.silen.integration.TuronClient;
import uz.uzkassa.silen.integration.billing.dto.CisInfoResponse;
import uz.uzkassa.silen.integration.turon.DropoutRequest;
import uz.uzkassa.silen.integration.turon.DropoutResponse;
import uz.uzkassa.silen.repository.AggregationHistoryRepository;
import uz.uzkassa.silen.repository.AggregationRepository;
import uz.uzkassa.silen.repository.DropoutRepository;
import uz.uzkassa.silen.repository.ProductRepository;
import uz.uzkassa.silen.repository.mongo.MarkRepository;
import uz.uzkassa.silen.utils.Utils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 5/4/2023 17:11
 */
@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DropoutService extends BaseService {
    AggregationRepository aggregationRepository;
    AggregationHistoryRepository aggregationHistoryRepository;
    MarkRepository markRepository;
    DropoutRepository dropoutRepository;
    TuronClient turonClient;
    ProductRepository productRepository;
    TrueApi trueApi;

    public Page<DropoutListDTO> findAllByFilter(DropoutFilter filter) {
        return dropoutRepository.findAllByFilter(filter).map(Dropout::toDTO);
    }

    public DropoutStatsDTO findAllByFilterStats(DropoutFilter filter) {
        return dropoutRepository.findAllByFilterStats(filter);
    }

    public DropoutListDTO get(String id) {
        return dropoutRepository.findById(id).orElseThrow(notFound()).toDTO();
    }

    public DropoutCheckDTO check(DropoutValidateDTO validateDTO) {
        final String code = Utils.clearPrintCode(validateDTO.getPrintCode());

        DropoutCheckDTO dropoutCheckDTO = new DropoutCheckDTO();
        if (Utils.isAggregation(validateDTO.getPrintCode())) {
            Aggregation aggregation = aggregationRepository.findFirstByCode(code).orElseThrow(badRequest("Код не найден"));
            if (Arrays.asList(AggregationStatus.RETURNED, AggregationStatus.CREATED).contains(aggregation.getStatus())) {
                dropoutCheckDTO.setPrintCode(aggregation.getPrintCode());
                dropoutCheckDTO.setParentCode(aggregation.getParentCode());
                dropoutCheckDTO.setPackageType(CodeType.AGGREGATION);
                dropoutCheckDTO.setPackageTypeName(CodeType.AGGREGATION.getNameRu());
                dropoutCheckDTO.setProduct(new ProductCommonDTO(aggregation.getProductId(), aggregation.getProduct().getVisibleName(), aggregation.getProduct().getBarcode()));
            } else {
                throw badRequest("Код не сможете списат. Код статус: " + aggregation.getStatus().getText()).get();
            }
        } else if (Utils.tobaccoBlockMatcher(validateDTO.getPrintCode())) {
            Mark block = markRepository.findFirstByCode(code).orElseThrow(notFound("Код не найден"));
            if (Arrays.asList(AggregationStatus.RETURNED, AggregationStatus.CREATED).contains(block.getStatus())) {
                dropoutCheckDTO.setPrintCode(block.getPrintCode());
                dropoutCheckDTO.setParentCode(block.getParentCode());
                dropoutCheckDTO.setPackageType(CodeType.BLOCK);
                dropoutCheckDTO.setPackageTypeName(CodeType.BLOCK.getNameRu());
                productRepository.findById(block.getProductId()).ifPresent(product -> dropoutCheckDTO.setProduct(new ProductCommonDTO(product.getId(), product.getVisibleName(), product.getBarcode())));
            } else {
                throw badRequest("Код не сможете списат. Код статус: " + block.getStatus().getText()).get();
            }
        } else {
            Mark mark = markRepository.findFirstByCode(code).orElseThrow(badRequest("Код не найден"));
            if (mark.getParentCode() != null) {
                Optional<Aggregation> firstByCode = aggregationRepository.findFirstByCode(mark.getParentCode());
                if (firstByCode.isPresent()) {
                    if (!Arrays.asList(AggregationStatus.RETURNED, AggregationStatus.CREATED, AggregationStatus.UPDATED).contains(firstByCode.get().getStatus())) {
                        throw badRequest("Код не сможете списат. Код статус: " + firstByCode.get().getStatus().getText()).get();
                    } else {
                        dropoutCheckDTO.setPrintCode(mark.getPrintCode());
                        dropoutCheckDTO.setParentCode(mark.getParentCode());
                        dropoutCheckDTO.setPackageType(CodeType.MARK);
                        dropoutCheckDTO.setPackageTypeName(CodeType.MARK.getNameRu());
                        productRepository.findById(mark.getProductId()).ifPresent(product -> dropoutCheckDTO.setProduct(new ProductCommonDTO(product.getId(), product.getVisibleName(), product.getBarcode())));
                    }
                }
            } else {
                if (Arrays.asList(AggregationStatus.RETURNED, AggregationStatus.CREATED).contains(mark.getStatus())) {
                    dropoutCheckDTO.setPrintCode(mark.getPrintCode());
                    dropoutCheckDTO.setParentCode(mark.getParentCode());
                    dropoutCheckDTO.setPackageType(CodeType.MARK);
                    dropoutCheckDTO.setPackageTypeName(CodeType.MARK.getNameRu());
                    productRepository.findById(mark.getProductId()).ifPresent(product -> dropoutCheckDTO.setProduct(new ProductCommonDTO(product.getId(), product.getVisibleName(), product.getBarcode())));
                } else {
                    throw badRequest("Код не сможете списат. Код статус: " + mark.getStatus().getText()).get();
                }
            }
        }

        return dropoutCheckDTO;
    }

    public CisInfoResponse[] partyCheck(MongoFilter mongoFilter) {

        Page<Mark> allByFilter = markRepository.findAllByFilter(mongoFilter);

        Set<String> dropoutMarks = allByFilter.stream()
            .map(Mark::getPrintCode)
            .collect(Collectors.toSet());

        final Organization organization = getCurrentOrganization();
        if (StringUtils.isAnyEmpty(organization.getOmsId(), organization.getTuronToken())) {
            throw badRequest("В данной организации отсутствует OMS_ID или TOKEN").get();
        }
        if (CollectionUtils.isEmpty(dropoutMarks)) {
            throw badRequest("В запросе не указан ни один КМ").get();
        }
        return trueApi.cisesInfo(organization.getTin(), organization.getTrueToken(), dropoutMarks);
    }

    @Transactional
    public List<String> create(final DropoutDTO dropoutDTO) {
        final Organization organization = getCurrentOrganization();
        final Set<String> sntins = new LinkedHashSet<>();
        sntins.addAll(dropoutDTO.getAggregations().stream().map(Utils::clearAggregationCode).collect(Collectors.toSet()));
        sntins.addAll(dropoutDTO.getBlocks()/*.stream().map(Utils::clearAggregationCode).collect(Collectors.toSet())*/);
        sntins.addAll(dropoutDTO.getMarks());
        // send dropout
        final DropoutRequest dropoutRequest = DropoutRequest.builder()
            .participantId(organization.getTin())
            .address(organization.getAddress())
            .dropoutReason(dropoutDTO.getDropoutReason().name())
            .withChild(true)
            .sntins(sntins)
            .build();
        log.info("DROPOUT: {}", dropoutRequest);
        final DropoutResponse dropoutResponse = turonClient.dropout(dropoutRequest, organization.getProductGroup().getExtension(), organization.getTuronToken(), organization.getOmsId());

        if (StringUtils.isEmpty(dropoutResponse.getReportId())) {
            throw new BadRequestException("Списания прошло не успешно!");
        }

        final List<String> errorCodes = new LinkedList<>();
        if (CollectionUtils.isNotEmpty(dropoutDTO.getAggregations())) {
            errorCodes.addAll(dropoutAggregation(dropoutDTO, dropoutResponse.getReportId()));
        }
        if (CollectionUtils.isNotEmpty(dropoutDTO.getBlocks())) {
            errorCodes.addAll(dropoutBlock(dropoutDTO, dropoutResponse.getReportId()));
        }
        if (CollectionUtils.isNotEmpty(dropoutDTO.getMarks())) {
            errorCodes.addAll(dropoutMark(dropoutDTO, dropoutResponse.getReportId()));
        }
        return errorCodes;
    }

    public void sendBatchQueue() {
        final List<String> organizationIds = dropoutRepository.findAllAggregationsOrg(CodeType.AGGREGATION);
        for (String organizationId : organizationIds) {
            sendBatch(organizationId);
        }
    }

    public void sendBatch(final String organizationId) {
        final Organization organization = organizationRepository.getReferenceById(organizationId);
        final List<String> codes = dropoutRepository.findAllAggregationsByOrg(CodeType.AGGREGATION, organizationId);
        // send dropout
        final DropoutRequest dropoutRequest = new DropoutRequest();
        dropoutRequest.setParticipantId(organization.getTin());
        dropoutRequest.setAddress(organization.getAddress());
        dropoutRequest.setDropoutReason(DropoutReason.DEFECT.name());
        dropoutRequest.setWithChild(true);
        dropoutRequest.setSntins(codes.stream().map(Utils::clearAggregationCode).collect(Collectors.toSet()));
        final DropoutResponse dropoutResponse = turonClient.dropout(dropoutRequest, organization.getProductGroup().getExtension(), organization.getTuronToken(), organization.getOmsId());

        if (StringUtils.isEmpty(dropoutResponse.getReportId())) {
            throw new BadRequestException("Списания прошло не успешно! " + organization.getName());
        }
    }

    @Transactional
    public Set<String> dropoutMark(DropoutDTO dropoutDTO, String reportId) {
        Set<String> errorCodes = new HashSet<>(dropoutDTO.getMarks());
        List<Mark> markList = markRepository.findAllByCodeIn(dropoutDTO.getMarks().stream().map(Utils::clearPrintCode).collect(Collectors.toSet()));
        List<Mark> marks = new LinkedList<>();
        Set<String> cleanAggregationCodes = dropoutDTO.getAggregations().stream().map(Utils::clearPrintCode).collect(Collectors.toSet());

        Map<String, Aggregation> aggregationsBatch = new HashMap<>();
        List<AggregationHistory> aggregationsHistoryBatch = new LinkedList<>();
        for (Mark mark : markList) {
            if (mark.getParentCode() != null && !cleanAggregationCodes.contains(mark.getParentCode())) {
                if (aggregationsBatch.containsKey(mark.getParentCode())) {
                    Aggregation aggregation = aggregationsBatch.get(mark.getParentCode());
                    aggregation.setStatus(AggregationStatus.UPDATED);
                    aggregation.setDescription(AggregationStatus.UPDATED.getText());
//                    aggregation.setQuantity(aggregation.getQuantity() - 1);
                    aggregationsBatch.put(mark.getParentCode(), aggregation);
                } else {
                    Optional<Aggregation> firstByCode = aggregationRepository.findFirstByCode(mark.getParentCode());
                    if (firstByCode.isPresent()) {
                        firstByCode.get().setStatus(AggregationStatus.UPDATED);
                        firstByCode.get().setDescription(AggregationStatus.UPDATED.getText());
//                        firstByCode.get().setQuantity(firstByCode.get().getQuantity() - 1);
                        aggregationsBatch.put(mark.getParentCode(), firstByCode.get());
                        aggregationsHistoryBatch.add(AggregationHistory.fromAggregation(firstByCode.get()));
                    }
                }
                Dropout dropout = new Dropout();
                dropout.setOrganizationId(mark.getOrganizationId());
                dropout.setReportId(reportId);
                dropout.setDropoutReason(dropoutDTO.getDropoutReason());
                dropout.setCode(mark.getCode());
                dropout.setPrintCode(mark.getPrintCode());
                dropout.setCodeId(mark.getId());
                dropout.setCodeType(CodeType.MARK);
                dropout.setProductId(mark.getProductId());
                dropoutRepository.save(dropout);
            }

            mark.setUsed(true);
            //mark.setParentCode(null);
            mark.setStatus(AggregationStatus.DROPOUT);
            marks.add(mark);
            errorCodes.remove(mark.getPrintCode());
        }
        if (CollectionUtils.isNotEmpty(aggregationsBatch.values())) {
            aggregationRepository.saveAll(aggregationsBatch.values());
        }
        if (CollectionUtils.isNotEmpty(aggregationsHistoryBatch)) {
            aggregationHistoryRepository.saveAll(aggregationsHistoryBatch);
        }
        if (CollectionUtils.isNotEmpty(marks)) {
            markRepository.saveAll(marks);
            marks.forEach(WarehousePayload::dropoutMark);
        }
        return errorCodes;
    }

    @Transactional
    public Set<String> dropoutBlock(DropoutDTO dropoutDTO, String reportId) {
        Set<String> errorCodes = new HashSet<>(dropoutDTO.getBlocks());
        List<Mark> blocks = markRepository.findAllByCodeInAndStatusIn(dropoutDTO.getBlocks().stream().map(Utils::clearPrintCode).collect(Collectors.toSet()), Arrays.asList(AggregationStatus.CREATED, AggregationStatus.RETURNED));
        Set<String> cleanAggregationCodes = dropoutDTO.getAggregations().stream().map(Utils::clearPrintCode).collect(Collectors.toSet());

        for (Mark block : blocks) {
            if (block.getParentCode() != null && !cleanAggregationCodes.contains(block.getParentCode())) {
                aggregationRepository.findFirstByCode(block.getParentCode()).ifPresent(aggregation -> {
                    aggregation.setStatus(AggregationStatus.UPDATED);
                    aggregation.setDescription(AggregationStatus.UPDATED.getText());
                    aggregation.setQuantity(aggregation.getQuantity() - 1);
                    aggregationRepository.save(aggregation);
                });
                Dropout dropout = new Dropout();
                dropout.setReportId(reportId);
                dropout.setDropoutReason(dropoutDTO.getDropoutReason());
                dropout.setCodeType(CodeType.BLOCK);
                dropout.setCodeId(block.getId());
                dropout.setCode(block.getCode());
                dropout.setPrintCode(block.getPrintCode());
                dropout.setProductId(block.getProductId());
                dropout.setOrganizationId(block.getOrganizationId());
                dropoutRepository.save(dropout);
            }
            block.setStatus(AggregationStatus.DROPOUT);
            block.setDescription(AggregationStatus.DROPOUT.getText());
            aggregationHistoryRepository.save(AggregationHistory.fromBlock(block));
            markRepository.save(block);

            List<Mark> markListBatch = markRepository.findAllByParentCode(block.getCode());
            if (CollectionUtils.isNotEmpty(markListBatch)) {
                markRepository.saveAll(markListBatch);
            }
            errorCodes.remove(block.getPrintCode());
        }

        return errorCodes;
    }

    @Transactional
    public Set<String> dropoutAggregation(DropoutDTO dropoutDTO, String reportId) {
        Set<String> errorCodes = new HashSet<>(dropoutDTO.getAggregations());
        List<Aggregation> aggregations = aggregationRepository.findAllByCodeInAndStatusIn(dropoutDTO.getAggregations().stream().map(Utils::clearPrintCode).collect(Collectors.toList()), Arrays.asList(AggregationStatus.CREATED, AggregationStatus.RETURNED));
        for (Aggregation aggregation : aggregations) {
            aggregation.setStatus(AggregationStatus.DROPOUT);
            aggregation.setDescription(AggregationStatus.DROPOUT.getText());
            aggregationRepository.save(aggregation);
            aggregationHistoryRepository.save(AggregationHistory.fromAggregation(aggregation));

            Dropout dropout = new Dropout();
            dropout.setOrganizationId(aggregation.getOrganizationId());
            dropout.setReportId(reportId);
            dropout.setDropoutReason(dropoutDTO.getDropoutReason());
            dropout.setCode(aggregation.getCode());
            dropout.setPrintCode(aggregation.getPrintCode());
            dropout.setCodeId(aggregation.getId());
            dropout.setCodeType(CodeType.AGGREGATION);
            dropout.setProductId(aggregation.getProductId());
            dropoutRepository.save(dropout);

            List<Mark> allPrintCodeByParentCode = markRepository.findAllByParentCode(aggregation.getCode());
            dropoutDTO.getMarks().addAll(allPrintCodeByParentCode.stream().map(Mark::getPrintCode).collect(Collectors.toSet()));
            dropoutMark(dropoutDTO, reportId);
            errorCodes.remove(aggregation.getPrintCode());
        }

        return errorCodes;

    }

}

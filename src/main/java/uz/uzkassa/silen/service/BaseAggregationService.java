package uz.uzkassa.silen.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.uzkassa.silen.domain.Aggregation;
import uz.uzkassa.silen.domain.Organization;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.domain.redis.RedisAggregation;
import uz.uzkassa.silen.dto.mq.Payload;
import uz.uzkassa.silen.enumeration.AggregationStatus;
import uz.uzkassa.silen.enumeration.AggregationType;
import uz.uzkassa.silen.enumeration.MarkStatus;
import uz.uzkassa.silen.enumeration.ProductGroup;
import uz.uzkassa.silen.integration.TrueApi;
import uz.uzkassa.silen.integration.asilbelgi.CisInfo;
import uz.uzkassa.silen.integration.billing.dto.CisInfoResponse;
import uz.uzkassa.silen.integration.turon.UtilisationResponseDTO;
import uz.uzkassa.silen.rabbitmq.producer.RabbitMqProducer;
import uz.uzkassa.silen.repository.AggregationRepository;
import uz.uzkassa.silen.repository.mongo.MarkRepository;
import uz.uzkassa.silen.repository.redis.RedisAggregationRepository;
import uz.uzkassa.silen.utils.Utils;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Aggregation} and {@link Mark}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class BaseAggregationService {
    MarkRepository markRepository;
    RabbitMqProducer rabbitMqProducer;
    AggregationRepository aggregationRepository;
    RedisAggregationRepository redisAggregationRepository;
    UtilisationService utilisationService;
    TrueApi trueApi;


    /**
     * Note !
     * Do not create AggregationHistory here
     *
     * @param payload SaveAggregationMarksPayload
     */
    @Transactional
    public void saveMarks(final Payload payload) {
        final Aggregation aggregation = aggregationRepository.findFirstByCode(payload.getId()).orElse(null);
        if (aggregation == null) {
            return;
        }
        final RedisAggregation redisAggregation = redisAggregationRepository.findFirstByCode(aggregation.getCode()).orElse(null);
        if (redisAggregation == null) {
            return;
        }
        boolean aggregationChanged = false;
        final Organization organization = aggregation.getOrganization();
        final Map<String, String> codes = redisAggregation.getMarks()
            .stream()
            .filter(code -> !Utils.isAggregation(code))
            .collect(Collectors.toMap(Utils::clearPrintCode, Utils::fillWithUniCode));
        final Set<String> clearCodes = codes.keySet();
        final Set<String> aggrCodes = redisAggregation.getMarks()
            .stream()
            .filter(Utils::isAggregation)
            .map(Utils::clearAggregationCode)
            .collect(Collectors.toSet());
        final Map<String, CisInfo> turonCodes = new HashMap<>();

        if (CollectionUtils.isNotEmpty(clearCodes)) {
            if (!redisAggregation.isOffline()) {
                try {
                    final CisInfoResponse[] cisInfoResponses = trueApi.cisesInfo(organization.getTin(), organization.getTrueToken(), clearCodes);
                    for (CisInfoResponse cisInfoResponse : cisInfoResponses) {
                        final CisInfo cisInfo = cisInfoResponse.getCisInfo();
                        turonCodes.put(cisInfo.getCis(), cisInfo);
                    }
                } catch (Exception e) {
                    aggregation.setStatus(AggregationStatus.ERROR);
                    aggregation.setDescription("Ошибка при проверке КМ: " + e.getMessage());
                    aggregationChanged = true;
                    log.error("Error while cises info", e);
                }
            }

            final Map<String, Mark> marks = new HashMap<>();
            final Set<String> utiliseCodes = new HashSet<>();
            final Set<String> notFoundCodes = new HashSet<>();
            for (String code : clearCodes) {
                final Mark mark = markRepository.findFirstByCode(code).orElseGet(Mark::new);
                mark.setParentCode(redisAggregation.getCode());
                mark.setUsed(true);
                mark.setStatus(AggregationStatus.CREATED);
                if (mark.getId() == null) {
                    mark.setPrintCode(codes.get(code));
                    mark.setCode(code);
                    mark.setProductId(redisAggregation.getProductId());
                    mark.setCreatedDate(LocalDateTime.now());
                    mark.setOrganizationId(redisAggregation.getOrganizationId());
                }
                if (MapUtils.isEmpty(turonCodes)) {
                    marks.put(codes.get(code), mark);
                    continue;
                }
                if (!redisAggregation.isOffline()) {
                    if (turonCodes.containsKey(code)) {
                        final CisInfo cisInfo = turonCodes.get(code);
                        mark.setTuronStatus(cisInfo.getStatus());
                        if (MarkStatus.EMITTED == cisInfo.getStatus()) {
                            utiliseCodes.add(codes.get(code));
                        }
                    } else {
                        mark.setStatus(AggregationStatus.ERROR);
                        mark.setTuronStatus(MarkStatus.ERROR);
                        notFoundCodes.add(code);
                    }
                }
                marks.put(codes.get(code), mark);
            }
            if (CollectionUtils.isNotEmpty(notFoundCodes)) {
                aggregation.setStatus(AggregationStatus.ERROR);
                aggregation.setDescription("КМ не найден: " + notFoundCodes);
                aggregationRepository.save(aggregation);
                return;
            }
            if (CollectionUtils.isNotEmpty(utiliseCodes)) {
                try {
                    UtilisationResponseDTO utilisationResponseDTO;
                    if (ProductGroup.Pharma == organization.getProductGroup() && aggregation.getSerialNum() != null) {
                        utilisationResponseDTO = utilisationService.create(utiliseCodes, organization, aggregation.getProductionDate(), aggregation.getExpirationDate(), aggregation.getSerialNum().getSerialNumber());
                    } else {
                        utilisationResponseDTO = utilisationService.create(utiliseCodes, organization, aggregation.getProductionDate());
                    }
                    if (utilisationResponseDTO != null && utilisationResponseDTO.getReportId() != null) {
                        aggregation.setUtilisationId(utilisationResponseDTO.getReportId());
                    }
                    aggregation.setUtilisationTime(LocalDateTime.now());
                    for (String code : utiliseCodes) {
                        marks.get(code).setTuronStatus(MarkStatus.APPLIED);
                        marks.get(code).setReportId(aggregation.getUtilisationId());
                    }
                } catch (Exception e) {
                    aggregation.setStatus(AggregationStatus.ERROR);
                    aggregation.setDescription(e.getMessage());
                    log.error("Error while utilisation", e);
                }
                aggregationChanged = true;
            }
            markRepository.saveAll(marks.values());
        } else if (CollectionUtils.isNotEmpty(aggrCodes)) {
            aggregationRepository.updateParentCode(aggrCodes, aggregation.getCode());
        }

        if (redisAggregation.isOffline()) {
            aggregation.setStatus(AggregationStatus.ERROR);
            aggregation.setDescription("Агрегировано в режиме оффлайн");
            return;
        }

        if (aggregationChanged) {
            aggregationRepository.save(aggregation);
        }

        if (redisAggregation.isSendUtilise() && aggregation.getStatus() != AggregationStatus.ERROR) {
            rabbitMqProducer.resendAggregationToTuron(redisAggregation.getCode(), 300000);
            return;
        }

        if (AggregationType.PRINTING != aggregation.getAggregationType()) return;
        rabbitMqProducer.sendAggregationStatusChangedEvent(aggregation.toPayload());
    }
}

package uz.uzkassa.silen.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.uzkassa.silen.domain.OrderProduct;
import uz.uzkassa.silen.domain.Organization;
import uz.uzkassa.silen.domain.Party;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.dto.CheckCisesDTO;
import uz.uzkassa.silen.dto.filter.MongoFilter;
import uz.uzkassa.silen.dto.mongo.MarkDTO;
import uz.uzkassa.silen.enumeration.AggregationStatus;
import uz.uzkassa.silen.enumeration.ExportReportType;
import uz.uzkassa.silen.enumeration.MarkStatus;
import uz.uzkassa.silen.integration.TrueApi;
import uz.uzkassa.silen.integration.billing.dto.CisInfoResponse;
import uz.uzkassa.silen.integration.asilbelgi.TrueApiRequest;
import uz.uzkassa.silen.repository.PartyRepository;
import uz.uzkassa.silen.repository.mongo.MarkRepository;
import uz.uzkassa.silen.utils.CyrillicLatinConverter;
import uz.uzkassa.silen.exceptions.BadRequestException;

import java.util.*;

/**
 * Service Implementation for managing {@link uz.uzkassa.silen.domain.mongo.Mark}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class MarkService extends BaseService {
    MarkRepository markRepository;
    TrueApi trueApi;
    PartyRepository partyRepository;
    ExportReportService exportReportService;

    public Page<MarkDTO> findAll(MongoFilter filter) {
        return markRepository.findAllByFilter(filter).map(Mark::toDto);
    }


    public CisInfoResponse[] cisesInfo(CheckCisesDTO checkCisesDTO) {
        final String organizationId = Optional.ofNullable(checkCisesDTO.getOrganizationId()).orElse(getCurrentOrganizationId());
        final Organization organization = organizationRepository.findById(organizationId).orElseThrow(notFound());
        if (StringUtils.isEmpty(organization.getTrueToken())) {
            throw badRequest("Отсутствует токен организации").get();
        }
        return trueApi.cisesInfo(organization.getTin(), organization.getTrueToken(), checkCisesDTO.getCodes());
    }

    public Set<String> aggregatedList(TrueApiRequest request) {
        final String organizationId = request.getOrganizationId() == null ? getCurrentOrganizationId() : request.getOrganizationId();
        final Set<String> codes = request.getCodes() == null ? Collections.singleton(request.getCode()) : request.getCodes();
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(notFound());
        Map<String, Map<String, List>> result = trueApi.aggregatedList(organization.getTin(), organization.getTrueToken(), organization.getProductGroup(), codes, null);
        if (result != null && !result.keySet().isEmpty()) {
            for (String key : result.keySet()) {
                Map<String, List> map = result.get(key);
                return map.keySet();
            }
        }
        return null;
    }

    public MarkDTO checkMark(MarkDTO markDTO) {
        Organization organization = getCurrentOrganization();
        Mark mark = markRepository.findById(markDTO.getId()).orElseThrow(notFound());

        CisInfoResponse[] result = trueApi.cisesInfo(organization.getTin(), organization.getTrueToken(), Collections.singleton(markDTO.getCode()));
        if (result != null && result.length > 0) {
            mark.setCode(markDTO.getCode());
            mark.setTuronStatus(result[0].getCisInfo().getStatus());
            if (MarkStatus.APPLIED.equals(result[0].getCisInfo().getStatus())) {
                mark.setStatus(AggregationStatus.DRAFT);
            }
            markRepository.save(mark);
            return mark.toDto();
        }
        throw new BadRequestException("КМ отсутствует");
    }

    public void generateReport(MongoFilter filter) {
        String attachmentName;
        if (filter.getPartyId() != null) {
            Party party = partyRepository.findById(filter.getPartyId()).orElseThrow(notFound());
            OrderProduct orderProduct = party.getOrderProduct();
            attachmentName = orderProduct.getOrder().getNumber() + "_" +
                CyrillicLatinConverter.cyrilicToLatin(orderProduct.getProduct().getShortName()) + "_" +
                CyrillicLatinConverter.cyrilicToLatin(party.getDescription());
        } else {
            attachmentName = "Mark_Codes";
        }
        exportReportService.export(filter, ExportReportType.MARK_KM, filter.getFileType(), attachmentName);
    }
}

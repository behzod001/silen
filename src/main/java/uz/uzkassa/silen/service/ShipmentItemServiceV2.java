package uz.uzkassa.silen.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.uzkassa.silen.domain.Aggregation;
import uz.uzkassa.silen.domain.Organization;
import uz.uzkassa.silen.domain.Shipment;
import uz.uzkassa.silen.domain.ShipmentItem;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.domain.mongo.ShipmentMark;
import uz.uzkassa.silen.domain.redis.RedisShipmentItem;
import uz.uzkassa.silen.dto.warehouse.ShipmentCode;
import uz.uzkassa.silen.dto.warehouse.ShipmentItemCodeV2DTO;
import uz.uzkassa.silen.dto.warehouse.ShipmentItemDTO;
import uz.uzkassa.silen.dto.warehouse.ShipmentItemWithCodes;
import uz.uzkassa.silen.enumeration.AggregationStatus;
import uz.uzkassa.silen.enumeration.PackageType;
import uz.uzkassa.silen.enumeration.ProductGroup;
import uz.uzkassa.silen.enumeration.ShipmentStatus;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.exceptions.ErrorConstants;
import uz.uzkassa.silen.integration.TrueApi;
import uz.uzkassa.silen.integration.asilbelgi.CisInfo;
import uz.uzkassa.silen.integration.billing.dto.CisInfoResponse;
import uz.uzkassa.silen.repository.AggregationRepository;
import uz.uzkassa.silen.repository.ShipmentRepository;
import uz.uzkassa.silen.repository.mongo.MarkRepository;
import uz.uzkassa.silen.repository.mongo.ShipmentMarkRepository;
import uz.uzkassa.silen.repository.redis.RedisShipmentCodeRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static uz.uzkassa.silen.utils.Utils.clearPrintCode;
import static uz.uzkassa.silen.utils.Utils.isAggregation;
import static uz.uzkassa.silen.utils.Utils.isMark;

/**
 * Service Implementation for managing {@link ShipmentItem}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ShipmentItemServiceV2 extends BaseService {
    private final MarkRepository markRepository;
    private final ShipmentRepository shipmentRepository;
    private final AggregationRepository aggregationRepository;
    private final ShipmentMarkRepository shipmentMarkRepository;
    private final RedisShipmentCodeRepository shipmentCodeRepository;
    private final TrueApi trueApi;
    private final TelegramBotApi botApi;

    @Transactional
    public ShipmentItemDTO addMark(final ShipmentItemCodeV2DTO dto) {
        final Shipment shipment = shipmentRepository.findById(dto.getShipmentId()).orElseThrow(notFound());
        if (ShipmentStatus.CLOSED.equals(shipment.getStatus())) {
            throw badRequest("Отгрузка завершена").get();
        }
        validateCountLimit(dto.getShipmentId());
        final ShipmentItemWithCodes result = validateCode(dto);
        final RedisShipmentItem item = result.getShipmentItem();

        final Set<String> codes = result.getCodes();
        if (CollectionUtils.isNotEmpty(codes)) {
            saveMarks(codes, dto.getCode());
        }
        updateQuantity(item, result.getUnit(), true);
        shipmentCodeRepository.save(item);
        saveShipmentMark(dto.getCode(), item.getId(), result.getUnit());

        return null;
    }

    /**
     * checking limit of codes for once shipment
     *
     * @param shipmentId default = 5000
     */
    private void validateCountLimit(final String shipmentId) {
        BigDecimal existsCodesCount = BigDecimal.ONE;
        final List<RedisShipmentItem> list = shipmentCodeRepository.findAllByShipmentId(shipmentId);
        for (final RedisShipmentItem redisShipmentItem : list) {
            for (final ShipmentCode shipmentCode : redisShipmentItem.getTypes()) {
                existsCodesCount = existsCodesCount.add(BigDecimal.valueOf(shipmentCode.getCount())); //Todo remove nested loop
            }
        }
        log.debug("Added to shipment codes count is {}", existsCodesCount);
        if (existsCodesCount.compareTo(BigDecimal.valueOf(5000)) == 0) {
            throw new BadRequestException("В одном отгрузке не должно быть больше 5000 кодов");
        }
    }

    public ShipmentItemWithCodes validateCode(final ShipmentItemCodeV2DTO dto) {
        final String code = dto.getCode();
        final boolean isMark = isMark(code);
        final boolean isAggregation = isAggregation(code);
        if (!(isMark || isAggregation)) {
            throw badRequest("Код не соответствует стандартному формату").get();
        }
        final String cleanCode = clearPrintCode(code);
        if (shipmentMarkRepository.existsShipmentMarkByCode(cleanCode)) {
            throw new BadRequestException(isMark ? "Код маркировки уже отгружен" : "Код аггрегации уже отгружен", isMark ? ErrorConstants.MARK_USED_ERROR_CODE : ErrorConstants.AGGREGATION_USED_ERROR_CODE, cleanCode);
        }

        final Organization organization = getCurrentOrganization();
        final ShipmentItemWithCodes result = new ShipmentItemWithCodes();

        if (!isMark) {
            final Aggregation aggregation = aggregationRepository.findFirstByCode(cleanCode).orElseThrow(notFound("Код аггрегации не найден"));
            if (aggregation.getStatus().checkStatusContainNotAbleToShip()) {
                throw new BadRequestException("Код не может быть отгружен. \nКод статус " + aggregation.getStatus().getText());
            }
            if (aggregation.getParentCode() != null) {
                if (Arrays.asList(ProductGroup.Beer, ProductGroup.Tobacco, ProductGroup.Appliances, ProductGroup.Pharma).contains(organization.getProductGroup())) {
                    if (shipmentMarkRepository.existsShipmentMarkByCode(aggregation.getParentCode())) {
                        throw new BadRequestException("Код аггрегации уже отгружен", ErrorConstants.AGGREGATION_USED_ERROR_CODE, cleanCode);
                    } else {
                        aggregationRepository.findFirstByCode(aggregation.getParentCode()).ifPresent(parent -> {
                            if (parent.getStatus().checkStatusContainNotAbleToShip()) {
                                throw new BadRequestException("Код не может быть отгружен. \nКод статус " + aggregation.getStatus().getText());
                            }
                        });
                    }
                } else {
                    throw new BadRequestException("Ошибка! Код состоит в агрегации. КА: " + aggregation.getParentCode());
                }
            }
            final String productId = aggregation.getProductId();
            final RedisShipmentItem shipmentItem = shipmentCodeRepository.findFirstByShipmentIdAndProductId(dto.getShipmentId(), productId).orElseGet(RedisShipmentItem::new);
            if (shipmentItem.getId() == null) {
                shipmentItem.setShipmentId(dto.getShipmentId());
                shipmentItem.setProductId(productId);
                shipmentItem.setProductName(aggregation.getProduct().getName());
                shipmentItem.setBarcode(aggregation.getProduct().getBarcode());
                shipmentItem.setQty(BigDecimal.ZERO);
                result.setUnit(aggregation.getPackageType());
            }
        } else {
            final Mark mark = markRepository.findFirstByCode(cleanCode).orElseThrow(notFound("КМ не найден"));
            if (mark.getStatus().checkStatusContainNotAbleToShip()) {
                throw new BadRequestException("Код не может быть отгружен. \nКод статус " + mark.getStatus().getText());
            } else if (mark.getParentCode() != null) {
                if (EnumSet.of(ProductGroup.Beer, ProductGroup.Pharma).contains(organization.getProductGroup())) {
                    if (shipmentMarkRepository.existsShipmentMarkByCode(mark.getParentCode())) {
                        throw new BadRequestException("Код аггрегации уже отгружен", ErrorConstants.AGGREGATION_USED_ERROR_CODE, cleanCode);
                    } else {
                        aggregationRepository.findFirstByCode(mark.getParentCode()).ifPresent(parent -> {
                            if (parent.getStatus().checkStatusContainNotAbleToShip()) {
                                throw new BadRequestException("Код не может быть отгружен. \nКод статус " + parent.getStatus().getText());
                            }
                        });
                        // checkin parent block
                        markRepository.findFirstByCode(mark.getParentCode()).ifPresent(parent -> {
                            if (parent.getStatus().checkStatusContainNotAbleToShip()) {
                                throw new BadRequestException("Код не может быть отгружен. \nКод статус " + parent.getStatus().getText());
                            }
                        });
                    }
                } else {
                    throw new BadRequestException("Ошибка! Код состоит в агрегации. КА: " + mark.getParentCode());
                }
            }
            final String productId = mark.getProductId();
            final RedisShipmentItem shipmentItem = shipmentCodeRepository.findFirstByShipmentIdAndProductId(dto.getShipmentId(), productId).orElseGet(RedisShipmentItem::new);
            if (shipmentItem.getId() == null) {
                shipmentItem.setShipmentId(dto.getShipmentId());
                shipmentItem.setProductId(productId);
                shipmentItem.setProductName(mark.getProductName());
                shipmentItem.setQty(BigDecimal.ZERO);
            }
        }
        if (dto.isTrueCheck()) {
            if (StringUtils.isEmpty(organization.getTrueToken())) {
                throw badRequest("Отсутствует токен организации").get();
            }
            final Set<String> codes = checkCodeIsCompatibleForShip(organization, cleanCode, isMark);
            result.setCodes(codes);
        }
        return result;
    }

    public Set<String> checkCodeIsCompatibleForShip(final Organization organization, final String code, final boolean isMark) {
        final CisInfoResponse[] result = trueApi.cisesInfo(organization.getTin(), organization.getTrueToken(), Collections.singleton(code));
        if (result != null && result.length > 0) {
            final CisInfo cisInfo = result[0].getCisInfo();
            if (organization.getProductGroup().checkIsCanShipCodeByStatus(cisInfo.getStatus())) {
                if (isMark) {
                    return cisInfo.getChild();
                }
                return Optional.ofNullable(cisInfo.getChild()).orElseThrow(badRequest("Код аггрегации пустой!"));
            } else {
                final String errorMessage = cisInfo.getStatus() != null ? "Ошибка аггрегации! Статус КМ : " + cisInfo.getStatus().getText() : "Ошибка аггрегации! Статус КМ : NULL";
                final StringBuilder message = new StringBuilder("INN: ").append(getCurrentOrganizationTin()).append(" \n").append("CODE: ").append(code).append(" \n").append(errorMessage);
//                botApi.sendMessageToAdminChannel(message.toString());
                throw new BadRequestException(errorMessage);
            }
        }
        return null;
    }

    private void saveShipmentMark(final String code, final Long shipmentItemId, final PackageType packageType) {
        final String cleanCode = clearPrintCode(code);

        final ShipmentMark mark = new ShipmentMark();
        mark.setPrintCode(code);
        mark.setCode(cleanCode);
        if (packageType != null) {
            mark.setUnit(packageType.getCode());
        } else {
            mark.setUnit(null);
        }
        mark.setShipmentItemId(shipmentItemId);
        shipmentMarkRepository.save(mark);
    }

    private Integer updateQuantity(final RedisShipmentItem item, final PackageType unit, final boolean add) {
        Integer qty = 0;
        final Map<PackageType, ShipmentCode> types = new LinkedHashMap<>();
        for (ShipmentCode code : item.getTypes()) {
            types.put(code.getUnit(), code);
        }
        final ShipmentCode type = types.get(unit);
        if (unit == null) {
            qty = 1;
        } else if (PackageType.BOX.equals(unit)) {
            qty = types.get(PackageType.BOX).getCapacity();
        } else if (PackageType.PALLET.equals(unit)) {
            qty = types.get(PackageType.BOX).getCapacity() * types.get(PackageType.PALLET).getCapacity();
        }
        type.setCount(add ? type.getCount() + 1 : type.getCount() - 1);
        types.put(unit, type);
        item.setTypes(new LinkedHashSet<>(types.values()));

        if (add) {
            item.setQty(item.getQty().add(new BigDecimal(qty)));
        } else {
            item.setQty(item.getQty().subtract(new BigDecimal(qty)));
        }
        return qty;
    }

    private void saveMarks(final Set<String> codes, final String aggregationCode) {
        final List<Mark> markList = markRepository.findAllByCodeIn(codes);
        for (final Mark markExists : markList) {
            if (markExists.getParentCode() != null) {
                markExists.setParentCode(clearPrintCode(aggregationCode));
                markExists.setUsed(true);
                markExists.setStatus(AggregationStatus.SHIPPED);
            }
            markRepository.save(markExists);
            codes.remove(markExists.getCode());
        }
        if (CollectionUtils.isNotEmpty(codes)) {
            for (final String code : codes) {
                final Mark mark = new Mark();
                mark.setCode(code);
                mark.setPrintCode(code);
                mark.setCreatedDate(LocalDateTime.now());
                mark.setOrganizationId(getCurrentOrganizationId());
                mark.setParentCode(clearPrintCode(aggregationCode));
                mark.setUsed(true);
                mark.setStatus(AggregationStatus.SHIPPED);
                markRepository.save(mark);
            }
        }

    }
}

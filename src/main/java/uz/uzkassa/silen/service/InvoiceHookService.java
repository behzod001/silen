package uz.uzkassa.silen.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.compress.utils.Sets;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.uzkassa.silen.domain.Invoice;
import uz.uzkassa.silen.domain.ListInvoice;
import uz.uzkassa.silen.dto.eimzo.CertificateDto;
import uz.uzkassa.silen.dto.eimzo.Pkcs7Dto;
import uz.uzkassa.silen.dto.eimzo.SignatureData;
import uz.uzkassa.silen.dto.eimzo.SignerDto;
import uz.uzkassa.silen.dto.invoice.FacturaCustomer;
import uz.uzkassa.silen.dto.invoice.FacturaDTO;
import uz.uzkassa.silen.dto.invoice.FacturaNewDTO;
import uz.uzkassa.silen.enumeration.FacturaType;
import uz.uzkassa.silen.enumeration.Status;
import uz.uzkassa.silen.integration.EimzoClient;
import uz.uzkassa.silen.integration.FacturaClient;
import uz.uzkassa.silen.repository.ListInvoiceRepository;
import uz.uzkassa.silen.utils.DateUtils;
import uz.uzkassa.silen.exceptions.BadRequestException;

import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by: Azazello
 * Date: 11/23/2019 10:38 PM
 */

@Service
@Slf4j
@RequiredArgsConstructor
public class InvoiceHookService extends BaseService {
    private final CustomerService customerService;
    private final ListInvoiceRepository listInvoiceRepository;
    private final FacturaClient facturaClient;
    private final TelegramBotApi telegramBotApi;
    private final EimzoClient eimzoClient;

    private static ConcurrentHashMap<String, Status> map = new ConcurrentHashMap<>();

    @Transactional
    public void onUpdate(final FacturaNewDTO facturaNewDTO) {
        if (facturaNewDTO != null) {
            final String facturaId = facturaNewDTO.getFacturaId();
            final Status status = facturaNewDTO.getState();
            if (StringUtils.isEmpty(facturaId)) {
                telegramBotApi.sendMessageToAdminChannel(StringUtils.join("UPDATE", "\n\n NO FACTURA_ID", "\nJSON\n", facturaNewDTO));
                return;
            } else if (map.containsKey(facturaId) && map.get(facturaNewDTO.getFacturaId()) == status) {
                return;
            }
            map.put(facturaId, status);
            if (checkOrganization(facturaNewDTO.getSellerTin(), facturaNewDTO.getBuyerTin())) {
                final ListInvoice invoice = listInvoiceRepository.findOneByFacturaId(facturaId)
                    .orElseGet(ListInvoice::new);
                if (invoice.getId() == null) {
                    invoice.setFacturaId(facturaId);
                    invoice.setFacturaProductId(facturaNewDTO.getFacturaProductId());
                    invoice.setNumber(facturaNewDTO.getFacturaNo());
                    if (facturaNewDTO.getFacturaDate() != null) {
                        invoice.setInvoiceDate(facturaNewDTO.getFacturaDate().toLocalDate());
                    }
                    invoice.setSellerTin(facturaNewDTO.getSellerTin());
                    invoice.setSellerName(facturaNewDTO.getSellerName());
                    invoice.setBuyerTin(facturaNewDTO.getBuyerTin());
                    invoice.setBuyerName(facturaNewDTO.getBuyerName());
                    invoice.setTotal(facturaNewDTO.getPayableTotal());
                    invoice.setNotes(facturaNewDTO.getNotes());
                    String sign = facturaClient.getSellerFacturaSign(facturaNewDTO.getSellerTin(), facturaId, status, false);
                    if (StringUtils.isEmpty(sign) && StringUtils.isNotEmpty(facturaNewDTO.getBuyerTin())) {
                        sign = facturaClient.getBuyerFacturaSign(facturaNewDTO.getBuyerTin(), facturaId, status);
                    }
                    if (StringUtils.isNotEmpty(sign)) {
                        updateSignatureData(sign, invoice);
                    }
                    invoice.setSignatureContent(sign);
                    listInvoiceRepository.save(invoice);
                }
                invoice.setStatus(facturaNewDTO.getState());
                listInvoiceRepository.save(invoice);

                checkForInvalidInvoice(invoice);
            }
        }
    }

    @Deprecated
    @Transactional
    public void onReceive(final String sign) {
        final Pkcs7Dto pkcs7Dto = eimzoClient.backendVerifyAttached(sign);
        if (pkcs7Dto.isSuccess()) {
            final FacturaDTO facturaDTO = parseBase64(pkcs7Dto.getPkcs7Info().getDocumentBase64());
            if (facturaDTO != null) {
                if (StringUtils.isEmpty(facturaDTO.getFacturaId())) {
                    telegramBotApi.sendMessageToAdminChannel(StringUtils.join("RECEIVE\n\n NO FACTURA_ID", "\nDTO\n", facturaDTO));
                    return;
                }
                if (map.containsKey(facturaDTO.getFacturaId())) {
                    return;
                }
                map.put(facturaDTO.getFacturaId(), Status.PENDING);
                final SignerDto signerDto = pkcs7Dto.getPkcs7Info().getSigners().get(0);
                final CertificateDto certificateDto = signerDto.getCertificate().get(0);

                if (checkOrganization(facturaDTO.getSellerTin(), facturaDTO.getBuyerTin())) {
                    final ListInvoice invoice = listInvoiceRepository.findOneByFacturaId(facturaDTO.getFacturaId())
                        .orElseGet(ListInvoice::new);
                    if (invoice.getId() == null) {
                        invoice.setFacturaId(facturaDTO.getFacturaId());
                        if (facturaDTO.getFacturaDoc() != null) {
                            invoice.setNumber(facturaDTO.getFacturaDoc().getFacturaNo());
                            invoice.setInvoiceDate(facturaDTO.getFacturaDoc().getFacturaDate());
                        }
                        invoice.setSellerTin(facturaDTO.getSellerTin());
                        final FacturaCustomer seller = customerService.getCustomer(facturaDTO.getSellerTin(), false);
                        if(seller != null){
                            invoice.setSellerName(seller.getName());
                        }
                        if ((facturaDTO.getSingleSidedType() == null || facturaDTO.getSingleSidedType() == 0) && StringUtils.isNotEmpty(facturaDTO.getBuyerTin())) {
                            invoice.setBuyerTin(facturaDTO.getBuyerTin());
                            final FacturaCustomer buyer = customerService.getCustomer(facturaDTO.getBuyerTin(), false);
                            if(buyer != null){
                                invoice.setBuyerName(buyer.getName());
                            }
                        }
                        if (facturaDTO.getProductList() != null) {
                            invoice.setTotal(facturaDTO.getProductList().getDeliverySumWithVat());
                        }
                    }
                    invoice.setStatus(Status.PENDING);
                    invoice.setSignatureContent(sign);
                    if (invoice.getSendDate() == null || StringUtils.isEmpty(invoice.getSendBy())) {
                        invoice.setSendNumber(Integer.parseInt(certificateDto.getSerialNumber(), 16) + "");
                        invoice.setSendBy(certificateDto.getSubjectInfo().getFullName());
                        invoice.setSendDate(DateUtils.parse(signerDto.getTimeStampInfo().getTime()));
                    }
                    listInvoiceRepository.save(invoice);
                }
            }
        } else {
            log.debug("JSON: {}", pkcs7Dto);
            throw new BadRequestException("Not verified, reason: " + pkcs7Dto.getMessage());
        }
    }

    @Deprecated
    @Transactional
    public void onAcceptReject(final String sign, final Status status) {
        final Pkcs7Dto pkcs7Dto = eimzoClient.backendVerifyAttached(sign);
        if (pkcs7Dto.isSuccess()) {
            final FacturaDTO facturaDTO = parseBase64(pkcs7Dto.getPkcs7Info().getDocumentBase64());
            if (facturaDTO != null) {
                if (StringUtils.isEmpty(facturaDTO.getFacturaId())) {
                    telegramBotApi.sendMessageToAdminChannel(StringUtils.join("RECEIVE\n\n NO FACTURA_ID", "\nDTO\n", facturaDTO));
                    return;
                }
                if (map.containsKey(facturaDTO.getFacturaId()) && map.get(facturaDTO.getFacturaId()) == status) {
                    return;
                }
                map.put(facturaDTO.getFacturaId(), status);

                final Optional<ListInvoice> invoiceOptional = listInvoiceRepository.findOneByFacturaId(facturaDTO.getFacturaId());

                if (invoiceOptional.isPresent() && status != invoiceOptional.get().getStatus()) {
                    final ListInvoice invoice = invoiceOptional.get();

                    final SignerDto signerDto = pkcs7Dto.getPkcs7Info().getSigners().size() > 1 ?
                        pkcs7Dto.getPkcs7Info().getSigners().get(1) : pkcs7Dto.getPkcs7Info().getSigners().get(0);
                    final CertificateDto certificateDto = signerDto.getCertificate().get(0);

                    invoice.setNotes(facturaDTO.getNotes());
                    invoice.setApprovedNumber(Integer.parseInt(certificateDto.getSerialNumber(), 16) + "");
                    invoice.setApprovedBy(certificateDto.getSubjectInfo().getFullName());
                    invoice.setApprovedDate(DateUtils.parse(signerDto.getTimeStampInfo().getTime()));

                    if (invoice.getSendDate() == null || StringUtils.isEmpty(invoice.getSendBy())) {
                        final SignerDto senderSign = pkcs7Dto.getPkcs7Info().getSigners().get(0);
                        final CertificateDto senderCert = senderSign.getCertificate().get(0);

                        invoice.setSendNumber(Integer.parseInt(senderCert.getSerialNumber(), 16) + "");
                        invoice.setSendBy(senderCert.getSubjectInfo().getFullName());
                        invoice.setSendDate(DateUtils.parse(senderSign.getTimeStampInfo().getTime()));
                    }
                    invoice.setStatus(status);
//                        invoice.setSignatureContent(sign);
                    listInvoiceRepository.save(invoice);
                }
            }
        } else {
            log.debug("JSON: {}", pkcs7Dto);
            throw new BadRequestException("Not verified, reason: " + pkcs7Dto.getMessage());
        }
    }

    private boolean checkOrganization(final String sellerTin, final String buyerTin) {
        return organizationRepository.existsOrganizationByTinInAndDeletedIsFalse(Sets.newHashSet(sellerTin, buyerTin));
    }

    private void updateSignatureData(String sign, ListInvoice invoice) {
        Pkcs7Dto pkcs7Dto = eimzoClient.backendVerifyAttached(sign);
        if (pkcs7Dto.isSuccess()
            && pkcs7Dto.getPkcs7Info() != null
            && CollectionUtils.isNotEmpty(pkcs7Dto.getPkcs7Info().getSigners())) {
            SignatureData signatureData = null;

            if (StringUtils.isAnyEmpty(invoice.getSendBy(), invoice.getSendNumber())) {
                signatureData = parseSignatureData(pkcs7Dto.getPkcs7Info().getSigners().get(0));
                invoice.setSenderData(signatureData);
            }
            if (StringUtils.isAnyEmpty(invoice.getApprovedBy(), invoice.getApprovedNumber()) && pkcs7Dto.getPkcs7Info().getSigners().size() > 1) {
                signatureData = parseSignatureData(pkcs7Dto.getPkcs7Info().getSigners().get(1));
                invoice.setApproverData(signatureData);
            }
        } else {
            log.debug("JSON: {}", pkcs7Dto);
        }
    }

    private void checkForInvalidInvoice(final ListInvoice listInvoice) {
        if (listInvoice.getInvoice() == null) {
            return;
        }
        final Invoice invoice = listInvoice.getInvoice();
        if (Status.ACCEPTED == listInvoice.getStatus()
            && invoice.getType().equals(FacturaType.EDITED.getId())
            && StringUtils.isNotBlank(invoice.getOldFacturaId())) {

            listInvoiceRepository.findOneByFacturaId(invoice.getOldFacturaId())
                .ifPresent(invoice1 -> {
                    invoice1.setInvalid(true);
                    listInvoiceRepository.save(invoice1);
                });
        }
    }
}

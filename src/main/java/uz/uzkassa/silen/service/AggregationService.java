package uz.uzkassa.silen.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.uzkassa.silen.kafka.KafkaConstants;
import uz.uzkassa.silen.domain.Aggregation;
import uz.uzkassa.silen.domain.AggregationHistory;
import uz.uzkassa.silen.domain.Organization;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.domain.redis.RedisAggregation;
import uz.uzkassa.silen.dto.filter.AggregationFilter;
import uz.uzkassa.silen.dto.marking.*;
import uz.uzkassa.silen.dto.mongo.MarkDTO;
import uz.uzkassa.silen.dto.mq.Payload;
import uz.uzkassa.silen.dto.mq.WarehousePayload;
import uz.uzkassa.silen.enumeration.*;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.exceptions.ErrorConstants;
import uz.uzkassa.silen.integration.TrueApi;
import uz.uzkassa.silen.integration.TuronClient;
import uz.uzkassa.silen.integration.asilbelgi.CisInfo;
import uz.uzkassa.silen.integration.asilbelgi.ReportInfo;
import uz.uzkassa.silen.integration.billing.dto.CisInfoResponse;
import uz.uzkassa.silen.integration.turon.AggregationItem;
import uz.uzkassa.silen.integration.turon.CreateAggregationRequest;
import uz.uzkassa.silen.integration.turon.CreateAggregationResponse;
import uz.uzkassa.silen.integration.turon.UtilisationResponseDTO;
import uz.uzkassa.silen.kafka.producer.KafkaProducer;
import uz.uzkassa.silen.repository.AggregationHistoryRepository;
import uz.uzkassa.silen.repository.AggregationRepository;
import uz.uzkassa.silen.repository.mongo.MarkRepository;
import uz.uzkassa.silen.repository.redis.RedisAggregationRepository;
import uz.uzkassa.silen.repository.redis.RedisLastAggregationMarksRepository;
import uz.uzkassa.silen.utils.Utils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Aggregation}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AggregationService extends BaseService {
    TrueApi trueApi;
    TuronClient turonClient;
    MarkRepository markRepository;
    UtilisationService utilisationService;
    AggregationRepository aggregationRepository;
    AggregationHistoryRepository aggregationHistoryRepository;
    RedisAggregationRepository redisAggregationRepository;
    RedisLastAggregationMarksRepository redisLastAggregationMarksRepository;
    BillingPermissionService billingPermissionService;
    KafkaProducer<WarehousePayload> kafkaProducer;

    @Transactional
    public String create(final AggregationCreateDTO aggregationDTO, final boolean fromUpdate, final Organization organizationOptional) {
        final Organization organization = Optional.ofNullable(organizationOptional).orElseGet(this::getCurrentOrganizationAndValidateOmsIdAndTuronToken);
        final ProductGroup productGroup = organization.getProductGroup();
        final boolean isAlcohol = productGroup == ProductGroup.Alcohol;
        final boolean isPharma = productGroup == ProductGroup.Pharma;
        final boolean isWater = productGroup == ProductGroup.Water;

        if (aggregationDTO.getMarks().size() != aggregationDTO.getQuantity()) {
            throw badRequest("Неверное Количество КМ").get();
        }
        if (isPharma) {
            if (aggregationDTO.getSerialId() == null)
                throw badRequest("Серия не выбрано").get();
            if (aggregationDTO.getProductionDate() == null)
                throw badRequest("Дата производства не определена").get();
            if (aggregationDTO.getExpirationDate() == null)
                throw badRequest("Срок годности не определена").get();
        }

        if (!fromUpdate && !aggregationDTO.isOffline()) {
            if (isAlcohol) {
                billingPermissionService.checkIsAvailableCount(organization.getId(), organization.isDemoAllowed(), organization.getParentId(), productGroup, 1);
            } else if (isPharma) {
                if (organization.getTypes().contains(OrganizationType.Importer)) {
                    billingPermissionService.checkIsAvailableCount(organization.getId(), organization.isDemoAllowed(), organization.getParentId(), productGroup, aggregationDTO.getQuantity());
                }
            }
        }

        Aggregation aggregation;
        if (StringUtils.isNotEmpty(aggregationDTO.getPrintCode())) {
            final String code = Utils.clearPrintCode(aggregationDTO.getPrintCode());
            aggregation = aggregationRepository.findFirstByCode(code).orElseThrow(notFound("Код агрегации не найден: " + aggregationDTO.getPrintCode()));
            if (aggregation.getStatus() != AggregationStatus.DRAFT) {
                throw new BadRequestException("Код агрегации уже использован", ErrorConstants.AGGREGATION_USED_ERROR_CODE);
            }
        } else {
            aggregation = generateAggregation(aggregationDTO.getUnit(), organization.getId(), aggregationDTO.getGcp());
        }
        aggregation.setProductId(aggregationDTO.getProductId());
        aggregation.setQuantity(aggregationDTO.getQuantity());
        aggregation.setSerialId(aggregationDTO.getSerialId());
        aggregation.setLogin(Optional.ofNullable(getCurrentUserLogin()).orElseGet(aggregationDTO::getLogin));
        aggregation.setProductionDate(LocalDateTime.now());
        aggregation.setAggregationType(aggregationDTO.getType());
        aggregation.setStatus(AggregationStatus.IN_PROGRESS);
        aggregation.setPartyAggregationId(aggregationDTO.getAggregationPartyId());
        // productionTime for sending to utilization
        if (isPharma) {
            LocalDateTime currentTime = LocalDateTime.now();
            if (aggregationDTO.getProductionDate() != null) {
                aggregation.setProductionDate(aggregationDTO.getProductionDate().atStartOfDay()
                    .withHour(currentTime.getHour())
                    .withMinute(currentTime.getMinute())
                    .withSecond(currentTime.getSecond()));
            }
            if (aggregationDTO.getExpirationDate() != null) {
                aggregation.setExpirationDate(aggregationDTO.getExpirationDate().atStartOfDay()
                    .withHour(currentTime.getHour())
                    .withMinute(currentTime.getMinute())
                    .withSecond(currentTime.getSecond()));
            }
        }
        if (isAlcohol || isPharma || isWater) {
            if (StringUtils.isBlank(organization.getTrueToken())) {
                throw badRequest("Турон токен отсутствует").get();
            } else {
                final String firstCode = Utils.clearPrintCode(aggregationDTO.getMarks().iterator().next());
                try {
                    trueApi.cisesInfo(organization.getTin(), organization.getTrueToken(), Collections.singleton(firstCode));
                } catch (Exception e) {
                    log.error("Aggregation creation error", e);
                    if (e.getMessage().contains("Connection refused")) {
                        throw new BadRequestException(ErrorConstants.ERR_CONNECTION_ERROR, e.getMessage(), ErrorConstants.TURON_NOT_AVAILABLE_ERROR_CODE, null);
                    } else {
                        throw badRequest(e.getMessage()).get();
                    }
                }
            }
        } else {
            final Set<String> snTins = aggregationDTO.getMarks().stream().map(Utils::clearPrintCode).collect(Collectors.toSet());
            AggregationItem aggregationItem = new AggregationItem();
            aggregationItem.setAggregatedItemsCount(aggregation.getQuantity());
            aggregationItem.setAggregationUnitCapacity(aggregation.getQuantity());
            aggregationItem.setAggregationType(AggregationType.AGGREGATION);
            aggregationItem.setUnitSerialNumber(aggregation.getCode());
            aggregationItem.setSntins(snTins);
            final CreateAggregationRequest request = new CreateAggregationRequest(Collections.singletonList(aggregationItem), organization.getTin());
            if (ProductGroup.Tobacco == productGroup) {
                request.setProductionLineId(productGroup.getNameRu());
            }
            final CreateAggregationResponse response = turonClient.createAggregation(request, productGroup.getExtension(), organization.getTuronToken(), organization.getOmsId());
            if (response == null) {
                throw badRequest("Timeout").get();
            } else {
                aggregation.setReportId(response.getReportId());
                aggregation.setAggregationDate(LocalDateTime.now());
                aggregation.setStatus(AggregationStatus.CREATED);
            }
            // add aggregation to warehouse production
            afterCommitExecutor.execute(() -> {
                final WarehousePayload payload = new WarehousePayload();
                payload.setOrganizationId(organization.getId());
                payload.setOperationDate(LocalDateTime.now());
                payload.setOperation(WarehouseOperation.Production);
                payload.setNote("Произведено");
                payload.setUnit(PackageType.fromCode(aggregation.getUnit()));
                payload.setAlcoholCount(BigDecimal.valueOf(aggregation.getQuantity().longValue()));
                payload.setProductId(aggregation.getProductId());
                payload.setSerialId(aggregation.getSerialId());
                payload.setCode(aggregation.getCode());
                payload.setLogin(aggregation.getLogin());
                payload.setAggregationStatus(aggregation.getStatus());
                kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC, payload);
                log.debug("aggregationRepository.save {}", aggregation);

            });
        }
        aggregationRepository.save(aggregation);

        // save marks to redis
        final RedisAggregation redisAggregation = new RedisAggregation();
        redisAggregation.setCode(aggregation.getCode());
        redisAggregation.setMarks(aggregationDTO.getMarks());
        redisAggregation.setProductId(aggregationDTO.getProductId());
        redisAggregation.setOrganizationId(organization.getId());
        redisAggregation.setSendUtilise(isAlcohol || isPharma || isWater);
        redisAggregation.setOffline(aggregationDTO.isOffline());
        redisAggregationRepository.save(redisAggregation);

        if (AggregationType.PRINTING == aggregation.getAggregationType()) {
            // notify current aggregation state
            rabbitMqProducer.sendAggregationStatusChangedEvent(aggregation.toPayload());
        } else {
            redisLastAggregationMarksRepository.deleteAll(redisLastAggregationMarksRepository.findAllBySessionIdAndProductId(getCurrentSessionId(), aggregationDTO.getProductId()));
        }
        //send to save mongo
        afterCommitExecutor.execute(() -> rabbitMqProducer.sendForSaveAggregationMarks(new Payload(aggregation.getCode())));
        return fromUpdate ? aggregation.getId() : aggregation.getPrintCode();
    }

    @Transactional
    public String update(String id, AggregationUpdateDTO aggregationUpdateDTO) {
        if (StringUtils.isEmpty(aggregationUpdateDTO.getAggregation())) {
            throw badRequest("Коды маркировки отсутствуют.").get();
        }
        final Organization organization = getCurrentOrganizationAndValidateOmsIdAndTuronToken();
        final ProductGroup productGroup = organization.getProductGroup();
        final boolean isAlcohol = productGroup == ProductGroup.Alcohol;
        final boolean isPharma = productGroup == ProductGroup.Pharma;

        if (!aggregationUpdateDTO.isOffline()) {
            if (isAlcohol) {
                billingPermissionService.checkIsAvailableCount(organization.getId(), organization.isDemoAllowed(), organization.getParentId(), productGroup, 1);
            } else if (isPharma) {
                if (organization.getTypes().contains(OrganizationType.Importer)) {
                    billingPermissionService.checkIsAvailableCount(organization.getId(), organization.isDemoAllowed(), organization.getParentId(), productGroup, aggregationUpdateDTO.getChildMarks().size());
                }
            }
        }

        final Aggregation oldAggregation = aggregationRepository.findById(id).orElseThrow(notFound("Код не найден"));
        if (oldAggregation.getStatus().checkingIfAggregationNotAbleToUpdate())
            throw badRequest("Код не агрегирован").get();

        AggregationCreateDTO aggregationDTO = new AggregationCreateDTO();
        aggregationDTO.setProductId(oldAggregation.getProductId());
        aggregationDTO.setGcp(oldAggregation.getGcp());
        aggregationDTO.setSerialId(oldAggregation.getSerialId());
        aggregationDTO.setUnit(PackageType.fromCode(oldAggregation.getUnit()));
        aggregationDTO.setQuantity(aggregationUpdateDTO.getChildMarks().size());
        aggregationDTO.setDescription(aggregationUpdateDTO.getDescription());
        aggregationDTO.setPrintCode(aggregationUpdateDTO.getAggregation());
        aggregationDTO.setDescription(aggregationUpdateDTO.getAggregation());
        aggregationDTO.setMarks(aggregationUpdateDTO.getChildMarks());
        aggregationDTO.setOffline(aggregationUpdateDTO.isOffline());
        aggregationDTO.setProductionDate(aggregationUpdateDTO.getProductionDate());
        aggregationDTO.setExpirationDate(aggregationUpdateDTO.getExpirationDate());
        aggregationDTO.setType(aggregationUpdateDTO.getType());
        aggregationDTO.setAggregationPartyId(oldAggregation.getPartyAggregationId());

        String response = create(aggregationDTO, true, organization);

        if (StringUtils.isNotEmpty(response)) {
            Set<String> updateChildren = aggregationUpdateDTO.getChildMarks().stream().map(Utils::clearPrintCode).collect(Collectors.toSet());
                if (PackageType.BOX.getCode() == oldAggregation.getUnit()) {
                    List<Mark> oldMarks = markRepository.findAllByParentCode(oldAggregation.getCode());
                    Map<String, Mark> oldBatch = oldMarks.stream().filter(mark -> !updateChildren.contains(mark.getCode())).collect(Collectors.toMap(Mark::getCode, oldChildMarkItem -> {
                        oldChildMarkItem.setParentCode(null);
                        if (!AggregationStatus.DROPOUT.equals(oldChildMarkItem.getStatus())) {
                            oldChildMarkItem.setStatus(AggregationStatus.DRAFT);
                            oldChildMarkItem.setUsed(false);
                        }
                        return oldChildMarkItem;
                    }));
                    markRepository.saveAll(oldBatch.values());
                }
            // oldAggregation
            int qty = oldAggregation.getQuantity();
            oldAggregation.setQuantity(0);
            oldAggregation.setStatus(AggregationStatus.UTILIZED);
            oldAggregation.setPartyAggregationId(null);
            String utilizedMessage = String.format("Агрегация утилизировано от оператора %s в час %s. Новый код агрегация %s", getCurrentUserLogin(), LocalDateTime.now(), aggregationUpdateDTO.getAggregation());
            oldAggregation.setDescription(utilizedMessage);
            oldAggregation.setLogin(getCurrentUserLogin());
            aggregationRepository.save(oldAggregation);

            WarehousePayload warehousePayload = new WarehousePayload();
            warehousePayload.setOrganizationId(getCurrentOrganizationId());
            warehousePayload.setOperationDate(LocalDateTime.now());
            warehousePayload.setOperation(WarehouseOperation.Utilized);
            warehousePayload.setNote(utilizedMessage);
            warehousePayload.setUnit(oldAggregation.getPackageType());
            warehousePayload.setAlcoholCount(BigDecimal.valueOf(qty));
            warehousePayload.setProductId(oldAggregation.getProductId());
            warehousePayload.setSerialId(oldAggregation.getSerialId());
            warehousePayload.setCode(oldAggregation.getCode());
            kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC, warehousePayload);

            if (AggregationType.PRINTING == oldAggregation.getAggregationType()) {
                afterCommitExecutor.execute(() -> rabbitMqProducer.sendAggregationStatusChangedEvent(oldAggregation.toPayload()));
            }
        }
        return response;
    }

    public Page<AggregationListDTO> findAllByFilter(AggregationFilter filter) {
        if (StringUtils.isNotEmpty(filter.getSearch())) {
            final String code = Utils.clearPrintCode(filter.getSearch());
            if (Utils.isMark(filter.getSearch()) || Utils.alcoholMatcherWithoutCX(filter.getSearch())) {
                markRepository.findFirstByCode(code).ifPresent(value -> filter.setSearch(value.getParentCode()));
            } else {
                filter.setSearch(code);
            }
        }
        return aggregationRepository.findAllByFilter(filter)
            .map(aggregation -> {
            AggregationListDTO aggregationListDTO = new AggregationListDTO();
            aggregationListDTO.setId(aggregation.getId());
            aggregationListDTO.setQuantity(aggregation.getQuantity());
            aggregationListDTO.setPrintCode(aggregation.getPrintCode());
            aggregationListDTO.setLogin(aggregation.getLogin());
            aggregationListDTO.setUnitName(PackageType.fromCode(aggregation.getUnit()).getNameRu());
            aggregationListDTO.setLastModifiedDate(aggregation.getLastModifiedDate());
            aggregationListDTO.setTuronAggregationId(aggregation.getReportId());
            aggregationListDTO.setSerialId(aggregation.getSerialId());
            aggregationListDTO.setGcp(aggregation.getGcp());
            aggregationListDTO.setDescription(aggregation.getDescription());
            if (aggregation.getStatus() != null) {
                aggregationListDTO.setStatus(aggregation.getStatus());
                aggregationListDTO.setStatusName(aggregation.getStatus().getText());
            }
            if (aggregation.getProduct() != null) {
                aggregationListDTO.setProductName(aggregation.getProduct().getVisibleName());
                aggregationListDTO.setBarcode(aggregation.getProduct().getBarcode());
            }
            if (isAgency()) {
                if (aggregation.getOrganization() != null) {
                    aggregationListDTO.setOrganizationName(aggregation.getOrganization().getName());
                }
            }
            if (aggregation.getSerialNum() != null) {
                aggregationListDTO.setSerial(aggregation.getSerialNum().toSelectItem());
            }
            if (aggregation.getAggregationType() != null) {
                aggregationListDTO.setType(aggregation.getAggregationType());
                aggregationListDTO.setTypeName(aggregation.getAggregationType().getText());
            }
            return aggregationListDTO;
        });
    }

    public AggregationDTO findOne(String id) {
        Aggregation aggregation = aggregationRepository.findById(id).orElseThrow(notFound());
        AggregationDTO dto = aggregation.toDto(isAgency());
        if (PackageType.PALLET.equals(dto.getUnit())) {
            List<String> cleanCodes = aggregationRepository.findAllCodeByParentCode(aggregation.getCode());
            dto.setMarks(new HashSet<>(cleanCodes));
        }
        List<Mark> marks = markRepository.findAllByParentCode(aggregation.getCode());
        for (Mark mark : marks) {
            dto.getCleanMarks().add(mark.getCode());
            dto.getMarks().add(mark.getPrintCode());
            if (AggregationStatus.DROPOUT.equals(mark.getStatus())) {
                dto.getDropoutMarks().add(mark.getPrintCode());
            }
        }
        return dto;
    }

    public AggregationStatsDTO getStats(AggregationFilter filter) {
        if (StringUtils.isNotEmpty(filter.getSearch())) {
            final String code = Utils.clearPrintCode(filter.getSearch());
            if (Utils.isMark(filter.getSearch()) || Utils.alcoholMatcherWithoutCX(filter.getSearch())) {
                markRepository.findFirstByCode(code).ifPresent(value -> filter.setSearch(value.getParentCode()));
            } else {
                filter.setSearch(code);
            }
        }
        return aggregationRepository.getStats(filter);
    }

    @Transactional
    public void sendAggregation(final String code, final boolean isQueue) {
        final Optional<Aggregation> aggregationOptional = aggregationRepository.findFirstByCode(code);
        if (aggregationOptional.isEmpty() || !EnumSet.of(AggregationStatus.ERROR, AggregationStatus.UPDATED, AggregationStatus.IN_PROGRESS).contains(aggregationOptional.get().getStatus())) {
            return;
        }
        final Aggregation aggregation = aggregationOptional.get();

        try {
            final Organization organization = aggregation.getOrganization();
            final ProductGroup productGroup = organization.getProductGroup();
            final boolean isAlcohol = productGroup == ProductGroup.Alcohol;
            final boolean isPharma = productGroup == ProductGroup.Pharma;
            log.debug("SendAggregation {} ================= STATUS {} =============================", aggregation.getCode(), aggregation.getStatus());

            if (isAlcohol) {
                billingPermissionService.checkIsAvailableCount(organization.getId(), organization.isDemoAllowed(), organization.getParentId(), productGroup, 1);
            } else if (isPharma) {
                if (organization.getTypes().contains(OrganizationType.Importer)) {
                    billingPermissionService.checkIsAvailableCount(organization.getId(), organization.isDemoAllowed(), organization.getParentId(), productGroup, aggregation.getQuantity());
                }
            }

            final List<Mark> marks = markRepository.findAllByParentCode(aggregation.getCode());
            Set<String> snTins = marks.stream().map(Mark::getCode).collect(Collectors.toSet());
            if (PackageType.PALLET.equals(aggregationOptional.get().getPackageType())) {
                snTins.addAll(aggregationRepository.findAllCodeByParentCode(code));
            }
            if (CollectionUtils.isEmpty(snTins)) {
                throw new BadRequestException("Коды маркировки отсутствуют");
            } else if (snTins.size() != aggregation.getQuantity()) {
                throw new BadRequestException("Количество не соответствует");
            }
            final AggregationItem aggregationItem = new AggregationItem();
            aggregationItem.setAggregatedItemsCount(aggregation.getQuantity());
            aggregationItem.setAggregationUnitCapacity(aggregation.getQuantity());
            aggregationItem.setAggregationType(AggregationType.AGGREGATION);
            aggregationItem.setUnitSerialNumber(aggregation.getCode());
            aggregationItem.setSntins(snTins);
            final CreateAggregationRequest request = new CreateAggregationRequest(Collections.singletonList(aggregationItem), organization.getTin());
            if (ProductGroup.Tobacco.equals(organization.getProductGroup())) {
                request.setProductionLineId(ProductGroup.Tobacco.getNameRu());
            }
            final CreateAggregationResponse response = turonClient.createAggregation(request, organization.getProductGroup().getExtension(), organization.getTuronToken(), organization.getOmsId());
            if (response == null) {
                throw badRequest("Timeout").get();
            } else {
                aggregation.setReportId(response.getReportId());
                aggregation.setAggregationDate(LocalDateTime.now());
                aggregation.setStatus(AggregationStatus.CREATED);
                aggregationRepository.saveAndFlush(aggregation);
                if (isAlcohol) {
                    billingPermissionService.decrementAggregationCountAndSave(organization.getId(), organization.isDemoAllowed(), productGroup, 1);
                } else if (isPharma && AggregationStatus.CREATED == aggregation.getStatus()) {
                    if (organization.getTypes().contains(OrganizationType.Importer)) {
                        billingPermissionService.decrementAggregationCountAndSave(organization.getId(), organization.isDemoAllowed(), productGroup, aggregation.getQuantity());
                    }
                }
                // add aggregation to warehouse production this includes aggregation history also
                final WarehousePayload payload = new WarehousePayload();
                payload.setOrganizationId(organization.getId());
                payload.setOperationDate(LocalDateTime.now());
                payload.setOperation(WarehouseOperation.Production);
                payload.setNote("Произведено " + aggregation.getPrintCode());
                payload.setUnit(PackageType.fromCode(aggregation.getUnit()));
                payload.setAlcoholCount(BigDecimal.valueOf(aggregation.getQuantity().longValue()));
                payload.setProductId(aggregation.getProductId());
                payload.setSerialId(aggregation.getSerialId());
                payload.setCode(aggregation.getCode());
                payload.setAggregationStatus(aggregation.getStatus());
                kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC, payload);
                redisAggregationRepository.findFirstByCode(aggregation.getCode())
                    .ifPresent(redisAggregationRepository::delete);
            }
        } catch (Exception e) {
            log.error("Error sending aggregation", e);
            aggregation.setDescription(e.getMessage());
            aggregation.setStatus(AggregationStatus.ERROR);
            aggregationRepository.saveAndFlush(aggregation);
            if (!isQueue) {
                throw new BadRequestException(e.getMessage());
            }
        }
        if (AggregationType.PRINTING != aggregation.getAggregationType()) return;
        // notify current aggregation state
        rabbitMqProducer.sendAggregationStatusChangedEvent(aggregation.toPayload());
    }

    public Aggregation generateAggregation(final PackageType packageType, final String organizationId, final String gcp) {
        final Aggregation aggregation = new Aggregation();
        aggregation.setOrganizationId(organizationId);
        aggregation.setSsccCode("(00)");
        aggregation.setUnit(packageType.getCode());
        aggregation.setCompanyPrefix(gcp);
        aggregation.setGcp(gcp);
        int maxIntNumber = Math.toIntExact(aggregationRepository.nextVal("org_seq_" + gcp));
        aggregation.setIntNumber(maxIntNumber);

        aggregation.setSerialNumber(Utils.generateSerialNumber(maxIntNumber));
        final String tempNumber = StringUtils.join(aggregation.getUnit(), aggregation.getCompanyPrefix(), aggregation.getSerialNumber());
        aggregation.setCode(StringUtils.join(tempNumber, Utils.generateControlNumber(tempNumber)));
        aggregation.setPrintCode(StringUtils.join(aggregation.getSsccCode(), aggregation.getCode()));

        return aggregation;
    }

    @Transactional
    public AggregationStatus resendAggregation(String id) {
        Aggregation aggregation = aggregationRepository.findById(id).orElseThrow(notFound());
        sendAggregation(aggregation.getCode(), false);
        return aggregation.getStatus();
    }

    public AggregationDTO findAggregationWithMarks(String id) {
        return aggregationRepository.findById(id).map(aggregation -> {
            AggregationDTO aggregationDTO = aggregation.toDto(isAgency());
            Set<MarkDTO> marks = markRepository.findAllByParentCode(aggregation.getCode()).stream().map(Mark::toDto).collect(Collectors.toSet());
            aggregationDTO.getMarkDTOs().addAll(marks);
            return aggregationDTO;
        }).orElseThrow(notFound());
    }

    @Transactional
    public void setAggregationStatus(String id, AggregationStatus status) {
        Aggregation aggregation = aggregationRepository.findById(id).orElseThrow(notFound("Агрегация не найден"));

        if (!Arrays.asList(AggregationStatus.CREATED, AggregationStatus.IN_PROGRESS).contains(aggregation.getStatus())) {
            throw badRequest("Агрегация в статусе '" + aggregation.getStatus().getText() + "'").get();
        } else {
            aggregation.setStatus(status);
            aggregationRepository.save(aggregation);
            aggregationHistoryRepository.save(AggregationHistory.fromAggregation(aggregation));
        }
    }

    public void checkAndResend(final String organizationId, final EnumSet<AggregationStatus> statuses, final LocalDateTime dateTime) {
        final Organization organization = organizationRepository.getReferenceById(organizationId);
        if (StringUtils.isEmpty(organization.getTrueToken())) {
            return;
        }
        final List<Aggregation> aggregations = aggregationRepository.findAllByOrganizationIdAndStatusInAndAggregationDateAfter(organizationId, statuses, dateTime);
        final Set<String> codes = aggregations.stream().map(Aggregation::getCode).collect(Collectors.toSet());
        try {
            final CisInfoResponse[] cisInfoResponses = trueApi.cisesInfo(organization.getTin(), organization.getTrueToken(), codes);
            for (CisInfoResponse cisInfoResponse : cisInfoResponses) {
                if (StringUtils.isAllEmpty(cisInfoResponse.getErrorCode(), cisInfoResponse.getErrorMessage())) {
                    final CisInfo cisInfo = cisInfoResponse.getCisInfo();
                    codes.remove(cisInfo.getCis());
                }
            }
        } catch (Exception e) {
            log.error("Check aggregations", e);
        }
        if (CollectionUtils.isNotEmpty(codes)) {
            codes.forEach(id -> rabbitMqProducer.resendAggregationToTuron(id, 0));
        }
    }

    @Transactional
    public void checkAndUtilise(String code) {
        Aggregation aggregation = aggregationRepository.findFirstByCode(code).orElseThrow(notFound());
        checkAndUtilise(aggregation);
    }

    @Transactional
    public void checkAndUtilise(Aggregation aggregation) {
        if (aggregation.getOrganization() == null) {
            log.error("Aggregation organization is null");
            return;
        }
        final Organization organization = aggregation.getOrganization();
        final ProductGroup productGroup = organization.getProductGroup();
        if (productGroup.sendToApplyMarks()) {
            try {
                // checking all marks is APPLIED and return back not APPLIED marks set
                Set<String> notAppliedMarks = checkMarksIsUtilised(aggregation);
                if (CollectionUtils.isNotEmpty(notAppliedMarks)) {
                    UtilisationResponseDTO utilisationResponseDTO;
                    if (ProductGroup.Pharma == productGroup) {
                        utilisationResponseDTO = utilisationService.create(notAppliedMarks, organization, aggregation.getProductionDate(), aggregation.getExpirationDate(), aggregation.getSerialNum().getSerialNumber());
                    } else {
                        utilisationResponseDTO = utilisationService.create(notAppliedMarks, organization, aggregation.getCreatedDate());
                    }
                    if (utilisationResponseDTO != null && utilisationResponseDTO.getReportId() != null) {
                        aggregation.setUtilisationId(utilisationResponseDTO.getReportId());
                        aggregation.setUtilisationTime(LocalDateTime.now());
                        aggregation.setDescription("");
                        aggregationRepository.save(aggregation);
                        rabbitMqProducer.resendAggregationToTuron(aggregation.getCode(), 300000);
                    }
                } else {
                    rabbitMqProducer.resendAggregationToTuron(aggregation.getCode(), 0);
                }

            } catch (Exception e) {
                log.error("Error check aggregations", e);
                aggregation.setUtilisationTime(LocalDateTime.now());
                aggregation.setDescription(e.getMessage());
                aggregation.setStatus(AggregationStatus.ERROR);
                aggregationRepository.save(aggregation);
            }
        } else {
            rabbitMqProducer.resendAggregationToTuron(aggregation.getCode(), 0);
        }
    }

    private Set<String> checkMarksIsUtilised(Aggregation aggregation) {
        final Organization organization = aggregation.getOrganization();
        if (organization == null) {
            log.error("Aggregation organization is null");
            return Collections.emptySet();
        }

        if (aggregation.getProduct() == null) {
            log.error("Aggregation product is null");
            return Collections.emptySet();
        }

        List<Mark> allByParentCode = markRepository.findAllByParentCodeAndTuronStatusIsNot(aggregation.getCode(), MarkStatus.APPLIED);
        if (CollectionUtils.isEmpty(allByParentCode)) {
            log.error("Aggregation marks is empty");
            return Collections.emptySet();
        }
        log.info("Aggregation code {} children marks {}", aggregation.getCode(), allByParentCode.size());
        Set<String> utiliseCodes = new HashSet<>();

        Map<String, Mark> codes = new HashMap<>();
        for (Mark mark : allByParentCode) {
            codes.putIfAbsent(mark.getCode(), mark);
        }

        CisInfoResponse[] cisInfoResponses = trueApi.cisesInfo(organization.getTin(), organization.getTrueToken(), codes.keySet());
        for (CisInfoResponse cisInfoResponse : cisInfoResponses) {
            if (codes.containsKey(cisInfoResponse.getCisInfo().getCis())) {
                codes.get(cisInfoResponse.getCisInfo().getCis());
                Mark mark = codes.get(cisInfoResponse.getCisInfo().getCis());
                mark.setTuronStatus(cisInfoResponse.getCisInfo().getStatus());
                codes.put(mark.getCode(), mark);
                if (MarkStatus.EMITTED.equals(cisInfoResponse.getCisInfo().getStatus())) {
                    utiliseCodes.add(mark.getPrintCode());
                }
            }
        }
        markRepository.saveAll(codes.values());
        return utiliseCodes;
    }

    @Transactional
    public Map<String, String> batchSending(List<String> aggregationIds) {
        List<Aggregation> aggregationList = aggregationRepository.findAllById(aggregationIds);
        Map<String, String> responseMap = new HashMap<>();
        EnumSet<AggregationStatus> statusEnumSet = EnumSet.of(AggregationStatus.ERROR, AggregationStatus.IN_PROGRESS);
        for (Aggregation aggregation : aggregationList) {
            if (!statusEnumSet.contains(aggregation.getStatus())) {
                responseMap.put(aggregation.getCode(), aggregation.getCode() + " статус в " + aggregation.getStatus().getText());
            } else {
                try {
                    sendAggregation(aggregation.getCode(), false);
                } catch (Exception e) {
                    log.error("Error batch sending aggregation", e);
                    responseMap.put(aggregation.getCode(), e.getMessage());
                }
            }
        }
        return responseMap;
    }

    public void aggregateByReportId(Set<String> reportIdSet) {
        List<Aggregation> aggregations = aggregationRepository.findAllByUtilisationIdIn(reportIdSet);
        for (Aggregation aggregation : aggregations) {
            ReportInfo reportInfo = trueApi.getUtilisedMarks(aggregation.getOrganization().getTrueToken(), aggregation.getUtilisationId());
            if (reportInfo == null) {
                log.error("Report info is null");
                continue;
            }
            if (CollectionUtils.isEmpty(reportInfo.getJson().getSntins())) {
                log.error("Report info json is empty {}", reportInfo.getJson());
                continue;
            }
            Set<String> clearMark = reportInfo.getJson().getSntins().stream().map(Utils::clearPrintCode).collect(Collectors.toSet());
            for (Mark mark : markRepository.findAllByCodeIn(clearMark)) {
                if (StringUtils.isEmpty(mark.getParentCode())) {
                    mark.setParentCode(aggregation.getCode());
                    mark.setUsed(true);
                    markRepository.save(mark);
                }
            }
            rabbitMqProducer.resendAggregationToTuron(aggregation.getCode(), 0);
        }
    }

    public List<String> print(AggregationFilter filter) {
        return aggregationRepository.findAllByFilter(filter)
            .map(Aggregation::getPrintCode)
            .getContent();
    }

    public void testKafkaProducer(WarehousePayload payload) {
        kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC, payload);
    }
}
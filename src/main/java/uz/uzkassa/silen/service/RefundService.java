package uz.uzkassa.silen.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.uzkassa.silen.kafka.KafkaConstants;
import uz.uzkassa.silen.domain.ShipmentItem;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.domain.mongo.ShipmentMark;
import uz.uzkassa.silen.dto.mq.WarehousePayload;
import uz.uzkassa.silen.dto.warehouse.ShipmentCode;
import uz.uzkassa.silen.enumeration.AggregationStatus;
import uz.uzkassa.silen.enumeration.PackageType;
import uz.uzkassa.silen.enumeration.WarehouseOperation;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.kafka.producer.KafkaProducer;
import uz.uzkassa.silen.repository.AggregationRepository;
import uz.uzkassa.silen.repository.ShipmentItemRepository;
import uz.uzkassa.silen.repository.mongo.MarkRepository;
import uz.uzkassa.silen.repository.mongo.ShipmentMarkRepository;
import uz.uzkassa.silen.utils.Utils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class RefundService extends BaseService {
    MarkRepository markRepository;
    AggregationRepository aggregationRepository;
    ShipmentMarkRepository shipmentMarkRepository;
    ShipmentItemRepository shipmentItemRepository;
    KafkaProducer<WarehousePayload> kafkaProducer;

    @Transactional
    public String save(String printCode) {
        if (!Utils.validatePrintCode(printCode)) {
            throw new BadRequestException("Код не соответствует стандартному формату", printCode);
        }

        final String code = Utils.clearPrintCode(printCode);
        ShipmentMark shipmentMark = shipmentMarkRepository.findFirstByCode(code).orElseThrow(() -> new BadRequestException("Данный код не отгружен"));
        ShipmentItem shipmentItem = shipmentItemRepository.findById(shipmentMark.getShipmentItemId()).orElseThrow(() -> new BadRequestException("Отгрузга ещё не завершён"));

        //check code is not utilized or not
        if (Utils.isMark(printCode)) {//            if (Utils.tobaccoBlockMatcher(printCode)) {
//                refundByBlock(printCode);
//            } else {
            refundByMark(code, shipmentItem, shipmentMark);
//            }
        } else {
            refundByAggregation(code, shipmentItem, shipmentMark);
        }
        return code;
    }

    @Transactional
    public void refundByAggregation(String code, ShipmentItem shipmentItem, ShipmentMark shipmentMark) {
        if (shipmentMark.getUnit().equals(PackageType.PALLET.getCode())) {
            aggregationRepository.findFirstByCode(code)
                .ifPresent(aggregationPallet -> {
                    // create returned history
                    aggregationPallet.setStatus(AggregationStatus.RETURNED);
                    aggregationRepository.save(aggregationPallet);
                    kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC, WarehousePayload.returnPallet(aggregationPallet));

                    // getting pallets child marks
                    List<Mark> boxes = markRepository.findAllByParentCode(code);
                    boxes.forEach(mark -> aggregationRepository.findFirstByCode(mark.getCode())
                        .ifPresent(aggregationBox -> {
                            // create returned history
                            aggregationBox.setStatus(AggregationStatus.RETURNED);
                            aggregationRepository.save(aggregationBox);
                            kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC,WarehousePayload.returnBox(aggregationBox));
                        }));
                });
            //remove from shipmentItem
            for (ShipmentCode shipmentCode : shipmentItem.getTypes()) {
                if (shipmentCode.getUnit() == PackageType.fromCode(shipmentMark.getUnit())) {
                    shipmentCode.setCount(shipmentCode.getCount() - 1);
                    shipmentItem.setQty(shipmentItem.getQty().subtract(BigDecimal.valueOf(shipmentCode.getCapacity())));
                }
            }
            shipmentItemRepository.save(shipmentItem);
            shipmentMarkRepository.deleteAllByCode(shipmentMark.getCode());
        }
        if (shipmentMark.getUnit().equals(PackageType.BOX.getCode())) {
            aggregationRepository.findFirstByCode(code)
                .ifPresent(aggregation -> {
                    // create returned history
                    aggregation.setStatus(AggregationStatus.RETURNED);
                    aggregationRepository.save(aggregation);
                    kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC,WarehousePayload.returnBox(aggregation));
                });
            //remove from shipmentItem
            for (ShipmentCode shipmentCode : shipmentItem.getTypes()) {
                if (shipmentCode.getUnit() == PackageType.fromCode(shipmentMark.getUnit())) {
                    shipmentCode.setCount(shipmentCode.getCount() - 1);
                    shipmentItem.setQty(shipmentItem.getQty().subtract(BigDecimal.valueOf(shipmentCode.getCapacity())));
                }
            }
            shipmentItemRepository.save(shipmentItem);
            shipmentMarkRepository.deleteByCode(shipmentMark.getCode());
        }
    }

    @Transactional
    public void refundByMark(String code, ShipmentItem shipmentItem, ShipmentMark shipmentMark) {
        Optional<Mark> mark = markRepository.findFirstByCode(code);
        if (mark.isPresent()) {
            if (mark.get().getParentCode() != null) {
                throw new BadRequestException("КМ состоит в агрегации. Для возврата отсканируйте его код агрегации " + mark.get().getParentCode());
            }
            mark.get().setUsed(false);
            mark.get().setStatus(AggregationStatus.RETURNED);
            markRepository.save(mark.get());

            // add to warehouse like returned
            WarehousePayload payload = new WarehousePayload();
            payload.setOrganizationId(shipmentItem.getShipment().getOrganizationId());
            payload.setOperationDate(LocalDateTime.now());
            payload.setOperation(WarehouseOperation.Returned);
            payload.setNote("Возвращен");
            payload.setUnit(PackageType.BOTTLE);
            payload.setAlcoholCount(BigDecimal.ONE);
            payload.setProductId(shipmentItem.getProductId());
            payload.setCode(code);
            kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC, payload);
        }

        // remove from shipmentItem
        for (ShipmentCode shipmentCode : shipmentItem.getTypes()) {
            if (shipmentCode.getUnit() == null) {
                shipmentCode.setCount(shipmentCode.getCount() - 1);
                shipmentItem.setQty(shipmentItem.getQty().subtract(BigDecimal.valueOf(shipmentCode.getCapacity())));
                log.debug("ShipmentItem {} successfully changed to: {} qty: {}", shipmentItem.getId(), shipmentCode.getCount(), shipmentItem.getQty());
            }
        }
        shipmentItemRepository.save(shipmentItem);
        // remove from shipMark
        shipmentMarkRepository.delete(shipmentMark);
    }
}

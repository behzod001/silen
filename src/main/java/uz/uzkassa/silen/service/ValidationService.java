package uz.uzkassa.silen.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.uzkassa.silen.domain.Organization;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.domain.redis.RedisLastAggregationMarks;
import uz.uzkassa.silen.dto.EnumDTO;
import uz.uzkassa.silen.dto.marking.CodeDTO;
import uz.uzkassa.silen.dto.marking.ValidationDTO;
import uz.uzkassa.silen.dto.redis.RedisLastAggregationMarksDTO;
import uz.uzkassa.silen.enumeration.AggregationStatus;
import uz.uzkassa.silen.enumeration.CodeType;
import uz.uzkassa.silen.enumeration.FileType;
import uz.uzkassa.silen.enumeration.PackageType;
import uz.uzkassa.silen.excel.ReadMarksCsv;
import uz.uzkassa.silen.excel.ReadMarksExcel;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.exceptions.ErrorConstants;
import uz.uzkassa.silen.integration.TrueApi;
import uz.uzkassa.silen.integration.asilbelgi.CisInfo;
import uz.uzkassa.silen.integration.billing.dto.CisInfoResponse;
import uz.uzkassa.silen.repository.AggregationRepository;
import uz.uzkassa.silen.repository.ProductRepository;
import uz.uzkassa.silen.repository.UserRepository;
import uz.uzkassa.silen.repository.mongo.MarkRepository;
import uz.uzkassa.silen.repository.redis.RedisBlockedPartyRepository;
import uz.uzkassa.silen.repository.redis.RedisLastAggregationMarksRepository;
import uz.uzkassa.silen.security.SecurityUtils;
import uz.uzkassa.silen.utils.DateUtils;
import uz.uzkassa.silen.utils.Utils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ValidationService extends BaseService {
    private final RedisLastAggregationMarksRepository redisLastAggregationMarksRepository;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;
    private final TrueApi trueApi;
    private final MarkRepository markRepository;
    private final AggregationRepository aggregationRepository;
    private final ReadMarksExcel readMarksExcel;
    private final ReadMarksCsv readMarksCsv;
    private final RedisBlockedPartyRepository redisBlockedPartyRepository;

    public String validateBlock(ValidationDTO dto) {
        final boolean isBlock = Utils.tobaccoBlockMatcher(dto.getCode());
        final boolean isPacks = Utils.tobaccoMatcher(dto.getCode());

        if (!isPacks && !isBlock) {
            throw new BadRequestException("Код не можете аггрегировать.");
        }
        if (Utils.checkIsEqualBarcodes(dto.getBarcode(), dto.getCode()))
            throw new BadRequestException("Ошибка! Несоответствие штрихкода:\nKM: " + dto.getCode() + "\n");

        if (dto.isTrueCheck()) {
            if (StringUtils.isEmpty(getCurrentOrganization().getTrueToken())) {
                throw badRequest("Отсутствует токен организации").get();
            }
        }

        if (!dto.isChild() && isBlock) {
            final String code = Utils.clearTobaccoBlock(dto.getCode());
            markRepository.findFirstByCode(code).ifPresent(mark -> {
                if (!AggregationStatus.DRAFT.equals(mark.getStatus())) {
                    throw new BadRequestException("Код аггрегации уже был использован", ErrorConstants.AGGREGATION_USED_ERROR_CODE, dto.getCode());
                }
            });
        } else {
            if (isBlock) {
                throw new BadRequestException("Код не можете аггрегировать внутри блок.");
            }
            final String code = Utils.clearTobaccoPack(dto.getCode());
            markRepository.findFirstByCode(code).ifPresent(mark -> {
                if (mark.getParentCode() != null) {
                    throw new BadRequestException("Ошибка! Код состоит в агрегации. КА: " + mark.getParentCode());
                } else if (mark.getStatus().checkingIfMarkNotAbleToAggregate()) {
                    throw new BadRequestException("Код не можете агрегировать. \nКод статус '" + mark.getStatus().getText() + "'");
                }
            });
            if (dto.isTrueCheck()) {
                return checkChildFromTrueApi(code);
            }
        }
        return dto.getCode();
    }

    /**
     * @param cleanCode cleared from {@link Utils#clearPrintCode(String)}
     * @return String
     */
    private String checkChildFromTrueApi(String cleanCode) {
        final Organization currentOrganization = getCurrentOrganization();
        if (StringUtils.isEmpty(currentOrganization.getTrueToken())) {
            throw badRequest("Отсутствует токен организации").get();
        }

        final CisInfoResponse[] result = trueApi.cisesInfo(currentOrganization.getTin(), currentOrganization.getTrueToken(), Collections.singleton(cleanCode));
        if (result != null && result.length > 0) {
            final CisInfo cisInfo = result[0].getCisInfo();
            if (cisInfo != null && cisInfo.getStatus() != null) {
                if (currentOrganization.getProductGroup().checkIsCanAggregateCodeByStatus(cisInfo.getStatus())) {
                    return cleanCode;
                } else {
                    final String errorMessage = cisInfo.getStatus() != null ? "Ошибка агрегации! Статус Код : " + cisInfo.getStatus().getText() : "Ошибка агрегации! Статус Код : NULL";
//                    botApi.sendMessageToAdminChannel("INN: " + currentOrganization.getTin() + " \n" + "CODE: " + cleanCode + " \n" + errorMessage);
                    throw badRequest(errorMessage).get();
                }
            } else {
                throw badRequest("Код маркировки отсутствует в системе Турона!").get();
            }
        } else {
            throw badRequest("Код маркировки отсутствует в системе Турона!").get();
        }
    }

    public String validate(final ValidationDTO dto) {
        final String code = Utils.clearPrintCode(dto.getCode());
        final RedisLastAggregationMarks redisMark = redisLastAggregationMarksRepository.findById(code).orElse(null);
        if (redisMark != null) {
            final String userLogin = Optional.ofNullable(redisMark.getUserLogin()).orElse("другим оператором");
            throw new BadRequestException(StringUtils.join("Код уже сканирован со стороны ", userLogin, " в ", DateUtils.format(redisMark.getScanTime())), dto.getCode());
        }

            if (!dto.isChild()) {
                if (!Utils.validateCode(dto.getCode(), dto.getUnit())) {
                    throw new BadRequestException("Код не соответствует стандартному формату", dto.getCode());
                }
                if (CodeType.AGGREGATION.equals(dto.getUnit().getCodeType())) {
                    aggregationRepository.findFirstByCode(code).map(aggregation -> {
                        if (aggregation.getStatus() != AggregationStatus.DRAFT) {
                            throw new BadRequestException("Код агрегации уже был использован", ErrorConstants.AGGREGATION_USED_ERROR_CODE, dto.getCode());
                        }
                        if (!StringUtils.equals(aggregation.getOrganizationId(), getCurrentOrganizationId())) {
                            throw badRequest("KA не принадлежит этой организации").get();
                        }
                        return aggregation;
                    }).orElseThrow(badRequest("Код агрегации отсутствует в системе: " + dto.getCode()));
                } else {
                    markRepository.findFirstByCode(code).map(block -> {
                        if (!block.isBlock()) {
                            throw new BadRequestException("Сканирован код маркировок", ErrorConstants.AGGREGATION_USED_ERROR_CODE, dto.getCode());
                        }
                        if (block.getStatus() != AggregationStatus.DRAFT) {
                            throw new BadRequestException("Код блок уже был использован", ErrorConstants.AGGREGATION_USED_ERROR_CODE, dto.getCode());
                        }
                        if (!StringUtils.equals(block.getOrganizationId(), getCurrentOrganizationId())) {
                            throw badRequest("KA не принадлежит этой организации").get();
                        }
                        return block;
                    }).orElseThrow(badRequest("Код блок отсутствует в системе: " + dto.getCode()));
                }
            } else {
                if (PackageType.BOX.equals(dto.getUnit())) {
                    // check only markv & block
                    if (!Utils.validateCode(dto.getCode(), null) && !Utils.validateCode(dto.getCode(), PackageType.BLOCK)) {
                        throw new BadRequestException("Код не соответствует стандартному формату", dto.getCode());
                    }

                    //throw when children is not mark || block
                    Mark childMark = markRepository.findFirstByCode(code).orElseThrow(badRequest("Код отсуствует в базе данных!"));

                    if (StringUtils.isNotEmpty(childMark.getParentCode())) {
                        throw badRequest("Ошибка! Код состоит в агрегации. КА: " + childMark.getParentCode()).get();
                    }
                    if (childMark.isUsed() || childMark.getStatus().checkingIfMarkNotAbleToAggregate(childMark.isBlock())) {
                        throw badRequest("Код не можете агрегировать. \nКод статус '" + childMark.getStatus().getText() + "'").get();
                    }
                    if (childMark.getPartyId() != null && redisBlockedPartyRepository.existsById(childMark.getPartyId())) {
                        throw badRequest("Ошибка! Это партия блокировано в целях агрегации").get();
                    }
                }
                if (PackageType.PALLET.equals(dto.getUnit())) {
                    // check only aggregation box
                    if (!Utils.validateCode(dto.getCode(), PackageType.BOX)) {
                        throw new BadRequestException("Код не соответствует стандартному формату", dto.getCode());
                    }
                    //throw when children is not aggregation box
                    aggregationRepository.findFirstByCode(code)
                            .map(aggregation -> {
                                if (dto.getUnit().equals(aggregation.getPackageType())) {
                                    throw badRequest("Не можете агрегировать код").get();
                                }
                                if (aggregation.getStatus().checkingIfAggregationNotAbleToAggregate()) {
                                    throw badRequest("Код не можете аггрегировать. \nКод статус '" + aggregation.getStatus().getText() + "'").get();
                                }
                                if (!Objects.equals(aggregation.getProduct().getBarcode(), dto.getBarcode())) {
                                    throw badRequest("Ошибка! Несоответствие штрихкода: \nKA:" + dto.getCode() + " \nПродукция: " + aggregation.getProduct().getName()).get();
                                }
                                if (!StringUtils.equals(aggregation.getOrganizationId(), getCurrentOrganizationId())) {
                                    throw badRequest("KA не принадлежит этой организации").get();
                                }
                                return aggregation;
                            }).orElseThrow(badRequest("КА отсуствует в базе данных!"));
                }
                if (PackageType.BLOCK.equals(dto.getUnit())) {
                    // check only mark
                    if (!Utils.validateCode(dto.getCode(), PackageType.BOTTLE)) {
                        throw new BadRequestException("Код не соответствует стандартному формату", dto.getCode());
                    }
                    if (!Utils.checkIsEqualBarcodes(dto.getCode(), dto.getBarcode())) {
                        throw new BadRequestException("Код не соответствует стандартному формату", dto.getCode());
                    }

                    //throw when children is not mark
                    markRepository.findFirstByCode(code).ifPresent(mark -> {
                        if (StringUtils.isNotEmpty(mark.getParentCode())) {
                            throw badRequest("Ошибка! Код состоит в агрегации. КА: " + mark.getParentCode()).get();
                        }
                        if (mark.isUsed() || mark.getStatus().checkingIfMarkNotAbleToAggregate(mark.isBlock())) {
                            throw badRequest("Код не можете агрегировать. \nКод статус '" + mark.getStatus().getText() + "'").get();
                        }
                        if (mark.getPartyId() != null && redisBlockedPartyRepository.existsById(mark.getPartyId())) {
                            throw badRequest("Ошибка! Это партия блокировано в целях агрегации").get();
                        }
                    });
                }

                if (dto.isTrueCheck()) {
                    checkChildFromTrueApi(code);
                }
            }
            RedisLastAggregationMarks validCode = RedisLastAggregationMarks.builder()
                .id(code)
                .productId(dto.getProductId())
                .userId(getCurrentUserId())
                .sessionId(getCurrentSessionId())
                .printCode(dto.getCode())
                .scanTime(LocalDateTime.now())
                .userLogin(getCurrentUserLogin())
                .unit(Optional.ofNullable(dto.getUnit()).map(PackageType::getCode).orElse(null))
                .child(dto.isChild())
                .build();
            log.debug("Valid code redis info {}", validCode);
        redisLastAggregationMarksRepository.save(validCode);
        return dto.getCode();
    }

    public RedisLastAggregationMarksDTO checkCode(final CodeDTO dto) {
        if (StringUtils.isNotEmpty(dto.getCode())) {
            final String code = Utils.clearPrintCode(dto.getCode());
            final RedisLastAggregationMarks mark = redisLastAggregationMarksRepository.findById(code)
                .orElseThrow(badRequest("Код не был сканирован"));
            final RedisLastAggregationMarksDTO result = RedisLastAggregationMarksDTO.builder()
                .code(mark.getId())
                .printCode(mark.getPrintCode())
                .sessionId(mark.getSessionId())
                .scanTime(mark.getScanTime())
//                .unit(PackageType.fromCode(mark.getUnit()))
                .build();
            if (StringUtils.isNotBlank(mark.getProductId())) {
                result.setProductName(productRepository.getReferenceById(mark.getProductId()).getName());
            }
            if (StringUtils.isNotBlank(mark.getUserId())) {
                result.setUserName(userRepository.getReferenceById(mark.getUserId()).getName());
            }
            return result;
        }
        return null;
    }

    public void clearCode(final CodeDTO dto) {
        if (StringUtils.isNotEmpty(dto.getCode())) {
            final String code = Utils.clearPrintCode(dto.getCode());
            redisLastAggregationMarksRepository.findById(code)
                .ifPresent(redisLastAggregationMarksRepository::delete);
        } else if (StringUtils.isNotEmpty(dto.getProductId())) {
            List<RedisLastAggregationMarks> lastAggregationMarks = redisLastAggregationMarksRepository.findAllBySessionIdAndProductId(SecurityUtils.getCurrentSessionId(), dto.getProductId());
            lastAggregationMarks.forEach(redisLastAggregationMarks -> {
                redisLastAggregationMarksRepository.deleteById(redisLastAggregationMarks.getId());
            });
        } else throw badRequest("Код не был сканирован").get();
    }

    public void clearAll() {
        redisLastAggregationMarksRepository.deleteAll();
    }

    public Map<String, String> readFromFileAndValidate(final FileType fileType, final MultipartFile multipartFile) {
        final Map<String, EnumDTO> codes = new HashMap<>();
        try {
            if (FileType.EXCEL.equals(fileType)) {
                codes.putAll(readMarksExcel.startRead(multipartFile));
            } else {
                codes.putAll(readMarksCsv.startRead(multipartFile));
            }
        } catch (IOException e) {
            log.error("Something went wrong ", e);
            throw badRequest("Файл не может быть прочитан").get();
        }
        if (MapUtils.isEmpty(codes)) {
            throw badRequest("Коды отсутствуют в файле").get();
        }
        final Organization organization = getCurrentOrganization();
        final Set<String> clearCodes = codes.keySet();
        final Set<String> turonCodes = new HashSet<>();

        try {
            final CisInfoResponse[] cisInfoResponses = trueApi.cisesInfo(organization.getTin(), organization.getTrueToken(), codes.keySet());
            for (CisInfoResponse cisInfoResponse : cisInfoResponses) {
                turonCodes.add(cisInfoResponse.getCisInfo().getCis());
            }
        } catch (Exception e) {
            log.error("Something went wrong ", e);
            throw badRequest("Ошибка при проверке КМ: " + e.getMessage()).get();
        }

        if (CollectionUtils.isEmpty(turonCodes)) {
            throw badRequest("Коды отсуствуют в Туроне").get();
        }
        final List<Mark> codesInDb = markRepository.findAllByCodeIn(clearCodes);
        final Map<String, Mark> usedMap = codesInDb.stream().collect(Collectors.toMap(Mark::getCode, Function.identity()));

        for (String code : clearCodes) {
            if (!turonCodes.contains(code)) {
                codes.get(code).setName("Ошибка! Код отсуствует в Туроне: " + code);
            }
            if (usedMap.containsKey(code)) {
                final Mark mark = usedMap.get(code);
                if (mark.getParentCode() != null) {
                    codes.get(code).setName("Ошибка! Код состоит в агрегации. КА: " + mark.getParentCode());
                } else if (mark.isUsed()) {
                    codes.get(code).setName("Ошибка! Код использован: " + code);
                } else if (mark.getStatus().checkingIfMarkNotAbleToAggregate(mark.isBlock())) {
                    codes.get(code).setName("Ошибка! Код не можете агрегировать. \nКод статус '" + mark.getStatus().getText() + "'");
                } else if (mark.getPartyId() != null && redisBlockedPartyRepository.existsById(mark.getPartyId())) {
                    codes.get(code).setName("Ошибка! Это партия блокировано в целях агрегации");
                }
            }
        }
        return codes.values().stream().collect(Collectors.toMap(EnumDTO::getCode, EnumDTO::getName));
    }

    public Set<String> getCodes(final String productId, final PackageType unit, final boolean child) {
        final List<RedisLastAggregationMarks> lastAggregationMarks = redisLastAggregationMarksRepository.findAllBySessionIdAndProductId(SecurityUtils.getCurrentSessionId(), productId);
        if (CollectionUtils.isNotEmpty(lastAggregationMarks)) {
            return lastAggregationMarks.stream()
                .filter(code -> code.isChild() == child && code.getUnit() == unit.getCode())
                .map(RedisLastAggregationMarks::getPrintCode)
                .collect(Collectors.toSet());
        } else {
            return Collections.emptySet();
        }
    }

    public List<String> getSessionCodes(final String productId, final Long sessionId) {
        final List<RedisLastAggregationMarks> marks = redisLastAggregationMarksRepository.findAllBySessionIdAndProductId(sessionId, productId);
        if (CollectionUtils.isNotEmpty(marks)) {
            return marks.stream()
                .map(RedisLastAggregationMarks::getPrintCode)
                .collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }
}

package uz.uzkassa.silen.service;

import com.google.common.collect.Lists;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Hibernate;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.uzkassa.silen.domain.Aggregation;
import uz.uzkassa.silen.domain.Organization;
import uz.uzkassa.silen.domain.PartyAggregation;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.domain.redis.RedisBlockedParty;
import uz.uzkassa.silen.dto.PrintAggregationDTO;
import uz.uzkassa.silen.dto.PrintAggregationMarkDTO;
import uz.uzkassa.silen.dto.SelectItem;
import uz.uzkassa.silen.dto.filter.PartyAggregationFilter;
import uz.uzkassa.silen.dto.marking.AggregationCreateDTO;
import uz.uzkassa.silen.dto.mq.PartyAggregationPayload;
import uz.uzkassa.silen.dto.partyaggregation.AggregationStateMessage;
import uz.uzkassa.silen.dto.partyaggregation.AggregationStatusChangedPayload;
import uz.uzkassa.silen.dto.partyaggregation.PartyAggregationDTO;
import uz.uzkassa.silen.enumeration.*;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.exceptions.ErrorConstants;
import uz.uzkassa.silen.integration.TrueApi;
import uz.uzkassa.silen.repository.AggregationRepository;
import uz.uzkassa.silen.repository.PartyAggregationRepository;
import uz.uzkassa.silen.repository.mongo.MarkRepository;
import uz.uzkassa.silen.repository.redis.RedisBlockedPartyRepository;
import uz.uzkassa.silen.security.SecurityUtils;
import uz.uzkassa.silen.utils.Utils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class PartyAggregationService extends BaseService {
    PartyAggregationRepository partyAggregationRepository;
    MarkRepository markRepository;
    RedisBlockedPartyRepository redisBlockedPartyRepository;
    BillingPermissionService billingPermissionService;
    AggregationService aggregationService;
    AggregationRepository aggregationRepository;
    TrueApi trueApi;
    PushNotificationService pushNotificationService;

    @Transactional
    public String create(PartyAggregationDTO dto) {
        final String organizationId = getCurrentOrganizationId();

        // check and throw if not enough
        Long notUsedMarksCount = checkPartiesNotUsedMarksCount(dto.getParties(), dto.getCapacity());
        Long quantity = notUsedMarksCount / dto.getCapacity();
        PartyAggregation partyAggregation = new PartyAggregation();
        partyAggregation.setOrganizationId(organizationId);
        if (StringUtils.isNotEmpty(dto.getNumber())) {
            if (partyAggregationRepository.existsPartyAggregationByOrganizationIdAndNumberIgnoreCase(organizationId, dto.getNumber())) {
                throw new BadRequestException("Aggregation с таким номером уже существует");
            } else {
                partyAggregation.setNumber(dto.getNumber());
            }
        } else {
            generateNumber(organizationId, partyAggregation, "AGGREGATION-");
        }
        partyAggregation.setParties(dto.getParties());
        partyAggregation.setProductId(dto.getProductId());
        partyAggregation.setStatus(PartyAggregationStatus.NEW);
        partyAggregation.setSerialId(dto.getSerialId());
        partyAggregation.setStoreId(dto.getStoreId());
        partyAggregation.setGcp(dto.getGcp());
        partyAggregation.setDescription(dto.getDescription());
        partyAggregation.setProductionDate(dto.getProductionDate());
        partyAggregation.setExpirationDate(dto.getExpirationDate());
        partyAggregation.setQuantity(quantity.intValue());
        partyAggregation.setCapacity(dto.getCapacity());
        partyAggregation.setOffline(dto.isOffline());
        partyAggregation.setOrderId(dto.getOrderId());
        partyAggregationRepository.save(partyAggregation);

        return partyAggregation.getId();
    }

    public Page<PartyAggregationDTO> findAllByFilter(PartyAggregationFilter filter) {
        return partyAggregationRepository.findAllByFilter(filter).map(PartyAggregation::toDto);
    }

    public List<SelectItem> getItems(PartyAggregationFilter filter) {
        return partyAggregationRepository.findAllByFilter(filter)
            .map(PartyAggregation::toSelectItem)
            .getContent();
    }

    public PartyAggregationDTO getOne(String id) {
        PartyAggregation partyAggregation = partyAggregationRepository.findById(id).orElseThrow(notFound());
        List<AggregationStateMessage> codes = aggregationRepository.findAllByPartyAggregationIdOrderByCreatedDateAsc(id)
            .stream().map(aggregation -> new AggregationStateMessage(aggregation.getPrintCode(), aggregation.getStatus().name(), aggregation.getStatus().getText()))
            .collect(Collectors.toList());

        PartyAggregationDTO dto = partyAggregation.toDto();
        dto.setCodes(codes);
        return dto;
    }

    @Transactional
    public String update(String id, PartyAggregationDTO dto) {
        PartyAggregation partyAggregation = partyAggregationRepository.findById(id).orElseThrow(notFound());
        // check and throw if not enough
        Long notUsedMarksCount = checkPartiesNotUsedMarksCount(dto.getParties(), dto.getCapacity());
        Long quantity = notUsedMarksCount / dto.getCapacity();
        if (StringUtils.isNotEmpty(dto.getNumber()) && !Objects.equals(partyAggregation.getNumber(), dto.getNumber())) {
            if (partyAggregationRepository.existsPartyAggregationByOrganizationIdAndNumberIgnoreCase(partyAggregation.getOrganizationId(), dto.getNumber())) {
                throw new BadRequestException("Aggregation с таким номером уже существует");
            } else {
                partyAggregation.setNumber(dto.getNumber());
            }
        } else {
            generateNumber(partyAggregation.getOrganizationId(), partyAggregation, "AGGREGATION-");
        }
        partyAggregation.setParties(dto.getParties());
        partyAggregation.setProductId(dto.getProductId());
        partyAggregation.setSerialId(dto.getSerialId());
        partyAggregation.setStoreId(dto.getStoreId());
        partyAggregation.setGcp(dto.getGcp());
        partyAggregation.setDescription(dto.getDescription());
        partyAggregation.setProductionDate(dto.getProductionDate());
        partyAggregation.setExpirationDate(dto.getExpirationDate());
        partyAggregation.setQuantity(quantity.intValue());
        partyAggregation.setCapacity(dto.getCapacity());
        partyAggregation.setOrderId(dto.getOrderId());
        partyAggregationRepository.save(partyAggregation);

        return partyAggregation.getId();
    }

    private Long checkPartiesNotUsedMarksCount(Set<String> parties, Integer capacity) {
        // get parties not used marks count
        Long partiesNotUsedMarksCount = markRepository.countAllByPartyIdInAndUsedIsFalse(parties);
        if (partiesNotUsedMarksCount < capacity) {
            throw badRequest("Не достаточно количество не использованных маркировок").get();
        }
        return partiesNotUsedMarksCount;
    }

    public void aggregate(String id) {
        PartyAggregation partyAggregation = partyAggregationRepository.findById(id).orElseThrow(notFound());
        if (PartyAggregationStatus.NEW != partyAggregation.getStatus()) {
            throw badRequest("Операция уже завершена").get();
        }
        // checking tokens for integrations
        final Organization organization = partyAggregation.getOrganization();
        if (StringUtils.isBlank(organization.getTrueToken())) {
            throw badRequest("AslBelgi токен отсутствует").get();
        }
        if (StringUtils.isAnyBlank(organization.getTuronToken(), organization.getOmsId())) {
            throw badRequest("AslBelgi токен отсутствует").get();
        }

        // find party aggregation
        Hibernate.initialize(partyAggregation.getParties());
        if (CollectionUtils.isNotEmpty(partyAggregation.getParties())) {
            String firstParty = partyAggregation.getParties().stream().findFirst().orElseThrow(badRequest("По партию маркировок не найден"));
            final Mark testMark = markRepository.findFirstByPartyId(firstParty).orElseThrow(badRequest("По партию маркировок не найден"));
            final String firstCode = Utils.clearPrintCode(testMark.getPrintCode());
            try {
                trueApi.cisesInfo(organization.getTin(), organization.getTrueToken(), Collections.singleton(firstCode));
            } catch (Exception e) {
                if (e.getMessage().contains("Connection refused")) {
                    throw new BadRequestException(ErrorConstants.ERR_CONNECTION_ERROR, e.getMessage(), ErrorConstants.TURON_NOT_AVAILABLE_ERROR_CODE, null);
                } else {
                    throw badRequest(e.getMessage()).get();
                }
            }
        }
        Long notUsedMarksCount = checkPartiesNotUsedMarksCount(partyAggregation.getParties(), partyAggregation.getCapacity());
        // check for aggregation exists available count
        final ProductGroup productGroup = organization.getProductGroup();
        final boolean isAlcohol = productGroup == ProductGroup.Alcohol;
        final boolean isPharma = productGroup == ProductGroup.Pharma;

        // evaluate aggregation count
        Long quantityAggregation = notUsedMarksCount / partyAggregation.getCapacity();
        if (isAlcohol) {
            billingPermissionService.checkIsAvailableCount(organization.getId(), organization.isDemoAllowed(), organization.getParentId(), organization.getProductGroup(), quantityAggregation.intValue());
        }
        if (isPharma) {
            billingPermissionService.checkIsAvailableCount(organization.getId(), organization.isDemoAllowed(), organization.getParentId(), organization.getProductGroup(), notUsedMarksCount.intValue());
        }

        partyAggregation.setStatus(PartyAggregationStatus.IN_PROGRESS);
        partyAggregation.setQuantity(quantityAggregation.intValue());
        partyAggregationRepository.save(partyAggregation);

        // here create blocked parties in redis
        for (String partyId : partyAggregation.getParties()) {
            redisBlockedPartyRepository.save(new RedisBlockedParty(partyId));
        }

        // send to rabbit for process
        PartyAggregationPayload partyAggregationPayload = new PartyAggregationPayload();
        partyAggregationPayload.setId(id);
        partyAggregationPayload.setGcp(partyAggregation.getGcp());
        partyAggregationPayload.setParties(partyAggregation.getParties());
        partyAggregationPayload.setOrganizationId(organization.getId());
        partyAggregationPayload.setProductGroup(organization.getProductGroup());
        partyAggregationPayload.setCreatedBy(SecurityUtils.getCurrentUserLogin());
        rabbitMqProducer.sendPartyAggregation(partyAggregationPayload);
    }

    private void generateNumber(String organizationId, PartyAggregation shipment, String prefix) {
        Integer intNumber = partyAggregationRepository.getMaxIntNumber(organizationId);
        String number = null;
        if (intNumber == null) {
            intNumber = 1;
        }
        boolean exists = true;
        while (exists) {
            number = prefix + intNumber;
            if (partyAggregationRepository.existsPartyAggregationByOrganizationIdAndNumberIgnoreCase(organizationId, number)) {
                intNumber++;
            } else {
                exists = false;
            }
        }
        shipment.setIntNumber(intNumber);
        shipment.setNumber(number);
    }


    @Transactional
    public void partyAggregate(PartyAggregationPayload payload) {
        final PartyAggregation partyAggregation = partyAggregationRepository.findById(payload.getId()).orElseThrow(notFound("Party not found"));

        List<String> other = new ArrayList<>();
        for (String partyId : payload.getParties()) {
            // not used marks by current partyId
            List<String> markList = markRepository.findAllByPartyIdAndUsedIsFalse(partyId).stream().map(Mark::getPrintCode).collect(Collectors.toList());

            // partitions for aggregation
            List<List<String>> partitions = Lists.partition(markList, partyAggregation.getCapacity());
            for (List<String> partition : partitions) {
                if (partition.size() < partyAggregation.getCapacity()) {
                    other.addAll(partition);
                    continue;
                }
                // collect partition marks print codes
                generateSingleAggregation(partyAggregation, new HashSet<>(partition));
            }
        }
        // from any parties
        if (other.size() >= partyAggregation.getCapacity()) {
            List<List<String>> otherPartitions = Lists.partition(other, partyAggregation.getCapacity());
            for (List<String> partition : otherPartitions) {
                // collect partition marks print codes
                generateSingleAggregation(partyAggregation, new HashSet<>(partition));
            }
        }
        // after success remove party block
        payload.getParties().forEach(redisBlockedPartyRepository::deleteById);
    }

    public void generateSingleAggregation(PartyAggregation partyAggregation, Set<String> marks) {
        AggregationCreateDTO aggregationDTO = new AggregationCreateDTO();
        aggregationDTO.setProductId(partyAggregation.getProductId());
        aggregationDTO.setGcp(partyAggregation.getGcp());
        aggregationDTO.setSerialId(partyAggregation.getSerialId());
        aggregationDTO.setUnit(PackageType.fromCode(partyAggregation.getUnit()));
        aggregationDTO.setQuantity(partyAggregation.getCapacity());
        aggregationDTO.setDescription("");
        aggregationDTO.setMarks(marks);
        aggregationDTO.setOffline(false);
        aggregationDTO.setProductionDate(partyAggregation.getProductionDate());
        aggregationDTO.setExpirationDate(partyAggregation.getExpirationDate());
        aggregationDTO.setType(AggregationType.PRINTING);
        aggregationDTO.setAggregationPartyId(partyAggregation.getId());
        aggregationDTO.setLogin(partyAggregation.getLastModifiedBy());
        aggregationService.create(aggregationDTO, false, partyAggregation.getOrganization());
    }

    public List<PrintAggregationDTO> getForPrinting(String id) {
        final PartyAggregation partyAggregation = partyAggregationRepository.findById(id).orElseThrow(notFound());

        List<Aggregation> aggregations = aggregationRepository.findAllByPartyAggregationIdOrderByCreatedDateAsc(id);
        if (aggregations.size() != partyAggregation.getQuantity())
            throw badRequest("Процесс не завершен").get();
        List<PrintAggregationDTO> responseList = new ArrayList<>();
        for (Aggregation aggregation : aggregations) {
            Set<PrintAggregationMarkDTO> allByParentCode = markRepository.findAllByParentCode(aggregation.getCode())
                .stream()
                .map(mark -> {
                    PrintAggregationMarkDTO printAggregationMarkDTO = new PrintAggregationMarkDTO();
                    printAggregationMarkDTO.setPrintCode(mark.getPrintCode());
                    Matcher matcher = Utils.parseMarkForPharma(mark.getPrintCode());
                    if (matcher.find()) {
                        printAggregationMarkDTO.setSerialNumber(matcher.group(3));
                    }
                    return printAggregationMarkDTO;
                })
                .collect(Collectors.toSet());
            responseList.add(new PrintAggregationDTO(aggregation.getPrintCode(), allByParentCode));
        }
        return responseList;
    }


    public void partyAggregationStatusCheck(AggregationStatusChangedPayload payload) {
        // notify current aggregation state
        pushNotificationService.sendPartyAggregation(payload, payload.getPartyAggregationId());

        Optional<PartyAggregation> findById = partyAggregationRepository.findById(payload.getPartyAggregationId());
        if (findById.isEmpty()) return;
        PartyAggregation partyAggregation = findById.get();

        if (PartyAggregationStatus.IN_PROGRESS != partyAggregation.getStatus()) return;
        Long aggregatedQuantity = aggregationRepository.countAllByPartyAggregationIdAndStatusIn(partyAggregation.getId(), Arrays.asList(AggregationStatus.CREATED, AggregationStatus.ERROR));
        if (partyAggregation.getQuantity() != aggregatedQuantity.intValue()) return;

        partyAggregation.setStatus(PartyAggregationStatus.CLOSED);
        partyAggregationRepository.save(partyAggregation);

        // notify current party aggregation state
        AggregationStatusChangedPayload finishedPayload = new AggregationStatusChangedPayload();
        finishedPayload.setPartyAggregationId(partyAggregation.getId());
        finishedPayload.setStatus(partyAggregation.getStatus().name());
        finishedPayload.setStatusName(partyAggregation.getStatus().getText());
        pushNotificationService.sendPartyAggregation(finishedPayload, partyAggregation.getId());
    }
}

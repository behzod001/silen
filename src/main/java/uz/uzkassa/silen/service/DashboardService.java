package uz.uzkassa.silen.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import uz.uzkassa.silen.domain.Organization;
import uz.uzkassa.silen.domain.Product;
import uz.uzkassa.silen.dto.dashboard.*;
import uz.uzkassa.silen.dto.filter.OrganizationFilter;
import uz.uzkassa.silen.dto.filter.ProductFilter;
import uz.uzkassa.silen.enumeration.ProductType;
import uz.uzkassa.silen.repository.OrganizationRepository;
import uz.uzkassa.silen.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class DashboardService {
    private final OrganizationRepository organizationRepository;
    private final ProductRepository productRepository;

    public List<OrganizationsDTO> getOrganizations() {
        return organizationRepository.findOrganizations();
    }

    public List<StatsV2DTO> getStatsTemp(DashboardFilter filter) {
        Set<ProductType> productTypes = ProductType.byOrganizationTypes(filter.getOrganizationTypes());
        List<StatsV2DTO> result = new ArrayList<>();
        for(ProductType type : productTypes){
            result.add(new StatsV2DTO(type));
        }
        return result;
    }

    public List<OrganizationStatsDTO> getDataTemp(OrganizationFilter filter) {
        Set<ProductType> productTypes = ProductType.byOrganizationTypes(filter.getOrganizationTypes());
        List<StatsV2DTO> stats = new ArrayList<>();
        for(ProductType type : productTypes){
            stats.add(new StatsV2DTO(type));
        }

        List<Organization> organizations = organizationRepository.findAllByFilter(filter).getContent();
        List<OrganizationStatsDTO> result = new ArrayList<>();
        for(Organization organization : organizations){
            result.add(new OrganizationStatsDTO(organization.getId(), organization.getName(), stats));
        }

        return result;
    }
    public List<OrganizationStatsDTO> getDataFromWarehouse(OrganizationFilter filter) {
        Set<ProductType> productTypes = ProductType.byOrganizationTypes(filter.getOrganizationTypes());
        List<StatsV2DTO> stats = new ArrayList<>();
        for(ProductType type : productTypes){
            stats.add(new StatsV2DTO(type));
        }

        List<Organization> organizations = organizationRepository.findAllByFilter(filter).getContent();
        List<OrganizationStatsDTO> result = new ArrayList<>();
        for(Organization organization : organizations){
            result.add(new OrganizationStatsDTO(organization.getId(), organization.getName(), stats));
        }

        return result;
    }

    public List<StatsV2DTO> getProductsTemp(DashboardFilter filter) {
        ProductFilter productFilter = new ProductFilter();
        productFilter.setOrganizationId(filter.getOrganizationId());
        productFilter.setProductType(filter.getProductType());
        List<Product> products = productRepository.findAllByFilter(productFilter).getContent();
        List<StatsV2DTO> result = new ArrayList<>();
        for(Product product : products){
            result.add(new StatsV2DTO(product.getName()));
        }
        return result;
    }

    public List<RiskStatsDTO> getRiskDataTemp(DashboardFilter filter) {
        Set<ProductType> productTypes = ProductType.byOrganizationTypes(filter.getOrganizationTypes());
        List<RiskStatsDTO> result = new ArrayList<>();
        for(ProductType type : productTypes){
            RiskStatsDTO dto = new RiskStatsDTO(type);
            ProductFilter productFilter = new ProductFilter();
            productFilter.setProductType(type);
            List<Product> products = productRepository.findAllByFilter(productFilter).getContent();
            for(Product product : products){
                dto.getData().add(new RiskStatsDTO(product.getName()));
            }
            result.add(dto);
        }
        return result;
    }
}

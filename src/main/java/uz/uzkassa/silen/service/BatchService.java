package uz.uzkassa.silen.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.uzkassa.silen.domain.Aggregation;
import uz.uzkassa.silen.domain.BatchAggregation;
import uz.uzkassa.silen.domain.Organization;
import uz.uzkassa.silen.dto.filter.BatchFilter;
import uz.uzkassa.silen.dto.marking.BatchAggregationDTO;
import uz.uzkassa.silen.dto.marking.BatchAggregationListDTO;
import uz.uzkassa.silen.dto.mq.Payload;
import uz.uzkassa.silen.enumeration.AggregationStatus;
import uz.uzkassa.silen.enumeration.BatchAggregationStatus;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.integration.TrueApi;
import uz.uzkassa.silen.repository.AggregationRepository;
import uz.uzkassa.silen.repository.BatchAggregationRepository;
import uz.uzkassa.silen.utils.Utils;

import java.util.*;

/**
 * Service Implementation for managing {@link BatchAggregation}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class BatchService extends BaseService {
    private final BatchAggregationRepository batchAggregationRepository;
    private final AggregationRepository aggregationRepository;
    private final TrueApi trueApi;

    @Transactional
    public void save(BatchAggregationDTO batchAggregationDTO) {
        final String organizationId = getCurrentOrganizationId();
        if (StringUtils.isEmpty(batchAggregationDTO.getGcp())) {
            throw badRequest("Отсуствует код организации").get();
        }

        BatchAggregation batchAggregation = new BatchAggregation();
        batchAggregation.setOrganizationId(organizationId);
        batchAggregation.setQuantity(batchAggregationDTO.getQuantity());
        batchAggregation.setUnit(batchAggregationDTO.getUnit().getCode());
        batchAggregation.setDescription(batchAggregationDTO.getDescription());
        batchAggregation.setGcp(batchAggregationDTO.getGcp());
        batchAggregation.setStatus(BatchAggregationStatus.PENDING);

        Integer maxIntNumber = batchAggregationRepository.getMaxIntNumber(organizationId);
        maxIntNumber = maxIntNumber == null ? 1 : maxIntNumber + 1;
        batchAggregation.setIntNumber(maxIntNumber);
        batchAggregation.setNumber("BATCH-" + maxIntNumber);

        batchAggregationRepository.save(batchAggregation);
        rabbitMqProducer.sendBatchAggregation(new Payload(batchAggregation.getId()));
    }

    public Page<BatchAggregationListDTO> findAll(BatchFilter filter) {
        return batchAggregationRepository.findAllByFilter(filter)
            .map(BatchAggregationListDTO::new);
    }

    public BatchAggregationListDTO findOne(String id) {
        return batchAggregationRepository.findById(id).map(BatchAggregation::toDto).orElseThrow(notFound());
    }

    @Transactional
    public void batchAggregation(final Payload payload) {
        BatchAggregation batchAggregation = batchAggregationRepository.findById(payload.getId())
            .orElseThrow(notFound("Batch Aggregation not found " + payload.getId()));
        final Organization organization = batchAggregation.getOrganization();
        final String organizationGcp = batchAggregation.getGcp();
        final Integer quantity = batchAggregation.getQuantity();

        int i = 0;
        while (i < quantity) {
            try {
                int maxIntNumber = Math.toIntExact(aggregationRepository.nextVal("org_seq_" + organizationGcp));
                Aggregation aggregation = new Aggregation();
                aggregation.setOrganizationId(organization.getId());
                aggregation.setBatchId(batchAggregation.getId());
                aggregation.setLogin(batchAggregation.getCreatedBy());
                aggregation.setSsccCode("(00)");
                aggregation.setUnit(batchAggregation.getUnit());
                aggregation.setCompanyPrefix(organizationGcp);
                aggregation.setIntNumber(maxIntNumber);
                aggregation.setGcp(batchAggregation.getGcp());
                aggregation.setSerialNumber(Utils.generateSerialNumber(maxIntNumber));

                String tempNumber = StringUtils.join(aggregation.getUnit(), aggregation.getCompanyPrefix(), aggregation.getSerialNumber());

                aggregation.setCode(StringUtils.join(tempNumber, Utils.generateControlNumber(tempNumber)));
                aggregation.setPrintCode(StringUtils.join(aggregation.getSsccCode(), aggregation.getCode()));
                try {
                    final String code = aggregation.getCode();
                    Map<String, Map<String, List>> result = trueApi.aggregatedList(organizationGcp, organization.getTrueToken(), organization.getProductGroup(), Collections.singleton(code), null);
                    if (result != null && !result.keySet().isEmpty() && CollectionUtils.isNotEmpty(result.get(code).keySet())) {
                        continue;
                    }
                } catch (BadRequestException e) {
                    log.error("Checking generated KA exists in TURON", e);
                }
                aggregation.setStatus(AggregationStatus.DRAFT);
                aggregationRepository.save(aggregation);
                i++;
            } catch (Exception e) {
                i--;
            }
        }
        batchAggregation.setStatus(BatchAggregationStatus.COMPLETED);
    }

    @Transactional
    public void batchAggregationRecursion(final Payload payload) {
        BatchAggregation batchAggregation = batchAggregationRepository.findById(payload.getId()).orElseThrow(notFound("Batch Aggregation not found " + payload.getId()));
        final Organization organization = batchAggregation.getOrganization();
        final String organizationGcp = batchAggregation.getGcp();
        final int quantity = batchAggregation.getQuantity() - Optional.ofNullable(payload.getLongId()).orElse(0L).intValue();
        Map<String, Aggregation> aggregationCodes = new HashMap<>();
        int generated = 0;
        while (generated < quantity) {
            int maxIntNumber = Math.toIntExact(aggregationRepository.nextVal("org_seq_" + organizationGcp));
            Aggregation aggregation = new Aggregation();
            aggregation.setOrganizationId(organization.getId());
            aggregation.setBatchId(batchAggregation.getId());
            aggregation.setLogin(batchAggregation.getCreatedBy());
            aggregation.setSsccCode("(00)");
            aggregation.setUnit(batchAggregation.getUnit());
            aggregation.setCompanyPrefix(organizationGcp);
            aggregation.setIntNumber(maxIntNumber);
            aggregation.setGcp(batchAggregation.getGcp());
            aggregation.setSerialNumber(Utils.generateSerialNumber(maxIntNumber));
            aggregation.setStatus(AggregationStatus.DRAFT);
            String tempNumber = StringUtils.join(aggregation.getUnit(), aggregation.getCompanyPrefix(), aggregation.getSerialNumber());

            aggregation.setCode(StringUtils.join(tempNumber, Utils.generateControlNumber(tempNumber)));
            aggregation.setPrintCode(StringUtils.join(aggregation.getSsccCode(), aggregation.getCode()));
            aggregationCodes.put(aggregation.getCode(), aggregation);
            generated++;
        }

        try {
            final Set<String> codes = aggregationCodes.keySet();
            Map<String, Map<String, List>> result = trueApi.aggregatedList(organizationGcp, organization.getTrueToken(), organization.getProductGroup(), codes, null);

            if (result == null || result.keySet().isEmpty()) {
                log.info("Batch aggregation saved {}", aggregationCodes.size());
                aggregationRepository.saveAll(aggregationCodes.values());
            } else {
                long turonExists = 0L;
                for (String turonCode : result.keySet()) {
                    if (aggregationCodes.containsKey(turonCode)) {
                        aggregationCodes.remove(turonCode);
                        turonExists++;
                    }
                }
                aggregationRepository.saveAll(aggregationCodes.values());
                if (turonExists > 0) {
                    payload.setLongId(generated - turonExists);
                    batchAggregationRecursion(payload);
                    log.info("Batch aggregation recreate {}", aggregationCodes.size());
                }
            }
        } catch (BadRequestException e) {
            if (e.getMessage().equals("Ошибка от TrueApi!\nКМ не найдены")) {
                aggregationRepository.saveAll(aggregationCodes.values());
            } else {
                log.error("Checking generated KA exists in TURON", e);
                batchAggregation.setErrorMessage(e.getMessage());
                batchAggregationRepository.save(batchAggregation);
            }
        }
        batchAggregation.setStatus(BatchAggregationStatus.COMPLETED);
    }

    public Long getStats(BatchFilter batchFilter) {
        return batchAggregationRepository.getStats(batchFilter);
    }
}

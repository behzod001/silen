package uz.uzkassa.silen.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.uzkassa.silen.domain.ExportReport;
import uz.uzkassa.silen.enumeration.ReportStatus;
import uz.uzkassa.silen.dto.filter.AggregationFilter;
import uz.uzkassa.silen.dto.filter.MongoFilter;
import uz.uzkassa.silen.dto.filter.ReportFilter;
import uz.uzkassa.silen.dto.ExportReportDTO;
import uz.uzkassa.silen.dto.mq.ExportReportPayload;
import uz.uzkassa.silen.enumeration.ExportReportType;
import uz.uzkassa.silen.enumeration.FileType;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.handler.CsvAggregationReportExportHandler;
import uz.uzkassa.silen.handler.CsvMarkExportHandler;
import uz.uzkassa.silen.handler.ExcelAggregationReportExportHandler;
import uz.uzkassa.silen.handler.ExcelMarkExportHandler;
import uz.uzkassa.silen.repository.ExportReportRepository;
import uz.uzkassa.silen.security.SecurityUtils;
import uz.uzkassa.silen.transaction.AfterCommitExecutor;

import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 8/29/2023 11:13
 */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Slf4j
@RequiredArgsConstructor
public class ExportReportService extends BaseService {
    AfterCommitExecutor afterCommitExecutor;
    // for generating csv files
    CsvMarkExportHandler csvMarkHandler;
    CsvAggregationReportExportHandler csvAggregationReportHandler;
    // for generating excel files
    ExcelAggregationReportExportHandler excelAggregationReportHandler;
    ExcelMarkExportHandler excelMarkHandler;

    ExportReportUploadService exportReportUploadService;
    ExportReportRepository exportReportRepository;

    @Transactional
    public void export(final Object filter, final ExportReportType exportReportType, final FileType fileType, final String attachmentName) {
        final String organizationId = SecurityUtils.getCurrentOrganizationId();
        exportReportRepository.findFirstByOrganizationIdAndStatusOrderByCreatedDateDesc(organizationId, ReportStatus.PENDING)
            .ifPresent(exportReport -> {
                if (exportReport.getCreatedDate().isAfter(LocalDateTime.now().minusMinutes(10))) {
                    throw badRequest("Действие выполнить невозможн. Ожидайте генерации предыдущего файла.").get();
                } else {
                    exportReport.setStatus(ReportStatus.ERROR);
                    exportReportRepository.save(exportReport);
                }
            });

        ExportReport exportReport = new ExportReport();
        exportReport.setType(exportReportType);
        exportReport.setFileType(fileType);
        exportReport.setName(attachmentName + fileType.getExtension());
        exportReport.setOrganizationId(organizationId);
        exportReportRepository.save(exportReport);
        final ExportReportPayload payload = new ExportReportPayload(filter, exportReport.getId());
        afterCommitExecutor.execute(() -> rabbitMqProducer.sendExportReport(payload));
    }

    @Transactional
    public void startGenerate(final ExportReportPayload payload) {
        final Optional<ExportReport> exportReportOptional = exportReportRepository.findById(payload.getExportReportId());
        if (exportReportOptional.isEmpty()) return;
        final ExportReport exportReport = exportReportOptional.get();

        try (final ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            switch (exportReport.getType()) {
                case AGGREGATION_BY_PRODUCT -> {
                    final AggregationFilter filter = objectMapper.convertValue(payload.getFilter(), AggregationFilter.class);
                    if (FileType.EXCEL == exportReport.getFileType()) {
                        excelAggregationReportHandler.writeToStream(filter, outputStream);
                    } else {
                        csvAggregationReportHandler.writeToStream(filter, outputStream);
                    }
                }
                case MARK_KM -> {
                    final MongoFilter mongoFilter = objectMapper.convertValue(payload.getFilter(), MongoFilter.class);
                    if (FileType.EXCEL == exportReport.getFileType()) {
                        excelMarkHandler.writeToStream(mongoFilter, outputStream);
                    } else {
                        csvMarkHandler.writeToStream(mongoFilter, outputStream);
                    }
                }
            }

            final CustomMultipartFile multipartFile = new CustomMultipartFile();
            multipartFile.setContentType(exportReport.getFileType().getContentType());
            multipartFile.setOriginalName(exportReport.getName());

            multipartFile.setBytes(outputStream.toByteArray());

            final String fileUploadedPath = exportReportUploadService.fileUpload(multipartFile, exportReport.getName(), exportReport.getId().toString());
            if (fileUploadedPath == null) {
                throw new BadRequestException("An unaccepted error has occurred while uploading file");
            }
            exportReport.setPath(fileUploadedPath);
            exportReport.setStatus(ReportStatus.COMPLETED);
            exportReport.setExpireDate(LocalDateTime.now().plusDays(3));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            exportReport.setStatus(ReportStatus.ERROR);
            exportReport.setErrorMessage(e.getMessage());
        }
        exportReportRepository.save(exportReport);
    }

    public Page<ExportReportDTO> getList(final ReportFilter filter) {
        return exportReportRepository.findAllByFilter(filter)
            .map(ExportReport::toDTO);
    }
}

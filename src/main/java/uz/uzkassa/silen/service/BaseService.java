package uz.uzkassa.silen.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.config.ApplicationProperties;
import uz.uzkassa.silen.domain.Organization;
import uz.uzkassa.silen.domain.Settings;
import uz.uzkassa.silen.dto.eimzo.CertificateDto;
import uz.uzkassa.silen.dto.eimzo.SignatureData;
import uz.uzkassa.silen.dto.eimzo.SignerDto;
import uz.uzkassa.silen.dto.invoice.FacturaDTO;
import uz.uzkassa.silen.dto.invoice.FacturaRejectDTO;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.rabbitmq.producer.RabbitMqProducer;
import uz.uzkassa.silen.repository.OrganizationRepository;
import uz.uzkassa.silen.repository.SettingsRepository;
import uz.uzkassa.silen.security.SecurityUtils;
import uz.uzkassa.silen.transaction.AfterCommitExecutor;
import uz.uzkassa.silen.utils.DateUtils;

import jakarta.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.Base64;
import java.util.Locale;
import java.util.function.Supplier;

@Slf4j
@Component
public abstract class BaseService {
    @Autowired
    @Lazy
    private MessageSource messageSource;
    @Autowired
    @Lazy
    protected AfterCommitExecutor afterCommitExecutor;
    @Autowired
    @Lazy
    protected RabbitMqProducer rabbitMqProducer;
    @Autowired
    @Lazy
    protected ApplicationProperties applicationProperties;
    @Autowired
    @Lazy
    protected ObjectMapper objectMapper;
    @Autowired
    @Lazy
    protected OrganizationRepository organizationRepository;
    @Autowired
    @Lazy
    protected SettingsRepository settingsRepository;
    @Autowired
    @Lazy
    protected CacheService cacheService;

    protected static String getCurrentUserId() {
        return SecurityUtils.getCurrentUserId();
    }

    protected static String getCurrentUserLogin() {
        return SecurityUtils.getCurrentUserLogin();
    }

    protected static String getCurrentOrganizationId() {
        return SecurityUtils.getCurrentOrganizationId();
    }

    protected static String getCurrentOrganizationTin() {
        return SecurityUtils.getCurrentOrganizationTin();
    }

    protected Organization getCurrentOrganization() {
        return organizationRepository.findById(getCurrentOrganizationId()).orElseThrow(notFound("Организация не найдена"));
    }

    protected String getAppVersion() {
        return settingsRepository.findFirstByOrderById().map(Settings::getAppVersion).orElse(null);
    }

    protected Long getCurrentSessionId() {
        return SecurityUtils.getCurrentSessionId();
    }

    protected Organization getCurrentOrganizationAndValidateOmsIdAndTuronToken() {
        final Organization organization = getCurrentOrganization();
        if (StringUtils.isAnyEmpty(organization.getOmsId(), organization.getTuronToken())) {
            throw badRequest("В данной организации отсутствует OMS_ID или TOKEN").get();
        }
        return organization;
    }

    protected static boolean isAgency() {
        return getCurrentOrganizationId() == null;
    }

    protected Supplier<EntityNotFoundException> notFound() {
        return EntityNotFoundException::new;
    }

    protected Supplier<EntityNotFoundException> notFound(String message) {
        return () -> new EntityNotFoundException(message);
    }

    protected Supplier<BadRequestException> badRequest(String error) {
        return () -> new BadRequestException(error);
    }

    protected String localize(String code) {
        return this.localize(LocaleContextHolder.getLocale(), code, null);
    }

    protected String localize(String code, Object[] params) {
        return localize(LocaleContextHolder.getLocale(), code, params);
    }

    protected String localize(Locale locale, String code, Object[] params) {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        return messageSource.getMessage(code, params, locale);
    }


    protected FacturaDTO parseBase64(final String base64){
        final String json = new String(Base64.getDecoder().decode(base64));
        FacturaDTO result = null;
        try {
            result = objectMapper.readValue(json, FacturaDTO.class);
        } catch (IOException e) {
            log.error(e.getMessage());
            try {
                FacturaRejectDTO facturaRejectDTO = objectMapper.readValue(json, FacturaRejectDTO.class);
                result = facturaRejectDTO.getFactura();
                result.setNotes(facturaRejectDTO.getNotes());
            } catch (IOException ex) {
                log.info("JSON: {}", json);
                log.error(ex.getMessage());
            }
        }
        return result;
    }

    protected SignatureData parseSignatureData(final SignerDto signerDto) {
        final CertificateDto certificateDto = signerDto.getCertificate().get(0);
        final SignatureData data = new SignatureData();
        data.setNumber(String.valueOf(Integer.parseInt(certificateDto.getSerialNumber(), 16)));
        data.setOwner(certificateDto.getSubjectInfo().getFullName());
        data.setDateTime(DateUtils.parse(signerDto.getTimeStampInfo().getTime()));
        return data;
    }
}

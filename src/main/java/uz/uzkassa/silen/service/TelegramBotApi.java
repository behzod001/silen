package uz.uzkassa.silen.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.config.ApplicationProperties;
import uz.uzkassa.silen.dto.TelegramMessageSendDTO;
import uz.uzkassa.silen.kafka.producer.KafkaProducer;

import java.util.Arrays;
import java.util.Collection;

@Component
@Slf4j
public class TelegramBotApi {
    private final ApplicationProperties applicationProperties;
    private final KafkaProducer<TelegramMessageSendDTO> kafkaProducer;
    private static final String TELEGRAM_TOPIC = "TELEGRAM-TOPIC";
    private final Environment environment;
    //    private final TelegramBot logBot;

    public TelegramBotApi(ApplicationProperties applicationProperties, KafkaProducer<TelegramMessageSendDTO> kafkaProducer, Environment environment) {
        this.applicationProperties = applicationProperties;
        this.kafkaProducer = kafkaProducer;
//        clientBot = new TelegramBot(applicationProperties.getClientBotToken());
        this.environment = environment;
    }

    public void sendMessageToAdminChannel(final String message) {
        Collection<String> activeProfiles = Arrays.asList(environment.getActiveProfiles());
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("PROFILE: ").append(activeProfiles).append("\n");
        stringBuilder.append(message).append("\n");
        kafkaProducer.sendMessage(TELEGRAM_TOPIC, TelegramMessageSendDTO.builder().changeId(applicationProperties.getLogChannelId()).message(stringBuilder.toString()).build());
//        logBot.execute(new SendMessage(applicationProperties.getLogChannelId(), message)
//            .parseMode(ParseMode.HTML).disableWebPagePreview(true), new Callback<SendMessage, SendResponse>() {
//            @Override
//            public void onResponse(SendMessage request, SendResponse response) {
//            }
//
//            @Override
//            public void onFailure(SendMessage request, IOException e) {
//                log.info(">>>>>>>>>>>>>>>>>>>>> Message errors: {} <<<<<<<<<<<<<<<<<<<<<<", e.getMessage());
//            }
//        });
    }
}

package uz.uzkassa.silen.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.uzkassa.silen.kafka.KafkaConstants;
import uz.uzkassa.silen.domain.Aggregation;
import uz.uzkassa.silen.domain.AggregationHistory;
import uz.uzkassa.silen.domain.Organization;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.dto.AggregationHistoryDTO;
import uz.uzkassa.silen.dto.MaterialReportDTO;
import uz.uzkassa.silen.dto.MaterialReportFilter;
import uz.uzkassa.silen.dto.filter.AggregationFilter;
import uz.uzkassa.silen.dto.marking.AggregationStatsDTO;
import uz.uzkassa.silen.dto.mq.WarehousePayload;
import uz.uzkassa.silen.enumeration.AggregationStatus;
import uz.uzkassa.silen.enumeration.PackageType;
import uz.uzkassa.silen.enumeration.ProductGroup;
import uz.uzkassa.silen.enumeration.WarehouseOperation;
import uz.uzkassa.silen.integration.TrueApi;
import uz.uzkassa.silen.integration.billing.dto.CisInfoResponse;
import uz.uzkassa.silen.kafka.producer.KafkaProducer;
import uz.uzkassa.silen.repository.AggregationHistoryRepository;
import uz.uzkassa.silen.repository.AggregationRepository;
import uz.uzkassa.silen.repository.ShipmentRepository;
import uz.uzkassa.silen.repository.mongo.MarkRepository;
import uz.uzkassa.silen.repository.mongo.ShipmentMarkRepository;
import uz.uzkassa.silen.repository.redis.RedisShipmentCodeRepository;
import uz.uzkassa.silen.security.SecurityUtils;
import uz.uzkassa.silen.utils.Utils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AggregationHistoryService extends BaseService {
    TrueApi trueApi;
    MarkRepository markRepository;
    ShipmentRepository shipmentRepository;
    AggregationRepository aggregationRepository;
    ShipmentMarkRepository shipmentMarkRepository;
    RedisShipmentCodeRepository shipmentCodeRepository;
    AggregationHistoryRepository aggregationHistoryRepository;
    private final LocalizationService localizationService;
    KafkaProducer<WarehousePayload> kafkaProducer;

    public void create(WarehousePayload payload) {
        if (EnumSet.of(PackageType.BOX, PackageType.PALLET).contains(payload.getPackageType())) {
            aggregationRepository.findFirstByCode(payload.getCode())
                .ifPresent(aggregation -> {
                    AggregationHistory history = new AggregationHistory();
                    history.setStatus(aggregation.getStatus());
                    history.setUnit(aggregation.getUnit());
                    history.setProductId(aggregation.getProductId());
                    history.setQuantity(aggregation.getQuantity());
                    history.setLogin(aggregation.getLogin());
                    history.setPrintCode(aggregation.getPrintCode());
                    history.setCode(aggregation.getCode());
                    history.setTuronAggregationId(aggregation.getReportId());
                    history.setOrganizationId(aggregation.getOrganizationId());
                    history.setSerialId(aggregation.getSerialId());
                    history.setShipmentId(aggregation.getShipmentId());
                    history.setAggregationId(aggregation.getId());
                    aggregationHistoryRepository.save(history);
                });
        } else {
            markRepository.findFirstByCode(payload.getCode())
                    .filter(Mark::isBlock)
                    .ifPresent(block -> {
                        AggregationHistory history = new AggregationHistory();
                        history.setStatus(block.getStatus());
                        history.setUnit(PackageType.BLOCK.getCode());
                        history.setProductId(block.getProductId());
                        history.setQuantity(block.getQuantity());
                        history.setLogin(block.getLogin());
                        history.setPrintCode(block.getPrintCode());
                        history.setCode(block.getCode());
                        history.setTuronAggregationId(block.getReportId());
                        history.setOrganizationId(block.getOrganizationId());
                        history.setAggregationId(block.getId());
                        aggregationHistoryRepository.save(history);
                    });
        }
    }
    public Page<AggregationHistoryDTO> findAll(AggregationFilter filter) {
        filterSearchByCode(filter);
        return aggregationHistoryRepository.findAllByFilter(filter).map(aggregationHistory -> aggregationHistory.toDto(isAgency()));
    }

    public AggregationStatsDTO getStats(AggregationFilter filter) {
        filterSearchByCode(filter);
        return aggregationHistoryRepository.getStats(filter);
    }

    public CisInfoResponse[] checkMarkFromTrueApi(Set<String> printCode) {
        Organization organization = getCurrentOrganization();
        final Set<String> codes = new LinkedHashSet<>();
        if (ProductGroup.Tobacco.equals(organization.getProductGroup())) {
            codes.addAll(printCode.stream()
                .map(code -> {
                    if (Utils.isAggregation(code)) {
                        StringBuilder cleared = new StringBuilder("00");
                        cleared.append(cleared);
                        return cleared.toString();
                    }
                    return Utils.clearPrintCode(code);
                }).collect(Collectors.toSet()));
        } else {
            codes.addAll(printCode.stream().map(Utils::clearPrintCode).collect(Collectors.toSet()));
        }
        return trueApi.cisesInfo(organization.getTin(), organization.getTrueToken(), codes);
    }

    /**
     * <table border="2">
     *   <tr >
     *     <th>Event</th>
     *     <th>From</th>
     *     <th>PackageType</th>
     *     <th>STOCK</th>
     *     <th>Production</th>
     *     <th>Shipp</th>
     *   </tr>
     *   <tr >
     *     <td rowspan="5">utilize </td>
     *     <td rowspan="5">shipment</td>
     *     <td>Bottle</td>
     *     <td>?</td>
     *     <td>?</td>
     *     <td>?</td>
     *   </tr>
     *   <tr>
     *     <td>Pallet</td>
     *     <td>?</td>
     *     <td>-</td>
     *     <td>-</td>
     *   </tr>
     *   <tr>
     *     <td>Child Box</td>
     *     <td>+</td>
     *     <td>?</td>
     *     <td>-</td>
     *   </tr>
     *   <tr>
     *     <td>Box</td>
     *     <td>?</td>
     *     <td>-</td>
     *     <td>-</td>
     *   </tr>
     *   <tr>
     *     <td>Child Box</td>
     *     <td>?</td>
     *     <td>-</td>
     *     <td>-</td>
     *   </tr>
     *
     *   <tr >
     *     <td rowspan="5">utilize </td>
     *     <td rowspan="5">created</td>
     *     <td>Bottle</td>
     *     <td>?</td>
     *     <td>?</td>
     *     <td>?</td>
     *   </tr>
     *   <tr>
     *     <td>Pallet</td>
     *     <td>-</td>
     *     <td>-</td>
     *     <td>?</td>
     *   </tr>
     *   <tr>
     *     <td>Child Box</td>
     *     <td>?</td>
     *     <td>?</td>
     *     <td>?</td>
     *   </tr>
     *   <tr>
     *     <td>Box</td>
     *     <td>-</td>
     *     <td>-</td>
     *     <td>?</td>
     *   </tr>
     *   <tr>
     *     <td>Child Box</td>
     *     <td>-</td>
     *     <td>-</td>
     *     <td>?</td>
     *   </tr>
     * </table>
     */
    @Transactional
    public HashMap<String, List<String>> utilize(String printCode) {
        final String code = Utils.clearPrintCode(printCode);
        Aggregation aggregation = aggregationRepository.findFirstByCode(code).orElseThrow(notFound("Код аггрегации не найден"));
        if (aggregation.getStatus().checkingIfAggregationNotAbleToUtilize()) {
            final String utilizedMessage = localizationService.getMessage("aggregation.utilized.message", aggregation.getStatus().getText());
            throw badRequest(utilizedMessage).get();
        }
        shipmentMarkRepository.findFirstByCode(code).ifPresent(shipmentMark -> {
            if (aggregation.getShipment() == null) {
                shipmentCodeRepository.findById(shipmentMark.getShipmentItemId()).ifPresent(shipmentItem -> {
                    shipmentRepository.findById(shipmentItem.getShipmentId()).ifPresent(shipment -> {
                        throw badRequest("Действие невомозможо. Код находится в стадии отгрузки! " + shipment.getNumber()).get();
                    });
                });
            }
        });
        HashMap<String, List<String>> resultMap = new HashMap<>();
        resultMap.put("aggregation", new LinkedList<>());
        resultMap.put("mark", new LinkedList<>());
        //check aggregation
        resultMap.get("aggregation").add(code);
        final String utilizedMessage = localizationService.getMessage("utilized.message", SecurityUtils.getCurrentUserLogin(), LocalDateTime.now());
        if (PackageType.PALLET.getCode() == aggregation.getUnit()) {
            List<Mark> childMarks = markRepository.findAllByParentCode(code);
            for (Mark mark : childMarks) {
                resultMap.get("aggregation").add(mark.getPrintCode());
                mark.setParentCode(null);
                mark.setUsed(false);
                mark.setStatus(AggregationStatus.DRAFT);
                markRepository.save(mark);
            }

            List<Aggregation> childBoxes = aggregationRepository.findAllByParentCode(code);
            for (Aggregation child : childBoxes) {
                child.setParentCode(null);
                aggregationRepository.save(child);
            }
            // create aggregation history
            aggregation.setStatus(AggregationStatus.UTILIZED);
            aggregation.setErrorMessage(String.format(utilizedMessage, SecurityUtils.getCurrentUserLogin(), LocalDateTime.now()));
            aggregationRepository.save(aggregation);

            //send to warehouse
            WarehousePayload warehousePayload = new WarehousePayload();
            warehousePayload.setOperation(WarehouseOperation.Utilized);
            warehousePayload.setNote("Утилизировано");
            warehousePayload.setUnit(PackageType.PALLET);
            warehousePayload.setOperationDate(LocalDateTime.now());
            warehousePayload.setOrganizationId(aggregation.getOrganizationId());
            warehousePayload.setProductId(aggregation.getProductId());
            warehousePayload.setCode(aggregation.getCode());
            warehousePayload.setAlcoholCount(BigDecimal.ZERO);
            kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC,warehousePayload);
            return resultMap;
        }
        if (PackageType.BOX.getCode() == aggregation.getUnit()) {

            markRepository.findAllByParentCode(code).forEach(item -> {
                item.setParentCode(null);
                item.setUsed(false);
                item.setStatus(AggregationStatus.DRAFT);
                markRepository.save(item);
                resultMap.get("mark").add(item.getPrintCode());
            });
            // create aggregation history
            aggregation.setStatus(AggregationStatus.UTILIZED);
            aggregation.setDescription(String.format(utilizedMessage, SecurityUtils.getCurrentUserLogin(), LocalDateTime.now()));
            aggregationRepository.save(aggregation);

            WarehousePayload warehousePayload = new WarehousePayload();
            warehousePayload.setOperation(WarehouseOperation.Utilized);
            warehousePayload.setNote("Утилизировано");
            warehousePayload.setUnit(PackageType.BOX);
            warehousePayload.setOperationDate(LocalDateTime.now());
            warehousePayload.setOrganizationId(aggregation.getOrganizationId());
            warehousePayload.setProductId(aggregation.getProductId());
            warehousePayload.setCode(aggregation.getCode());
            warehousePayload.setAlcoholCount(BigDecimal.valueOf(aggregation.getQuantity()));
            kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC,warehousePayload);

            return resultMap;
        }
        return resultMap;
    }

    private void filterSearchByCode(AggregationFilter filter) {
        if (StringUtils.isNotEmpty(filter.getSearch())) {
            final String code = Utils.clearPrintCode(filter.getSearch());
            if (Utils.isMark(filter.getSearch()) || Utils.alcoholMatcherWithoutCX(filter.getSearch())) {
                markRepository.findFirstByCode(code).ifPresent(value -> filter.setSearch(value.getParentCode()));
            } else {
                filter.setSearch(code);
            }
        }
    }

    public Page<MaterialReportDTO> materialReport(MaterialReportFilter filter) {
        Page<MaterialReportDTO> page = aggregationHistoryRepository.materialReport(filter);

        if (filter.getTo() != null) {
            filter.setTo(filter.getFrom());
            filter.setFrom(null);
        }

        Page<MaterialReportDTO> toToday = aggregationHistoryRepository.materialReport(filter);
        return page.map(dto -> {
            toToday.stream()
                .filter(materialReportDTO -> materialReportDTO.getProductId().equals(dto.getProductId()))
                .findFirst()
                .ifPresent(materialReportDTO -> {
                    dto.setBalanceStartBottle(materialReportDTO.getBalanceEndBottle());
                    dto.setBalanceStartBottleSum(materialReportDTO.getBalanceEndBottle().multiply(dto.getPrice()));
                    dto.setBalanceStartBox(materialReportDTO.getBalanceEndBox());
                    dto.setBalanceStartPallet(materialReportDTO.getBalanceEndPallet());
                });
            return dto;
        });
    }
}

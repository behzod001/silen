package uz.uzkassa.silen.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.uzkassa.silen.domain.AggregationHistory;
import uz.uzkassa.silen.domain.Organization;
import uz.uzkassa.silen.domain.Product;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.dto.filter.MongoFilter;
import uz.uzkassa.silen.dto.marking.AggregationCreateDTO;
import uz.uzkassa.silen.dto.marking.BlockUpdateDTO;
import uz.uzkassa.silen.dto.mongo.BlockDTO;
import uz.uzkassa.silen.dto.mongo.MarkDTO;
import uz.uzkassa.silen.dto.mq.WarehousePayload;
import uz.uzkassa.silen.enumeration.*;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.exceptions.ErrorConstants;
import uz.uzkassa.silen.integration.TrueApi;
import uz.uzkassa.silen.integration.TuronClient;
import uz.uzkassa.silen.integration.asilbelgi.CisInfo;
import uz.uzkassa.silen.integration.billing.dto.CisInfoResponse;
import uz.uzkassa.silen.integration.turon.AggregationItem;
import uz.uzkassa.silen.integration.turon.CreateAggregationRequest;
import uz.uzkassa.silen.integration.turon.CreateAggregationResponse;
import uz.uzkassa.silen.integration.turon.UtilisationResponseDTO;
import uz.uzkassa.silen.kafka.KafkaConstants;
import uz.uzkassa.silen.kafka.producer.KafkaProducer;
import uz.uzkassa.silen.repository.AggregationHistoryRepository;
import uz.uzkassa.silen.repository.ProductRepository;
import uz.uzkassa.silen.repository.mongo.MarkRepository;
import uz.uzkassa.silen.utils.Utils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class BlockService extends BaseService {
    TuronClient turonClient;
    MarkRepository markRepository;
    ProductRepository productRepository;
    AggregationHistoryRepository aggregationHistoryRepository;
    BillingPermissionService billingPermissionService;
    KafkaProducer<WarehousePayload> kafkaProducer;
    TrueApi trueApi;
    UtilisationService utilisationService;

    @Transactional
    public String create(AggregationCreateDTO aggregationCreateDTO, final boolean fromUpdate, final Organization organizationOptional) {
        if (CollectionUtils.isEmpty(aggregationCreateDTO.getMarks())) {
            throw badRequest("Коды маркировки отсутствуют.").get();
        } else if (aggregationCreateDTO.getMarks().size() != aggregationCreateDTO.getQuantity()) {
            throw badRequest("Неверное Количество КМ").get();
        }
        if (StringUtils.isEmpty(aggregationCreateDTO.getChildProductId())) {
            throw badRequest("Необходимо указать штрих-код КМ").get();
        }
        if (StringUtils.isEmpty(aggregationCreateDTO.getPrintCode())) {
            throw badRequest("Необходимо указать код КМ").get();
        }

        Set<String> snTins = aggregationCreateDTO.getMarks().stream().map(Utils::clearPrintCode).collect(Collectors.toSet());
        Set<String> usedCodes = markRepository.findAllByCodeIn(snTins)
                .stream()
                .filter(Mark::isUsed)
                .map(Mark::getPrintCode)
                .collect(Collectors.toSet());
        if (CollectionUtils.isNotEmpty(usedCodes)) {
            throw new BadRequestException("Коды уже использованы", ErrorConstants.MARK_USED_ERROR_CODE, usedCodes);
        }

        final Product childProduct = productRepository.findById(aggregationCreateDTO.getChildProductId()).orElseThrow(notFound());
        Set<String> snTinsOther = snTins.stream().filter(snTin -> Utils.checkIsEqualBarcodes(childProduct.getBarcode(), snTin)).collect(Collectors.toSet());
        if (CollectionUtils.isNotEmpty(snTinsOther)) {
            throw new BadRequestException("Ошибка! Несоответствие штрихкода:\nKM: " + aggregationCreateDTO.getPrintCode());
        }
        final Organization organization = Optional.ofNullable(organizationOptional).orElseGet(this::getCurrentOrganizationAndValidateOmsIdAndTuronToken);
        final ProductGroup productGroup = organization.getProductGroup();
        final boolean isAlcohol = productGroup == ProductGroup.Alcohol;
        final boolean isPharma = productGroup == ProductGroup.Pharma;

        if (!fromUpdate && !aggregationCreateDTO.isOffline()) {
            if (isAlcohol) {
                billingPermissionService.checkIsAvailableCount(organization.getId(), organization.isDemoAllowed(), organization.getParentId(), productGroup, 1);
            } else if (isPharma) {
                if (organization.getTypes().contains(OrganizationType.Importer)) {
                    billingPermissionService.checkIsAvailableCount(organization.getId(), organization.isDemoAllowed(), organization.getParentId(), productGroup, aggregationCreateDTO.getQuantity());
                }
            }
        }

        if (StringUtils.isBlank(organization.getTrueToken())) {
            throw badRequest("Турон токен отсутствует").get();
        }
        final String code = Utils.clearPrintCode(aggregationCreateDTO.getPrintCode());
        Mark block = markRepository.findFirstByCode(code).orElseThrow(notFound("Код аггрегации не найден: " + aggregationCreateDTO.getPrintCode()));

        if (!AggregationStatus.DRAFT.equals(block.getStatus())) {
            throw new BadRequestException("Код аггрегации уже использован", ErrorConstants.AGGREGATION_USED_ERROR_CODE);
        }
        if (!aggregationCreateDTO.getProductId().equals(block.getProductId()))
            throw badRequest("Ошибка! Несоответствие штрихкода:\nKM: " + aggregationCreateDTO.getPrintCode()).get();

        final Map<String, CisInfo> turonCodes = new HashMap<>();
        if (!aggregationCreateDTO.isOffline()) {
            try {
                HashSet<String> checkCodes = new HashSet<>(snTins);
                checkCodes.add(aggregationCreateDTO.getPrintCode());
                final CisInfoResponse[] cisInfoResponses = trueApi.cisesInfo(organization.getTin(), organization.getTrueToken(), checkCodes);
                for (CisInfoResponse cisInfoResponse : cisInfoResponses) {
                    final CisInfo cisInfo = cisInfoResponse.getCisInfo();
                    turonCodes.put(cisInfo.getCis(), cisInfo);
                }
            } catch (Exception e) {
                log.error("Aggregation creation error", e);
                if (e.getMessage().contains("Connection refused")) {
                    throw new BadRequestException(ErrorConstants.ERR_CONNECTION_ERROR, e.getMessage(), ErrorConstants.TURON_NOT_AVAILABLE_ERROR_CODE, null);
                } else {
                    throw badRequest(e.getMessage()).get();
                }
            }
        }

        final Set<String> utiliseCodes = new HashSet<>();
        final Set<String> notFoundCodes = new HashSet<>();
        final Map<String, Mark> marks = new HashMap<>();
        List<Mark> children = markRepository.findAllByCodeIn(snTins);
        for (Mark mark : children) {
            mark.setParentCode(code);
            mark.setUsed(true);
            mark.setStatus(AggregationStatus.CREATED);

            if (!aggregationCreateDTO.isOffline()) {
                if (turonCodes.containsKey(mark.getCode())) {
                    final CisInfo cisInfo = turonCodes.get(mark.getCode());
                    mark.setTuronStatus(cisInfo.getStatus());
                    if (MarkStatus.EMITTED == cisInfo.getStatus()) {
                        utiliseCodes.add(mark.getPrintCode());
                    }
                } else {
                    mark.setStatus(AggregationStatus.ERROR);
                    mark.setTuronStatus(MarkStatus.ERROR);
                    notFoundCodes.add(mark.getCode());
                }
            }

            marks.put(mark.getCode(), mark);
        }
        if (CollectionUtils.isNotEmpty(notFoundCodes)) {
            throw new BadRequestException("КМ не найден: " + notFoundCodes, ErrorConstants.AGGREGATION_USED_ERROR_CODE);
        }

        if (CollectionUtils.isNotEmpty(utiliseCodes)) {
            try {
                LocalDateTime currentTime = LocalDateTime.now();
                if (aggregationCreateDTO.getProductionDate() != null) {
                    block.setProductionDate(aggregationCreateDTO.getProductionDate().atStartOfDay()
                            .withHour(currentTime.getHour())
                            .withMinute(currentTime.getMinute())
                            .withSecond(currentTime.getSecond()));
                }

                UtilisationResponseDTO utilisationResponseDTO = utilisationService.create(utiliseCodes, organization, block.getProductionDate());
                if (utilisationResponseDTO != null && utilisationResponseDTO.getReportId() != null) {
                    block.setUtilisationId(utilisationResponseDTO.getReportId());
                    block.setUtilisationTime(LocalDateTime.now());
                    for (String utilizedCode : utiliseCodes) {
                        marks.get(utilizedCode).setTuronStatus(MarkStatus.APPLIED);
                        marks.get(utilizedCode).setUtilisationId(block.getUtilisationId());
                        marks.get(utilizedCode).setUtilisationTime(block.getUtilisationTime());
                    }
                }
            } catch (Exception e) {
                log.error("Error while utilisation", e);
                throw badRequest(e.getMessage()).get();
            }
        }
        markRepository.saveAll(marks.values());

        block.setQuantity(aggregationCreateDTO.getQuantity());
        block.setStatus(AggregationStatus.IN_PROGRESS);
        block.setLogin(getCurrentUserLogin());
        block.setUsed(true);
        markRepository.save(block);

        return fromUpdate ? block.getId() : block.getPrintCode();
    }

    @Transactional
    public String update(String id, BlockUpdateDTO updateDTO) {
        if (CollectionUtils.isEmpty(updateDTO.getChildMarks()))
            throw new BadRequestException("Коды маркировок обязательны");

        if (Utils.checkIsEqualBarcodes(updateDTO.getBarcode(), updateDTO.getAggregation())) {
            throw new BadRequestException("Ошибка! Несоответствие штрихкода:\nKА: " + updateDTO.getAggregation());
        }
        final Organization organization = getCurrentOrganizationAndValidateOmsIdAndTuronToken();
        final ProductGroup productGroup = organization.getProductGroup();
        final boolean isAlcohol = productGroup == ProductGroup.Alcohol;
        final boolean isPharma = productGroup == ProductGroup.Pharma;

        if (!updateDTO.isOffline()) {
            if (isAlcohol) {
                billingPermissionService.checkIsAvailableCount(organization.getId(), organization.isDemoAllowed(), organization.getParentId(), productGroup, 1);
            } else if (isPharma) {
                if (organization.getTypes().contains(OrganizationType.Importer)) {
                    billingPermissionService.checkIsAvailableCount(organization.getId(), organization.isDemoAllowed(), organization.getParentId(), productGroup, updateDTO.getChildMarks().size());
                }
            }
        }
        if (Utils.checkIsEqualBarcodes(updateDTO.getChildBarcode(), updateDTO.getBarcode())) {
            throw new BadRequestException("Ошибка! Несоответствие штрихкода:\nKM: " + updateDTO.getBarcode());
        }
        List<String> unFormat = updateDTO.getChildMarks()
            .stream()
            .filter(mark -> Utils.checkIsEqualBarcodes(updateDTO.getChildBarcode(), mark))
            .collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(unFormat)) {
            throw new BadRequestException("Ошибка! Несоответствие штрихкода:\nKM: " + String.join(",", unFormat));
        }

        Mark oldAggregation = markRepository.findById(id).orElseThrow(notFound("Код не найден"));
        markRepository.findFirstByCode(Utils.clearPrintCode(updateDTO.getAggregation())).orElseThrow(notFound("Код не найден"));
        if (!Arrays.asList(AggregationStatus.UPDATED, AggregationStatus.CREATED).contains(oldAggregation.getStatus()))
            throw new BadRequestException("Код не агрегирован");
        markRepository.findAllByParentCode(oldAggregation.getCode()).forEach(oldMarkItem -> {
            oldMarkItem.setUsed(false);
            oldMarkItem.setParentCode(null);
            oldMarkItem.setStatus(AggregationStatus.DRAFT);
            markRepository.save(oldMarkItem);
        });
        AggregationCreateDTO aggregationDTO = new AggregationCreateDTO();
        aggregationDTO.setProductId(oldAggregation.getProductId());
        aggregationDTO.setUnit(oldAggregation.getUnit());
        aggregationDTO.setQuantity(updateDTO.getChildMarks().size());
        aggregationDTO.setDescription(updateDTO.getDescription());
        aggregationDTO.setPrintCode(updateDTO.getAggregation());
        aggregationDTO.setDescription(updateDTO.getAggregation());
        aggregationDTO.setMarks(updateDTO.getChildMarks());
        aggregationDTO.setOffline(updateDTO.isOffline());
        aggregationDTO.setProductionDate(updateDTO.getProductionDate());
        aggregationDTO.setExpirationDate(updateDTO.getExpirationDate());
        aggregationDTO.setType(updateDTO.getType());

        String response = create(aggregationDTO, true, organization);

        String utilizedMessage = String.format("Агрегация утилизировано от оператора %s в час %s. Новый код агрегация %s", getCurrentUserLogin(), LocalDateTime.now(), oldAggregation.getCode());
        oldAggregation.setStatus(AggregationStatus.UTILIZED);
        oldAggregation.setDescription(utilizedMessage);
        oldAggregation.setAggregationDate(LocalDateTime.now());
        oldAggregation.setQuantity(0);
        oldAggregation.setLogin(getCurrentUserLogin());
        markRepository.save(oldAggregation);

        WarehousePayload warehousePayload = new WarehousePayload();
        warehousePayload.setOrganizationId(getCurrentOrganizationId());
        warehousePayload.setOperationDate(LocalDateTime.now());
        warehousePayload.setOperation(WarehouseOperation.Utilized);
        warehousePayload.setNote(utilizedMessage);
        warehousePayload.setUnit(PackageType.BLOCK);
        warehousePayload.setAlcoholCount(BigDecimal.valueOf(oldAggregation.getQuantity()));
        warehousePayload.setProductId(oldAggregation.getProductId());
        warehousePayload.setCode(oldAggregation.getCode());
        kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC, warehousePayload);

        return response;
    }


    public Page<BlockDTO> findAllAggregationBlock(MongoFilter filter) {
        if (StringUtils.isNotEmpty(filter.getSearch())) {
            final String code = Utils.clearPrintCode(filter.getSearch());
            if (Utils.isMark(filter.getSearch())) {
                markRepository.findFirstByCode(code).ifPresent(value -> filter.setSearch(value.getParentCode()));
            } else {
                filter.setSearch(code);
            }
        }
        filter.setOrderBy("aggregationDate");
        return markRepository.findAllByFilter(filter).map(Mark::toListBlockDto);
    }

    public BigDecimal findAllAggregationBlockStats(MongoFilter filter) {
        if (StringUtils.isNotEmpty(filter.getSearch())) {
            final String code = Utils.clearPrintCode(filter.getSearch());
            if (Utils.isMark(filter.getSearch())) {
                markRepository.findFirstByCode(code).ifPresent(value -> filter.setSearch(value.getParentCode()));
            } else {
                filter.setSearch(code);
            }
        }
        return markRepository.findAllByFilterStats(filter);
    }

    public BlockDTO findBlockWithMarks(String id) {
        return markRepository.findById(id).map(aggregation -> {
            BlockDTO dto = aggregation.toBlockDto();

            List<MarkDTO> marks = markRepository.findAllByParentCode(aggregation.getCode()).stream().map(Mark::toDto).collect(Collectors.toList());
            dto.setMarks(marks.stream().map(MarkDTO::getPrintCode).collect(Collectors.toSet()));
            dto.getCleanMarks().addAll(marks.stream().map(MarkDTO::getPrintCode).map(Utils::clearPrintCode).collect(Collectors.toSet()));

            if (aggregation.getProductId() != null) {
                Product product = productRepository.findById(dto.getProductId()).orElseThrow(notFound());
                dto.setBarcode(product.getBarcode());
                dto.setProductId(product.getId());
                dto.setProductName(product.getVisibleName());
            }

            if (CollectionUtils.isNotEmpty(marks) && marks.get(0) != null) {
                Product product = productRepository.findById(marks.get(0).getProductId()).orElseThrow(notFound());
                dto.setChildBarcode(product.getBarcode());
                dto.setChildProductId(product.getId());
                dto.setChildProductName(product.getVisibleName());
            }

            return dto;
        }).orElseThrow(notFound());
    }

    @Transactional
    public void sendAggregationBlock(String id) {
        Mark block = markRepository.findById(id).orElseThrow(notFound());
        if (!AggregationStatus.ERROR.equals(block.getStatus()) && !AggregationStatus.UPDATED.equals(block.getStatus())) {
            throw badRequest("Аггрегация уже создана").get();
        }
        final Organization organization = organizationRepository.getReferenceById(block.getOrganizationId());
        final ProductGroup productGroup = organization.getProductGroup();
        final boolean isAlcohol = productGroup == ProductGroup.Alcohol;
        final boolean isPharma = productGroup == ProductGroup.Pharma;

        if (isAlcohol) {
            billingPermissionService.checkIsAvailableCount(organization.getId(), organization.isDemoAllowed(), organization.getParentId(), productGroup, 1);
        } else if (isPharma) {
            if (organization.getTypes().contains(OrganizationType.Importer)) {
                billingPermissionService.checkIsAvailableCount(organization.getId(), organization.isDemoAllowed(), organization.getParentId(), productGroup, block.getQuantity());
            }
        }

        final String extension = organization.getProductGroup().getExtension();
        List<Mark> marks = markRepository.findAllByParentCode(block.getCode());
        if (CollectionUtils.isEmpty(marks)) {
//            StringBuilder message = new StringBuilder("ПЕРЕОТПРАВКА КА в ТУРОН \n");
//            message.append("ОТСУТСТВУЕТ КМ в базе \n");
//            message.append("-------------------------------------\n");
//            message.append("ИНН: ").append(organization.getTin()).append("\n");
//            message.append("КА: ").append(block.getCode());
//            botApi.sendMessageToAdminChannel(message.toString());

            markRepository.save(block);
            return;
        }
        Set<String> snTins = marks.stream().map(Mark::getCode).collect(Collectors.toSet());
        AggregationItem aggregationItem = new AggregationItem();
        aggregationItem.setAggregatedItemsCount(block.getQuantity());
        aggregationItem.setAggregationUnitCapacity(block.getQuantity());
        aggregationItem.setAggregationType(AggregationType.AGGREGATION);
        aggregationItem.setUnitSerialNumber(block.getCode());
        aggregationItem.setSntins(snTins);

        CreateAggregationRequest request = new CreateAggregationRequest(Collections.singletonList(aggregationItem), organization.getTin());
        if (ProductGroup.Tobacco.equals(organization.getProductGroup())) {
            request.setProductionLineId(organization.getId());
        }
        CreateAggregationResponse response = turonClient.createAggregation(request, extension, organization.getTuronToken(), organization.getOmsId());

        if (response == null) {
            throw badRequest("Timeout").get();
        } else {
            block.setUsed(true);
            block.setReportId(response.getReportId());
            block.setAggregationDate(LocalDateTime.now());
            block.setStatus(AggregationStatus.CREATED);
            if (isAlcohol) {
                billingPermissionService.decrementAggregationCountAndSave(organization.getId(), organization.isDemoAllowed(), productGroup, 1);
            } else if (isPharma) {
                if (organization.getTypes().contains(OrganizationType.Importer)) {
                    billingPermissionService.decrementAggregationCountAndSave(organization.getId(), organization.isDemoAllowed(), productGroup, block.getQuantity());
                }
            }
        }

        markRepository.save(block);
        aggregationHistoryRepository.save(AggregationHistory.fromBlock(block));
    }
}

package uz.uzkassa.silen.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.uzkassa.silen.kafka.KafkaConstants;
import uz.uzkassa.silen.domain.*;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.domain.mongo.ShipmentMark;
import uz.uzkassa.silen.domain.redis.RedisShipmentItem;
import uz.uzkassa.silen.dto.ListResult;
import uz.uzkassa.silen.dto.mq.Payload;
import uz.uzkassa.silen.dto.mq.WarehousePayload;
import uz.uzkassa.silen.dto.warehouse.ShipmentCode;
import uz.uzkassa.silen.dto.warehouse.ShipmentItemCodeDTO;
import uz.uzkassa.silen.dto.warehouse.ShipmentItemDTO;
import uz.uzkassa.silen.enumeration.*;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.exceptions.ErrorConstants;
import uz.uzkassa.silen.integration.TrueApi;
import uz.uzkassa.silen.integration.asilbelgi.CisInfo;
import uz.uzkassa.silen.integration.billing.dto.CisInfoResponse;
import uz.uzkassa.silen.kafka.producer.KafkaProducer;
import uz.uzkassa.silen.repository.*;
import uz.uzkassa.silen.repository.mongo.MarkRepository;
import uz.uzkassa.silen.repository.mongo.ShipmentMarkRepository;
import uz.uzkassa.silen.repository.redis.RedisShipmentCodeRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static uz.uzkassa.silen.utils.Utils.*;

/**
 * Service Implementation for managing {@link ShipmentItem}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ShipmentItemService extends BaseService {
    TrueApi trueApi;
    MarkRepository markRepository;
    ProductRepository productRepository;
    SerialNumberRepository serialNumberRepository;
    ShipmentRepository shipmentRepository;
    AggregationRepository aggregationRepository;
    ShipmentItemRepository shipmentItemRepository;
    ShipmentMarkRepository shipmentMarkRepository;
    RedisShipmentCodeRepository shipmentCodeRepository;
    KafkaProducer<WarehousePayload> kafkaProducer;

    @Transactional
    public ShipmentItemDTO save(final ShipmentItemDTO dto) {
        final Shipment shipment = shipmentRepository.findById(dto.getShipmentId()).orElseThrow(notFound());
        if (ShipmentStatus.CLOSED.equals(shipment.getStatus())) {
            throw badRequest("Отгрузка завершена").get();
        }
        final Product product = productRepository.findById(dto.getProductId()).orElseThrow(notFound("Продукт не найден"));

        RedisShipmentItem redisShipmentItem = new RedisShipmentItem();
        redisShipmentItem.setShipmentId(dto.getShipmentId());
        redisShipmentItem.setTypes(dto.getTypes());
        redisShipmentItem.setProductId(dto.getProductId());
        redisShipmentItem.setOrderQty(dto.getOrderQty());
        redisShipmentItem.setProductId(product.getId());
        redisShipmentItem.setProductName(product.getVisibleName());
        redisShipmentItem.setBarcode(product.getBarcode());
        redisShipmentItem.setQty(BigDecimal.ZERO);
        if (StringUtils.isNotEmpty(dto.getSerialId())) {
            SerialNumber serialNumber = serialNumberRepository.findById(dto.getSerialId()).orElseThrow(notFound());
            redisShipmentItem.setSerialId(serialNumber.getId());
            redisShipmentItem.setSerialNumber(serialNumber.getNameWithDate());
        }
        redisShipmentItem.setId(shipmentItemRepository.getNextSequenceValue());
        redisShipmentItem = shipmentCodeRepository.save(redisShipmentItem);

        shipment.setStatus(ShipmentStatus.IN_PROGRESS);
        shipmentRepository.save(shipment);

        return redisShipmentItem.toDto();
    }

    @Transactional
    public ShipmentItemDTO update(ShipmentItemDTO dto) {
        RedisShipmentItem redisShipmentItem = shipmentCodeRepository.findById(dto.getId()).orElseThrow(notFound());
        Shipment shipment = shipmentRepository.findById(dto.getShipmentId()).orElseThrow(notFound());
        if (ShipmentStatus.CLOSED.equals(shipment.getStatus())) {
            throw badRequest("Отгрузка завершена").get();
        }
        if (redisShipmentItem.getQty().compareTo(BigDecimal.ZERO) != 0 && !redisShipmentItem.getProductId().equals(dto.getProductId())) {
            throw badRequest("Продукция не может быть изменена").get();
        }
        Product product = productRepository.findById(dto.getProductId()).orElseThrow(notFound("Продукт не найден"));

        redisShipmentItem.setProductId(dto.getProductId());
        redisShipmentItem.setProductName(product.getVisibleName());
        redisShipmentItem.setBarcode(product.getBarcode());
        redisShipmentItem.setOrderQty(dto.getOrderQty());
        redisShipmentItem.setQty(reCalculateQty(dto.getTypes()));
        redisShipmentItem.setTypes(dto.getTypes());
        if (StringUtils.isNotEmpty(dto.getSerialId())) {
            SerialNumber serialNumber = serialNumberRepository.findById(dto.getSerialId()).orElseThrow(notFound("Серия не найден"));
            redisShipmentItem.setSerialId(serialNumber.getId());
            redisShipmentItem.setSerialNumber(serialNumber.getNameWithDate());
        } else {
            redisShipmentItem.setSerialId(null);
            redisShipmentItem.setSerialNumber(null);
        }
        redisShipmentItem = shipmentCodeRepository.save(redisShipmentItem);

        return redisShipmentItem.toDto();
    }

    /**
     * This endpoint retrieves a list of shipment items by the specified shipment ID. The response will contain details of the shipment items including their IDs, shipment numbers, status, product details, package type, quantity, order quantity, types, codes, barcode, and serial information.
     * The response will be in JSON format and will include an array of shipment item objects, each containing the mentioned details.
     * Example response:
     * <pre>
     *     [
     *     {
     *         "id": 0,
     *         "shipmentId": "",
     *         "shipmentNumber": "",
     *         "shipmentStatus": "",
     *         "shipmentStatusName": "",
     *         "productId": "",
     *         "productName": "",
     *         "packageType": "",
     *         "qty": 0,
     *         "orderQty": null,
     *         "types": [
     *             {
     *                 "unit": null,
     *                 "unitName": null,
     *                 "capacity": 0,
     *                 "count": 0
     *             }
     *         ],
     *         "codes": null,
     *         "barcode": "",
     *         "serialId": "",
     *         "serialNumber": ""
     *     }
     * ]
     * </pre>
     *
     * @param shipmentId
     * @return
     */
    public List<ShipmentItemDTO> findAllByShipmentId(String shipmentId) {
        return shipmentItemRepository.findAllByShipmentIdAndReturnIdIsNull(shipmentId)
            .stream().map(shipmentItem -> shipmentItem.toDto(false))
            .collect(Collectors.toList());
    }
    public BigDecimal reCalculateQty(LinkedHashSet<ShipmentCode> dtoTypes) {
        Integer qty = 0;
        Map<PackageType, ShipmentCode> types = new LinkedHashMap<>();
        for (ShipmentCode code : dtoTypes) {
            types.put(code.getUnit(), code);
        }
        if (types.get(null) != null) {
            qty += types.get(null).getCount();
        }
        if (types.get(PackageType.BOX) != null) {
            qty += types.get(PackageType.BOX).getCapacity()
                * types.get(PackageType.BOX).getCount();
        }
        if (types.get(PackageType.PALLET) != null) {
            qty += types.get(PackageType.BOX).getCapacity()
                * types.get(PackageType.PALLET).getCapacity()
                * types.get(PackageType.PALLET).getCount();
        }
        return BigDecimal.valueOf(qty);
    }

    @Transactional
    public ShipmentItemDTO addMark(ShipmentItemCodeDTO dto) {
        RedisShipmentItem item = shipmentCodeRepository.findById(dto.getId()).orElseThrow(notFound());
        validateCountLimit(item.getShipmentId());

        Set<String> codes = validateCode(dto);
        saveShipmentMark(dto, dto.getUnit());
        if (codes != null) {
            saveMarks(codes, dto.getCode());
        }
        updateQuantity(item, dto.getUnit(), true);
        shipmentCodeRepository.save(item);

        return item.toDto();
    }

    /**
     * checking limit of codes for once shipment
     *
     * @param shipmentId default = 5000
     */
    private void validateCountLimit(String shipmentId) {
        BigDecimal existsCodesCount = BigDecimal.ONE;
        List<RedisShipmentItem> list = shipmentCodeRepository.findAllByShipmentId(shipmentId);
        for (RedisShipmentItem redisShipmentItem : list) {
            for (ShipmentCode shipmentCode : redisShipmentItem.getTypes()) {
                existsCodesCount = existsCodesCount.add(BigDecimal.valueOf(shipmentCode.getCount()));
            }
        }
        log.debug("Added to shipment codes count is {}", existsCodesCount);
        if (existsCodesCount.compareTo(BigDecimal.valueOf(5000)) == 0) {
            throw new BadRequestException("В одном отгрузке не должно быть больше 5000 кодов");
        }
    }

    @Transactional
    public ShipmentItemDTO removeMark(ShipmentItemCodeDTO dto) {
        final String code = clearPrintCode(dto.getCode());
        ShipmentMark mark = shipmentMarkRepository.findFirstByCode(code).orElseThrow(badRequest("Этот код не отгружен"));
        if (mark.getShipmentItemId() != null && !mark.getShipmentItemId().equals(dto.getId())) {
            throw badRequest("Код не принадлежит к этой отгрузке").get();
        }

        Shipment shipment = shipmentRepository.findById(dto.getShipmentId()).orElseThrow(notFound());
        ShipmentItemDTO result;
        if (shipment.getStatus().equals(ShipmentStatus.CLOSED)) {
            ShipmentItem item = shipmentItemRepository.getReferenceById(dto.getId());
            Integer qty = updateQuantity(item, mark.getUnit() != null ? PackageType.fromCode(mark.getUnit()) : null, false);

            WarehousePayload payloadReturn = new WarehousePayload();
            payloadReturn.setOrganizationId(shipment.getOrganizationId());
            payloadReturn.setCustomerId(shipment.getCustomerId());
            payloadReturn.setOperationDate(LocalDateTime.now());
            payloadReturn.setOperation(WarehouseOperation.Returned);
            payloadReturn.setNote("Возвращен");
            payloadReturn.setProductId(item.getProductId());
            // sending to queue by unit
            payloadReturn.setUnit(mark.getPackageType());
            payloadReturn.setCode(mark.getCode());
            payloadReturn.setAlcoholCount(BigDecimal.valueOf(qty));
            kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC,payloadReturn);

            aggregationRepository.findFirstByCode(code).ifPresent(aggregation -> {
                aggregation.setStatus(AggregationStatus.RETURNED);
                aggregationRepository.save(aggregation);
            });

            result = item.toDto(true);
        } else {
            RedisShipmentItem item = shipmentCodeRepository.findById(dto.getId()).orElseThrow(notFound());
            updateQuantity(item, mark.getUnit() != null ? PackageType.fromCode(mark.getUnit()) : null, false);
            shipmentCodeRepository.save(item);
            result = item.toDto();
        }

        markRepository.findFirstByCode(code).ifPresent(setUsedFalse -> {
            setUsedFalse.setParentCode(null);
            setUsedFalse.setUsed(false);
            setUsedFalse.setStatus(AggregationStatus.DRAFT);
            markRepository.save(setUsedFalse);
        });
        shipmentMarkRepository.delete(mark);

        return result;
    }

    private void updateQuantity(RedisShipmentItem item, PackageType unit, boolean add) {
        Integer qty = 0;
        Map<PackageType, ShipmentCode> types = new LinkedHashMap<>();
        for (ShipmentCode code : item.getTypes()) {
            types.put(code.getUnit(), code);
        }
        ShipmentCode type = types.get(unit);
        if (unit == null) {
            qty = 1;
        } else if (PackageType.BOX.equals(unit)) {
            qty = types.get(PackageType.BOX).getCapacity();
        } else if (PackageType.PALLET.equals(unit)) {
            qty = types.get(PackageType.BOX).getCapacity() * types.get(PackageType.PALLET).getCapacity();
        }
        type.setCount(add ? type.getCount() + 1 : type.getCount() - 1);
        types.put(unit, type);
        item.setTypes(new LinkedHashSet<>(types.values()));

        if (add) {
            item.setQty(item.getQty().add(new BigDecimal(qty)));
        } else {
            item.setQty(item.getQty().subtract(new BigDecimal(qty)));
        }
    }

    private Integer updateQuantity(ShipmentItem item, PackageType unit, boolean add) {
        Integer qty = 0;
        Map<PackageType, ShipmentCode> types = new LinkedHashMap<>();
        for (ShipmentCode code : item.getTypes()) {
            types.put(code.getUnit(), code);
        }
        ShipmentCode type = types.get(unit);
        if (unit == null) {
            qty = 1;
        } else if (PackageType.BOX.equals(unit)) {
            qty = types.get(PackageType.BOX).getCapacity();
        } else if (PackageType.PALLET.equals(unit)) {
            qty = types.get(PackageType.BOX).getCapacity() * types.get(PackageType.PALLET).getCapacity();
        }
        type.setCount(add ? type.getCount() + 1 : type.getCount() - 1);
        types.put(unit, type);
        item.setTypes(new LinkedHashSet<>(types.values()));

        if (add) {
            item.setQty(item.getQty().add(new BigDecimal(qty)));
        } else {
            item.setQty(item.getQty().subtract(new BigDecimal(qty)));
        }
        return qty;
    }

    public Set<String> validateCode(ShipmentItemCodeDTO dto) {
        if (!validateShipmentCode(dto.getCode(), dto.getUnit())) {
            throw badRequest("Код не соответствует стандартному формату").get();
        }
        if (isMark(dto.getCode()) && checkIsEqualBarcodes(dto.getBarcode(), dto.getCode())) {
            throw new BadRequestException("Ошибка! Несоответствие штрихкода: \nKM: " + dto.getCode());
        }

        final String code = clearPrintCode(dto.getCode());
        if (shipmentMarkRepository.existsShipmentMarkByCode(code)) {
            throw new BadRequestException(dto.getUnit() == null ? "Код маркировки уже отгружен" : "Код аггрегации уже отгружен", dto.getUnit() == null ? ErrorConstants.MARK_USED_ERROR_CODE : ErrorConstants.AGGREGATION_USED_ERROR_CODE, code);
        }

        Organization organization = getCurrentOrganization();
        if (StringUtils.isEmpty(organization.getTrueToken())) {
            throw badRequest("Отсутствует токен организации").get();
        }

        if (dto.getUnit() != null) {
            aggregationRepository.findFirstByCode(code)
                .ifPresent(aggregation -> {
                    if (!aggregation.getPackageType().equals(dto.getUnit())) {
                        throw badRequest(aggregation.getPackageType().getNameRu() + " не можете добавить как " + dto.getUnit().getNameRu()).get();
                    }
                if (aggregation.getStatus().checkStatusContainNotAbleToShip()) {
                    throw new BadRequestException("Код не может быть отгружен. \nКод статус " + aggregation.getStatus().getText());
                }
                if (aggregation.getProduct() != null && checkIsEqualBarcodes(dto.getBarcode(), aggregation.getProduct().getBarcode())) {
                    throw new BadRequestException("Ошибка! Несоответствие штрихкода: \nКод: " + dto.getCode() + "\nПродукция: " + aggregation.getProduct().getName());
                }
                if (aggregation.getParentCode() != null) {
                    if (Arrays.asList(ProductGroup.Beer, ProductGroup.Tobacco, ProductGroup.Appliances, ProductGroup.Pharma).contains(organization.getProductGroup())) {
                        if (shipmentMarkRepository.existsShipmentMarkByCode(aggregation.getParentCode())) {
                            throw new BadRequestException(dto.getUnit() == null ? "Код маркировки уже отгружен" : "Код аггрегации уже отгружен", dto.getUnit() == null ? ErrorConstants.MARK_USED_ERROR_CODE : ErrorConstants.AGGREGATION_USED_ERROR_CODE, code);
                        } else {
                            aggregationRepository.findFirstByCode(aggregation.getParentCode()).ifPresent(parent -> {
                                if (parent.getStatus().checkStatusContainNotAbleToShip()) {
                                    throw new BadRequestException("Код не может быть отгружен. \nКод статус " + aggregation.getStatus().getText());
                                }
                            });
                        }
                    } else {
                        throw new BadRequestException("Ошибка! Код состоит в агрегации. КА: " + aggregation.getParentCode());
                    }
                }
            });
        } else {
            markRepository.findFirstByCode(code)
                .ifPresent(mark -> {
                    if (mark.getStatus().checkStatusContainNotAbleToShip()) {
                        throw new BadRequestException("Код не может быть отгружен. \nКод статус " + mark.getStatus().getText());
                    } else if (checkIsEqualBarcodes(dto.getBarcode(), mark.getCode())) {
                        throw new BadRequestException("Ошибка! Несоответствие штрихкода: \nKM: " + dto.getCode() + "\nПродукция: " + mark.getProductName());
                    } else if (mark.getParentCode() != null) {
                        if (EnumSet.of(ProductGroup.Beer, ProductGroup.Pharma).contains(organization.getProductGroup())) {
                            if (shipmentMarkRepository.existsShipmentMarkByCode(mark.getParentCode())) {
                                throw new BadRequestException("Код аггрегации уже отгружен", ErrorConstants.AGGREGATION_USED_ERROR_CODE, code);
                            } else {
                                aggregationRepository.findFirstByCode(mark.getParentCode()).ifPresent(parent -> {
                                    if (parent.getStatus().checkStatusContainNotAbleToShip()) {
                                        throw new BadRequestException("Код не может быть отгружен. \nКод статус " + parent.getStatus().getText());
                                    }
                                });
                                // checkin parent block
                                markRepository.findFirstByCode(mark.getParentCode()).ifPresent(parent -> {
                                    if (parent.getStatus().checkStatusContainNotAbleToShip()) {
                                        throw new BadRequestException("Код не может быть отгружен. \nКод статус " + parent.getStatus().getText());
                                    }
                                });
                            }
                        } else {
                            throw new BadRequestException("Ошибка! Код состоит в агрегации. КА: " + mark.getParentCode());
                        }

                    }
                });
        }
        if (dto.isTrueCheck()) {
            return checkCodeIsCompatibleForShip(organization, code, dto.getUnit());
        }
        return null;
    }

    public Set<String> checkCodeIsCompatibleForShip(final Organization organization, final String code, final PackageType unit) {
        final CisInfoResponse[] result = trueApi.cisesInfo(organization.getTin(), organization.getTrueToken(), Collections.singleton(code));
        if (result != null && result.length > 0) {
            final CisInfo cisInfo = result[0].getCisInfo();
            if (organization.getProductGroup().checkIsCanShipCodeByStatus(cisInfo.getStatus())) {
                if (unit == null) {
                    return cisInfo.getChild();
                }
                return Optional.ofNullable(cisInfo.getChild()).orElseThrow(badRequest("Код аггрегации пустой!"));
            } else {
                final String errorMessage = cisInfo.getStatus() != null ? "Ошибка аггрегации! Статус КМ : " + cisInfo.getStatus().getText() : "Ошибка аггрегации! Статус КМ : NULL";
//                final StringBuilder message = new StringBuilder("INN: ").append(getCurrentOrganizationTin()).append(" \n").append("CODE: ").append(code).append(" \n").append(errorMessage);
//                botApi.sendMessageToAdminChannel(message.toString());
                throw new BadRequestException(errorMessage);
            }
        }
        return null;
    }

    private void saveShipmentMark(ShipmentItemCodeDTO dto, PackageType packageType) {
        final String code = clearPrintCode(dto.getCode());

        ShipmentMark mark = new ShipmentMark();
        mark.setPrintCode(dto.getCode());
        mark.setCode(code);
        if (packageType != null) {
            mark.setUnit(packageType.getCode());
        } else {
            mark.setUnit(null);
        }
        mark.setShipmentItemId(dto.getId());
        shipmentMarkRepository.save(mark);
    }

    private void saveMarks(Set<String> codes, String aggregationCode) {
        List<Mark> markList = markRepository.findAllByCodeIn(codes);
        for (Mark markExists : markList) {
            if (markExists.getParentCode() != null) {
                markExists.setParentCode(clearPrintCode(aggregationCode));
                markExists.setUsed(true);
                markExists.setStatus(AggregationStatus.SHIPPED);
            }
            markRepository.save(markExists);
            codes.remove(markExists.getCode());
        }
        if (CollectionUtils.isNotEmpty(codes)) {
            for (String code : codes) {
                Mark mark = new Mark();
                mark.setCode(code);
                mark.setPrintCode(code);
                mark.setCreatedDate(LocalDateTime.now());
                mark.setOrganizationId(getCurrentOrganizationId());
                mark.setParentCode(clearPrintCode(aggregationCode));
                mark.setUsed(true);
                mark.setStatus(AggregationStatus.SHIPPED);
                markRepository.save(mark);
            }
        }

    }

    public ShipmentItemDTO findOne(Long id, PackageType unit) {
        return shipmentCodeRepository.findById(id).map(shipmentItem -> {
            ShipmentItemDTO dto = shipmentItem.toDto();
            List<ShipmentMark> codes;
            if (unit == null) {
                codes = shipmentMarkRepository.findAllByShipmentItemIdAndUnitIsNull(id);
            } else {
                codes = shipmentMarkRepository.findAllByShipmentItemIdAndUnit(id, unit.getCode());
            }
            dto.setCodes(codes.stream().map(ShipmentMark::getPrintCode).collect(Collectors.toSet()));
            return dto;
        }).orElseThrow(notFound());
    }

    public ShipmentItemDTO getByCode(String printCode) {
        final String code = clearPrintCode(printCode);
        ShipmentMark mark = shipmentMarkRepository.findFirstByCodeAndShipmentItemIdIsNotNull(code).orElseThrow(notFound());
        return shipmentItemRepository.findById(mark.getShipmentItemId()).map(shipmentItem -> {
            ShipmentItemDTO dto = shipmentItem.toDto(false);
            dto.setCodes(shipmentMarkRepository.findAllByShipmentItemId(mark.getShipmentItemId()).stream().map(ShipmentMark::getPrintCode).collect(Collectors.toSet()));
            return dto;
        }).orElseThrow(notFound());
    }

    public ListResult<ShipmentMark> getCodes(Long id) {
        List<ShipmentMark> list = shipmentMarkRepository.findAllByShipmentItemId(id);
        return new ListResult<>(list, (long) list.size());
    }

    @Transactional
    public void delete(Long id) {
        RedisShipmentItem item = shipmentCodeRepository.findById(id)
            .orElseThrow(badRequest("Отгрузка завершена"));
        /*Shipment shipment = shipmentRepository.findById(item.getShipmentId()).orElseThrow(notFound());
        if (ShipmentStatus.CLOSED.equals(shipment.getStatus())) {
            throw badRequest("Отгрузка завершена").get();
        }*/
        List<ShipmentMark> shipmentMarkList = shipmentMarkRepository.findAllByShipmentItemIdAndUnitIsNull(id);
        shipmentMarkList.forEach(shipmentMark -> markRepository.findFirstByCode(shipmentMark.getCode())
            .ifPresent(mark -> {
                if (mark.getParentCode() == null) {
                    mark.setStatus(AggregationStatus.DRAFT);
                    mark.setUsed(false);
                } else {
                    mark.setStatus(AggregationStatus.CREATED);
                }
                markRepository.save(mark);
            }));
        shipmentMarkRepository.deleteAllByShipmentItemId(id);
        shipmentCodeRepository.delete(item);
    }


    /**
     * Get serial number's aggregations and add they are to shipment item by unit type
     */
    public void fillShipmentItem(Long id) {
        RedisShipmentItem redisShipmentItem = shipmentCodeRepository.findById(id).orElseThrow(notFound(""));
        Payload payload = new Payload(redisShipmentItem.getId());
        rabbitMqProducer.sendFillShipmentItem(payload);
    }

    public void fillWithBox(Long id) {
        shipmentCodeRepository.findById(id).ifPresent(redisShipmentItem -> {
            if (redisShipmentItem.getSerialId() != null) {
                int boxCount = 0;
                int palletCount = 0;
                // collect only aggregations like BOX, PALLET
                List<Aggregation> aggregationsBySerial = aggregationRepository.findAllBySerialIdAndStatusAndDeletedIsFalse(redisShipmentItem.getSerialId(), AggregationStatus.CREATED);
                if (CollectionUtils.isNotEmpty(aggregationsBySerial)) {
                    List<ShipmentMark> batchInsert = new LinkedList<>();
                    for (Aggregation aggregation : aggregationsBySerial) {
                        ShipmentMark shipmentMark = new ShipmentMark();
                        shipmentMark.setPrintCode(aggregation.getPrintCode());
                        shipmentMark.setCode(aggregation.getCode());
                        shipmentMark.setUnit(aggregation.getUnit());
                        shipmentMark.setShipmentItemId(id);
                        batchInsert.add(shipmentMark);
                        if (batchInsert.size() > 50) {
                            shipmentMarkRepository.saveAll(batchInsert);
                            batchInsert.clear();
                        }
                        if (PackageType.BOX.getCode() == aggregation.getUnit()) {
                            boxCount++;
                        } else
                            palletCount++;
                    }
                    if (CollectionUtils.isNotEmpty(batchInsert)) {
                        shipmentMarkRepository.saveAll(batchInsert);
                        batchInsert.clear();
                    }
                    Map<PackageType, ShipmentCode> types = redisShipmentItem.getTypes().stream().collect(Collectors.toMap(ShipmentCode::getUnit, value -> value));
                    if (types.get(PackageType.BOX) != null) {
                        types.get(PackageType.BOX).setCount(boxCount);
                        redisShipmentItem.setQty(BigDecimal.valueOf((long) boxCount * types.get(PackageType.BOX).getCapacity()));
                    }
                    if (types.get(PackageType.PALLET) != null) {
                        types.get(PackageType.PALLET).setCount(palletCount);
                        redisShipmentItem.setQty(BigDecimal.valueOf((long) boxCount * types.get(PackageType.BOX).getCapacity() * types.get(PackageType.PALLET).getCapacity()));
                    }
                    shipmentCodeRepository.save(redisShipmentItem);
                }
            }

        });
    }
}

package uz.uzkassa.silen.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.domain.Organization;
import uz.uzkassa.silen.dto.UtilisationDTO;
import uz.uzkassa.silen.enumeration.ProductGroup;
import uz.uzkassa.silen.enumeration.UsageType;
import uz.uzkassa.silen.integration.TuronClient;
import uz.uzkassa.silen.integration.turon.UtilisationRequestDTO;
import uz.uzkassa.silen.integration.turon.UtilisationResponseDTO;
import uz.uzkassa.silen.utils.DateUtils;
import uz.uzkassa.silen.utils.ServerUtils;
import uz.uzkassa.silen.utils.Utils;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UtilisationService extends BaseService implements Serializable {
    TuronClient turonClient;

    /**
     * for {@link ProductGroup#Alcohol}
     *
     * @param codes
     * @param organization
     * @param productionDateTime
     * @return
     */
    public UtilisationResponseDTO create(final Set<String> codes, final Organization organization, LocalDateTime productionDateTime) {
        final UtilisationRequestDTO utilisationRequestDTO = new UtilisationRequestDTO();
        utilisationRequestDTO.setSntins(codes);
        utilisationRequestDTO.setUsageType(UsageType.USED_FOR_PRODUCTION);
        utilisationRequestDTO.setProductionDate(productionDateTime.minusHours(6).format(DateUtils.ISO_INSTANT));
        utilisationRequestDTO.setProductionLineId(organization.getId());
        return turonClient.apply(organization.getTuronToken(), organization.getProductGroup().getExtension(), organization.getOmsId(), utilisationRequestDTO);
    }

    /**
     * for {@link ProductGroup#Pharma}
     *
     * @param codes
     * @param organization
     * @param productionDateTime
     * @param expiredDateTime
     * @param serialNumber
     * @return
     */
    public UtilisationResponseDTO create(final Set<String> codes, final Organization organization, LocalDateTime productionDateTime, LocalDateTime expiredDateTime, String serialNumber) {

        final UtilisationRequestDTO utilisationRequestDTO = new UtilisationRequestDTO();
        utilisationRequestDTO.setSntins(codes);
        utilisationRequestDTO.setUsageType(UsageType.USED_FOR_PRODUCTION);
        utilisationRequestDTO.setProductionLineId(organization.getId());
        utilisationRequestDTO.setProductionDate(productionDateTime.minusHours(6).format(DateUtils.ISO_INSTANT));
        utilisationRequestDTO.setSeriesNumber(ServerUtils.serialNumberCut(serialNumber));
        LocalDateTime expiredDate = Optional.ofNullable(expiredDateTime).orElse(LocalDateTime.now().plusYears(2));
        utilisationRequestDTO.setExpirationDate(expiredDate.minusHours(6).format(DateUtils.ISO_INSTANT));

        return turonClient.apply(organization.getTuronToken(), organization.getProductGroup().getExtension(), organization.getOmsId(), utilisationRequestDTO);
    }

    public UtilisationResponseDTO create(UtilisationDTO utilisationDTO) {
        Optional<Organization> organization = organizationRepository.findById(Optional.ofNullable(utilisationDTO.getOrganizationId()).orElse(getCurrentOrganizationId()));
        if (organization.isPresent() && organization.get().getProductGroup().sendToApplyMarks()) {
            Set<String> cisesWithUnicode = utilisationDTO.getCises().stream().map(Utils::fillWithUniCode).collect(Collectors.toSet());
            UtilisationRequestDTO utilisationRequestDTO = new UtilisationRequestDTO();
            utilisationRequestDTO.setSntins(cisesWithUnicode);
            utilisationRequestDTO.setUsageType(UsageType.USED_FOR_PRODUCTION);
            utilisationRequestDTO.setProductionDate(utilisationDTO.getProductionDate().minusHours(6).format(DateUtils.ISO_INSTANT));
            utilisationRequestDTO.setProductionLineId(organization.get().getId());
            if (ProductGroup.Pharma.equals((organization.get().getProductGroup()))) {
                utilisationRequestDTO.setExpirationDate(utilisationDTO.getProductionDate().format(DateUtils.ISO_INSTANT));
                utilisationRequestDTO.setSeriesNumber(ServerUtils.serialNumberCut(utilisationRequestDTO.getSeriesNumber()));
            }
            return turonClient.apply(organization.get().getTuronToken(), organization.get().getProductGroup().getExtension(), organization.get().getOmsId(), utilisationRequestDTO);
        }
        return null;
    }
}

package uz.uzkassa.silen.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.uzkassa.silen.kafka.KafkaConstants;
import uz.uzkassa.silen.domain.*;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.domain.mongo.ShipmentMark;
import uz.uzkassa.silen.domain.redis.RedisShipmentItem;
import uz.uzkassa.silen.dto.ShipmentHistoryDTO;
import uz.uzkassa.silen.dto.ShipmentStatsDTO;
import uz.uzkassa.silen.dto.filter.ShipmentFilter;
import uz.uzkassa.silen.dto.mq.Payload;
import uz.uzkassa.silen.dto.mq.WarehousePayload;
import uz.uzkassa.silen.dto.warehouse.*;
import uz.uzkassa.silen.enumeration.*;
import uz.uzkassa.silen.kafka.producer.KafkaProducer;
import uz.uzkassa.silen.repository.AggregationRepository;
import uz.uzkassa.silen.repository.PurchaseOrderRepository;
import uz.uzkassa.silen.repository.ShipmentItemRepository;
import uz.uzkassa.silen.repository.ShipmentRepository;
import uz.uzkassa.silen.repository.mongo.MarkRepository;
import uz.uzkassa.silen.repository.mongo.ShipmentMarkRepository;
import uz.uzkassa.silen.repository.redis.RedisShipmentCodeRepository;
import uz.uzkassa.silen.security.SecurityUtils;
import uz.uzkassa.silen.utils.Utils;
import uz.uzkassa.silen.exceptions.BadRequestException;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Shipment}.
 */
@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ShipmentService extends BaseService {
    InvoiceService invoiceService;
    MarkRepository markRepository;
    ShipmentRepository shipmentRepository;
    AggregationRepository aggregationRepository;
    ShipmentItemRepository shipmentItemRepository;
    ShipmentMarkRepository shipmentMarkRepository;
    RedisShipmentCodeRepository shipmentCodeRepository;
    PurchaseOrderRepository purchaseOrderRepository;
    KafkaProducer<WarehousePayload> kafkaProducer;

    @Transactional
    public String create(ShipmentDTO dto) {
        final String organizationId = getCurrentOrganizationId();
        Shipment shipment = new Shipment();
        shipment.setOrganizationId(organizationId);
        if (StringUtils.isNotEmpty(dto.getNumber())) {
            if (shipmentRepository.existsShipmentByOrganizationIdAndNumberIgnoreCase(organizationId, dto.getNumber())) {
                throw new BadRequestException("Отгрузка с таким номером уже существует");
            } else {
                shipment.setNumber(dto.getNumber());
            }
        } else {
            generateShipmentNumber(organizationId, shipment, "SHIP-");
        }
        shipment.setCustomerId(dto.getCustomerId());
        shipment.setShipmentDate(dto.getShipmentDate());
        shipment.setStatus(ShipmentStatus.NEW);
        shipment.setCommittent(dto.isCommittent());
        shipment.setStoreId(dto.getStoreId());
        shipment.setDocumentType(dto.getDocumentType());
        shipment = shipmentRepository.save(shipment);

        return shipment.getId();
    }

    public void generateShipmentNumber(String organizationId, Shipment shipment, String prefix) {
        Integer intNumber = shipmentRepository.getMaxIntNumber(organizationId);
        String number = null;
        if (intNumber == null) {
            intNumber = 1;
        }
        boolean exists = true;
        while (exists) {
            number = prefix + intNumber;
            if (shipmentRepository.existsShipmentByOrganizationIdAndNumberIgnoreCase(organizationId, number)) {
                intNumber++;
            } else {
                exists = false;
            }
        }
        shipment.setIntNumber(intNumber);
        shipment.setNumber(number);
    }

    @Transactional
    public void update(String id, ShipmentDTO dto) {
        shipmentRepository.findById(id).ifPresent(shipment -> {
            if (ShipmentStatus.CLOSED.equals(shipment.getStatus())) {
                throw badRequest("Отгрузка уже завершена").get();
            }
            if (StringUtils.isNotEmpty(dto.getNumber())) {
                if (shipmentRepository.existsShipmentByOrganizationIdAndNumberIgnoreCaseAndIdIsNot(getCurrentOrganizationId(), dto.getNumber(), id)) {
                    throw new BadRequestException("Отгрузка с таким номером уже существует");
                } else {
                    shipment.setNumber(dto.getNumber());
                }
            }
            shipment.setCustomerId(dto.getCustomerId());
            shipment.setShipmentDate(dto.getShipmentDate());
            shipment.setCommittent(dto.isCommittent());
            shipment.setStoreId(dto.getStoreId());
            shipment.setDocumentType(dto.getDocumentType());
        });
    }

    public Page<ShipmentListDTO> findAllByFilter(ShipmentFilter filter) {
        if (StringUtils.isNotEmpty(filter.getSearch())) {
            Optional<ShipmentMark> markOptional = shipmentMarkRepository.findFirstByCode(Utils.clearPrintCode(filter.getSearch()));
            if (markOptional.isPresent() && markOptional.get().getShipmentItemId() != null) {
                ShipmentItem item = shipmentItemRepository.getReferenceById(markOptional.get().getShipmentItemId());
                filter.setId(item.getShipmentId());
            }
        }
        return shipmentRepository.findAllByFilter(filter).map(Shipment::toListDto);
    }

    public List<ShipmentListDTO> getItems(ShipmentFilter filter) {
        filter.setStatus(ShipmentStatus.CLOSED);
        return shipmentRepository.findAllByFilter(filter).map(Shipment::toListDto).getContent();
    }

    public List<ShipmentStatsDTO> findAllForStats(ShipmentFilter filter) {
        if (StringUtils.isNotEmpty(filter.getSearch())) {
            Optional<ShipmentMark> markOptional = shipmentMarkRepository.findFirstByCode(Utils.clearPrintCode(filter.getSearch()));
            if (markOptional.isPresent() && markOptional.get().getShipmentItemId() != null) {
                ShipmentItem item = shipmentItemRepository.getReferenceById(markOptional.get().getShipmentItemId());
                filter.setId(item.getShipmentId());
            }
        }
        return shipmentRepository.findAllByFilterForStats(filter);
    }

    public Page<ShipmentDTO> findAllWithoutOrgIdFilter(ShipmentFilter filter) {
        return shipmentRepository.findAllByFilterWithoutOrgId(filter).map(Shipment::toDto);
    }

    public ShipmentDetailsDTO getOne(String id) {
        Shipment shipment = shipmentRepository.findById(id).orElseThrow(notFound());
        ShipmentDetailsDTO dto = new ShipmentDetailsDTO();
        dto.setId(shipment.getId());
        dto.setNumber(shipment.getNumber());
        dto.setShipmentDate(shipment.getShipmentDate());
        if (shipment.getCustomer() != null) {
            dto.setCustomer(shipment.getCustomer().toSelectItem());
        }
        if (shipment.getStatus() != null) {
            dto.setStatus(shipment.getStatus().toDto());
        }
        List<ShipmentItemDTO> items;
        if (ShipmentStatus.CLOSED.equals(shipment.getStatus())) {
            items = shipmentItemRepository.findAllByShipmentIdOrderById(id).stream().map(shipmentItem -> shipmentItem.toDto(false)).collect(Collectors.toList());
        } else {
            items = shipmentCodeRepository.findAllByShipmentIdOrderById(id).stream().map(RedisShipmentItem::toDto).collect(Collectors.toList());
        }
        dto.setItems(items);
        dto.setCommittent(shipment.isCommittent());
        if (shipment.getDocumentType() != null) {
            dto.setDocumentType(shipment.getDocumentType().toSelectItem());
        }
        if (shipment.getStore() != null) {
            dto.setStore(shipment.getStore().toSelectItem());
        }
        return dto;
    }

    @Transactional
    public void close(String id) {
        Shipment shipment = shipmentRepository.findById(id).orElseThrow(notFound());
        shipment.setStatus(ShipmentStatus.CLOSED);
        shipmentRepository.save(shipment);

        List<RedisShipmentItem> shipmentItems = shipmentCodeRepository.findAllByShipmentIdOrderById(shipment.getId());
        if (CollectionUtils.isNotEmpty(shipmentItems)) {
            List<ShipmentItem> items = new ArrayList<>();
            ShipmentItem entity;
            for (RedisShipmentItem redisItem : shipmentItems) {
                RedisShipmentItem calculatedItem = calculateQty(redisItem);
                entity = new ShipmentItem();
                entity.setId(calculatedItem.getId());
                entity.setProductId(calculatedItem.getProductId());
                entity.setShipmentId(calculatedItem.getShipmentId());
                entity.setQty(calculatedItem.getQty());
                entity.setTypes(calculatedItem.getTypes());
                entity.setSerialId(calculatedItem.getSerialId());
                items.add(entity);
            }
            log.debug("Save shipment items on close");
            shipmentItemRepository.saveAll(items);
            shipmentCodeRepository.deleteAll(shipmentItems);
        }

        if (shipment.getPurchaseOrder() != null) {
            PurchaseOrder purchaseOrder = shipment.getPurchaseOrder();
            purchaseOrder.setStatus(PurchaseOrderStatus.CLOSED);
            purchaseOrderRepository.save(purchaseOrder);
        }

        Payload payload = new Payload();
        payload.setId(id);
        payload.setExtraId(SecurityUtils.getCurrentUserLogin());
        log.debug("ShipmentClose action payload {}", payload);
        switch (shipment.getDocumentType()) {
            case ACCEPTANCE_TRANSFER_ACT:
                rabbitMqProducer.sendShipmentAct(payload);
                break;
            case INVOICE:
                rabbitMqProducer.sendShipmentInvoice(payload);
                break;
        }
        rabbitMqProducer.sendShipmentItemsToWarehouse(payload);
    }

    public void convert(String id) {
        invoiceService.createShipmentInvoice(id, false);
    }

    @Transactional
    public void delete(String id) {
        Shipment shipment = shipmentRepository.findById(id).orElseThrow(notFound());
        if (!isAgency() && ShipmentStatus.CLOSED.equals(shipment.getStatus())) {
            throw badRequest("Невозможно удалить завершенную отгрузку").get();
        }
        if (ShipmentStatus.CLOSED.equals(shipment.getStatus())) {
            List<ShipmentItem> items = shipmentItemRepository.findAllByShipmentId(id);
            for (ShipmentItem item : items) {
                shipmentMarkRepository.findAllByShipmentItemIdAndUnitIsNull(item.getId())
                    .forEach(shipmentMark -> markRepository.findFirstByCode(shipmentMark.getCode())
                        .ifPresent(mark -> {
                            if (mark.getParentCode() == null) {
                                mark.setStatus(AggregationStatus.DRAFT);
                                mark.setUsed(false);
                            } else {
                                mark.setStatus(AggregationStatus.CREATED);
                            }
                            markRepository.save(mark);
                        }));
                shipmentMarkRepository.deleteAllByShipmentItemId(item.getId());
            }
            shipmentItemRepository.deleteAll(items);
        } else {
            List<RedisShipmentItem> items = shipmentCodeRepository.findAllByShipmentId(id);

            for (RedisShipmentItem item : items) {
                Long redisShipmentItemId = item.getId();
                //TODO: all shipment item codes need able to ship
                shipmentMarkRepository.findAllByShipmentItemIdAndUnitIsNull(redisShipmentItemId)
                    .forEach(shipmentMark -> markRepository.findFirstByCode(shipmentMark.getCode())
                        .ifPresent(mark -> {
                            if (mark.getParentCode() == null) {
                                mark.setStatus(AggregationStatus.DRAFT);
                                mark.setUsed(false);
                            } else {
                                mark.setStatus(AggregationStatus.CREATED);
                            }
                            markRepository.save(mark);
                        }));
                shipmentMarkRepository.deleteAllByShipmentItemId(redisShipmentItemId);
            }
            shipmentCodeRepository.deleteAll(items);
        }
        shipmentRepository.deleteById(id);
        if (shipment.getPurchaseOrder() != null) {
            PurchaseOrder purchaseOrder = shipment.getPurchaseOrder();
            if (PurchaseOrderStatus.IN_PROGRESS.equals(purchaseOrder.getStatus())) {
                purchaseOrder.setStatus(PurchaseOrderStatus.APPROVED);
                purchaseOrderRepository.save(purchaseOrder);
            }
        }
    }

    public void removeDeletedCodes() {
        shipmentMarkRepository.deleteAllByShipmentItemIdIsNull();
    }

    public Page<WarehouseSelectItem> getShippedProductsStats(ShipmentFilter filter) {
        return shipmentRepository.getShippedProductsStats(filter);
    }

    public Page<ShipmentHistoryDTO> getShipHistory(ShipmentFilter filter) {
        filter.setStatus(ShipmentStatus.CLOSED);
        return shipmentRepository.findAllByFilter(filter).map(shipment -> {
            ShipmentHistoryDTO historyDTO = new ShipmentHistoryDTO();
            historyDTO.setShipment(shipment.toSelectItem());
            historyDTO.setInvoice(shipment.getInvoices().stream().filter(listInvoice -> listInvoice.getStatus().equals(Status.ACCEPTED)).map(ListInvoice::toSelectItem).collect(Collectors.toList()));
            return historyDTO;
        });
    }

    public BigDecimal getShipHistorySummary(ShipmentFilter filter) {
        return shipmentRepository.getShipHistorySummary(filter);
    }

    /**
     * Method will execute from RabbitMQ task
     * payload.id is shipmentId
     * payload.extraId is who is closed shipment user's login
     *
     * @param closedActionPayload {@link Payload}
     */
    @Transactional
    public void sendClosedShipment(Payload closedActionPayload) {
        final String shipmentId = closedActionPayload.getId();
        final String userLogin = closedActionPayload.getExtraId();
        Shipment shipment = shipmentRepository.findById(shipmentId).orElseGet(Shipment::new);
        if (shipment == null) return;

        for (ShipmentItem shipmentItem : shipment.getItems()) {
            List<ShipmentMark> allCodesByShipmentItemId = shipmentMarkRepository.findAllByShipmentItemId(shipmentItem.getId());
            Set<String> disAggregateParent = new HashSet<>();
            Set<String> aggregations = new HashSet<>();
            Set<String> blocks = new HashSet<>();
            Set<String> marks = new HashSet<>();

            for (ShipmentMark shipmentMark : allCodesByShipmentItemId) {
                PackageType packageType = shipmentMark.getPackageType();
                if (CodeType.AGGREGATION.equals(packageType.getCodeType())) {
                    aggregations.add(shipmentMark.getCode());
                }
                if (PackageType.BLOCK.equals(packageType)) {
                    blocks.add(shipmentMark.getCode());
                }
                if (CodeType.MARK.equals(packageType.getCodeType())) {
                    marks.add(shipmentMark.getCode());
                }
            }

            // only aggregations
            if (CollectionUtils.isNotEmpty(aggregations)) {
                List<Aggregation> aggregationList = aggregationRepository.findAllByCodeInOrParentCodeIn(aggregations);
                List<Aggregation> aggregationBatch = new LinkedList<>();
                for (Aggregation aggregationItem : aggregationList) {
                    if (aggregationItem.getParentCode() != null && !aggregations.contains(aggregationItem.getParentCode())) {
                        disAggregateParent.add(aggregationItem.getParentCode());
                        aggregationItem.setParentCode(null);
                    }
                    // change here aggregations
                    aggregationItem.setStatus(AggregationStatus.SHIPPED);
                    aggregationItem.setShipmentId(shipmentId);
                    aggregationBatch.add(aggregationItem);
                    //sending aggregation item like shipped
                    sendAggregationToWarehouse(aggregationItem, shipment, shipmentItem.getProductId(), userLogin);
                }
                if (CollectionUtils.isNotEmpty(aggregationBatch)) {
                    aggregationRepository.saveAll(aggregationBatch);
                }
            }
            // only blocks
            if (CollectionUtils.isNotEmpty(blocks)) {
                List<Mark> blockList = markRepository.findAllByCodeIn(blocks);
                List<Mark> blockBatch = new LinkedList<>();
                for (Mark block : blockList) {
                    if (block.getParentCode() != null && !aggregations.contains(block.getParentCode())) {
                        disAggregateParent.add(block.getParentCode());
                        block.setParentCode(null);
                    }
                    block.setStatus(AggregationStatus.SHIPPED);
                    blockBatch.add(block);
                    //sending block item like shipped
                    sendBlockToWarehouse(block, shipment);
                }
                if (CollectionUtils.isNotEmpty(blockBatch)) {
                    markRepository.saveAll(blockBatch);
                }
            }
            // only marks
            if (CollectionUtils.isNotEmpty(marks)) {
                List<Mark> markList = markRepository.findAllByCodeIn(marks);
                List<Mark> markBatch = new LinkedList<>();
                for (Mark markItem : markList) {
                    if (markItem.getParentCode() != null && (!aggregations.contains(markItem.getParentCode()) || !blocks.contains(markItem.getParentCode()))) {
                        disAggregateParent.add(markItem.getParentCode());
                        markItem.setParentCode(null);
                    }
                    markItem.setUsed(true);
                    markItem.setStatus(AggregationStatus.SHIPPED);
                    markBatch.add(markItem);
                }
                if (CollectionUtils.isNotEmpty(markBatch)) {
                    markRepository.saveAll(markBatch);
                }
                if (CollectionUtils.isNotEmpty(marks)) {
                    // send to warehouse all bottles which parent is null
                    WarehousePayload payload = new WarehousePayload();
                    payload.setOrganizationId(shipment.getOrganizationId());
                    payload.setOperation(WarehouseOperation.Production);
                    payload.setNote(WarehouseOperation.Production.getNameRu());
                    payload.setUnit(PackageType.BOTTLE);
                    payload.setAlcoholCount(BigDecimal.valueOf(marks.size()));
                    payload.setProductId(shipmentItem.getProductId());
                    payload.setLogin(userLogin);
                    payload.setOperationDate(LocalDateTime.now());
                    kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC,payload);

                    payload.setOperation(WarehouseOperation.ShipmentBuyer);
                    payload.setNote("Отгружено");
                    log.debug("ShipmentBuyer payload UNIT {}", payload);
                    kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC,payload);
                }
            }
            // disaggregate parent boxes
            if (CollectionUtils.isNotEmpty(disAggregateParent)) {
                //update all aggregations
                List<Aggregation> aggregationList = aggregationRepository.findAllByUnitAndCodeIn(PackageType.BOX.getCode(), disAggregateParent);
                for (Aggregation aggregation : aggregationList) {

                    WarehousePayload payload = new WarehousePayload();
                    payload.setOperation(WarehouseOperation.Utilized);
                    payload.setNote("Дизагрегировано " + aggregation.getCode());
                    payload.setUnit(aggregation.getPackageType());
                    payload.setAlcoholCount(BigDecimal.valueOf(aggregation.getQuantity()));
                    payload.setOperationDate(LocalDateTime.now());
                    payload.setOrganizationId(aggregation.getOrganizationId());
                    payload.setProductId(aggregation.getProductId());
                    payload.setCode(aggregation.getCode());
                    payload.setLogin(userLogin);
                    kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC,payload);

                    aggregation.setQuantity(0);
                    aggregation.setStatus(AggregationStatus.DISAGGREGATED);
                    aggregationRepository.save(aggregation);

                    // if box content is mark
                    markRepository.findAllByParentCode(aggregation.getCode()).forEach(mark -> {
                        mark.setUsed(false);
                        mark.setStatus(null);
                        mark.setParentCode(null);
                        markRepository.save(mark);
                    });
                }

                List<Mark> byCodeIn = markRepository.findAllByCodeIn(disAggregateParent);
                for (Mark block : byCodeIn) {
                    WarehousePayload payload = new WarehousePayload();
                    payload.setOperation(WarehouseOperation.Utilized);
                    payload.setNote("Дизагрегировано " + block.getCode());
                    payload.setUnit(block.getUnit());
                    payload.setAlcoholCount(BigDecimal.valueOf(block.getQuantity()));
                    payload.setOperationDate(LocalDateTime.now());
                    payload.setOrganizationId(block.getOrganizationId());
                    payload.setProductId(block.getProductId());
                    payload.setCode(block.getCode());
                    payload.setLogin(userLogin);
                    kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC,payload);

                    block.setQuantity(0);
                    block.setStatus(AggregationStatus.DISAGGREGATED);
                    markRepository.save(block);

                    markRepository.findAllByParentCode(block.getCode()).forEach(mark -> {
                        mark.setUsed(false);
                        mark.setParentCode(null);
                        mark.setStatus(null);
                        markRepository.save(mark);
                    });
                }
            }
        }
    }

    /**
     * send aggregation to warehouse. WarehouseOperation is ShipmentBuyer
     *
     * @param aggregation @see {@link Aggregation}
     * @param shipment    @see {@link Shipment}
     * @param productId   @see {@link Product}
     */
    private void sendAggregationToWarehouse(Aggregation aggregation, Shipment shipment, String productId, String userLogin) {
        WarehousePayload payload = new WarehousePayload();
        payload.setOrganizationId(shipment.getOrganizationId());
        payload.setOperationDate(LocalDateTime.now());
        payload.setOperation(WarehouseOperation.ShipmentBuyer);
        payload.setNote("Отгружено");
        payload.setProductId(productId);
        payload.setUnit(aggregation.getPackageType());
        payload.setAlcoholCount(BigDecimal.valueOf(aggregation.getQuantity()));
        payload.setCode(aggregation.getCode());
        payload.setLogin(userLogin);
        kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC,payload);
    }

    /**
     * send block to warehouse. WarehouseOperation is ShipmentBuyer
     *
     * @param block    @see {@link Mark}
     * @param shipment @see {@link Shipment}
     */
    private void sendBlockToWarehouse(Mark block, Shipment shipment) {
        WarehousePayload payload = new WarehousePayload();
        payload.setOrganizationId(shipment.getOrganizationId());
        payload.setOperationDate(LocalDateTime.now());
        payload.setOperation(WarehouseOperation.ShipmentBuyer);
        payload.setNote("Отгружено");
        payload.setProductId(block.getProductId());
        payload.setUnit(PackageType.BLOCK);
        payload.setAlcoholCount(BigDecimal.valueOf(block.getQuantity()));
        payload.setCode(block.getCode());
        payload.setLogin(shipment.getLastModifiedBy());
        kafkaProducer.sendMessage(KafkaConstants.WAREHOUSE_TOPIC,payload);
    }

    public List<ShipmentItemDTO> sync(final String id) {
        final Shipment shipment = shipmentRepository.findById(id).orElseThrow(notFound());
        if (ShipmentStatus.CLOSED.equals(shipment.getStatus())) {
            throw badRequest("Отгрузка уже завершена").get();
        }
        final List<RedisShipmentItem> list = shipmentCodeRepository.findAllByShipmentIdOrderById(id);
        for (RedisShipmentItem item : list) {
            if (item != null && item.getTypes() != null) {
                shipmentCodeRepository.save(calculateQty(item));
            }
        }
        return list.stream().map(RedisShipmentItem::toDto).collect(Collectors.toList());
    }

    private RedisShipmentItem calculateQty(RedisShipmentItem item) {
        Integer qty = 0;
        final Map<PackageType, ShipmentCode> types = new LinkedHashMap<>();
        for (ShipmentCode code : item.getTypes()) {
            types.put(code.getUnit(), code);
        }
        final List<ShipmentMark> marks = shipmentMarkRepository.findAllByShipmentItemId(item.getId());
        final Map<Integer, Long> count = new HashMap<>();
        for (ShipmentMark mark : marks) {
            count.putIfAbsent(mark.getUnit(), 0L);
            count.put(mark.getUnit(), count.get(mark.getUnit()) + 1);
        }
        if (count.get(null) != null) {
            final Integer markCount = count.get(null).intValue();
            types.get(null).setCount(markCount);
            qty += markCount;
        }
        if (types.get(PackageType.BOX) != null && count.get(PackageType.BOX.getCode()) != null) {
            final Integer markCount = count.get(PackageType.BOX.getCode()).intValue();
            types.get(PackageType.BOX).setCount(markCount);
            qty += types.get(PackageType.BOX).getCapacity() * markCount;
        }
        if (types.get(PackageType.PALLET) != null && count.get(PackageType.PALLET.getCode()) != null) {
            final Integer markCount = count.get(PackageType.PALLET.getCode()).intValue();
            types.get(PackageType.BOX).setCount(markCount);
            qty += types.get(PackageType.BOX).getCapacity() * types.get(PackageType.PALLET).getCapacity() * markCount;
        }
        item.setTypes(new LinkedHashSet<>(types.values()));
        item.setQty(BigDecimal.valueOf(qty));
        return item;
    }
}

package uz.uzkassa.silen.service;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import uz.uzkassa.silen.domain.Carriages;
import uz.uzkassa.silen.domain.*;
import uz.uzkassa.silen.dto.WaybillListDTO;
import uz.uzkassa.silen.dto.eimzo.Pkcs7Dto;
import uz.uzkassa.silen.dto.eimzo.SignatureCertificateDto;
import uz.uzkassa.silen.dto.filter.CustomerFilter;
import uz.uzkassa.silen.dto.filter.WaybillFilter;
import uz.uzkassa.silen.dto.invoice.FacturaDTO;
import uz.uzkassa.silen.dto.invoice.FacturaProductDTO;
import uz.uzkassa.silen.dto.invoice.ResponseDTO;
import uz.uzkassa.silen.dto.waybill.*;
import uz.uzkassa.silen.enumeration.*;
import uz.uzkassa.silen.integration.EimzoClient;
import uz.uzkassa.silen.integration.FacturaClient;
import uz.uzkassa.silen.integration.SoliqClient;
import uz.uzkassa.silen.repository.*;
import uz.uzkassa.silen.security.SecurityUtils;
import uz.uzkassa.silen.exceptions.BadRequestException;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class WaybillService extends BaseService {

    private final WaybillRepository waybillRepository;
    private final WaybillProductRepository waybillProductRepository;
    private final WaybillProductGroupRepository waybillProductGroupRepository;
    private final CustomerRepository customerRepository;
    private final FacturaClient facturaClient;
    private final EimzoClient eimzoClient;
    private final WaybillExecutorRepository waybillExecutorRepository;
    private final ListInvoiceRepository listInvoiceRepository;
    private final AcceptanceTransferActRepository acceptanceTransferActRepository;
    private final OrganizationPermissionRepository permissionRepository;
    private final SoliqClient soliqClient;
    private final ProductRepository productRepository;
    private final ExciseRepository exciseRepository;

    @Transactional
    public String create(final WaybillDTO waybillDTO) {
        Waybill waybill = new Waybill();
        waybill.setWaybillType(WaybillType.OUTGOING);
        waybill.setOrganizationId(SecurityUtils.getCurrentOrganizationId());
        final ResponseDTO responseDTO = facturaClient.getGuid();
        waybill.setWaybillLocalId(responseDTO != null ? responseDTO.getData() : null);
        waybill.setDeliveryType(DeliveryType.fromCode(waybillDTO.getDeliveryType()));
        if (waybillDTO.getWaybillDoc() != null) {
            waybill.setWaybillNo(waybillDTO.getWaybillDoc().getWaybillNo());
            waybill.setWaybillDate(waybillDTO.getWaybillDoc().getWaybillDate());
        }
        if (waybillDTO.getContractDoc() != null) {
            waybill.setContractNo(waybillDTO.getContractDoc().getContractNo());
            waybill.setContractDate(waybillDTO.getContractDoc().getContractDate());
        }

        if (waybillDTO.getConsignor() != null) {
            waybill.setConsignorTinOrPinfl(waybillDTO.getConsignor().getTinOrPinfl());
            waybill.setConsignorName(waybillDTO.getConsignor().getName());
            waybill.setConsignorBranchCode(waybillDTO.getConsignor().getBranchCode());
            waybill.setConsignorBranchName(waybillDTO.getConsignor().getBranchName());
        }

        if (waybillDTO.getConsignee() != null) {
            waybill.setConsigneeTinOrPinfl(waybillDTO.getConsignee().getTinOrPinfl());
            waybill.setConsigneeName(waybillDTO.getConsignee().getName());
            waybill.setConsigneeBranchCode(waybillDTO.getConsignee().getBranchCode());
            waybill.setConsigneeBranchName(waybillDTO.getConsignee().getBranchName());
        }

        if (waybillDTO.getFreightForwarder() != null) {
            waybill.setFreightForwarderTinOrPinfl(waybillDTO.getFreightForwarder().getTinOrPinfl());
            waybill.setFreightForwarderName(waybillDTO.getFreightForwarder().getName());
            waybill.setFreightForwarderBranchCode(waybillDTO.getFreightForwarder().getBranchCode());
            waybill.setFreightForwarderBranchName(waybillDTO.getFreightForwarder().getBranchName());
        }

        if (waybillDTO.getCarrier() != null) {
            waybill.setCarrierTinOrPinfl(waybillDTO.getCarrier().getTinOrPinfl());
            waybill.setCarrierName(waybillDTO.getCarrier().getName());
            waybill.setCarrierBranchCode(waybillDTO.getCarrier().getBranchCode());
            waybill.setCarrierBranchName(waybillDTO.getCarrier().getBranchName());
        }
        if (waybillDTO.getClient() != null) {
            waybill.setClientTinOrPinfl(waybillDTO.getClient().getTinOrPinfl());
            waybill.setClientName(waybillDTO.getClient().getName());
            waybill.setClientBranchCode(waybillDTO.getClient().getBranchCode());
            waybill.setClientBranchName(waybillDTO.getClient().getBranchName());
            waybill.setClientContractNo(waybillDTO.getClient().getContractNo());
            waybill.setClientContractDate(waybillDTO.getClient().getContractDate());
        }

        if (waybillDTO.getPayer() != null) {
            waybill.setPayerTinOrPinfl(waybillDTO.getPayer().getTinOrPinfl());
            waybill.setPayerName(waybillDTO.getPayer().getName());
            waybill.setPayerBranchCode(waybillDTO.getPayer().getBranchCode());
            waybill.setPayerBranchName(waybillDTO.getPayer().getBranchName());
            waybill.setPayerContractNo(waybillDTO.getPayer().getContractNo());
            waybill.setPayerContractDate(waybillDTO.getPayer().getContractDate());
        }
        if (waybillDTO.getResponsiblePerson() != null) {
            waybill.setResponsiblePersonPinfl(waybillDTO.getResponsiblePerson().getPinfl());
            waybill.setResponsiblePersonFullName(waybillDTO.getResponsiblePerson().getFullName());
        }

        waybill.setHasCommittent(waybillDTO.getHasCommittent());
        waybill.setStatus(WaybillStatus.DRAFT);
        waybill.setTotalDistance(waybillDTO.getTotalDistance());
        waybill.setDeliveryCost(waybillDTO.getDeliveryCost());
        waybill.setTotalDeliveryCost(waybillDTO.getTotalDeliveryCost());
        waybill.setTransportType(TransportType.fromCode(waybillDTO.getTransportType()));

        // Roadway transport
        if (waybillDTO.getRoadway() != null) {
            final TransportDTO roadway = waybillDTO.getRoadway();
            WaybillTransport waybillTransport = new WaybillTransport();
            if (roadway.getDriver() != null) {
                waybillTransport.setDriverPinfl(roadway.getDriver().getPinfl());
                waybillTransport.setDriverFullName(roadway.getDriver().getPinfl());
            }
            if (roadway.getTruckDTO() != null) {
                waybillTransport.setTruckRegNo(roadway.getTruckDTO().getRegNo());
                waybillTransport.setTruckModel(roadway.getTruckDTO().getModel());
            }
            if (roadway.getTrailer() != null) {
                waybillTransport.setTrailerRegNo(roadway.getTrailer().getRegNo());
                waybillTransport.setTrailerModel(roadway.getTrailer().getModel());
            }
            waybillTransport.setTrailerType(roadway.getTrailerType());
            waybillTransport.setTransportType(TransportType.fromCode(waybillDTO.getTransportType()));

            if (!CollectionUtils.isEmpty(roadway.getCarriages())) {
                for (TruckDTO truckDTO : roadway.getCarriages()) {
                    Carriages carriages = new Carriages();
                    carriages.setModel(truckDTO.getModel());
                    carriages.setRegNo(truckDTO.getRegNo());
                    waybillTransport.getCarriages().add(carriages);
                }
            }
            for (ProductGroupDTO groupDTO : roadway.getProductGroups()) {
                WaybillProductGroups productGroups = new WaybillProductGroups();
                if (groupDTO.getLoadingTrustee() != null) {
                    productGroups.setLoadingTrusteePinfl(groupDTO.getLoadingTrustee().getPinfl());
                    productGroups.setLoadingTrusteeName(groupDTO.getLoadingTrustee().getFullName());
                }
                if (groupDTO.getLoadingPoint() != null) {
                    productGroups.setLoadingRegionId(groupDTO.getLoadingPoint().getRegionId());
                    productGroups.setLoadingRegionName(groupDTO.getLoadingPoint().getRegionName());
                    productGroups.setLoadingDistrictId(groupDTO.getLoadingPoint().getDistrictCode());
                    productGroups.setLoadingDistrictName(groupDTO.getLoadingPoint().getDistrictName());
                    productGroups.setLoadingMahallaId(groupDTO.getLoadingPoint().getMahallaId());
                    productGroups.setLoadingMahallaName(groupDTO.getLoadingPoint().getMahallaName());
                    productGroups.setLoadingAddress(groupDTO.getLoadingPoint().getAddress());
                    productGroups.setLoadingLongitude(groupDTO.getLoadingPoint().getLongitude());
                    productGroups.setLoadingLatitude(groupDTO.getLoadingPoint().getLatitude());
                }

                if (groupDTO.getUnloadingTrustee() != null) {
                    productGroups.setUnloadingTrusteePinfl(groupDTO.getUnloadingTrustee().getPinfl());
                    productGroups.setUnloadingTrusteeName(groupDTO.getUnloadingTrustee().getFullName());
                }
                if (groupDTO.getUnloadingPoint() != null) {
                    productGroups.setUnloadingRegionId(groupDTO.getUnloadingPoint().getRegionId());
                    productGroups.setUnloadingRegionName(groupDTO.getUnloadingPoint().getRegionName());
                    productGroups.setUnloadingDistrictId(groupDTO.getUnloadingPoint().getDistrictCode());
                    productGroups.setUnloadingDistrictName(groupDTO.getUnloadingPoint().getDistrictName());
                    productGroups.setUnloadingMahallaId(groupDTO.getUnloadingPoint().getMahallaId());
                    productGroups.setUnloadingMahallaName(groupDTO.getUnloadingPoint().getMahallaName());
                    productGroups.setUnloadingAddress(groupDTO.getUnloadingPoint().getAddress());
                    productGroups.setUnloadingLongitude(groupDTO.getUnloadingPoint().getLongitude());
                    productGroups.setUnloadingLatitude(groupDTO.getUnloadingPoint().getLatitude());
                }

                if (groupDTO.getUnloadingEmpowermentDTO() != null) {
                    productGroups.setUnloadingEmpowermentId(groupDTO.getUnloadingEmpowermentDTO().getEmpowermentId());
                    productGroups.setUnloadingEmpowermentNo(groupDTO.getUnloadingEmpowermentDTO().getEmpowermentNo());
                    productGroups.setUnloadingEmpowermentDateOfExpire(groupDTO.getUnloadingEmpowermentDTO().getEmpowermentDateOfExpire());
                    productGroups.setUnloadingEmpowermentDateOfIssue(groupDTO.getUnloadingEmpowermentDTO().getEmpowermentDateOfIssue());
                }

                if (groupDTO.getProductInfoDTO() != null && !CollectionUtils.isEmpty(groupDTO.getProductInfoDTO().getProducts())) {
                    productGroups.setTotalDeliverySum(groupDTO.getProductInfoDTO().getTotalDeliverySum());
                    productGroups.setTotalWeightBrutto(groupDTO.getProductInfoDTO().getTotalWeightBrutto());
                    for (Products product : groupDTO.getProductInfoDTO().getProducts()) {
                        WaybillProduct waybillProduct = new WaybillProduct();
                        waybillProduct.setOrdNo(product.getOrdNo());
                        waybillProduct.setCommittentTinOrPinfl(product.getCommittentTinOrPinfl());
                        waybillProduct.setCommittentName(product.getCommittentName());
                        waybillProduct.setProductName(product.getProductName());
                        waybillProduct.setCatalogCode(product.getCatalogCode());
                        waybillProduct.setCatalogName(product.getCatalogName());
                        waybillProduct.setPackageCode(product.getPackageCode());
                        waybillProduct.setPackageName(product.getPackageName());
                        waybillProduct.setAmount(product.getAmount());
                        waybillProduct.setPrice(product.getPrice());
                        waybillProduct.setDeliverySum(product.getDeliverySum());
                        waybillProduct.setWeightNetto(product.getWeightNetto());
                        waybillProduct.setWeightBrutto(product.getWeightBrutto());
                        waybillProduct.setProductId(product.getProductId());
                        waybillProduct.setProductGroup(productGroups);

                        productGroups.getProducts().add(waybillProduct);
                    }
                }

                productGroups.setWaybillTransport(waybillTransport);
                waybillTransport.getProductGroups().add(productGroups);
                waybill.setWaybillTransport(waybillTransport);
            }
            waybillTransport.setWaybill(waybill);
        }
        waybillRepository.save(waybill);

        return waybill.getId();
    }

    public String update(String id, final WaybillDTO waybillDTO) {
        Waybill waybill = waybillRepository.findById(id).orElseThrow(notFound("ТНН не найден"));
        waybill.setDeliveryType(DeliveryType.fromCode(waybillDTO.getDeliveryType()));
        if (waybillDTO.getWaybillDoc() != null) {
            waybill.setWaybillNo(waybillDTO.getWaybillDoc().getWaybillNo());
            waybill.setWaybillDate(waybillDTO.getWaybillDoc().getWaybillDate());
        }
        if (waybillDTO.getContractDoc() != null) {
            waybill.setContractNo(waybillDTO.getContractDoc().getContractNo());
            waybill.setContractDate(waybillDTO.getContractDoc().getContractDate());
        } else {
            waybill.setContractNo(null);
            waybill.setContractDate(null);
        }

        if (waybillDTO.getConsignor() != null) {
            waybill.setConsignorTinOrPinfl(waybillDTO.getConsignor().getTinOrPinfl());
            waybill.setConsignorName(waybillDTO.getConsignor().getName());
            waybill.setConsignorBranchCode(waybillDTO.getConsignor().getBranchCode());
            waybill.setConsignorBranchName(waybillDTO.getConsignor().getBranchName());
        }

        if (waybillDTO.getConsignee() != null) {
            waybill.setConsigneeTinOrPinfl(waybillDTO.getConsignee().getTinOrPinfl());
            waybill.setConsigneeName(waybillDTO.getConsignee().getName());
            waybill.setConsigneeBranchCode(waybillDTO.getConsignee().getBranchCode());
            waybill.setConsigneeBranchName(waybillDTO.getConsignee().getBranchName());
        }

        if (waybillDTO.getFreightForwarder() != null) {
            waybill.setFreightForwarderTinOrPinfl(waybillDTO.getFreightForwarder().getTinOrPinfl());
            waybill.setFreightForwarderName(waybillDTO.getFreightForwarder().getName());
            waybill.setFreightForwarderBranchCode(waybillDTO.getFreightForwarder().getBranchCode());
            waybill.setFreightForwarderBranchName(waybillDTO.getFreightForwarder().getBranchName());
        } else {
            waybill.setFreightForwarderTinOrPinfl(null);
            waybill.setFreightForwarderName(null);
            waybill.setFreightForwarderBranchCode(null);
            waybill.setFreightForwarderBranchName(null);
        }
        if (waybillDTO.getCarrier() != null) {
            waybill.setCarrierTinOrPinfl(waybillDTO.getCarrier().getTinOrPinfl());
            waybill.setCarrierName(waybillDTO.getCarrier().getName());
            waybill.setCarrierBranchCode(waybillDTO.getCarrier().getBranchCode());
            waybill.setCarrierBranchName(waybillDTO.getCarrier().getBranchName());
        } else {
            waybill.setCarrierTinOrPinfl(null);
            waybill.setCarrierName(null);
            waybill.setCarrierBranchCode(null);
            waybill.setCarrierBranchName(null);
        }
        if (waybillDTO.getClient() != null) {
            waybill.setClientTinOrPinfl(waybillDTO.getClient().getTinOrPinfl());
            waybill.setClientName(waybillDTO.getClient().getName());
            waybill.setClientBranchCode(waybillDTO.getClient().getBranchCode());
            waybill.setClientBranchName(waybillDTO.getClient().getBranchName());
            waybill.setClientContractNo(waybillDTO.getClient().getContractNo());
            waybill.setClientContractDate(waybillDTO.getClient().getContractDate());
        } else {
            waybill.setClientTinOrPinfl(null);
            waybill.setClientName(null);
            waybill.setClientBranchCode(null);
            waybill.setClientBranchName(null);
            waybill.setClientContractNo(null);
            waybill.setClientContractDate(null);
        }

        if (waybillDTO.getPayer() != null) {
            waybill.setPayerTinOrPinfl(waybillDTO.getPayer().getTinOrPinfl());
            waybill.setPayerName(waybillDTO.getPayer().getName());
            waybill.setPayerBranchCode(waybillDTO.getPayer().getBranchCode());
            waybill.setPayerBranchName(waybillDTO.getPayer().getBranchName());
            waybill.setPayerContractNo(waybillDTO.getPayer().getContractNo());
            waybill.setPayerContractDate(waybillDTO.getPayer().getContractDate());
        } else {
            waybill.setPayerTinOrPinfl(null);
            waybill.setPayerName(null);
            waybill.setPayerBranchCode(null);
            waybill.setPayerBranchName(null);
            waybill.setPayerContractNo(null);
            waybill.setPayerContractDate(null);
        }
        if (waybillDTO.getResponsiblePerson() != null) {
            waybill.setResponsiblePersonPinfl(waybillDTO.getResponsiblePerson().getPinfl());
            waybill.setResponsiblePersonFullName(waybillDTO.getResponsiblePerson().getFullName());
        } else {
            waybill.setResponsiblePersonPinfl(null);
            waybill.setResponsiblePersonFullName(null);

        }

        waybill.setHasCommittent(waybillDTO.getHasCommittent());
        waybill.setStatus(WaybillStatus.DRAFT);
        waybill.setTotalDistance(waybillDTO.getTotalDistance());
        waybill.setDeliveryCost(waybillDTO.getDeliveryCost());
        waybill.setTotalDeliveryCost(waybillDTO.getTotalDeliveryCost());
        waybill.setTransportType(TransportType.fromCode(waybillDTO.getTransportType()));

        // Roadway transport
        if (waybillDTO.getRoadway() != null) {
            final TransportDTO roadwayDTO = waybillDTO.getRoadway();
            WaybillTransport waybillTransport = Optional.ofNullable(waybill.getWaybillTransport()).orElse(new WaybillTransport());
            if (roadwayDTO.getDriver() != null) {
                waybillTransport.setDriverPinfl(roadwayDTO.getDriver().getPinfl());
                waybillTransport.setDriverFullName(roadwayDTO.getDriver().getPinfl());
            } else {
                waybillTransport.setDriverPinfl(null);
                waybillTransport.setDriverFullName(null);
            }
            if (roadwayDTO.getTruckDTO() != null) {
                waybillTransport.setTruckRegNo(roadwayDTO.getTruckDTO().getRegNo());
                waybillTransport.setTruckModel(roadwayDTO.getTruckDTO().getModel());
            } else {
                waybillTransport.setTruckRegNo(null);
                waybillTransport.setTruckModel(null);

            }
            if (roadwayDTO.getTrailer() != null) {
                waybillTransport.setTrailerRegNo(roadwayDTO.getTrailer().getRegNo());
                waybillTransport.setTrailerModel(roadwayDTO.getTrailer().getModel());
            } else {
                waybillTransport.setTrailerRegNo(null);
                waybillTransport.setTrailerModel(null);
            }
            waybillTransport.setTrailerType(roadwayDTO.getTrailerType());
            waybillTransport.setTransportType(TransportType.fromCode(waybillDTO.getTransportType()));

            waybillTransport.getCarriages().clear();
            if (!CollectionUtils.isEmpty(roadwayDTO.getCarriages())) {
                for (TruckDTO truckDTO : roadwayDTO.getCarriages()) {
                    Carriages carriages = new Carriages();
                    carriages.setModel(truckDTO.getModel());
                    carriages.setRegNo(truckDTO.getRegNo());
                    waybillTransport.getCarriages().add(carriages);
                }
            }
            if (!CollectionUtils.isEmpty(roadwayDTO.getProductGroups())) {
                for (ProductGroupDTO groupDTO : roadwayDTO.getProductGroups()) {
                    WaybillProductGroups productGroups = waybillTransport.getProductGroups().stream()
                        .filter(waybillProductGroups -> Objects.equals(groupDTO.getId(), waybillProductGroups.getId()))
                        .findFirst().orElse(new WaybillProductGroups());
                    if (groupDTO.getLoadingTrustee() != null) {
                        productGroups.setLoadingTrusteePinfl(groupDTO.getLoadingTrustee().getPinfl());
                        productGroups.setLoadingTrusteeName(groupDTO.getLoadingTrustee().getFullName());
                    } else {
                        productGroups.setLoadingTrusteePinfl(null);
                        productGroups.setLoadingTrusteeName(null);
                    }
                    if (groupDTO.getLoadingPoint() != null) {
                        productGroups.setLoadingRegionId(groupDTO.getLoadingPoint().getRegionId());
                        productGroups.setLoadingRegionName(groupDTO.getLoadingPoint().getRegionName());
                        productGroups.setLoadingDistrictId(groupDTO.getLoadingPoint().getDistrictCode());
                        productGroups.setLoadingDistrictName(groupDTO.getLoadingPoint().getDistrictName());
                        productGroups.setLoadingMahallaId(groupDTO.getLoadingPoint().getMahallaId());
                        productGroups.setLoadingMahallaName(groupDTO.getLoadingPoint().getMahallaName());
                        productGroups.setLoadingAddress(groupDTO.getLoadingPoint().getAddress());
                        productGroups.setLoadingLongitude(groupDTO.getLoadingPoint().getLongitude());
                        productGroups.setLoadingLatitude(groupDTO.getLoadingPoint().getLatitude());
                    }
                    if (groupDTO.getUnloadingTrustee() != null) {
                        productGroups.setUnloadingTrusteePinfl(groupDTO.getUnloadingTrustee().getPinfl());
                        productGroups.setUnloadingTrusteeName(groupDTO.getUnloadingTrustee().getFullName());
                    } else {
                        productGroups.setUnloadingTrusteePinfl(null);
                        productGroups.setUnloadingTrusteeName(null);
                    }
                    if (groupDTO.getUnloadingPoint() != null) {
                        productGroups.setUnloadingRegionId(groupDTO.getUnloadingPoint().getRegionId());
                        productGroups.setUnloadingRegionName(groupDTO.getUnloadingPoint().getRegionName());
                        productGroups.setUnloadingDistrictId(groupDTO.getUnloadingPoint().getDistrictCode());
                        productGroups.setUnloadingDistrictName(groupDTO.getUnloadingPoint().getDistrictName());
                        productGroups.setUnloadingMahallaId(groupDTO.getUnloadingPoint().getMahallaId());
                        productGroups.setUnloadingMahallaName(groupDTO.getUnloadingPoint().getMahallaName());
                        productGroups.setUnloadingAddress(groupDTO.getUnloadingPoint().getAddress());
                        productGroups.setUnloadingLongitude(groupDTO.getUnloadingPoint().getLongitude());
                        productGroups.setUnloadingLatitude(groupDTO.getUnloadingPoint().getLatitude());
                    }
                    if (groupDTO.getUnloadingEmpowermentDTO() != null) {
                        productGroups.setUnloadingEmpowermentId(groupDTO.getUnloadingEmpowermentDTO().getEmpowermentId());
                        productGroups.setUnloadingEmpowermentNo(groupDTO.getUnloadingEmpowermentDTO().getEmpowermentNo());
                        productGroups.setUnloadingEmpowermentDateOfExpire(groupDTO.getUnloadingEmpowermentDTO().getEmpowermentDateOfExpire());
                        productGroups.setUnloadingEmpowermentDateOfIssue(groupDTO.getUnloadingEmpowermentDTO().getEmpowermentDateOfIssue());
                    } else {
                        productGroups.setUnloadingEmpowermentId(null);
                        productGroups.setUnloadingEmpowermentNo(null);
                        productGroups.setUnloadingEmpowermentDateOfExpire(null);
                        productGroups.setUnloadingEmpowermentDateOfIssue(null);
                    }
                    if (groupDTO.getProductInfoDTO() != null && !CollectionUtils.isEmpty(groupDTO.getProductInfoDTO().getProducts())) {
                        productGroups.setTotalDeliverySum(groupDTO.getProductInfoDTO().getTotalDeliverySum());
                        productGroups.setTotalWeightBrutto(groupDTO.getProductInfoDTO().getTotalWeightBrutto());
                        for (Products product : groupDTO.getProductInfoDTO().getProducts()) {
                            WaybillProduct waybillProduct = productGroups.getProducts().stream()
                                .filter(waybillProduct1 -> Objects.equals(waybillProduct1.getId(), product.getId()))
                                .findFirst()
                                .orElse(new WaybillProduct());
                            waybillProduct.setOrdNo(product.getOrdNo());
                            waybillProduct.setCommittentTinOrPinfl(product.getCommittentTinOrPinfl());
                            waybillProduct.setCommittentName(product.getCommittentName());
                            waybillProduct.setProductName(product.getProductName());
                            waybillProduct.setCatalogCode(product.getCatalogCode());
                            waybillProduct.setCatalogName(product.getCatalogName());
                            waybillProduct.setPackageCode(product.getPackageCode());
                            waybillProduct.setPackageName(product.getPackageName());
                            waybillProduct.setAmount(product.getAmount());
                            waybillProduct.setPrice(product.getPrice());
                            waybillProduct.setDeliverySum(product.getDeliverySum());
                            waybillProduct.setWeightNetto(product.getWeightNetto());
                            waybillProduct.setWeightBrutto(product.getWeightBrutto());
                            waybillProduct.setProductId(product.getProductId());
                            waybillProduct.setProductGroup(productGroups);
                            productGroups.getProducts().add(waybillProduct);
                        }
                    }

                    productGroups.setWaybillTransport(waybillTransport);
                    waybillTransport.getProductGroups().add(productGroups);
                    waybill.setWaybillTransport(waybillTransport);
                }
            }

            waybillTransport.setWaybill(waybill);
        }
        waybillRepository.save(waybill);

        return waybill.getId();
    }

    public Page<WaybillListDTO> findAllByFilter(WaybillFilter filter) {
        return waybillRepository.findAllByFilter(filter)
            .map(Waybill::toListDTO);
    }

    public WaybillDTO getOne(String id) {
        Waybill waybill = waybillRepository.findById(id).orElseThrow(notFound("ТТЮ не найден"));
        return waybill.toDTO();
    }

    @Transactional
    public void send(WaybillSendDTO sendDTO) {
        final Organization organization = organizationRepository.findById(getCurrentOrganizationId())
            .orElseThrow(() -> new BadRequestException("Организация не найден"));
        Optional<OrganizationPermissions> invoiceBalance = Optional.empty();
        if (!organization.isDemoAllowed()) {
            invoiceBalance = permissionRepository.findFirstByOrganizationIdAndPermissionsKeyEqualsAndCountGreaterThan(organization.getId(), ServiceType.E_INVOICE.name(), 0);
            if (!invoiceBalance.isPresent()) {
                throw badRequest("Недостаточно средств на балансе").get();
            }
        }
        final UUID requestId = UUID.randomUUID();
        log.info("Waybill send ============ ID: {}, ============ REQUEST: {} ============", requestId, sendDTO);

        Waybill waybill = waybillRepository.findFirstByWaybillLocalId(sendDTO.getWaybillLocalId())
            .orElseThrow(() -> new BadRequestException("ТТЮ не найдена"));
        final Pkcs7Dto pkcs7Dto = eimzoClient.frontTimestamp(sendDTO.getSign());
        if (pkcs7Dto.isSuccess() && org.apache.commons.collections4.CollectionUtils.isNotEmpty(pkcs7Dto.getTimestampedSignerList())) {
            final String sign = pkcs7Dto.getPkcs7b64();
            final SignatureCertificateDto certificateDto = pkcs7Dto.getTimestampedSignerList().get(0);

            sendDTO.setSign(sign);
            facturaClient.sendWaybill(
                SecurityUtils.getCurrentLocale(),
                getCurrentOrganizationTin(),
                sendDTO,
                requestId,
                sendDTO.getWaybillLocalId()
            );

            waybill.setSignatureContent(sendDTO.getSign());
            waybill.setStatus(WaybillStatus.ConsignorSent);
            waybillRepository.save(waybill);

            WaybillExecutor waybillExecutor = new WaybillExecutor();
            waybillExecutor.setWaybillId(waybill.getId());
            waybillExecutor.setStatus(WaybillStatus.ConsignorSent);
            waybillExecutor.setOperationNumber(Integer.parseInt(certificateDto.getSerialNumber(), 16) + "");
            waybillExecutor.setOperationBy(certificateDto.getSubjectName().getFullName());
            waybillExecutor.setOperationDate(LocalDateTime.now());
            waybillExecutorRepository.save(waybillExecutor);
        } else {
            log.debug("JSON: {}", pkcs7Dto);
            throw new BadRequestException("Not verified, reason: " + pkcs7Dto.getMessage());
        }
        invoiceBalance.ifPresent(balance -> {
            balance.setCount(balance.getCount() - 1);
            permissionRepository.save(balance);
        });
    }

    @Transactional
    public void cancel(WaybillSendDTO waybillSendDTO) {
        Waybill waybill = waybillRepository.findFirstByWaybillLocalId(waybillSendDTO.getWaybillLocalId())
            .orElseThrow(() -> new BadRequestException("ТТЮ не найдена"));
        if (WaybillStatus.ConsigneeAccepted != waybill.getStatus()) {
            throw new BadRequestException("Невозможно отменить ТТЮ со статусом " + waybill.getStatus());
        }
        final Pkcs7Dto pkcs7Dto = eimzoClient.frontTimestamp(waybillSendDTO.getSign());
        if (pkcs7Dto.isSuccess() && org.apache.commons.collections4.CollectionUtils.isNotEmpty(pkcs7Dto.getTimestampedSignerList())) {
            final SignatureCertificateDto certificateDto = pkcs7Dto.getTimestampedSignerList().size() > 1 ?
                pkcs7Dto.getTimestampedSignerList().get(1) : pkcs7Dto.getTimestampedSignerList().get(0);

            waybillSendDTO.setSign(pkcs7Dto.getPkcs7b64());
            facturaClient.cancelWaybill(SecurityUtils.getCurrentLocale(), getCurrentOrganizationTin(), waybillSendDTO, waybill.getWaybillLocalId());

            waybill.setStatus(WaybillStatus.ConsignorCanceled);
            waybill.setNotes(waybillSendDTO.getNotes());
            waybillRepository.save(waybill);
            WaybillExecutor waybillExecutor = new WaybillExecutor();
            waybillExecutor.setWaybillId(waybill.getId());
            waybillExecutor.setStatus(WaybillStatus.ConsignorCanceled);
            waybillExecutor.setOperationNumber(Integer.parseInt(certificateDto.getSerialNumber(), 16) + "");
            waybillExecutor.setOperationBy(certificateDto.getSubjectName().getFullName());
            waybillExecutor.setOperationDate(LocalDateTime.now());
            waybillExecutorRepository.save(waybillExecutor);
        } else {
            log.debug("JSON: {}", pkcs7Dto);
            throw new BadRequestException("Not verified, reason: " + pkcs7Dto.getMessage());
        }
    }

    @Transactional
    public void acceptOrReject(WaybillSendDTO waybillSendDTO, final WaybillStatus status) {
        Waybill waybill = waybillRepository.findFirstByWaybillLocalId(waybillSendDTO.getWaybillLocalId())
            .orElseThrow(() -> new BadRequestException("ТТЮ не найдена"));
        if (WaybillStatus.ConsignorSent != waybill.getStatus()) {
            throw new BadRequestException("Невозможно отменить ТТЮ со статусом " + waybill.getStatus());
        }
        final Pkcs7Dto pkcs7Dto = eimzoClient.frontTimestamp(waybillSendDTO.getSign());
        if (pkcs7Dto.isSuccess() && org.apache.commons.collections4.CollectionUtils.isNotEmpty(pkcs7Dto.getTimestampedSignerList())) {
            final SignatureCertificateDto certificateDto = pkcs7Dto.getTimestampedSignerList().size() > 1 ?
                pkcs7Dto.getTimestampedSignerList().get(1) : pkcs7Dto.getTimestampedSignerList().get(0);

            waybillSendDTO.setSign(pkcs7Dto.getPkcs7b64());
            if (WaybillStatus.ConsigneeAccepted == status) {
                facturaClient.acceptWaybill(SecurityUtils.getCurrentLocale(), getCurrentOrganizationTin(), waybillSendDTO, waybillSendDTO.getWaybillLocalId());
                waybill.setStatus(WaybillStatus.ConsigneeAccepted);
            } else {
                facturaClient.rejectWaybill(SecurityUtils.getCurrentLocale(), getCurrentOrganizationTin(), waybillSendDTO, waybillSendDTO.getWaybillLocalId());
                waybill.setStatus(WaybillStatus.ConsigneeRejected);
            }


            waybill.setNotes(waybillSendDTO.getNotes());
            waybillRepository.save(waybill);
            WaybillExecutor waybillExecutor = new WaybillExecutor();
            waybillExecutor.setWaybillId(waybill.getId());
            waybillExecutor.setStatus(waybill.getStatus());
            waybillExecutor.setOperationNumber(Integer.parseInt(certificateDto.getSerialNumber(), 16) + "");
            waybillExecutor.setOperationBy(certificateDto.getSubjectName().getFullName());
            waybillExecutor.setOperationDate(LocalDateTime.now());
            waybillExecutorRepository.save(waybillExecutor);
        } else {
            log.debug("JSON: {}", pkcs7Dto);
            throw new BadRequestException("Not verified, reason: " + pkcs7Dto.getMessage());
        }
    }
    @Transactional
    public String convertInvoice(final Long id) {
        return listInvoiceRepository.findById(id)
            .filter(inv -> !inv.isDeleted())
            .map(invoice -> {
                if (Status.DRAFT.equals(invoice.getStatus())) {
                    return createFromInvoiceDraft(invoice);
                } else {
                    return createFromInvoiceSign(convertToDto(invoice.getSignatureContent()), invoice.getId());
                }
            })
            .orElseThrow(() -> new BadRequestException("С таким идентификатором СФ не найден"));
    }

    private FacturaDTO convertToDto(final String sign) {
        final Pkcs7Dto pkcs7Dto = eimzoClient.backendVerifyAttached(sign);
        if (pkcs7Dto.isSuccess()) {
            return parseBase64(pkcs7Dto.getPkcs7Info().getDocumentBase64());
        } else {
            throw new BadRequestException(pkcs7Dto.getMessage());
        }
    }


    public String createFromInvoiceDraft(ListInvoice listInvoice) {
        final Invoice invoice = listInvoice.getInvoice();
        if (invoice == null) {
            throw badRequest("СФ не найден").get();
        }
        final Organization seller = Optional.ofNullable(getCurrentOrganization()).orElseThrow(notFound("Продавец не найден"));
        final Customer buyer = customerRepository.findFirstByOrganizationIdAndTinAndDeletedIsFalse(seller.getId(), listInvoice.getBuyerTin()).orElseThrow(badRequest("Покупатель не найден"));

        Waybill waybill = new Waybill();
        waybill.setInvoiceId(listInvoice.getId());
        waybill.setWaybillType(WaybillType.OUTGOING);
        waybill.setOrganizationId(seller.getId());
        final ResponseDTO responseDTO = facturaClient.getGuid();
        waybill.setWaybillLocalId(responseDTO != null ? responseDTO.getData() : null);
        waybill.setDeliveryType(DeliveryType.DELIVERY);

        waybill.setWaybillNo(invoice.getNumber());
        waybill.setWaybillDate(invoice.getInvoiceDate());
        waybill.setContractNo(invoice.getContractNumber());
        waybill.setContractDate(invoice.getContractDate());

        waybill.setConsignorTinOrPinfl(seller.getTin());
        waybill.setConsignorName(seller.getName());

        waybill.setConsigneeTinOrPinfl(buyer.getTin());
        waybill.setConsigneeName(buyer.getName());

        waybill.setHasCommittent(invoice.getHasComittent());
        waybill.setStatus(WaybillStatus.DRAFT);
        waybill.setTotalDistance(BigDecimal.ZERO);
        waybill.setDeliveryCost(BigDecimal.ZERO);
        waybill.setTotalDeliveryCost(BigDecimal.ZERO);

        // converting to RoadwayTransport
        waybill.setTransportType(TransportType.Roadway);
        if (TransportType.Roadway == waybill.getTransportType()) {
            WaybillTransport roadwayTransport = new WaybillTransport();
            roadwayTransport.setTransportType(TransportType.Roadway);

            if (!CollectionUtils.isEmpty(invoice.getItems())) {
                WaybillProductGroups productGroups = new WaybillProductGroups();
                productGroups.setTotalDeliverySum(BigDecimal.ZERO);
                productGroups.setTotalWeightBrutto(BigDecimal.ZERO);

                if (seller.getDistrict() != null) {
                    productGroups.setLoadingRegionId(seller.getDistrict().getRegionId().intValue());
                    productGroups.setLoadingRegionName(seller.getDistrict().getRegion().getNameLocale());
                    productGroups.setLoadingDistrictId(seller.getDistrict().getCode().intValue());
                    productGroups.setLoadingDistrictName(seller.getDistrict().getNameLocale());
                }
                productGroups.setLoadingAddress(seller.getAddress());
                productGroups.setLoadingLongitude(Optional.ofNullable(seller.getLon()).orElse(0.0));
                productGroups.setLoadingLatitude(Optional.ofNullable(seller.getLat()).orElse(0.0));

                if (buyer.getDistrict() != null) {
                    productGroups.setUnloadingRegionId(buyer.getDistrict().getRegionId().intValue());
                    productGroups.setUnloadingRegionName(buyer.getDistrict().getRegion().getName());
                    productGroups.setUnloadingDistrictId(buyer.getDistrict().getCode().intValue());
                    productGroups.setUnloadingDistrictName(buyer.getDistrict().getName());
                    productGroups.setUnloadingAddress(buyer.getAddress());
                    productGroups.setUnloadingLongitude(buyer.getLon());
                    productGroups.setUnloadingLatitude(buyer.getLat());
                }

                if (invoice.getEmpowermentNo() != null) {
                    productGroups.setUnloadingEmpowermentId(null);
                    productGroups.setUnloadingEmpowermentNo(invoice.getEmpowermentNo());
                    productGroups.setUnloadingEmpowermentDateOfExpire(invoice.getEmpowermentDateOfExpire());
                    productGroups.setUnloadingEmpowermentDateOfIssue(invoice.getEmpowermentDateOfIssue());
                }

                for (InvoiceItem invoiceItem : invoice.getItems()) {
                    if (Objects.equals(invoiceItem.getCatalogCode(), "10499001018000000")) continue;
                    WaybillProduct waybillProduct = new WaybillProduct();
                    waybillProduct.setOrdNo(Integer.parseInt(invoiceItem.getOrderNo()));
                    waybillProduct.setCommittentTinOrPinfl(invoiceItem.getComittentTin());
                    waybillProduct.setCommittentName(invoiceItem.getComittentName());
                    waybillProduct.setProductName(invoiceItem.getName());
                    waybillProduct.setCatalogCode(invoiceItem.getCatalogCode());
                    waybillProduct.setCatalogName(invoiceItem.getCatalogName());
                    waybillProduct.setPackageCode(invoiceItem.getPackageCode());
                    waybillProduct.setPackageName(invoiceItem.getPackageName());
                    waybillProduct.setAmount(invoiceItem.getQty());
                    waybillProduct.setPrice(invoiceItem.getPrice());

                    waybillProduct.setProductWeightNetto(BigDecimal.ZERO);
                    waybillProduct.setProductWeightBrutto(BigDecimal.ZERO);
                    productRepository.findFirstByBarcodeAndOrganizationIdAndDeletedIsFalse(invoiceItem.getBarcode(), seller.getId())
                        .ifPresent(product -> {
                            waybillProduct.setProductWeightNetto(product.getWeightNetto());
                            waybillProduct.setProductWeightBrutto(product.getWeightBrutto());
                            final Optional<Excise> excise = exciseRepository.findOneByType(product.getType());
                            if (excise.isPresent() && excise.get().getAmount().compareTo(BigDecimal.ZERO) != 0) {
                                final BigDecimal exciseRate = excise.get().calculateRate(product.getCapacity(), product.getTitleAlcohol());
                                waybillProduct.setPrice(waybillProduct.getPrice().add(exciseRate));
                            }
                        });
                    waybillProduct.setDeliverySum(waybillProduct.getPrice().multiply(waybillProduct.getAmount()));
                    waybillProduct.setWeightNetto(waybillProduct.getAmount().multiply(waybillProduct.getProductWeightNetto()));
                    waybillProduct.setWeightBrutto(waybillProduct.getAmount().multiply(waybillProduct.getProductWeightBrutto()));

                    productGroups.setTotalDeliverySum(productGroups.getTotalDeliverySum().add(waybillProduct.getDeliverySum()));
                    productGroups.setTotalWeightBrutto(productGroups.getTotalWeightBrutto().add(waybillProduct.getWeightBrutto()));

                    // relation for WaybillProduct and WaybillProductGroup
                    productGroups.addProduct(waybillProduct);
                }

                // relation WaybillProductGroup with WaybillTransport
                roadwayTransport.addProductGroups(productGroups);
            }

            // relation WaybillTransport with Waybill
            waybill.addWaybillTransport(roadwayTransport);
        }
        waybillRepository.save(waybill);
        return waybill.getId();
    }

    public String createFromInvoiceSign(FacturaDTO facturaDTO, Long id) {

        final Organization seller = Optional.ofNullable(getCurrentOrganization()).orElseThrow(notFound("Продавец не найден"));

        Waybill waybill = new Waybill();
        waybill.setInvoiceId(id);
        waybill.setWaybillType(WaybillType.OUTGOING);
        waybill.setOrganizationId(SecurityUtils.getCurrentOrganizationId());
        final ResponseDTO responseDTO = facturaClient.getGuid();
        waybill.setWaybillLocalId(responseDTO != null ? responseDTO.getData() : null);
        waybill.setDeliveryType(DeliveryType.DELIVERY);

        if (facturaDTO.getFacturaDoc() != null) {
            waybill.setWaybillNo(facturaDTO.getFacturaDoc().getFacturaNo());
            waybill.setWaybillDate(facturaDTO.getFacturaDoc().getFacturaDate());
        }
        if (facturaDTO.getContractDoc() != null) {
            waybill.setContractNo(facturaDTO.getContractDoc().getContractNo());
            waybill.setContractDate(facturaDTO.getContractDoc().getContractDate());
        }

        if (facturaDTO.getSeller() != null) {
            waybill.setConsignorTinOrPinfl(facturaDTO.getSellerTin());
            waybill.setConsignorName(facturaDTO.getSeller().getName());
            waybill.setConsignorBranchCode(facturaDTO.getSeller().getBranchCode());
            waybill.setConsignorBranchCode(facturaDTO.getSeller().getBranchName());
        }

        if (facturaDTO.getBuyer() != null) {
            waybill.setConsigneeTinOrPinfl(facturaDTO.getBuyerTin());
            waybill.setConsigneeName(facturaDTO.getBuyer().getName());
            waybill.setConsigneeBranchName(facturaDTO.getBuyer().getBranchName());
            waybill.setConsigneeBranchCode(facturaDTO.getBuyer().getBranchCode());
        }

        waybill.setStatus(WaybillStatus.DRAFT);
        waybill.setTotalDistance(BigDecimal.ZERO);
        waybill.setDeliveryCost(BigDecimal.ZERO);
        waybill.setTotalDeliveryCost(BigDecimal.ZERO);

        // converting to RoadwayTransport
        waybill.setTransportType(TransportType.Roadway);
        if (TransportType.Roadway == waybill.getTransportType()) {
            WaybillTransport roadwayTransport = new WaybillTransport();
            roadwayTransport.setTransportType(TransportType.Roadway);

            if (facturaDTO.getProductList() != null && !CollectionUtils.isEmpty(facturaDTO.getProductList().getProducts())) {
                WaybillProductGroups productGroups = new WaybillProductGroups();
                productGroups.setTotalDeliverySum(BigDecimal.ZERO);
                productGroups.setTotalWeightBrutto(BigDecimal.ZERO);

                if (seller.getDistrict() != null) {
                    productGroups.setLoadingRegionId(seller.getDistrict().getRegionId().intValue());
                    productGroups.setLoadingRegionName(seller.getDistrict().getRegion().getNameLocale());
                    productGroups.setLoadingDistrictId(seller.getDistrict().getCode().intValue());
                    productGroups.setLoadingDistrictName(seller.getDistrict().getNameLocale());
                    productGroups.setLoadingAddress(seller.getAddress());
                }
                productGroups.setLoadingAddress(seller.getAddress());
                productGroups.setLoadingLongitude(Optional.ofNullable(seller.getLon()).orElse(0.0));
                productGroups.setLoadingLatitude(Optional.ofNullable(seller.getLat()).orElse(0.0));

                for (FacturaProductDTO facturaProductDTO : facturaDTO.getProductList().getProducts()) {
                    if (Objects.equals(facturaProductDTO.getCatalogCode(), "10499001018000000")) continue;
                    WaybillProduct waybillProduct = new WaybillProduct();
                    waybillProduct.setOrdNo(Integer.valueOf(facturaProductDTO.getOrdNo()));
                    waybillProduct.setCommittentTinOrPinfl(facturaProductDTO.getComittentTin());
                    waybillProduct.setCommittentName(facturaProductDTO.getComittentName());
                    waybillProduct.setProductName(facturaProductDTO.getName());
                    waybillProduct.setCatalogCode(facturaProductDTO.getCatalogCode());
                    waybillProduct.setCatalogName(facturaProductDTO.getCatalogName());
                    waybillProduct.setPackageCode(facturaProductDTO.getPackageCode());
                    waybillProduct.setPackageName(facturaProductDTO.getPackageName());
                    waybillProduct.setAmount(facturaProductDTO.getCount());
                    waybillProduct.setPrice(facturaProductDTO.getSumma());
                    waybillProduct.setProductWeightNetto(BigDecimal.ZERO);
                    waybillProduct.setProductWeightBrutto(BigDecimal.ZERO);

                    productRepository.findFirstByBarcodeAndOrganizationIdAndDeletedIsFalse(facturaProductDTO.getBarcode(), seller.getId())
                        .ifPresent(product -> {
                            waybillProduct.setProductWeightNetto(product.getWeightNetto());
                            waybillProduct.setProductWeightBrutto(product.getWeightBrutto());
                            final Optional<Excise> excise = exciseRepository.findOneByType(product.getType());
                            if (excise.isPresent() && excise.get().getAmount().compareTo(BigDecimal.ZERO) != 0) {
                                final BigDecimal exciseRate = excise.get().calculateRate(product.getCapacity(), product.getTitleAlcohol());
                                waybillProduct.setPrice(waybillProduct.getPrice().add(exciseRate));
                            }
                        });
                    waybillProduct.setDeliverySum(waybillProduct.getPrice().multiply(waybillProduct.getAmount()));
                    waybillProduct.setWeightNetto(waybillProduct.getAmount().multiply(waybillProduct.getProductWeightNetto()));
                    waybillProduct.setWeightBrutto(waybillProduct.getAmount().multiply(waybillProduct.getProductWeightBrutto()));
                    productGroups.setTotalDeliverySum(productGroups.getTotalDeliverySum().add(waybillProduct.getDeliverySum()));
                    productGroups.setTotalWeightBrutto(productGroups.getTotalWeightBrutto().add(waybillProduct.getWeightBrutto()));

                    // relation WaybillProduct with WaybillProductGroup
                    productGroups.addProduct(waybillProduct);
                }

                // relation WaybillProductGroup with WaybillTransport
                roadwayTransport.addProductGroups(productGroups);
            }

            // relation WaybillTransport with Waybill
            waybill.addWaybillTransport(roadwayTransport);
        }
        waybillRepository.save(waybill);
        return waybill.getId();
    }


    @Transactional
    public String createFromAct(String acceptanceTransferActId) {
        AcceptanceTransferAct acceptanceTransferAct = acceptanceTransferActRepository.findById(acceptanceTransferActId).orElseThrow(notFound("Акты приема-передачи не найден"));
        if (ObjectUtils.isEmpty(acceptanceTransferAct.getCustomer())) throw badRequest("Покупатель не найден").get();
        if (ObjectUtils.isEmpty(acceptanceTransferAct.getOrganization())) throw badRequest("Продавец не найден").get();

        Waybill waybill = new Waybill();
        waybill.setWaybillType(WaybillType.OUTGOING);
        waybill.setOrganizationId(SecurityUtils.getCurrentOrganizationId());
        final ResponseDTO responseDTO = facturaClient.getGuid();
        waybill.setWaybillLocalId(responseDTO != null ? responseDTO.getData() : null);
        waybill.setDeliveryType(DeliveryType.DELIVERY);

        waybill.setWaybillNo(acceptanceTransferAct.getActNumber());
        waybill.setWaybillDate(acceptanceTransferAct.getActDate());

        if (acceptanceTransferAct.getContract() != null) {
            waybill.setContractNo(acceptanceTransferAct.getContract().getNumber());
            waybill.setContractDate(acceptanceTransferAct.getContract().getDate());
        }

        Organization seller = acceptanceTransferAct.getOrganization();
        waybill.setConsignorTinOrPinfl(seller.getTin());
        waybill.setConsignorName(seller.getName());

        Customer buyer = acceptanceTransferAct.getCustomer();
        waybill.setConsigneeTinOrPinfl(buyer.getTin());
        waybill.setConsigneeName(buyer.getName());

        waybill.setHasCommittent(buyer.isCommittent());
        waybill.setStatus(WaybillStatus.DRAFT);
        waybill.setTotalDistance(BigDecimal.ZERO);
        waybill.setDeliveryCost(BigDecimal.ZERO);
        waybill.setTotalDeliveryCost(BigDecimal.ZERO);

        // converting to RoadwayTransport
        waybill.setTransportType(TransportType.Roadway);
        if (TransportType.Roadway == waybill.getTransportType()) {
            WaybillTransport roadwayTransport = new WaybillTransport();
            roadwayTransport.setTransportType(TransportType.Roadway);
            if (!CollectionUtils.isEmpty(acceptanceTransferAct.getProducts())) {

                WaybillProductGroups productGroups = new WaybillProductGroups();
                productGroups.setTotalDeliverySum(BigDecimal.ZERO);
                productGroups.setTotalWeightBrutto(BigDecimal.ZERO);

                if (seller.getDistrict() != null) {
                    productGroups.setLoadingRegionId(seller.getDistrict().getRegionId().intValue());
                    productGroups.setLoadingRegionName(seller.getDistrict().getRegion().getNameLocale());
                    productGroups.setLoadingDistrictId(Integer.parseInt(seller.getDistrictId()));
                    productGroups.setLoadingDistrictName(seller.getDistrict().getNameLocale());
                    productGroups.setLoadingAddress(seller.getAddress());
                    productGroups.setLoadingLongitude(seller.getLon());
                    productGroups.setLoadingLatitude(seller.getLat());
                }

                if (buyer.getDistrict() != null) {
                    productGroups.setUnloadingRegionId(buyer.getDistrict().getRegionId().intValue());
                    productGroups.setUnloadingRegionName(buyer.getDistrict().getRegion().getNameLocale());
                    productGroups.setUnloadingDistrictId(Integer.parseInt(buyer.getDistrictId()));
                    productGroups.setUnloadingDistrictName(buyer.getDistrict().getNameLocale());
                    productGroups.setUnloadingAddress(buyer.getAddress());
                    productGroups.setUnloadingLongitude(buyer.getLon());
                    productGroups.setUnloadingLatitude(buyer.getLat());
                }
                int orderNumber = 1;
                for (AcceptanceTransferActProduct acceptanceTransferActProduct : acceptanceTransferAct.getProducts()) {
                    final Product product = acceptanceTransferActProduct.getProduct();
                    WaybillProduct waybillProduct = new WaybillProduct();
                    waybillProduct.setOrdNo(orderNumber++);
                    if (buyer.isCommittent()) {
                        waybillProduct.setCommittentTinOrPinfl(buyer.getTin());
                        waybillProduct.setCommittentName(buyer.getName());
                    }
                    waybillProduct.setProductName(product.getName());
                    waybillProduct.setCatalogCode(product.getCatalogCode());
                    waybillProduct.setCatalogName(product.getCatalogName());
                    waybillProduct.setPackageCode(product.getPackageCode());
                    waybillProduct.setPackageName(product.getPackageName());
                    waybillProduct.setAmount(BigDecimal.valueOf(acceptanceTransferActProduct.getQuantity()));
                    waybillProduct.setPrice(acceptanceTransferActProduct.getPrice());

                    final Optional<Excise> excise = exciseRepository.findOneByType(product.getType());
                    if (excise.isPresent() && excise.get().getAmount().compareTo(BigDecimal.ZERO) != 0) {
                        final BigDecimal exciseRate = excise.get().calculateRate(product.getCapacity(), product.getTitleAlcohol());
                        waybillProduct.setPrice(waybillProduct.getPrice().add(exciseRate));
                    }
                    waybillProduct.setDeliverySum(waybillProduct.getPrice().multiply(waybillProduct.getAmount()));
                    waybillProduct.setProductWeightNetto(Optional.ofNullable(product.getWeightNetto()).orElse(BigDecimal.ZERO));
                    waybillProduct.setProductWeightBrutto(Optional.ofNullable(product.getWeightBrutto()).orElse(BigDecimal.ZERO));
                    waybillProduct.setWeightNetto(waybillProduct.getAmount().multiply(waybillProduct.getProductWeightNetto()));
                    waybillProduct.setWeightBrutto(waybillProduct.getAmount().multiply(waybillProduct.getProductWeightBrutto()));
                    // total calculation
                    productGroups.setTotalDeliverySum(productGroups.getTotalDeliverySum().add(acceptanceTransferActProduct.getPaymentSum()));
                    productGroups.setTotalWeightBrutto(productGroups.getTotalWeightBrutto().add(waybillProduct.getWeightBrutto()));

                    // relation WaybillProduct with WaybillProductGroup
                    productGroups.addProduct(waybillProduct);

                }
                //relation WaybillProductGroup with WaybillTransport
                roadwayTransport.addProductGroups(productGroups);
            }

            // relation WaybillTransport with Waybill
            waybill.addWaybillTransport(roadwayTransport);
        }
        waybillRepository.save(waybill);
        return waybill.getId();
    }

    public List<CompanyTransportDTO> getCompanyTransports(String tin, EnumSet<WaybillTransportType> types) {
        List<CompanyTransportDTO> list = new ArrayList<>();
        CompanyTransportDTO[] companyTransports = facturaClient.getCompanyTransports(tin);
        for (CompanyTransportDTO companyTransport : companyTransports) {
            if (types.contains(WaybillTransportType.fromCode(companyTransport.getTransportType()))) {
                list.add(companyTransport);
            }
        }
        return list;
    }

    public EmpowermentDTO[] getCompanyEmpowerments(String tin) {
        return facturaClient.getCompanyEmpowerments(tin);
    }

    public void delete(String id) {
        Waybill waybill = waybillRepository.findById(id).orElseThrow(notFound("ТНН не найден"));
        waybillRepository.delete(waybill);
    }
    public void deleteProductGroup(String id) {
        WaybillProductGroups waybillProductGroups = waybillProductGroupRepository.findById(id)
            .orElseThrow(notFound("Направление не найден"));
        waybillProductGroupRepository.delete(waybillProductGroups);
    }
    public void deleteProduct(String id) {
        WaybillProduct waybillProduct = waybillProductRepository.findById(id)
            .orElseThrow(notFound("Продукция не найден"));
        waybillProductRepository.delete(waybillProduct);
    }

    public List<WaybillCustomerDTO> getCompanyCustomers(CustomerFilter filter) {
        return customerRepository.findAllByFilter(filter)
            .map(customer -> {
                WaybillCustomerDTO dto = new WaybillCustomerDTO();
                dto.setId(customer.getId());
                dto.setTin(customer.getTin());
                dto.setName(customer.getName());
                dto.setCommittent(customer.isCommittent());
                return dto;
            }).getContent();
    }

    public PhysicalPersonDTO getPhysicalPersonData(String tinOrPinfl) {
        return soliqClient.getUserByTinOrPinfl(tinOrPinfl);
    }

    public void callback(WaybillCallback waybillCallback) {
        Waybill waybill = waybillRepository.findFirstByWaybillLocalId(waybillCallback.getWaybillLocalId())
            .orElseThrow(() -> new BadRequestException("ТТЮ не найдена"));
        waybill.setStatus(WaybillStatus.fromCode(waybillCallback.getCurrentStateId()));
        waybillRepository.save(waybill);

        /*WaybillExecutor waybillExecutor = new WaybillExecutor();
        waybillExecutor.setWaybillId(waybill.getId());
        waybillExecutor.setStatus(waybill.getStatus());
        waybillExecutor.setOperationNumber(Integer.parseInt(certificateDto.getSerialNumber(), 16) + "");
        waybillExecutor.setOperationBy(certificateDto.getSubjectName().getFullName());
        waybillExecutor.setOperationDate(LocalDateTime.now());
        waybillExecutorRepository.save(waybillExecutor);*/
    }
}


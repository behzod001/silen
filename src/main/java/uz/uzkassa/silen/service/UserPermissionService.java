package uz.uzkassa.silen.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.domain.Permission;
import uz.uzkassa.silen.domain.User;
import uz.uzkassa.silen.dto.UserPermissionsDTO;
import uz.uzkassa.silen.repository.PermissionRepository;
import uz.uzkassa.silen.repository.UserRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
@AllArgsConstructor
public class UserPermissionService extends BaseService {
    private final PermissionRepository permissionRepository;
    private final UserRepository userRepository;


    public void save(UserPermissionsDTO userPermissionsDTO) {
        List<Permission> allPermissions = permissionRepository.findAllByCodeIn(userPermissionsDTO.getPermissions());
        User user = userRepository.findById(userPermissionsDTO.getUserId()).orElseThrow(notFound("User not found"));
        user.setAuthorities(new HashSet<>(allPermissions));
        userRepository.save(user);
    }

    public Set<String> getPermissionsByUser(String userId) {
        User user = userRepository.findById(userId).orElseThrow(notFound("User not found"));
        return user.getAuthorities()
            .stream().map(Permission::getCode)
            .collect(Collectors.toSet());
    }
}

package uz.uzkassa.silen.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.uzkassa.silen.domain.Shipment;
import uz.uzkassa.silen.domain.ShipmentItem;
import uz.uzkassa.silen.domain.StoreIn;
import uz.uzkassa.silen.domain.StoreInItem;
import uz.uzkassa.silen.domain.mongo.ShipmentMark;
import uz.uzkassa.silen.domain.mongo.StoreInMark;
import uz.uzkassa.silen.dto.StoreInDTO;
import uz.uzkassa.silen.dto.StoreInDetailsDTO;
import uz.uzkassa.silen.dto.StoreInListDTO;
import uz.uzkassa.silen.dto.filter.StoreInFilter;
import uz.uzkassa.silen.dto.mq.Payload;
import uz.uzkassa.silen.enumeration.ShipmentStatus;
import uz.uzkassa.silen.enumeration.StockInStatus;
import uz.uzkassa.silen.enumeration.TransferType;
import uz.uzkassa.silen.repository.*;
import uz.uzkassa.silen.repository.mongo.ShipmentMarkRepository;
import uz.uzkassa.silen.repository.mongo.StoreInMarkRepository;
import uz.uzkassa.silen.transaction.AfterCommitExecutor;
import uz.uzkassa.silen.exceptions.BadRequestException;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 9/23/2023 16:30
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class StoreInService extends BaseService {
    private final StoreInRepository storeInRepository;
    private final StoreInItemRepository storeInItemRepository;
    private final AfterCommitExecutor afterCommitExecutor;
    private final ShipmentRepository shipmentRepository;
    private final ShipmentItemRepository shipmentItemRepository;
    private final ShipmentMarkRepository shipmentMarkRepository;
    private final StoreInMarkRepository storeInMarkRepository;
    private final StoreRepository storeRepository;

    public String create(StoreInDTO dto) {
        if (StringUtils.isNotBlank(dto.getShipmentId())) {
            Shipment shipment = shipmentRepository.findById(dto.getShipmentId()).orElseThrow(notFound("Отгрузка не найден"));
            if (ShipmentStatus.CLOSED != shipment.getStatus()) {
                throw badRequest("Отгрузка не завершён").get();
            }
        }
        final String organizationId = getCurrentOrganizationId();
        StoreIn storeIn = new StoreIn();
        if (StringUtils.isNotBlank(dto.getNumber())) {
            if (storeInRepository.existsStoreInByOrganizationIdAndNumberIgnoreCase(organizationId, dto.getNumber())) {
                throw badRequest("Возврат с таким номером уже существует").get();
            } else {
                storeIn.setNumber(dto.getNumber());
            }
        } else {
            generateStoreInNumber(organizationId, storeIn, TransferType.RETURN.getNumber());
        }
        if (dto.getStoreId() != null) {
            storeIn.setStoreId(dto.getStoreId());
        } else {
            storeRepository.findFirstByOrganizationIdAndDeletedIsFalseAndMainIsTrue(organizationId).ifPresent(store -> {
                storeIn.setStoreId(store.getId());
            });
        }
        storeIn.setCustomerId(dto.getCustomerId());
        storeIn.setShipmentId(dto.getShipmentId());
        storeIn.setStatus(StockInStatus.NEW);
        storeIn.setTransferType(TransferType.RETURN);
        storeIn.setOrganizationId(organizationId);
        storeIn.setTransferDate(dto.getTransferDate());
        return storeInRepository.save(storeIn).getId();
    }

    private void generateStoreInNumber(String organizationId, StoreIn storeIn, String name) {
        Integer intNumber = storeInRepository.getMaxNumberByOrganizationId(organizationId);
        boolean exists = true;
        while (exists) {
            name = name + intNumber;
            if (storeInRepository.existsStoreInByOrganizationIdAndNumberIgnoreCase(organizationId, name)) {
                intNumber++;
            } else {
                exists = false;
            }
        }
        storeIn.setIntNumber(intNumber);
        storeIn.setNumber(name);
    }

    public Page<StoreInListDTO> findAllByFilter(StoreInFilter filter) {
        return storeInRepository.findAllByFilter(filter).map(StoreIn::toListDto);
    }

    /**
     * This HTTP GET request retrieves information about a specific store-in with the provided ID.
     * The response will be in JSON format and will include details such as the store-in ID,
     * customer information, shipment details, transfer date, quantity, items, transfer type, and status.
     *
     * @param id
     * @return
     */
    public StoreInDetailsDTO findById(String id) {
        StoreIn storeIn = storeInRepository.findById(id).orElseThrow(notFound("Возврат не найден"));
        return storeIn.toDTO();
    }

    public void complete(String id, StockInStatus status) {
        StoreIn storeIn = storeInRepository.findById(id).orElseThrow(notFound());
        if (StockInStatus.COMPLETED.equals(status) && StockInStatus.COMPLETED.equals(storeIn.getStatus())) {
            throw new BadRequestException("Процесс уже завершен!");
        }
        storeIn.setStatus(status);
        storeInRepository.save(storeIn);
        afterCommitExecutor.execute(() -> {
            rabbitMqProducer.sendStoreIn(new Payload(storeIn.getId(), storeIn.getOrganizationId()));
        });
    }

    public StoreInDetailsDTO update(String id, StoreInDTO dto) {
        final String organizationId = getCurrentOrganizationId();
        StoreIn storeIn = storeInRepository.findById(id).orElseThrow(notFound(id));
        if (StockInStatus.NEW != storeIn.getStatus()) {
            throw new BadRequestException("Возврат не возможно изменить");
        }
        if (StringUtils.isNotEmpty(dto.getNumber())) {
            if (storeInRepository.existsStoreInByOrganizationIdAndNumberIgnoreCase(organizationId, dto.getNumber())) {
                throw new BadRequestException("Возврт с таким номером уже существует");
            } else {
                storeIn.setNumber(dto.getNumber());
            }
        } else {
            generateStoreInNumber(organizationId, storeIn, storeIn.getTransferType().getNumber());
        }
        storeIn.setStoreId(dto.getStoreId());
        storeIn.setTransferDate(dto.getTransferDate());

        return storeInRepository.save(storeIn).toDTO();
    }

    public void delete(String id) {
        StoreIn storeIn = storeInRepository.findById(id).orElseThrow(notFound(id));
        if (StockInStatus.NEW != storeIn.getStatus()) {
            throw new BadRequestException("Возврат удалить не возможно");
        }
        storeInItemRepository.deleteAll(storeInItemRepository.findAllByStoreInId(id));
        storeInRepository.delete(storeIn);
    }

    public String returnByShipmentId(String shipmentId) {
        Shipment shipment = shipmentRepository.findById(shipmentId).orElseThrow(notFound("Отгрузка не найден"));
        if (ShipmentStatus.CLOSED != shipment.getStatus()) {
            throw badRequest("Отгрузка не завершён").get();
        }
        Optional<StoreIn> isExists = storeInRepository.findFirstByShipmentIdOrderByCreatedDate(shipmentId);
        if (isExists.isPresent() && StockInStatus.NEW == isExists.get().getStatus()) {
            throw badRequest("Возврат уже существует с этой отгрузкой " + isExists.get().getNumber()).get();
        }
        StoreIn storeIn = new StoreIn();
        generateStoreInNumber(shipment.getOrganizationId(), storeIn, "RETURN-");
        storeIn.setCustomerId(shipment.getCustomerId());
        storeIn.setShipmentId(shipment.getId());
        storeIn.setStoreId(shipment.getStoreId());
        storeIn.setStatus(StockInStatus.NEW);
        storeIn.setTransferType(TransferType.RETURN);
        storeIn.setOrganizationId(shipment.getOrganizationId());
        storeIn.setTransferDate(LocalDateTime.now());
        storeInRepository.save(storeIn);

        List<ShipmentItem> shipmentItems = shipmentItemRepository.findAllByShipmentId(shipmentId);
        for (ShipmentItem shipmentItem : shipmentItems) {
            StoreInItem storeInItem = new StoreInItem();
            storeInItem.setStoreInId(storeIn.getId());
            storeInItem.setProductId(shipmentItem.getProductId());
            storeInItem.setSerialId(shipmentItem.getSerialId());
            storeInItem.setQty(shipmentItem.getQty().intValue());
            storeInItemRepository.save(storeInItem);

            List<ShipmentMark> allByShipmentItemCodes = shipmentMarkRepository.findAllByShipmentItemId(shipmentItem.getId());
            List<StoreInMark> convertedStoreMarks = new LinkedList<>();
            for (ShipmentMark shipmentMark : allByShipmentItemCodes) {
                StoreInMark storeInMark = new StoreInMark();
                storeInMark.setItemId(storeInItem.getId());
                storeInMark.setCode(shipmentMark.getCode());
                storeInMark.setPrintCode(shipmentMark.getPrintCode());
                storeInMark.setUnit(shipmentMark.getUnit());
                convertedStoreMarks.add(storeInMark);
            }
            storeInMarkRepository.saveAll(convertedStoreMarks);
        }

//        afterCommitExecutor.execute(() -> {
//            rabbitMqProducer.sendStoreIn(new Payload(storeIn.getId(), storeIn.getOrganizationId()));
//        });

        return storeIn.getId();
    }

}

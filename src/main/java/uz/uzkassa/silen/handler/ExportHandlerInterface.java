package uz.uzkassa.silen.handler;

import uz.uzkassa.silen.dto.filter.Filter;

import java.io.OutputStream;

public interface ExportHandlerInterface<T extends Filter> {

    void writeToStream(T filter, OutputStream outputStream);

}

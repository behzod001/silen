package uz.uzkassa.silen.handler;

import com.opencsv.CSVWriter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.dto.filter.MongoFilter;
import uz.uzkassa.silen.repository.mongo.MarkRepository;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Slf4j
@Lazy
@Component
@RequiredArgsConstructor
public class CsvMarkExportHandler extends CsvBaseExportHandler<MongoFilter> {
    private final MarkRepository markRepository;

    @Override
    protected void writeContent(final MongoFilter filter, final CSVWriter csvWriter) {
        final String requestId = UUID.randomUUID().toString();

        log.info("Start fetching: {}", requestId);
//        final Set<String> codes = markRepository.findAllByFilter(filter)
//            .stream().map(Mark::getPrintCode).collect(Collectors.toSet());
        Stream<Mark> marks = markRepository.findAllByPartyId(filter.getPartyId()).stream();
        if(filter.getUsed() != null){
            marks = marks.filter(m -> m.isUsed() == filter.getUsed().booleanValue());
        }
        final Set<String> codes = marks.map(Mark::getPrintCode).collect(Collectors.toSet());
        log.info("Complete fetching: {}, size: {}", requestId, codes.size());

        log.info("Start writing to CSV: {}", requestId);
        codes.forEach(printCode -> csvWriter.writeNext(new String[]{printCode}, false));
        log.info("Complete writing to CSV: {}", requestId);
    }

    @Override
    protected void writeHeader(MongoFilter filter, CSVWriter csvWriter) {

    }
}

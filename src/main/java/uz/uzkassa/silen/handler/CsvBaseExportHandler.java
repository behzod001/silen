package uz.uzkassa.silen.handler;

import com.opencsv.CSVWriter;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import uz.uzkassa.silen.dto.filter.Filter;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

@Slf4j
@Getter
public abstract class CsvBaseExportHandler<T extends Filter> implements ExportHandlerInterface<T> {
    protected int pageNumber = 0;
    protected int pageSizeLimit = 150_000;
    protected boolean withBoom = false;

    public void writeToStream(T filter, OutputStream outputStream) {

        try (OutputStreamWriter streamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
             CSVWriter csvWriter = new CSVWriter(streamWriter, CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END)) {

            // this is changeable by child. By default, false
            if (isWithBoom()) {
                addBOM(outputStream);
            }

            // change filter params to default settings
            filter.setPage(getPageNumber());
            filter.setSize(getPageSizeLimit());

            // writing header context
            writeHeader(filter, csvWriter);

            // write all content by writer
            writeContent(filter, csvWriter);

        } catch (IOException e) {
            log.error("Error on time write to stream", e);
        }
    }

    private void addBOM(OutputStream outputStream) {
        byte[] bom = {(byte) 0xEF, (byte) 0xBB, (byte) 0xBF};
        try {
            if (outputStream != null) {
                outputStream.write(bom);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    protected abstract void writeContent(T filter, CSVWriter csvWriter);

    protected abstract void writeHeader(T filter, CSVWriter csvWriter);
}

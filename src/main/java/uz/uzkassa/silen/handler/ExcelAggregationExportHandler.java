package uz.uzkassa.silen.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.domain.Aggregation;
import uz.uzkassa.silen.dto.filter.AggregationFilter;
import uz.uzkassa.silen.repository.AggregationRepository;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 11/6/2023 10:42
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ExcelAggregationExportHandler extends ExcelBaseExportHandler<AggregationFilter> {

    private final AggregationRepository repository;

    @Override
    protected void writeHeader(AggregationFilter filter, XSSFSheet sheet) {

    }

    @Override
    protected void writeBody(AggregationFilter filter, XSSFSheet sheet) {
        Page<Aggregation> aggregations = repository.findAllByFilter(filter);
        int rowIndex = 0;
        int cellIndex = 0;
        for (Aggregation agg : aggregations.getContent()) {
            XSSFRow row = sheet.createRow(rowIndex);
            XSSFCell cell = row.createCell(cellIndex);
            cell.setCellValue(agg.getPrintCode());
            rowIndex++;
        }
    }
}

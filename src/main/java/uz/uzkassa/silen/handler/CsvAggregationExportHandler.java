package uz.uzkassa.silen.handler;

import com.opencsv.CSVWriter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.domain.Aggregation;
import uz.uzkassa.silen.dto.filter.AggregationFilter;
import uz.uzkassa.silen.repository.AggregationRepository;

@Lazy
@Component
@RequiredArgsConstructor
public class CsvAggregationExportHandler extends CsvBaseExportHandler<AggregationFilter> {

    private final AggregationRepository aggregationRepository;

    @Override
    protected void writeContent(AggregationFilter filter, CSVWriter csvWriter) {
        aggregationRepository.findAllByFilter(filter).stream()
            .map(Aggregation::getPrintCode)
            .forEach(printCode -> csvWriter.writeNext(new String[]{printCode}, false));
    }

    @Override
    protected void writeHeader(AggregationFilter filter, CSVWriter csvWriter) {

    }
}

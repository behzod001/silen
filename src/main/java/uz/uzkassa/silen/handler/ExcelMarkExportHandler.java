package uz.uzkassa.silen.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.dto.filter.MongoFilter;
import uz.uzkassa.silen.repository.mongo.MarkRepository;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 11/6/2023 11:03
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ExcelMarkExportHandler extends ExcelBaseExportHandler<MongoFilter> {

    private final MarkRepository markRepository;

    @Override
    protected void writeHeader(final MongoFilter filter, final XSSFSheet sheet) {
        sheet.setHorizontallyCenter(true);
        sheet.setVerticallyCenter(true);
    }

    @Override
    protected void writeBody(final MongoFilter filter, final XSSFSheet sheet) {
        int rowIndex = 0;
        int cellIndex = 0;
        Stream<Mark> marks = markRepository.findAllByPartyId(filter.getPartyId()).stream();
        if(filter.getUsed() != null){
            marks = marks.filter(m -> m.isUsed() == filter.getUsed().booleanValue());
        }
        final Set<String> codes = marks.map(Mark::getPrintCode).collect(Collectors.toSet());
        for (final String mark : codes) {
            final XSSFRow row = sheet.createRow(rowIndex++);
            final XSSFCell cell = row.createCell(cellIndex);
            cell.setCellValue(mark);
        }
    }
}

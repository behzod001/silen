package uz.uzkassa.silen.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.domain.Aggregation;
import uz.uzkassa.silen.dto.filter.AggregationFilter;
import uz.uzkassa.silen.repository.AggregationRepository;
import uz.uzkassa.silen.repository.mongo.MarkRepository;

import java.util.*;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 8/28/2023 11:51
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ExcelAggregationReportExportHandler extends ExcelBaseExportHandler<AggregationFilter> {

    private final AggregationRepository aggregationRepository;
    private final MarkRepository markRepository;

    @Override
    protected void writeHeader(AggregationFilter filter, XSSFSheet sheet) {

    }

    @Override
    protected void writeBody(AggregationFilter filter, XSSFSheet sheet) {
//        XSSFCellStyle style = workbook.createCellStyle();
//        style.setAlignment(HorizontalAlignment.CENTER);
        Page<Aggregation> report = aggregationRepository.findAllByFilter(filter);
        Set<String> parentCodes = new HashSet<>();

        report.forEach(aggregation -> {
            parentCodes.add(aggregation.getCode());
        });
        Map<String, List<String>> marks = new HashMap<>();
        markRepository.findAllByParentCodeIn(parentCodes)
            .forEach(mark -> {
                if (marks.containsKey(mark.getParentCode())) {
                    marks.get(mark.getParentCode()).add(mark.getCode());
                } else {
                    List<String> markList = new LinkedList<>();
                    markList.add(mark.getCode());
                    marks.put(mark.getParentCode(), markList);
                }
            });
        int rowIndex = 0;
        for (Aggregation aggregation : report) {
            XSSFRow row = sheet.createRow(rowIndex++);
            XSSFCell cell = row.createCell(0);
            cell.setCellValue(aggregation.getCode());
            if (marks.containsKey(aggregation.getCode())) {
                for (String mark : marks.get(aggregation.getCode())) {
                    XSSFRow markRow = sheet.createRow(rowIndex++);
                    XSSFCell cell10 = markRow.createCell(1);
                    cell10.setCellValue(mark);
//                    cell10.setCellStyle(style);
                }
            }
        }
    }
}

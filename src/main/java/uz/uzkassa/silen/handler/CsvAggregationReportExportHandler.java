package uz.uzkassa.silen.handler;

import com.opencsv.CSVWriter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.domain.Aggregation;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.dto.filter.AggregationFilter;
import uz.uzkassa.silen.repository.AggregationRepository;
import uz.uzkassa.silen.repository.mongo.MarkRepository;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 8/30/2023 15:52
 */
@Component
@Lazy
@Slf4j
@RequiredArgsConstructor
public class CsvAggregationReportExportHandler extends CsvBaseExportHandler<AggregationFilter> {
    private final MarkRepository markRepository;
    private final AggregationRepository aggregationRepository;


    @Override
    protected void writeContent(final AggregationFilter filter, final CSVWriter csvWriter) {
        final Set<String> parentCodes = aggregationRepository.findAllByFilter(filter)
            .stream()
            .map(Aggregation::getCode)
            .collect(Collectors.toSet());

        final List<Mark> marks = markRepository.findAllByParentCodeIn(parentCodes);
        final Map<String, Set<String>> codes = marks.stream()
            .collect(Collectors.groupingBy(Mark::getParentCode,
                Collectors.mapping(Mark::getCode, Collectors.toSet())));
        for (final String parentCode : parentCodes) {
            csvWriter.writeNext(new String[]{parentCode}, false);
            if (codes.containsKey(parentCode)) {
                codes.get(parentCode)
                    .forEach(code -> csvWriter.writeNext(new String[]{code}, false));
            }
        }
    }

    @Override
    protected void writeHeader(final AggregationFilter filter, final CSVWriter csvWriter) {
//        ProductStatDTO stats = aggregationService.getAggregationProductStats(filter);
//        LinkedList<String> header = new LinkedList<>();
//        header.add("Продукция (название)");
//        header.add(stats.getProductName());
//        header.add("\n");
//        header.add("Продукция (штрихкод)");
//        header.add(stats.getBarcode());
//        header.add("\n");
//        if (stats.getSerialNumber() != null) {
//            header.add("Серия");
//            header.add(stats.getSerialNumber());
//            header.add("\n");
//        }
//        header.add("Кол-во кодов");
//        header.add(stats.getQtyAggregation().toString());
//        header.add("\n");
//        return header;
    }
}

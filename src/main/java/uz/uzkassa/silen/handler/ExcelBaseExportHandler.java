package uz.uzkassa.silen.handler;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import uz.uzkassa.silen.dto.filter.Filter;

import java.io.IOException;
import java.io.OutputStream;

@Slf4j
@Getter
public abstract class ExcelBaseExportHandler<T extends Filter> implements ExportHandlerInterface<T> {
    protected int pageSizeLimit = 150_000;
    protected int pageNumber = 0;
    protected String sheetName = "Отчет";

    protected abstract void writeHeader(T filter, XSSFSheet sheet);

    protected abstract void writeBody(T filter, XSSFSheet sheet);

    public void writeToStream(T filter, OutputStream outputStream) {
        try (XSSFWorkbook wb = new XSSFWorkbook()) {

            XSSFSheet sheet = wb.createSheet(getSheetName());

            // change filter params to default settings
            filter.setPage(getPageNumber());
            filter.setSize(getPageSizeLimit());

            // write header context
            writeHeader(filter, sheet);

            // write content
            writeBody(filter, sheet);

            wb.write(outputStream);
        } catch (IOException e) {
            log.error("Error on time write to stream", e);
        }
    }
}

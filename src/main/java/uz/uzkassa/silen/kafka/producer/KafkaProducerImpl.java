package uz.uzkassa.silen.kafka.producer;

import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class KafkaProducerImpl<T> implements KafkaProducer<T> {

  private final KafkaTemplate<String, T> kafkaTemplate;

  @Override
  public void sendMessage(final String topic, final T message) {
    this.kafkaTemplate.send(topic, message);
  }
}

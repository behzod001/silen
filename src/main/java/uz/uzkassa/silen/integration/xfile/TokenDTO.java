package uz.uzkassa.silen.integration.xfile;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by: Azazello
 * Date: 1/10/2020 2:32 PM
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TokenDTO implements Serializable {
    @JsonProperty("id_token")
    private String idToken;
}

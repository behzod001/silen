package uz.uzkassa.silen.integration.billing;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import uz.uzkassa.silen.config.httpclient.CustomRequest;
import uz.uzkassa.silen.config.httpclient.CustomResponse;
import uz.uzkassa.silen.dto.billingpermission.BillingErrorResponse;
import uz.uzkassa.silen.integration.billing.dto.LoginRequestDto;
import uz.uzkassa.silen.integration.billing.dto.TokenResponseDto;
import uz.uzkassa.silen.integration.billing.dto.agreement.AgreementDTO;
import uz.uzkassa.silen.integration.billing.dto.agreement.AgreementManagerRequestDTO;
import uz.uzkassa.silen.integration.billing.dto.agreement.CreateAgreementDTO;
import uz.uzkassa.silen.integration.billing.filter.XizmatFilter;
import uz.uzkassa.silen.integration.billing.dto.customerpublicoffer.CustomerPublicOfferCtoIdRequestDto;
import uz.uzkassa.silen.integration.billing.dto.customerpublicoffer.CustomerPublicOfferResponseDto;
import uz.uzkassa.silen.integration.billing.dto.quote.QuoteChangeStatusRequestDto;
import uz.uzkassa.silen.integration.billing.dto.quote.QuoteResponseDto;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.integration.BaseIntegrationClient;
import uz.uzkassa.silen.integration.billing.filter.*;
import uz.uzkassa.silen.service.TelegramBotApi;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;


@Component
@Slf4j
@Lazy
public class BillingClient extends BaseIntegrationClient {
    @Value("${application.billing.username}")
    String userName;

    @Value("${application.billing.password}")
    String password;

    public BillingClient(CustomRequest customRequest, ObjectMapper objectMapper, TelegramBotApi botApi, @Value("${application.billing.url}") String startEndpoint) {
        super(customRequest, objectMapper, botApi, startEndpoint);
    }


    /*@GetMapping(value = "/services/billing/api/ledger-detail/tin/{tin}/balance")
    ResponseEntity<Object> getBalanceByTin(@RequestHeader(value = "Authorization") String token, @PathVariable(value = "tin") String tin);*/
    public BigDecimal getBalanceByTin(String tin) {
        final String url = startEndpoint + "/services/billing/api/ledger-detail/tin/" + tin + "/balance";
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        return convertToResponse(response, BigDecimal.class);
    }

    /*@GetMapping(value = "/services/billing/api/xizmats/grouped", produces = "application/json")
    Object getGroupedXizmats(@RequestHeader(value = "Authorization") String token, @SpringQueryMap XizmatFilter filter);*/
    public Object getGroupedXizmats(XizmatFilter filter) {
        final String url = startEndpoint + "/services/billing/api/xizmats/grouped";
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null), filter.convertToParams());
        return convertToResponse(response, Object.class);
    }

    /*    @GetMapping(value = "/services/billing/api/xizmats/single", produces = "application/json")
    Object getSingleXizmats(@RequestHeader(value = "Authorization") String token, @SpringQueryMap XizmatFilter filter);*/
    public Object getSingleXizmats(XizmatFilter filter) {
        final String url = startEndpoint + "/services/billing/api/xizmats/single";
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null), filter.convertToParams());
        return convertToResponse(response, Object.class);
    }

    /*@GetMapping(value = "/services/billing/api/show-calculation/xizmat/{xizmatId}/customer/{tin}")
    ResponseEntity<BigDecimal> showCalculationByXizmatIdAndCustomerTin(
            @RequestHeader(value = "Authorization") String token,
            @PathVariable(value = "xizmatId") Long xizmatId,
            @PathVariable(value = "tin") String tin,
            @RequestParam(required = false, name = "info") String info
    );*/
    public BigDecimal showCalculationByXizmatIdAndCustomerTin(Long xizmatId, String tin, String info) {
        String url = startEndpoint + "/services/billing/api/show-calculation/xizmat/" + xizmatId + "/customer/" + tin;
        List<NameValuePair> params = new ArrayList<>();
        if (StringUtils.isNotEmpty(info)) {
            params.add(new BasicNameValuePair("info", info));
        }
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null), params);
        return convertToResponse(response, BigDecimal.class);
    }

    /*    @GetMapping(value = "/services/billing/api/customer-public-offer/customer/tin/{tin}")
    ResponseEntity<CustomerPublicOfferResponseDto> getCustomerOfferByTin(@RequestHeader(value = "Authorization") String token, @PathVariable(value = "tin") @NotNull String tin);*/
    public CustomerPublicOfferResponseDto getCustomerOfferByTin(String tin) {
        String url = startEndpoint + "/services/billing/api/customer-public-offer/customer/tin/" + tin;

        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        return convertToResponse(response, CustomerPublicOfferResponseDto.class);
    }

    /*    @GetMapping(value = "/services/billing/api/agreements-pageable")
    Object getAllAgreementsPageable(@RequestHeader(value = "Authorization") String token, @SpringQueryMap AgreementFilter filter);*/
    public Object getAllAgreementsPageable(AgreementFilter filter) {
        final String url = startEndpoint + "/services/billing/api/agreements-pageable";
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null), filter.convertToParams());
        return convertToResponse(response, Object.class);
    }

    /*@GetMapping(value = "/services/billing/api/ledger-detail/tin/{tin}/transactions")
    ResponseEntity<Object> getledgerDetailTransactions(
            @RequestHeader(value = "Authorization") String token,
            @PathVariable(value = "tin") String tin,
            @SpringQueryMap LedgerDetailFilter filter
    );*/
    public Object getledgerDetailTransactions(String tin, IntegrationBaseFilter filter) {
        final String url = startEndpoint + "/services/billing/api/ledger-detail/tin/" + tin + "/transactions";
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null), filter.convertToParams());
        return convertToResponse(response, Object.class);
    }

    /*@PostMapping(value = "/services/billing/api/agreements")
    ResponseEntity<Object> saveAgreement(@RequestHeader(value = "Authorization") String token, @RequestBody CreateAgreementDTO agreement);*/
    public Object saveAgreement(CreateAgreementDTO agreement) {
        final String url = startEndpoint + "/services/billing/api/agreements";
        try {
            String json = objectMapper.writeValueAsString(agreement);
            CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), json);
            return convertToResponse(response, Object.class);
        } catch (JsonProcessingException e) {
            log.debug("Billing saveAgreement convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    /*@GetMapping(value = "/services/billing/api/agreements/{id}", produces = "application/json")
    Object getAgreementById(@RequestHeader(value = "Authorization") String token, @PathVariable(value = "id") Long id);*/
    public Object getAgreementById(Long id) {
        final String url = startEndpoint + "/services/billing/api/agreements/" + id;
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        return convertToResponse(response, Object.class);
    }

    /*@GetMapping(value = "/services/billing/api/quote/{id}")
    ResponseEntity<QuoteResponseDto> getByQuoteId(@RequestHeader(value = "Authorization") String token,
                                                  @PathVariable(value = "id") String id);*/
    public QuoteResponseDto getByQuoteId(String id) {
        final String url = startEndpoint + "/services/billing/api/quote/" + id;
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        return convertToResponse(response, QuoteResponseDto.class);
    }

    /*    @GetMapping(value = "/services/billing/api/quote/{id}/get-template")
    ResponseEntity<String> getTemplateByQuoteId(@RequestHeader(value = "Authorization") String token,
                                                @PathVariable(value = "id") String id);*/
    public String getTemplateByQuoteId(String id) {
        final String url = startEndpoint + "/services/billing/api/quote/" + id + "/get-template";
        CustomResponse customResponse = customRequest.sendGetRequest(url, evaluateHeaders(null));
        if (HttpStatus.OK.value() == customResponse.getCode()) {
            return customResponse.getContent();
        } else {
            log.debug("GET Company balance by TIN response: {}", customResponse);
            throw new BadRequestException(StringUtils.isNotEmpty(customResponse.getErrorMessage()) ? customResponse.getErrorMessage() : customResponse.getContent());
        }
    }

/*        @PutMapping(value = "/services/billing/api/quote/{id}/change-status")
    ResponseEntity<QuoteResponseDto> changeStatus(@RequestHeader(value = "Authorization") String token,
                                                  @PathVariable(value = "id") String id,
                                                  @RequestBody QuoteChangeStatusRequestDto dto);*/

    public QuoteResponseDto changeStatus(String id, QuoteChangeStatusRequestDto dto) {
        final String url = startEndpoint + "/services/billing/api/quote/" + id + "/change-status";
        try {
            String json = objectMapper.writeValueAsString(dto);
            CustomResponse response = customRequest.sendPutRequest(url, evaluateHeaders(null), json);
            return convertToResponse(response, QuoteResponseDto.class);
        } catch (JsonProcessingException e) {
            log.debug("Billing changeStatus convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

/*            @PutMapping(value = "/services/billing/api/agreements/{id}/cancel")
    AgreementDTO cancelAgreement(@RequestHeader(value = "Authorization") String token, @PathVariable(value = "id") Long id);*/

    public AgreementDTO cancelAgreement(Long id) {
        final String url = startEndpoint + "/services/billing/api/agreements/" + id + "/cancel";
        CustomResponse response = customRequest.sendPutRequest(url, evaluateHeaders(null), null);
        return convertToResponse(response, AgreementDTO.class);
    }

    /*    @GetMapping(value = "/services/billing/api/invoice/get-by-quote-id/{id}")
        ResponseEntity<Page<Object>> getInvoiceByQuoteId(@RequestHeader(value = "Authorization") String token,
                                                         @PathVariable(value = "id") String id,
                                                         @RequestParam(value = "page") Integer page,
                                                         @RequestParam(value = "size") Integer size);*/
    public Object getInvoiceByQuoteId(String id, IntegrationBaseFilter filter) {
        final String url = startEndpoint + "/services/billing/api/invoice/get-by-quote-id/" + id;

        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null), filter.convertToParams());
        return convertToResponse(response, Object.class);
    }

    private TokenResponseDto authorize() {
        String url = startEndpoint + "/auth/login";
        try {
            String json = objectMapper.writeValueAsString(new LoginRequestDto(userName, password));
            CustomResponse response = customRequest.sendPostRequest(url, null, json);
            return convertToResponse(response, TokenResponseDto.class);
        } catch (JsonProcessingException e) {
            log.debug("Billing authorize convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    protected Map<String, String> evaluateHeaders(String token) {
        TokenResponseDto authorize = authorize();
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + authorize.getAccessToken());
        return headers;
    }

    @Override
    protected String getBaseUrl() {
        return null;
    }


    /*    @GetMapping(value = "/services/billing/api/ledger-detail/grouped")
    ResponseEntity<Object> getledgerDetailGruppedTransactions(
        @RequestHeader(value = "Authorization") String token,
        @SpringQueryMap LedgerDetailAppTypeFilter filter
    );*/
    public Object getledgerDetailGruppedTransactions(LedgerDetailAppTypeFilter filter) {
        final String url = startEndpoint + "/services/billing/api/ledger-detail/grouped";
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null), filter.convertToParams());
        return convertToResponse(response, Object.class);
    }

    /*    @GetMapping(value = "/services/billing/api/get-bonuses-amount")
    ResponseEntity<Object> getBonusesAmount(
        @RequestHeader(value = "Authorization") String token,
        @SpringQueryMap BonusResponseFilter filter);*/
    public Object getBonusesAmount(BonusResponseFilter filter) {
        final String url = startEndpoint + "/services/billing/api/get-bonuses-amount";
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null), filter.convertToParams());
        return convertToResponse(response, Object.class);
    }

    /*    @GetMapping(value = "/services/billing/api/bonuses-pageable")
    ResponseEntity<Object> getBonusesList(
        @RequestHeader(value = "Authorization") String token,
        @SpringQueryMap BonusResponseFilter filter);*/
    public Object getBonusesList(BonusResponseFilter filter) {
        final String url = startEndpoint + "/services/billing/api/bonuses-pageable";
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null), filter.convertToParams());
        return convertToResponse(response, Object.class);
    }

    /*    @GetMapping(value = "/services/billing/api/invoice/list")
    ResponseEntity<Object> billingInvoiceList(
        @RequestHeader(value = "Authorization") String token,
        @SpringQueryMap BillingInvoiceFilter filter
    );*/
    public Object billingInvoiceList(BillingInvoiceFilter filter) {
        final String url = startEndpoint + "/services/billing/api/invoice/list";
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null), filter.convertToParams());
        return convertToResponse(response, Object.class);
    }

    /*    @GetMapping(value = "/services/billing/api/invoice/get-customer-count-by-invoice-filter")
    ResponseEntity<Long> getBillingInvoiceCustomerCount(
        @RequestHeader(value = "Authorization") String token,
        @SpringQueryMap BillingInvoiceFilter filter
    );*/
    public Long getBillingInvoiceCustomerCount(BillingInvoiceFilter filter) {
        final String url = startEndpoint + "/services/billing/api/invoice/get-customer-count-by-invoice-filter";
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null), filter.convertToParams());
        return convertToResponse(response, Long.class);
    }

    /*    @GetMapping(value = "/services/billing/api/invoice/{id}/get-template")
    ResponseEntity<String> getInvoiceTemplateById(@RequestHeader(value = "Authorization") String token,
                                                  @PathVariable(value = "id") String id);*/
    public String getInvoiceTemplateById(String id) {
        final String url = startEndpoint + "/services/billing/api/invoice/" + id + "/get-template";
        CustomResponse customResponse = customRequest.sendGetRequest(url, evaluateHeaders(null));
        if (HttpStatus.OK.value() == customResponse.getCode()) {
            return customResponse.getContent();
        } else {
            log.debug("GET getInvoiceTemplateById by id response: {}", customResponse);
            throw new BadRequestException(StringUtils.isNotEmpty(customResponse.getErrorMessage()) ? customResponse.getErrorMessage() : customResponse.getContent());
        }
    }

    /*    @GetMapping(value = "/services/billing/api/invoice/{id}/check-invoice-xfile-status")
        ResponseEntity<Object> checkBillingInvoiceXfileStatus(
            @RequestHeader(value = "Authorization") String token,
            @PathVariable(value = "id") String id
        );*/
    public Object checkBillingInvoiceXfileStatus(String id) {
        final String url = startEndpoint + "/services/billing/api/invoice/" + id + "/check-invoice-xfile-status";
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        return convertToResponse(response, Object.class);
    }

    /*    @GetMapping(value = "/services/billing/api/public-offer/get-last-active/document-type/{documentType}")
    Object getLastActivePublicOffer(
        @RequestHeader(value = "Authorization") String token,
        @PathVariable(value = "documentType") @NotNull BillingDocumentType documentType
    );*/
    public Object getLastActivePublicOffer(String documentType) {
        final String url = startEndpoint + "/services/billing/api/public-offer/get-last-active/document-type/" + documentType;
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        return convertToResponse(response, Object.class);
    }

    /*@PostMapping(value = "/services/billing/api/customer-public-offer/create-by-tin")
    ResponseEntity<CustomerPublicOfferResponseDto> createCustomerOffer(@RequestHeader(value = "Authorization") String token, @RequestBody CustomerPublicOfferCtoIdRequestDto dto);*/
    public CustomerPublicOfferResponseDto createCustomerOffer(CustomerPublicOfferCtoIdRequestDto dto) {
        final String url = startEndpoint + "/services/billing/api/customer-public-offer/create-by-tin";
        try {
            String json = objectMapper.writeValueAsString(dto);
            CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), json);
            return convertToResponse(response, CustomerPublicOfferResponseDto.class);
        } catch (JsonProcessingException e) {
            log.debug("Billing createCustomerOffer convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    /*@GetMapping(value = "/services/billing/api/start-agreement/{tin}")
    ResponseEntity<Void> startAgreementWithTin(
            @RequestHeader(value = "Authorization") String token,
            @PathVariable(value = "tin") String tin
    );*/
    public Object startAgreementWithTin(String tin) {
        final String url = startEndpoint + "/services/billing/api/start-agreement/" + tin;
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        return convertToResponse(response, Object.class);
    }

    /*    @PostMapping("/services/billing/api/agreements/agreements-manager-update")
    ResponseEntity<String> updateAgreementManager(@RequestHeader(value = "Authorization") String token, @RequestBody AgreementManagerRequestDTO dto);*/
    public String updateAgreementManager(AgreementManagerRequestDTO dto) {
        final String url = startEndpoint + "/services/billing/api/agreements/agreements-manager-update";
        try {
            String json = objectMapper.writeValueAsString(dto);
            CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), json);
            return convertToResponse(response, String.class);
        } catch (JsonProcessingException e) {
            log.debug("Billing updateAgreementManager convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    public <T> T convertToResponse(CustomResponse customResponse, Class<T> toDto) {
        if (HttpStatus.CREATED.value() == customResponse.getCode()) {
            return super.convertToResponse(customResponse, toDto);
        }
        if (HttpStatus.OK.value() != customResponse.getCode()) {
            throw new BadRequestException(parseError(customResponse));
        }
        return super.convertToResponse(customResponse, toDto);
    }

    private String parseError(final CustomResponse response) {
        if (StringUtils.isNotBlank(response.getErrorMessage())) {
            return response.getErrorMessage();
        }
        final String content = response.getContent();
        try {
            final BillingErrorResponse responseDTO = objectMapper.readValue(content, BillingErrorResponse.class);
            return Optional.ofNullable(responseDTO)
                    .map(BillingErrorResponse::getDetail)
                    .orElse(content);
        } catch (IOException e) {
            return content;
        }
    }

    /*    @PostMapping(value = "/services/billing/api/quote/{id}/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseEntity<QuoteResponseDto> uploadToQuote(@RequestHeader(value = "Authorization") String token,
                                                   @PathVariable(value = "id") String id,
                                                   @RequestPart(value = "file") MultipartFile file);*/
    public QuoteResponseDto uploadToQuote(String id, MultipartFile file) throws IOException {
        final String url = startEndpoint + "/services/billing/api/quote/" + id + "/upload";

        CustomResponse response = customRequest.uploadFile(url, evaluateHeaders(null), file);
        return convertToResponse(response, QuoteResponseDto.class);
    }

    /*@PostMapping(value = "/services/billing/api/customer-public-offer/{id}/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseEntity<CustomerPublicOfferResponseDto> uploadFileToCustomerPublicOffer(@RequestHeader(value = "Authorization") String token,
                                                                                   @PathVariable(value = "id") String id,
                                                                                   @RequestPart(value = "file") MultipartFile file);*/
    public CustomerPublicOfferResponseDto uploadFileToCustomerPublicOffer(String id, MultipartFile file) {
        final String url = startEndpoint + "/services/billing/api/customer-public-offer/" + id + "/upload";

        CustomResponse response = customRequest.uploadFile(url, evaluateHeaders(null), file);
        return convertToResponse(response, CustomerPublicOfferResponseDto.class);
    }
}

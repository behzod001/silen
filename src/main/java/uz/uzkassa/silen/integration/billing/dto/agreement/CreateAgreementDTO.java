package uz.uzkassa.silen.integration.billing.dto.agreement;

import lombok.Getter;
import lombok.Setter;
import uz.uzkassa.silen.integration.billing.dto.BillingAttachmentDTO;
import uz.uzkassa.silen.integration.billing.dto.BranchJson;
import uz.uzkassa.silen.integration.billing.dto.CustomerDTO;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * A DTO for the {@link uz.uzkassa.billing.domain.Agreement} entity.
 */
@Getter
@Setter
public class CreateAgreementDTO implements Serializable {
    private String serviceType;
    private CustomerDTO customer;
    private XizmatDTO xizmat;
    private Long instances = 1L;
    private String description;
    private List<BranchJson> branches;
    private String operatorId;
    private String operatorName;
    private Set<BillingAttachmentDTO> attachments;
    private Boolean trial;


    private String firstName;
    private String lastName;
    private String patronymic;
    private String login;
    private String smartposCreatorId;
    private String noteFromExternalSystem;

//    @ApiModelProperty(hidden = true, accessMode = ApiModelProperty.AccessMode.READ_ONLY)
//    private Boolean cancelOnPeriodEnd = Boolean.FALSE;

//    @ApiModelProperty(hidden = true, accessMode = ApiModelProperty.AccessMode.READ_ONLY)
//    private AgreementStatus status = AgreementStatus.PENDING;

//    @ApiModelProperty(hidden = true, accessMode = ApiModelProperty.AccessMode.READ_ONLY)
//    private Long instances = 1L;

//    @ApiModelProperty(hidden = true, accessMode = ApiModelProperty.AccessMode.READ_ONLY)
//    private DiscountDTO discount;

    @Override
    public String toString() {
        return "CreateAgreementDTO{" +
//            ", serviceType='" + getServiceType() + "'" +
            ", customerName='" + (getCustomer() != null ? getCustomer().getName() : "") + "'" +
//            ", status='" + getStatus() + "'" +
            ", xizmatId=" + (getXizmat() != null ? getXizmat().getId() : "") +
//            ", discountId=" + (getDiscount() != null ? getDiscount().getId() : "") +
            "}";
    }
}

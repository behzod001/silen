package uz.uzkassa.silen.integration.billing.dto.agreement;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * A DTO for the {@link uz.uzkassa.billing.domain.Xizmat} entity.
 */
@Getter
@Setter
public class XizmatDTO  implements Serializable {
    private Long id;

    private String title;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof XizmatDTO)) {
            return false;
        }

        return id != null && id.equals(((XizmatDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "XizmatDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            "}";
    }
}

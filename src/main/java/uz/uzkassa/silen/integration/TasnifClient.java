package uz.uzkassa.silen.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.config.httpclient.CustomRequest;
import uz.uzkassa.silen.config.httpclient.CustomResponse;
import uz.uzkassa.silen.dto.invoice.*;
import uz.uzkassa.silen.service.TelegramBotApi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 29.11.2022 15:43
 */
@Component
@Slf4j
@Lazy
public class TasnifClient extends BaseIntegrationClient {

    public TasnifClient(final CustomRequest customRequest,
                        final ObjectMapper objectMapper,
                        final TelegramBotApi botApi,
                        @Value("${application.tasnifApiUrl}") final String startEndpoint) {
        super(customRequest, objectMapper, botApi, startEndpoint);
    }

    @Override
    protected String getBaseUrl() {
        return startEndpoint;
    }

    public MxikListDTO syncPackages(final String stringList) {
        final String url = getBaseUrl() + "/integration-mxik/check";

        final CustomResponse response = customRequest.sendPostRequest(url, null, stringList);
        return convertToResponse(response, MxikListDTO.class);
    }

    public List<MxikDTO> getCompanyCatalog(final String tin, final String search) {
        final String url = getBaseUrl() + "/integration-mxik/company/get/basket-products";

        final List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("lang", "ru"));
        params.add(new BasicNameValuePair("page", "0"));
        params.add(new BasicNameValuePair("size", "1000"));
        if (tin.length() == 9) {
            params.add(new BasicNameValuePair("tin", tin));
        } else {
            params.add(new BasicNameValuePair("pinfl", tin));
        }

        if (StringUtils.isNotEmpty(search)) {
            params.add(new BasicNameValuePair("text", search.toLowerCase()));
        }

        final CustomResponse response = customRequest.sendGetRequest(url, null, params);
        final MxikResponseDTO responseDTO = convertToResponse(response, MxikResponseDTO.class);
        if (responseDTO != null) {
            return responseDTO.getData();
        }
        return null;
    }

    public MxikDTO getByMxik(final String mxik) {
        final String url = getBaseUrl() + "/integration-mxik/search/by-params?lang=ru&mxikCode=" + mxik;

        final CustomResponse response = customRequest.sendGetRequest(url, null);
        final MxikResponseDTO responseDTO = convertToResponse(response, MxikResponseDTO.class);
        if (responseDTO != null && CollectionUtils.isNotEmpty(responseDTO.getData())) {
            return responseDTO.getData().get(0);
        }
        return null;
    }

    public List<PackageDTO> getPackages(final String mxik) {
        final String url = getBaseUrl() + "/integration-mxik/get/package?mxikCode=" + mxik;

        final CustomResponse response = customRequest.sendGetRequest(url, null);
        final MxikPackageResponseDTO responseDTO = convertToResponse(response, MxikPackageResponseDTO.class);
        if (responseDTO != null) {
            return responseDTO.getData();
        }
        return null;
    }

    @Override
    protected Map<String, String> evaluateHeaders(String token) {
        return null;
    }
}

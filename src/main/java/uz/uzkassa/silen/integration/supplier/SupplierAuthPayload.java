package uz.uzkassa.silen.integration.supplier;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 5/30/2023 11:39
 * {"companyType":"SUPPLIER","rememberMe":true,"password":"Aa1234567a","phone":"998935533188"}
 */
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SupplierAuthPayload {

    boolean rememberMe = true;

    String username;

    String password;

    public SupplierAuthPayload(String username, String password) {
        this.username = username;
        this.password = password;
    }
}

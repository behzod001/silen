package uz.uzkassa.silen.integration.supplier;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 5/29/2023 18:18
 */
@Getter
@Setter
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SerialDTO {

    String number;

    LocalDate date;
}

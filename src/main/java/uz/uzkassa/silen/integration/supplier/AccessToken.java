package uz.uzkassa.silen.integration.supplier;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 5/30/2023 11:58
 */
@Getter
@Setter
public class AccessToken {
    private String access_token;
}

package uz.uzkassa.silen.integration;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import uz.uzkassa.silen.integration.xfile.Credential;
import uz.uzkassa.silen.integration.xfile.TokenDTO;

@Component
@FeignClient(name = "Xfile", url = "http://192.168.201.79:8081/api")
public interface XfileClient {
    @PostMapping("/authenticate")
    ResponseEntity<TokenDTO> login(@RequestBody Credential credential);
}

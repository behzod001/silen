package uz.uzkassa.silen.integration.onec;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Powered by: Dilshod Madrahimov
 * Date: 17.12.2021 17:28
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderContract1CDTO implements Serializable {

    String number;

    LocalDate date;
}

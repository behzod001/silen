package uz.uzkassa.silen.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.config.httpclient.CustomRequest;
import uz.uzkassa.silen.config.httpclient.CustomResponse;
import uz.uzkassa.silen.dto.acceptancetransferact.AcceptanceTransferActDTO;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.integration.supplier.AccessToken;
import uz.uzkassa.silen.integration.supplier.CommittentTokenBatchDTO;
import uz.uzkassa.silen.integration.supplier.SupplierAuthPayload;
import uz.uzkassa.silen.service.TelegramBotApi;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
@Slf4j
@Lazy
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SupplierClient extends BaseIntegrationClient {

    String userName;
    String password;

    public SupplierClient(CustomRequest customRequest, ObjectMapper objectMapper, TelegramBotApi botApi,
                          @Value("${application.supplier.url}") String startEndpoint,
                          @Value("${application.supplier.username}") String userName,
                          @Value("${application.supplier.password}") String password
    ) {
        super(customRequest, objectMapper, botApi, startEndpoint);
        this.userName = userName;
        this.password = password;
    }

    public void sendAcceptanceTransferAct(AcceptanceTransferActDTO acceptanceTransferActDTO) {
        String url = startEndpoint + "acceptance-transfer-acts/silen/webhook/created";
        try {
            String json = objectMapper.writeValueAsString(acceptanceTransferActDTO);
            log.info("Send Acceptance Transfer Act JSON :{}", json);
            CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), json);
            log.info("Send Acceptance Transfer Act to Supply :{}", response);
        } catch (JsonProcessingException e) {
            log.error("Acceptance Transfer Act convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public void send(CommittentTokenBatchDTO committentTokenBatchDTO) {
        String url = startEndpoint + "companies/committent-token";
        try {
            String json = objectMapper.writeValueAsString(committentTokenBatchDTO);
            CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), json);
            log.info("Companies comitent tokens sent {}", response);
        } catch (JsonProcessingException e) {
            log.debug("Supplier committents convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public AccessToken authorize() {
        String url = startEndpoint + "account/login";
        try {
            String json = objectMapper.writeValueAsString(new SupplierAuthPayload(userName, password));
            CustomResponse response = customRequest.sendPostRequest(url, null, json);
            if (HttpStatus.OK.value() == response.getCode()) {
                try {
                    return objectMapper.readValue(response.getContent(), AccessToken.class);
                } catch (IOException e) {
                    log.debug("Supplier AccessToken response convert to JSON error: {}", e.getMessage());
                }
            } else {
                StringBuilder message = new StringBuilder("SUPPLIER SYNC TOKEN").append(" \n")
                    .append("====================").append(" \n")
                    .append("URL: ").append(url).append(" \n")
                    .append("REQUEST: ").append(json).append(" \n")
                    .append("RESPONSE: ").append(" \n")
                    .append(response);
                botApi.sendMessageToAdminChannel(message.toString());
            }
        } catch (JsonProcessingException e) {
            log.debug("Supplier committents convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
        return null;
    }

    @Override
    protected Map<String, String> evaluateHeaders(String token) {
        Map<String, String> headers = new HashMap<>();
        AccessToken newToken = authorize();
        String nullableToken = Optional.ofNullable(newToken.getAccess_token()).orElse("");
        headers.put("Authorization", "Bearer " + nullableToken);
        return headers;
    }

    @Override
    protected String getBaseUrl() {
        return null;
    }
}

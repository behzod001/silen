package uz.uzkassa.silen.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.config.httpclient.CustomRequest;
import uz.uzkassa.silen.config.httpclient.CustomResponse;
import uz.uzkassa.silen.dto.marking.TuronOrderDTO;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.exceptions.EntityNotFoundException;
import uz.uzkassa.silen.exceptions.TuronErrorResponse;
import uz.uzkassa.silen.integration.turon.*;
import uz.uzkassa.silen.service.TelegramBotApi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
@Lazy
public class TuronClient extends BaseIntegrationClient {

    public TuronClient(CustomRequest customRequest, ObjectMapper objectMapper, TelegramBotApi botApi, @Value("${application.turonApiUrl}") String startEndpoint) {
        super(customRequest, objectMapper, botApi, startEndpoint);
    }

    public CreateOrderResponseDTO createOrder(TuronOrderDTO turonOrderDTO, String extension, String clientToken, String omsId) {
        final String url = startEndpoint + extension + "/orders";
        try {
            List<NameValuePair> params = List.of(new BasicNameValuePair("omsId", omsId));
            String json = objectMapper.writeValueAsString(turonOrderDTO);
            CustomResponse response = customRequest.sendPostRequest(url, params, evaluateHeaders(clientToken), json);
            return convertToResponse(response, CreateOrderResponseDTO.class);
        } catch (JsonProcessingException e) {
            log.debug("Turon response convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public CheckStatusResponseDTO checkStatus(String extension, String orderId, String gtin, String clientToken, String omsId) {
        String url = startEndpoint + extension + "/buffer/status";
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("omsId", omsId));
        params.add(new BasicNameValuePair("orderId", orderId));
        params.add(new BasicNameValuePair("gtin", gtin));
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(clientToken), params);
        return convertToResponse(response, CheckStatusResponseDTO.class);
    }

    public GetMarksResponseDTO getMarks(String extension, String orderId, String lastBlockId, String gtin, Integer qty, String clientToken, String omsId) {
        String url = startEndpoint + extension + "/codes";
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("omsId", omsId));
        params.add(new BasicNameValuePair("orderId", orderId));
        params.add(new BasicNameValuePair("gtin", gtin));
        params.add(new BasicNameValuePair("quantity", String.valueOf(qty)));
        params.add(new BasicNameValuePair("lastBlockId", lastBlockId));
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(clientToken), params);
        return convertToResponse(response, GetMarksResponseDTO.class);
    }

    public GetMarksResponseDTO retry(String extension, String orderId, String gtin, String blockId, String clientToken, String omsId) {
        final String url = startEndpoint + extension + "/codes/retry";
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("omsId", omsId));
        params.add(new BasicNameValuePair("orderId", orderId));
        params.add(new BasicNameValuePair("gtin", gtin));
        if (StringUtils.isNotBlank(blockId)) {
            params.add(new BasicNameValuePair("blockId", blockId));
        }
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(clientToken), params);
        return convertToResponse(response, GetMarksResponseDTO.class);
    }

    public Object closeBuffer(String extension, String orderId, String gtin, String blockId, String clientToken, String omsId) {
        final String url = startEndpoint + extension + "/buffer/close";
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("omsId", omsId));
        params.add(new BasicNameValuePair("orderId", orderId));
        params.add(new BasicNameValuePair("gtin", gtin));
        params.add(new BasicNameValuePair("lastBlockId", blockId));
        CustomResponse response = customRequest.sendPostRequest(url, params, evaluateHeaders(clientToken), null);
        return convertToResponse(response, Object.class);
    }

    public CreateAggregationResponse createAggregation(CreateAggregationRequest request, String extension, String clientToken, String omsId) {
        String url = startEndpoint + extension + "/aggregation?omsId=" + omsId;
        try {
            String json = objectMapper.writeValueAsString(request);
            CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(clientToken), json);
            return convertToResponse(response, CreateAggregationResponse.class);
        } catch (JsonProcessingException e) {
            log.error("Convert JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public DropoutResponse dropout(DropoutRequest dropoutRequest, String extension, String clientToken, String omsId) {
        final String url = startEndpoint + extension + "/dropout";
        try {
            List<NameValuePair> params = List.of(new BasicNameValuePair("omsId", omsId));
            String json = objectMapper.writeValueAsString(dropoutRequest);
            CustomResponse response = customRequest.sendPostRequest(url, params, evaluateHeaders(clientToken), json);
            return convertToResponse(response, DropoutResponse.class);
        } catch (JsonProcessingException e) {
            log.debug("Turon dropout request convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public UtilisationResponseDTO apply(String clientToken, String extension, String omsId, UtilisationRequestDTO request) {
        final String url = startEndpoint + extension + "/utilisation";
        try {
            String json = objectMapper.writeValueAsString(request);
            List<NameValuePair> params = List.of(new BasicNameValuePair("omsId", omsId));
            CustomResponse response = customRequest.sendPostRequest(url, params, evaluateHeaders(clientToken), json);
            return convertToResponse(response, UtilisationResponseDTO.class);
        } catch (Exception e) {
            log.error("Turon utilisation request convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    protected Map<String, String> evaluateHeaders(String token) {
        Map<String, String> headers = new HashMap<>();
        headers.put("clientToken", token);
        return headers;
    }

    @Override
    protected String getBaseUrl() {
        return null;
    }

    public <T> T convertToResponse(CustomResponse customResponse, Class<T> toDto) {
        final String errorMessage = StringUtils.isNotEmpty(customResponse.getErrorMessage()) ? customResponse.getErrorMessage() : customResponse.getContent();
        switch (HttpStatus.valueOf(customResponse.getCode())) {
            case HttpStatus.OK -> {
                try {
                    return objectMapper.readValue(customResponse.getContent(), toDto);
                } catch (IOException e) {
                    log.debug("BaseIntegrationClient response convert to JSON error: {}", e.getMessage());
                }
            }
            case HttpStatus.CREATED -> {
                if (customResponse.getContent() == null) return null;
                return objectMapper.convertValue(customResponse.getContent(), toDto);
            }
            case NOT_FOUND -> throw new EntityNotFoundException(errorMessage);
            case BAD_REQUEST, INTERNAL_SERVER_ERROR ->
                    throw new BadRequestException(parseTuronException(customResponse));
            default -> {
                log.debug("GET response convert by TIN response: {}", customResponse);
                throw new BadRequestException(parseTuronException(customResponse));
            }
        }
        return null;
    }
    private String parseTuronException(final CustomResponse response) {
        if (StringUtils.isBlank(response.getContent())) {
            return response.getErrorMessage();
        }
        try {
            final TuronErrorResponse error = objectMapper.readValue(response.getContent(), TuronErrorResponse.class);
            return error.toString();
        } catch (JsonProcessingException e) {
            return "Error parse failed";
        }
    }
}

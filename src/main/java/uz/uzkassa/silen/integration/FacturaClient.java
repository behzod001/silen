package uz.uzkassa.silen.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.config.httpclient.CustomRequest;
import uz.uzkassa.silen.config.httpclient.CustomResponse;
import uz.uzkassa.silen.domain.FacturaHost;
import uz.uzkassa.silen.dto.invoice.*;
import uz.uzkassa.silen.dto.vm.SignVM;
import uz.uzkassa.silen.dto.waybill.CompanyEmpowermentsDTO;
import uz.uzkassa.silen.dto.waybill.CompanyTransportDTO;
import uz.uzkassa.silen.dto.waybill.EmpowermentDTO;
import uz.uzkassa.silen.dto.waybill.WaybillSendDTO;
import uz.uzkassa.silen.enumeration.Status;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.exceptions.EntityNotFoundException;
import uz.uzkassa.silen.repository.FacturaHostRepository;
import uz.uzkassa.silen.service.TelegramBotApi;

import java.io.IOException;
import java.util.*;

@Component
@Slf4j
@Lazy
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class FacturaClient extends BaseIntegrationClient {
    String password;
    String userName;
    FacturaHostRepository facturaHostRepository;

    public FacturaClient(final CustomRequest customRequest,
                         final ObjectMapper objectMapper,
                         final TelegramBotApi botApi,
                         @Value("${application.factura.url}") final String startEndpoint,
                         @Value("${application.factura.password}") final String password,
                         @Value("${application.factura.username}") final String userName,
                         final FacturaHostRepository facturaHostRepository) {
        super(customRequest, objectMapper, botApi, startEndpoint);
        this.password = password;
        this.userName = userName;
        this.facturaHostRepository = facturaHostRepository;
    }

    @Override
    protected String getBaseUrl() {
        return facturaHostRepository.findFirstByActiveIsTrue().map(FacturaHost::getHost)
            .map(s -> StringUtils.join(s, startEndpoint))
            .orElse("https://factura.yt.uz/provider/api/");
    }

    @Override
    public <T> T convertToResponse(CustomResponse customResponse, Class<T> toDto) {
        final String errorMessage = StringUtils.isNotEmpty(customResponse.getErrorMessage()) ? customResponse.getErrorMessage() : customResponse.getContent();
        switch (HttpStatus.valueOf(customResponse.getCode())) {
            case HttpStatus.OK -> {
                try {
                    return objectMapper.readValue(customResponse.getContent(), toDto);
                } catch (IOException e) {
                    log.error("Json parse error: {}", e.getMessage());
                }
            }
            case HttpStatus.CREATED -> {
                if (customResponse.getContent() == null) return null;
                return objectMapper.convertValue(customResponse.getContent(), toDto);
            }
            case NOT_FOUND -> throw new EntityNotFoundException(errorMessage);
            case BAD_REQUEST, INTERNAL_SERVER_ERROR -> throw new BadRequestException(errorMessage);
            default -> {
                log.debug("GET response convert by TIN response: {}", customResponse);
                throw new BadRequestException(parseNicError(customResponse));
            }
        }
        return null;
    }

    public List<LgotaDTO> getLgota(final String mxikCode) {
        final String url = StringUtils.join(getBaseUrl(), "ru/utils/Lgota/without-vat-products?TasnifCode=", mxikCode);
        final CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        final LgotaResponseDTO responseDTO = convertToResponse(response, LgotaResponseDTO.class);
        if (responseDTO != null) {
            return responseDTO.getRows();
        }
        return null;
    }

    public List<LgotaDTO> getLgotaByTin(final String tin) {
        final String url = StringUtils.join(getBaseUrl(), "ru/utils/Lgota/privileged-seller-companies?Tin=", tin);
        final CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        final LgotaResponseDTO responseDTO = convertToResponse(response, LgotaResponseDTO.class);
        if (responseDTO != null) {
            return responseDTO.getRows();
        }
        return Collections.emptyList();
    }

    public ResponseDTO getGuid() {
        final String url = StringUtils.join(getBaseUrl(), "ru/utils/guid");
        final CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        return convertToResponse(response, ResponseDTO.class);
    }

    public ProviderBindingDTO getProviders(final String tin) {
        final String url = StringUtils.join(getBaseUrl(), "ru/register/providerbinding/", tin);

        final CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        return convertToResponse(response, ProviderBindingDTO.class);
    }

    public void saveProviders(final SignVM sign) {
        final String url = StringUtils.join(getBaseUrl(), "ru/register/providerbinding");

        try {
            final String json = objectMapper.writeValueAsString(sign);
            final CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), json);
            if (HttpStatus.OK.value() != response.getCode()) {
                log.error("POST Save providers response: {}", response);
                throw new BadRequestException(parseNicError(response));
            }
        } catch (JsonProcessingException e) {
            log.error("Save providers convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public RegionDTO[] getRegions() {
        final String url = StringUtils.join(getBaseUrl(), "ru", "/waybills-v2/utils/catalogs/regions");

        final CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        return convertToResponse(response, RegionDTO[].class);
    }

    public DistrictDTO[] getDistricts(Long regionId) {
        final String url = StringUtils.join(getBaseUrl(), "ru", "/waybills-v2/utils/catalogs/districts");
        List<NameValuePair> params = List.of(new BasicNameValuePair("regionId", regionId.toString()));
        final CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null), params);
        return convertToResponse(response, DistrictDTO[].class);
    }

    public VillageDTO[] getVillages(String soato) {
        final String url = StringUtils.join(getBaseUrl(), "ru", "/waybills-v2/utils/catalogs/villages");
        List<NameValuePair> params = List.of(new BasicNameValuePair("soato", soato));
        final CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null), params);
        return convertToResponse(response, VillageDTO[].class);
    }

    public MeasureDTO[] getMeasures(final String lang) {
        final String url = StringUtils.join(getBaseUrl(), lang, "/catalogs/measure");

        final CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        return convertToResponse(response, MeasureDTO[].class);
    }

    public BankDTO[] getBanks(final String lang) {
        final String url = StringUtils.join(getBaseUrl(), lang, "/catalogs/bank");
        final CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        return convertToResponse(response, BankDTO[].class);
    }

    public void sendFactura(final String lang, final String tin, final FacturaSendDTO dto) {
        long start = System.currentTimeMillis();
        String url = StringUtils.join(getBaseUrl(), lang, "/", tin, "/facturas/seller");

        try {
            final String json = objectMapper.writeValueAsString(dto);
            final CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), json);
            if (HttpStatus.OK.value() == response.getCode()) {
                log.info("SEND FACTURA :{} ============== TIME: {}ms", dto.getFacturaId(), (System.currentTimeMillis() - start));
            } else {
                StringBuilder message = new StringBuilder("Type: ")
                    .append("FACTURA ID:").append(dto.getFacturaId()).append(" \n")
                    .append("SELLER TIN: ").append(tin).append(" \n")
                    .append("CLIENT IP: ").append(dto.getClientIp()).append(" \n")
                    .append("URL: ").append(url).append(" \n")
                    .append("RESPONSE: ").append(" \n")
                    .append(response);
                botApi.sendMessageToAdminChannel(message.toString());
                log.error("POST Send factura response: {}", response);
                throw new BadRequestException(parseNicError(response));
            }
        } catch (JsonProcessingException e) {
            log.error("Send factura convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public void cancelFactura(final String lang, final String tin, final FacturaSendDTO facturaSendDTO) {
        final String url = StringUtils.join(getBaseUrl(), lang, "/", tin, "/facturas/seller/cancel");

        try {
            final String json = objectMapper.writeValueAsString(facturaSendDTO);
            final CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), json);
            if (HttpStatus.OK.value() != response.getCode()) {
                log.error("POST Cancel factura response: {}", response);
                throw new BadRequestException(parseNicError(response));
            }
        } catch (JsonProcessingException e) {
            log.error("Cancel factura convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public void acceptOrReject(final String lang, final String tin, final FacturaSendDTO facturaSendDTO) {
        final String url = StringUtils.join(getBaseUrl(), lang, "/", tin, "/facturas/buyer");

        try {
            final String json = objectMapper.writeValueAsString(facturaSendDTO);
            final CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), json);
            if (HttpStatus.OK.value() != response.getCode()) {
                log.error("POST {} factura response: {} ", facturaSendDTO.getAction(), response);
                throw new BadRequestException(parseNicError(response));
            }
        } catch (JsonProcessingException e) {
            log.error("{} factura convert to JSON error: {}", facturaSendDTO.getAction(), e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public FacturaDTO getSellerFactura(final String tin, final String facturaId) {
        final String url = StringUtils.join(getBaseUrl(), "ru/", tin, "/facturas/seller/", facturaId);
        final CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        return convertToResponse(response, FacturaDTO.class);
    }

    public String getSellerFacturaSign(final String tin, final String facturaId, final Status status, boolean throwError) {
        final String url = StringUtils.join(getBaseUrl(), "ru/", tin, "/facturas/seller/signedfile/", facturaId, "?status=", status.getNicStatus());

        final CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        return convertToResponse(response, String.class);
    }

    public String getBuyerFacturaSign(final String tin, final String facturaId, final Status status) {
        final String url = StringUtils.join(getBaseUrl(), "ru/", tin, "/facturas/buyer/signedfile/", facturaId, "?status=", status.getNicStatus());

        final CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        return convertToResponse(response, String.class);
    }

    @Override
    protected Map<String, String> evaluateHeaders(final String token) {
        final Map<String, String> headers = new HashMap<>();
        final String credentials = Base64.getEncoder().encodeToString(StringUtils.join(userName, ":", password).getBytes());
        headers.put(HttpHeaders.AUTHORIZATION, StringUtils.join("Basic ", credentials));
        return headers;
    }

    private String parseNicError(final CustomResponse response) {
        if (StringUtils.isNotBlank(response.getErrorMessage())) {
            return response.getErrorMessage();
        }
        try {
            final FacturaErrorResponse responseDTO = objectMapper.readValue(response.getContent(), FacturaErrorResponse.class);
            return Optional.ofNullable(responseDTO)
                .map(FacturaErrorResponse::getErrorMessage)
                .orElseThrow(() -> new BadRequestException("Непредвиденная ошибка"));
        } catch (IOException e) {
            return response.getContent();
        }
    }

    public void sendWaybill(final String lang, final String tin, final WaybillSendDTO dto, final UUID requestId, final String id) {
        long start = System.currentTimeMillis();
        String url = StringUtils.join(getBaseUrl(), lang, "/", tin, "/waybills-v2/local/consignor");

        try {
            final String json = objectMapper.writeValueAsString(dto);
            final CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), json);
            if (HttpStatus.OK.value() == response.getCode()) {
                log.info("SEND WAYBILL :{} ============== TIME: {}ms", id, (System.currentTimeMillis() - start));
            } else {
                StringBuilder message = new StringBuilder("Type: ")
                    .append("REQUEST ID:").append(requestId).append(" \n")
                    .append("WAYBILL ID:").append(id).append(" \n")
                    .append("SELLER TIN: ").append(tin).append(" \n")
                    .append("CLIENT IP: ").append(dto.getClientIp()).append(" \n")
                    .append("URL: ").append(url).append(" \n")
                    .append("RESPONSE: ").append(" \n")
                    .append(response);
                botApi.sendMessageToAdminChannel(message.toString());
                log.error("POST Send waybill response: {}", response);
                throw new BadRequestException(parseNicError(response));
            }
        } catch (JsonProcessingException e) {
            log.error("Send waybill convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public void cancelWaybill(final String lang, final String tin, final WaybillSendDTO waybillSendDTO, final String id) {
        final String url = StringUtils.join(getBaseUrl(), lang, "/", tin, "/waybills-v2/local/consignor/cancel");

        try {
            final String json = objectMapper.writeValueAsString(waybillSendDTO);
            final CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), json);
            if (HttpStatus.OK.value() != response.getCode()) {
                StringBuilder message = new StringBuilder("Type: ")
                    .append("REQUEST: ").append(json).append(" \n")
                    .append("WAYBILL ID:").append(id).append(" \n")
                    .append("SELLER TIN: ").append(tin).append(" \n")
                    .append("CLIENT IP: ").append(waybillSendDTO.getClientIp()).append(" \n")
                    .append("URL: ").append(url).append(" \n")
                    .append("RESPONSE: ").append(" \n")
                    .append(response);
                botApi.sendMessageToAdminChannel(message.toString());
                log.error("POST Cancel waybill response: {}", response);
                throw new BadRequestException(parseNicError(response));
            }
        } catch (JsonProcessingException e) {
            log.error("Cancel waybill convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public void acceptWaybill(final String lang, final String tin, final WaybillSendDTO waybillSendDTO, final String id) {
        final String url = StringUtils.join(getBaseUrl(), lang, "/", tin, "/waybills-v2/local/consignee");

        try {
            final String json = objectMapper.writeValueAsString(waybillSendDTO);
            final CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), json);
            if (HttpStatus.OK.value() != response.getCode()) {
                StringBuilder message = new StringBuilder("Type: ")
                    .append("REQUEST: ").append(json).append(" \n")
                    .append("WAYBILL ID:").append(id).append(" \n")
                    .append("SELLER TIN: ").append(tin).append(" \n")
                    .append("CLIENT IP: ").append(waybillSendDTO.getClientIp()).append(" \n")
                    .append("URL: ").append(url).append(" \n")
                    .append("RESPONSE: ").append(" \n")
                    .append(response);
                botApi.sendMessageToAdminChannel(message.toString());
                log.error("POST Cancel waybill response: {}", response);
                throw new BadRequestException(parseNicError(response));
            }
        } catch (JsonProcessingException e) {
            log.error("{} waybill convert to JSON error: {}", waybillSendDTO.getWaybillLocalSignType(), e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public void rejectWaybill(final String lang, final String tin, final WaybillSendDTO waybillSendDTO, final String id) {
        final String url = StringUtils.join(getBaseUrl(), lang, "/", tin, "/waybills-v2/local/consignee/reject");

        try {
            final String json = objectMapper.writeValueAsString(waybillSendDTO);
            final CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), json);
            if (HttpStatus.OK.value() != response.getCode()) {
                log.error("POST {} waybill response: {} ", waybillSendDTO.getWaybillLocalSignType(), response);
                throw new BadRequestException(parseNicError(response));
            } else {
                StringBuilder message = new StringBuilder("Type: ")
                    .append("REQUEST: ").append(json).append(" \n")
                    .append("WAYBILL ID:").append(id).append(" \n")
                    .append("SELLER TIN: ").append(tin).append(" \n")
                    .append("CLIENT IP: ").append(waybillSendDTO.getClientIp()).append(" \n")
                    .append("URL: ").append(url).append(" \n")
                    .append("RESPONSE: ").append(" \n")
                    .append(response);
                botApi.sendMessageToAdminChannel(message.toString());
                log.error("POST Cancel waybill response: {}", response);
                throw new BadRequestException(parseNicError(response));
            }
        } catch (JsonProcessingException e) {
            log.error("{} waybill convert to JSON error: {}", waybillSendDTO.getWaybillLocalSignType(), e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public CompanyTransportDTO[] getCompanyTransports(String tin) {
        final String url = getBaseUrl() + "ru/waybills-v2/utils/transport?tinOrPinfl=" + tin;
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        CompanyTransportDTO[] result = convertToResponse(response, CompanyTransportDTO[].class);
        if (result != null) {
            return result;
        } else {
            throw new BadRequestException("Нет данных по данному ИНН", response.getCode());
        }
    }

    /**
     * 6 - отправлено дов.лицу;
     * 7 - доставлено дов.лицу, ожидает его подписи;
     * 10 - отправлено поставщику;
     * 15 - доставлено поставщику, ожидает его подписи.
     *
     * @param tin
     * @return
     */
    public EmpowermentDTO[] getCompanyEmpowerments(String tin) {
        final String url = getBaseUrl() + "ru/" + tin + "/empowerments/seller?stateId=15";

        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(null));
        CompanyEmpowermentsDTO result = convertToResponse(response, CompanyEmpowermentsDTO.class);
        if (result != null) {
            return result.getRows();
        } else {
            throw new BadRequestException("Нет данных по данному ИНН", response.getCode());
        }
    }

}

package uz.uzkassa.silen.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.config.httpclient.CustomRequest;
import uz.uzkassa.silen.config.httpclient.CustomResponse;
import uz.uzkassa.silen.dto.eimzo.Pkcs7Dto;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.service.TelegramBotApi;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
@Lazy
public class EimzoClient extends BaseIntegrationClient {
    public EimzoClient(CustomRequest customRequest,
                       ObjectMapper objectMapper,
                       TelegramBotApi botApi,
                       @Value("${application.eimzo.url}") String startEndpoint) {
        super(customRequest, objectMapper, botApi, startEndpoint);
    }

    public Pkcs7Dto frontTimestamp(final String sign) {
        try {
            final String url = this.startEndpoint + "/frontend/timestamp/pkcs7";
            CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), sign);
            if (response.getCode() == 200) {
                return objectMapper.readValue(response.getContent(), Pkcs7Dto.class);
            } else {
                log.error("Front timestamp sign: \n {}", sign);
                log.error("Front timestamp response: {}", response);
                throw new BadRequestException(StringUtils.isNotEmpty(response.getErrorMessage()) ? response.getErrorMessage() : response.getContent());
            }
        } catch (IOException e) {
            log.error("Front timestamp convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public Pkcs7Dto backendVerifyAttached(final String sign) {
        try {
            final String url = this.startEndpoint + "/backend/pkcs7/verify/attached";
            CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), sign);
            if (response.getCode() == 200) {
                return objectMapper.readValue(response.getContent(), Pkcs7Dto.class);
            } else {
                log.error("Backend verify attached sign: \n {}", sign);
                log.error("Backend verify attached response: {}", response);
                throw new BadRequestException(StringUtils.isNotEmpty(response.getErrorMessage()) ? response.getErrorMessage() : response.getContent());
            }
        } catch (IOException e) {
            log.error("Backend verify attached convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public Pkcs7Dto backendAuth(final String sign) {
        try {
            final String url = this.startEndpoint + "/backend/auth";
            CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(null), sign);
            if (response.getCode() == 200) {
                return objectMapper.readValue(response.getContent(), Pkcs7Dto.class);
            } else {
                log.error("Backend auth sign: \n {}", sign);
                log.error("Backend auth response: {}", response);
                throw new BadRequestException(StringUtils.isNotEmpty(response.getErrorMessage()) ? response.getErrorMessage() : response.getContent());
            }
        } catch (IOException e) {
            log.error("Backend auth convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    @Override
    protected Map<String, String> evaluateHeaders(String token) {
        Map<String, String> headers = new HashMap<>();
        headers.put("X-Real-IP", "89.236.216.227");
        headers.put("Host", "https://cabinet.silen.uz");
        headers.put(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE);
        return headers;
    }

    @Override
    protected String getBaseUrl() {
        return null;
    }
}

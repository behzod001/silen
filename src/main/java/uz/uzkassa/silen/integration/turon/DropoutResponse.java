package uz.uzkassa.silen.integration.turon;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 5/5/2023 16:17
 */
@Getter
@Setter
@NoArgsConstructor
public class DropoutResponse implements Serializable {

    String reportId;

    String omsId;
}

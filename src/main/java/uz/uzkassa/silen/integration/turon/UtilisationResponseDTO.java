package uz.uzkassa.silen.integration.turon;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@ToString
@Getter
@Setter
public class UtilisationResponseDTO implements Serializable {

    private String omsId;
    private String reportId;
}

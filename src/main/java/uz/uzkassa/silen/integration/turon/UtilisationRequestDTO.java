package uz.uzkassa.silen.integration.turon;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import uz.uzkassa.silen.enumeration.UsageType;

import jakarta.validation.constraints.Max;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 01.03.2023 12:57
 */
@ToString
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UtilisationRequestDTO {

    UsageType usageType = UsageType.USED_FOR_PRODUCTION;

    Set<String> sntins;

    String productionDate;

    String expirationDate;

    @Max(value = 20, message = "Контроль длины от 1 до 20 символов")
    String seriesNumber;

    String productionLineId;

    public Set<String> getSntins() {
        if (this.sntins == null) {
            this.sntins = new LinkedHashSet<>();
        }
        return sntins;
    }
}

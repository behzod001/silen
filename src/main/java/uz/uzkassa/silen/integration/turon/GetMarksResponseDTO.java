package uz.uzkassa.silen.integration.turon;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
public class GetMarksResponseDTO implements Serializable {
    private Set<String> codes;
    private String blockId;
}

package uz.uzkassa.silen.integration.turon;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 5/5/2023 16:16
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DropoutRequest implements Serializable {
    private Set<String> sntins;
    private String participantId;
    private String address;
    private String dropoutReason;
    private boolean withChild;
}

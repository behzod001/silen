package uz.uzkassa.silen.integration.turon;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CreateAggregationResponse implements Serializable {
    private String reportId;
}

package uz.uzkassa.silen.integration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import uz.uzkassa.silen.config.httpclient.CustomRequest;
import uz.uzkassa.silen.config.httpclient.CustomResponse;
import uz.uzkassa.silen.enumeration.ProductGroup;
import uz.uzkassa.silen.exceptions.BadRequestException;
import uz.uzkassa.silen.exceptions.TueApiErrorResponse;
import uz.uzkassa.silen.integration.asilbelgi.AuthKey;
import uz.uzkassa.silen.integration.asilbelgi.AuthToken;
import uz.uzkassa.silen.integration.billing.dto.CisInfoResponse;
import uz.uzkassa.silen.integration.asilbelgi.ReportInfo;
import uz.uzkassa.silen.service.LocalizationService;
import uz.uzkassa.silen.service.TelegramBotApi;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
@Slf4j
@Lazy
public class TrueApi extends BaseIntegrationClient {
    private final String trueApiReport;
    private final LocalizationService localizationService;

    public TrueApi(CustomRequest customRequest, ObjectMapper objectMapper, TelegramBotApi botApi,
                   @Value("${application.trueApiUrl}") String startEndpoint,
                   @Value("${application.trueApiReport}") String trueApiReport,
                   LocalizationService localizationService) {
        super(customRequest, objectMapper, botApi, startEndpoint);
        this.trueApiReport = trueApiReport;
        this.localizationService = localizationService;
    }

    @Override
    public <T> T convertToResponse(CustomResponse customResponse, Class<T> toDto) {
        switch (HttpStatus.valueOf(customResponse.getCode())) {
            case HttpStatus.OK -> {
                try {
                    return objectMapper.readValue(customResponse.getContent(), toDto);
                } catch (IOException e) {
                    log.debug("Billing response convert to JSON error: {}", e.getMessage());
                }
            }
            case BAD_GATEWAY, UNAUTHORIZED -> throw new BadRequestException(localizationService.getMessage("true.api.token.expired"));
            default -> {
                try {
                    TueApiErrorResponse tueApiErrorResponse = objectMapper.readValue(customResponse.getContent(), TueApiErrorResponse.class);
                    final String errorMessage = StringUtils.isNotEmpty(customResponse.getErrorMessage()) ? customResponse.getErrorMessage() : tueApiErrorResponse.toString();
                    throw new BadRequestException(errorMessage);
                } catch (JsonProcessingException e) {
                    log.error("Error occured on convert response to error", e);
                }
            }
        }
        return null;
    }

    public AuthKey getAuthKey() {
        final String url = startEndpoint + "auth/key";
        CustomResponse response = customRequest.sendGetRequest(url, null);
        return convertToResponse(response, AuthKey.class);
    }

    public String signIn(AuthKey key) {
        final String url = startEndpoint + "auth/simpleSignIn";

        try {
            String json = objectMapper.writeValueAsString(key);
            CustomResponse response = customRequest.sendPostRequest(url, null, json);
            AuthToken data = convertToResponse(response, AuthToken.class);
            return data.getToken();
        } catch (JsonProcessingException e) {
            log.debug("TueApi response convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public String refresh(String token) {
        final String url = startEndpoint + "auth/refresh";
            CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(token), null);
            AuthToken data = convertToResponse(response, AuthToken.class);
            return data.getToken();
    }

    public CisInfoResponse[] cisesInfo(final String inn, final String token, final Set<String> codes) {
        final String url = startEndpoint + "cises/info";
        final long start = System.currentTimeMillis();
        try {
            final String json = objectMapper.writeValueAsString(codes);
            final CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(token), json);
            log.debug("TIMED: {}ms ================== INN : {} ==================== CODE: {}", (System.currentTimeMillis() - start), inn, codes.iterator().next());
            return convertToResponse(response,CisInfoResponse[].class);

        } catch (final JsonProcessingException e) {
            log.error("TueApi response convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public Map<String, Map<String, List>> aggregatedList(String inn, String token, ProductGroup productGroup, Set<String> codes, String turonAggregationId) {
        final String url = startEndpoint + "cises/aggregated/list?pg=" + productGroup.getProductGroup();
        long start = System.currentTimeMillis();
        try {
            String json = objectMapper.writeValueAsString(codes);
            CustomResponse response = customRequest.sendPostRequest(url, evaluateHeaders(token), json);
            log.debug("TIMED: {}ms ================== INN : {} ==================== CODE: {}", (System.currentTimeMillis() - start), inn, codes.iterator().next());
            return convertToResponse(response, Map.class);
        } catch (JsonProcessingException e) {
            log.error("TueApi response convert to JSON error: {}", e.getMessage());
            throw new BadRequestException(e.getMessage());
        }
    }

    public ReportInfo getUtilisedMarks(String token, String aggregationId) {
        final String url = trueApiReport + aggregationId;
        CustomResponse response = customRequest.sendGetRequest(url, evaluateHeaders(token));
        log.debug("CustomResponse {}", response);
        return convertToResponse(response, ReportInfo.class);

    }

    @Override
    protected Map<String, String> evaluateHeaders(String token) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + token);
        return headers;
    }

    @Override
    protected String getBaseUrl() {
        return null;
    }
}

package uz.uzkassa.silen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.AcceptanceTransferAct;

import java.util.Optional;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 5/25/2023 12:38
 */
@Repository
public interface AcceptanceTransferActRepository extends JpaRepository<AcceptanceTransferAct, String>, AcceptanceTransferActRepositoryCustom {

    Optional<AcceptanceTransferAct> findFirstByOrganizationIdAndActNumber(String organizationId, String actNumber);
}

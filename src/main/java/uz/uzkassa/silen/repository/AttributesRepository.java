package uz.uzkassa.silen.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.Attributes;
import uz.uzkassa.silen.enumeration.ProductType;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Spring Data  repository for the FormAttributes entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AttributesRepository extends JpaRepository<Attributes, Long>, AttributesRepositoryCustom {
    Optional<Attributes> getByNameAndPrefixAndPostfixAndProductTypesIn(String name, String prefix, String postfix, Set<ProductType> productType);

    Page<Attributes> findAllByProductTypesInOrderByLastModifiedDate(Set<ProductType> productType, Pageable pageable);

    List<Attributes> findAllByProductTypesContainsOrderByLastModifiedDate(ProductType productType);

    Optional<Attributes> getAttributesBySorter(int sortOrder);
}

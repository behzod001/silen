package uz.uzkassa.silen.repository;

import org.springframework.data.domain.Page;
import uz.uzkassa.silen.domain.Dropout;
import uz.uzkassa.silen.dto.DropoutStatsDTO;
import uz.uzkassa.silen.dto.filter.DropoutFilter;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 5/8/2023 10:11
 */
public interface DropoutRepositoryCustom {
    Page<Dropout> findAllByFilter(DropoutFilter filter);

    DropoutStatsDTO findAllByFilterStats(DropoutFilter filter);
}

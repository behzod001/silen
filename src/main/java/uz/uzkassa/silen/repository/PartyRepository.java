package uz.uzkassa.silen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.Party;
import uz.uzkassa.silen.enumeration.AggregationStatus;

/**
 * Spring Data repository for the OrderProduct entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PartyRepository extends JpaRepository<Party, String>, PartyRepositoryCustom {
    boolean existsByOrderProductIdAndJobStatus(String orderProductId, AggregationStatus jobStatus);
}

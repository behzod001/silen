package uz.uzkassa.silen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.District;

import java.util.List;
import java.util.Optional;

@Repository
public interface DistrictRepository extends JpaRepository<District, String> {
    List<District> findAllByRegionId(Long regionId);

    Optional<District> findFirstByCodeAndRegionId(Long code, Long regionId);
}

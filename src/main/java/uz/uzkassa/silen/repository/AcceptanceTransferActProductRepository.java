package uz.uzkassa.silen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.AcceptanceTransferActProduct;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 5/25/2023 12:38
 */
@Repository
public interface AcceptanceTransferActProductRepository extends JpaRepository<AcceptanceTransferActProduct, String>, AcceptanceTransferActRepositoryCustom {

}

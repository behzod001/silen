package uz.uzkassa.silen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.Invoice;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long>, InvoiceRepositoryCustom {
}

package uz.uzkassa.silen.repository.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.StoreIn;
import uz.uzkassa.silen.dto.filter.StoreInFilter;
import uz.uzkassa.silen.repository.StoreInRepositoryCustom;
import uz.uzkassa.silen.security.SecurityUtils;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import java.util.Optional;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 9/13/2023 17:29
 */
@Repository
public class StoreInRepositoryImpl implements StoreInRepositoryCustom {

    @PersistenceContext(unitName = "slavePU")
    private EntityManager entityManager;

    @Override
    public Page<StoreIn> findAllByFilter(StoreInFilter filter) {
        final boolean hasSearch = StringUtils.isNotEmpty(filter.getSearch());
        final boolean hasSort = StringUtils.isNotEmpty(filter.getOrderBy());
        final String organizationId = Optional.ofNullable(filter.getOrganizationId()).orElse(SecurityUtils.getCurrentOrganizationId());

        StringBuilder sql = new StringBuilder();
        sql.append(" select o from StoreIn o");
        sql.append(" where o.deleted is not true");
        sql.append(" and o.organizationId = :organizationId");

        if (filter.getStoreId() != null) {
            sql.append(" and o.storeId = :storeId");
        }
        if (filter.getStatus() != null) {
            sql.append(" and o.status =:status");
        }
        if (filter.getTransferType() != null) {
            sql.append(" and o.transferType = :transferType");
        }
        if (filter.getUserId() != null) {
            sql.append(" and o.createdBy = :createdBy");
        }
        if (filter.getFrom() != null) {
            sql.append(" and o.lastModifiedDate >= :from");
        }
        if (filter.getTo() != null) {
            sql.append(" and o.lastModifiedDate <= :to");
        }
        if (hasSearch) {
            sql.append(" and (lower(o.number) like :searchKey");
            //sql.append(" or o.customer.tin like :searchKey");
            //sql.append(" or lower(o.customer.name) like :searchKey");
            sql.append(")");
        }
        String countSql = sql.toString().replaceFirst("select o", "select count(o.id)");

        sql.append(" order by");
        if (hasSort) {
            sql.append(" o.").append(filter.getOrderBy());
        } else {
            sql.append(" o.lastModifiedDate");
        }
        sql.append(" ").append(filter.getSortOrder().getName());

        TypedQuery<StoreIn> query = this.entityManager.createQuery(sql.toString(), StoreIn.class)
            .setFirstResult(filter.getStart())
            .setMaxResults(filter.getSize())
            .setParameter("organizationId", organizationId);
        TypedQuery<Long> countQuery = entityManager.createQuery(countSql, Long.class)
            .setParameter("organizationId", organizationId);

        if (filter.getStatus() != null) {
            query.setParameter("status", filter.getStatus());
            countQuery.setParameter("status", filter.getStatus());
        }
        if (filter.getTransferType() != null) {
            query.setParameter("transferType", filter.getTransferType());
            countQuery.setParameter("transferType", filter.getTransferType());
        }
        if (filter.getStoreId() != null) {
            query.setParameter("storeId", filter.getStoreId());
            countQuery.setParameter("storeId", filter.getStoreId());
        }
        if (filter.getShipmentId() != null) {
            query.setParameter("shipmentId", filter.getShipmentId());
            countQuery.setParameter("shipmentId", filter.getShipmentId());
        }
        if (filter.getFrom() != null) {
            query.setParameter("from", filter.getFrom());
            countQuery.setParameter("from", filter.getFrom());
        }
        if (filter.getTo() != null) {
            query.setParameter("to", filter.getTo());
            countQuery.setParameter("to", filter.getTo());
        }
        if (hasSearch) {
            query.setParameter("searchKey", filter.getSearchForQuery());
            countQuery.setParameter("searchKey", filter.getSearchForQuery());
        }
        return new PageImpl<>(query.getResultList(), filter.getPageable(), countQuery.getSingleResult());
    }
}

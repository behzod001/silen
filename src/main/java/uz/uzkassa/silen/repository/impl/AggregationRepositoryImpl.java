package uz.uzkassa.silen.repository.impl;

import jakarta.persistence.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import uz.uzkassa.silen.domain.Aggregation;
import uz.uzkassa.silen.dto.filter.AggregationFilter;
import uz.uzkassa.silen.dto.marking.AggregationStatsDTO;
import uz.uzkassa.silen.enumeration.PackageType;
import uz.uzkassa.silen.repository.AggregationRepositoryCustom;

import java.util.stream.Collectors;

@Slf4j
public class AggregationRepositoryImpl implements AggregationRepositoryCustom {
    @PersistenceContext(unitName = "slavePU")
    private EntityManager entityManager;

    @Override
    public Page<Aggregation> findAllByFilter(AggregationFilter filter) {
        final boolean hasSearch = StringUtils.isNotEmpty(filter.getSearch());
        final boolean hasSort = StringUtils.isNotEmpty(filter.getOrderBy());

        StringBuilder sql = new StringBuilder("select o from Aggregation o ");
        sql.append(" where o.deleted is not true");
        if (filter.getAggregationStatusList() != null) {
            String statusList = filter.getAggregationStatusList().stream().map(status -> "'" + status + "'").collect(Collectors.joining(","));
            sql.append(" and o.status in (").append(statusList).append(")");
        }
        if (filter.getOrganizationId() != null) {
            sql.append(" and o.organizationId=:organizationId");
        }
        if (filter.getPartyAggregationId() != null) {
            sql.append(" and o.partyAggregationId=:partyAggregationId");
        }
        if (filter.getType() != null) {
            sql.append(" and o.aggregationType=:aggregationType");
        }
        if (filter.getBatchId() != null) {
            sql.append(" and o.batchId=:batchId");
        }
        if (filter.getSerialId() != null) {
            sql.append(" and o.serialId=:serialId");
        }
        if (filter.getAggregationStatus() != null) {
            sql.append(" and o.status=:status");
        }
        if (filter.getUnit() != null) {
            sql.append(" and o.unit=:unit");
        }
        if (filter.getFrom() != null) {
            sql.append(" and o.lastModifiedDate>=:from");
        }
        if (filter.getTo() != null) {
            sql.append(" and o.lastModifiedDate<=:to");
        }
        if (filter.getProductId() != null) {
            sql.append(" and o.productId=:productId");
        }
        if (filter.getLogin() != null) {
            sql.append(" and lower(o.login) like :login");
        }
        if (hasSearch) {
            sql.append(" and (lower(o.code) like :searchKey)");
        }
        String countSql = sql.toString().replaceFirst("select o", "select count(o.id)");

        sql.append(" order by");
        if (hasSort) {
            sql.append(" o.").append(filter.getOrderBy());
        } else {
            sql.append(" o.lastModifiedDate");
        }
        sql.append(" ").append(filter.getSortOrder().getName());

        TypedQuery<Aggregation> query = this.entityManager.createQuery(sql.toString(), Aggregation.class)
            .setFirstResult(filter.getStart())
            .setMaxResults(filter.getSize());
        TypedQuery<Long> countQuery = entityManager.createQuery(countSql, Long.class);

        if (filter.getOrganizationId() != null) {
            query.setParameter("organizationId", filter.getOrganizationId());
            countQuery.setParameter("organizationId", filter.getOrganizationId());
        }
        if (filter.getPartyAggregationId() != null) {
            query.setParameter("partyAggregationId", filter.getPartyAggregationId());
            countQuery.setParameter("partyAggregationId", filter.getPartyAggregationId());
        }
        if (filter.getType() != null) {
            query.setParameter("aggregationType", filter.getType());
            countQuery.setParameter("aggregationType", filter.getType());
        }
        if (filter.getBatchId() != null) {
            query.setParameter("batchId", filter.getBatchId());
            countQuery.setParameter("batchId", filter.getBatchId());
        }
        if (filter.getSerialId() != null) {
            query.setParameter("serialId", filter.getSerialId());
            countQuery.setParameter("serialId", filter.getSerialId());
        }
        if (filter.getAggregationStatus() != null) {
            query.setParameter("status", filter.getAggregationStatus());
            countQuery.setParameter("status", filter.getAggregationStatus());
        }
        if (filter.getProductId() != null) {
            query.setParameter("productId", filter.getProductId());
            countQuery.setParameter("productId", filter.getProductId());
        }
        if (filter.getUnit() != null) {
            query.setParameter("unit", filter.getUnit().getCode());
            countQuery.setParameter("unit", filter.getUnit().getCode());
        }
        if (filter.getFrom() != null) {
            query.setParameter("from", filter.getFrom());
            countQuery.setParameter("from", filter.getFrom());
        }
        if (filter.getTo() != null) {
            query.setParameter("to", filter.getTo());
            countQuery.setParameter("to", filter.getTo());
        }
        if (filter.getLogin() != null) {
            query.setParameter("login", "%" + filter.getLogin().toLowerCase() + "%");
            countQuery.setParameter("login", "%" + filter.getLogin().toLowerCase() + "%");
        }
        if (hasSearch) {
            query.setParameter("searchKey", filter.getSearchForQuery());
            countQuery.setParameter("searchKey", filter.getSearchForQuery());
        }
        return new PageImpl<>(query.getResultList(), filter.getPageable(), countQuery.getSingleResult());
    }

    @Override
    public AggregationStatsDTO getStats(AggregationFilter filter) {
        final boolean hasSearch = StringUtils.isNotEmpty(filter.getSearch());
        StringBuilder sql = new StringBuilder("select ")
            .append(" count(a.id) filter (where a.unit = ").append(PackageType.BOX.getCode()).append(") as boxQuantity,")
            .append(" count(a.id) filter (where a.unit = ").append(PackageType.PALLET.getCode()).append(") as palletQuantity,")
            .append(" 0 as blockQuantity,")
            .append(" coalesce(sum(a.quantity), 0) as bottleQuantity")
            .append(" from aggregation a");
        if (filter.getProductId() != null) {
            sql.append(" inner join product p on p.id=:productId");
        }
        sql.append(" where a.deleted is not true ");
        if (filter.getOrganizationId() != null) {
            sql.append(" and a.organization_id=:organizationId");
        }
        if (filter.getPartyAggregationId() != null) {
            sql.append(" and a.party_aggregation_id=:partyAggregationId");
        }
        if (filter.getAggregationStatusList() != null) {
            String statusList = filter.getAggregationStatusList().stream().map(status -> "'" + status + "'").collect(Collectors.joining(","));
            sql.append(" and a.status in (").append(statusList).append(")");
        }
        if (filter.getType() != null) {
            sql.append(" and a.aggregation_type=:aggregationType");
        }
        if (filter.getBatchId() != null) {
            sql.append(" and a.batch_id=:batchId");
        }
        if (filter.getSerialId() != null) {
            sql.append(" and a.serial_id=:serialId");
        }
        if (filter.getAggregationStatus() != null) {
            sql.append(" and a.status=:status");
        }
        if (filter.getUnit() != null) {
            sql.append(" and a.unit=:unit");
        }
        if (filter.getProductId() != null) {
            sql.append(" and a.product_id=:productId");
        }
        if (filter.getFrom() != null) {
            sql.append(" and a.last_modified_date>=:from");
        }
        if (filter.getTo() != null) {
            sql.append(" and a.last_modified_date<=:to");
        }
        if (filter.getLogin() != null) {
            sql.append(" and lower(a.login) like :login");
        }
        if (hasSearch) {
            sql.append(" and (lower(a.code) like :searchKey");
            sql.append(")");
        }
        Query query = entityManager.createNativeQuery(sql.toString(), "AggregationStatsMapper");
        if (filter.getOrganizationId() != null) {
            query.setParameter("organizationId", filter.getOrganizationId());
        }
        if (filter.getPartyAggregationId() != null) {
            query.setParameter("partyAggregationId", filter.getPartyAggregationId());
        }
        if (filter.getType() != null) {
            query.setParameter("aggregationType", filter.getType());
        }
        if (filter.getBatchId() != null) {
            query.setParameter("batchId", filter.getBatchId());
        }
        if (filter.getSerialId() != null) {
            query.setParameter("serialId", filter.getSerialId());
        }
        if (filter.getAggregationStatus() != null) {
            query.setParameter("status", filter.getAggregationStatus().name());
        }
        if (filter.getProductId() != null) {
            query.setParameter("productId", filter.getProductId());
        }
        if (filter.getUnit() != null) {
            query.setParameter("unit", filter.getUnit().getCode());
        }
        if (filter.getFrom() != null) {
            query.setParameter("from", filter.getFrom());
        }
        if (filter.getTo() != null) {
            query.setParameter("to", filter.getTo());
        }
        if (filter.getLogin() != null) {
            query.setParameter("login", "%" + filter.getLogin() + "%");
        }
        if (hasSearch) {
            query.setParameter("searchKey", filter.getSearchForQuery());
        }
        try {
            return (AggregationStatsDTO) query.getSingleResult();
        } catch (NoResultException | NonUniqueResultException e) {
            return new AggregationStatsDTO();
        }
    }

}

package uz.uzkassa.silen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.Region;

@Repository
public interface RegionRepository extends JpaRepository<Region, Long> {
}

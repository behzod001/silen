package uz.uzkassa.silen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.uzkassa.silen.domain.WaybillExecutor;

public interface WaybillExecutorRepository extends JpaRepository<WaybillExecutor, String> {
}

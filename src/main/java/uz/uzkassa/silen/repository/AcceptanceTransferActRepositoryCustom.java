package uz.uzkassa.silen.repository;

import org.springframework.data.domain.Page;
import uz.uzkassa.silen.domain.AcceptanceTransferAct;
import uz.uzkassa.silen.dto.filter.AcceptanceTransferActFilter;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 5/25/2023 13:04
 */
public interface AcceptanceTransferActRepositoryCustom {

    Page<AcceptanceTransferAct> findAllByFilter(AcceptanceTransferActFilter filter);
}

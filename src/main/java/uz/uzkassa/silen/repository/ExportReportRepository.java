package uz.uzkassa.silen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.uzkassa.silen.domain.ExportReport;
import uz.uzkassa.silen.enumeration.ReportStatus;

import java.util.Optional;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 8/29/2023 11:16
 */
public interface ExportReportRepository extends JpaRepository<ExportReport, Long>, ExportReportRepositoryCustom {

    /**
     * checking is this organization had already pending export report
     * organization must have only one pending export report
     *
     * @param organizationId
     * @param status         {@link ReportStatus default ReportStatus.PENDING}
     * @return
     */
    Optional<ExportReport> findFirstByOrganizationIdAndStatusOrderByCreatedDateDesc(String organizationId, ReportStatus status);
}

package uz.uzkassa.silen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.AggregationHistory;

@Repository
public interface AggregationHistoryRepository extends JpaRepository<AggregationHistory, String>, AggregationHistoryRepositoryCustom {

}

package uz.uzkassa.silen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.uzkassa.silen.domain.PartyAggregation;
import uz.uzkassa.silen.enumeration.PartyAggregationStatus;

public interface PartyAggregationRepository extends JpaRepository<PartyAggregation, String>, PartyAggregationRepositoryCustom {

    @Query("select max(intNumber) from Shipment where organizationId=?1")
    Integer getMaxIntNumber(String organizationId);

    boolean existsPartyAggregationByOrganizationIdAndNumberIgnoreCase(String organizationId, String number);

    boolean existsPartyAggregationByOrganizationIdAndNumberIgnoreCaseAndIdIsNot(String organizationId, String number, String id);

    @Query("update PartyAggregation set status= :status where id=:id")
    void updateStatusParty(@Param("id") String id, @Param("status") PartyAggregationStatus status);
}

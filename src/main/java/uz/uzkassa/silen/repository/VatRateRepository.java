package uz.uzkassa.silen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.VatRate;

import java.util.Optional;

/**
 * Created by: Xayrullayev Bexzod
 * Date: 04.01.2023 11:04
 */
@Repository
public interface VatRateRepository extends JpaRepository<VatRate, Long> {

    Optional<VatRate> findFirstByCodeAndDeletedIsFalse(String code);

}

package uz.uzkassa.silen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.Bank;

import java.util.List;

/**
 * Created by: Azazello
 * Date: 12/22/2019 8:11 PM
 */
@Repository
public interface BankRepository extends JpaRepository<Bank, String> {
    List<Bank> findAllByOrderByNameRuAsc();
}

package uz.uzkassa.silen.repository;

import org.springframework.data.domain.Page;
import uz.uzkassa.silen.domain.ExportReport;
import uz.uzkassa.silen.dto.filter.ReportFilter;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 8/29/2023 15:41
 */
public interface ExportReportRepositoryCustom {
    Page<ExportReport> findAllByFilter(ReportFilter filter);
}

package uz.uzkassa.silen.repository.redis;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.redis.RedisAggregation;

import java.util.Optional;

@Repository
public interface RedisAggregationRepository extends CrudRepository<RedisAggregation, Long> {
    Optional<RedisAggregation> findFirstByCode(String code);
}

package uz.uzkassa.silen.repository.redis;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.redis.RedisLastAggregationMarks;
import uz.uzkassa.silen.enumeration.PackageType;

import java.util.List;


@Repository
public interface RedisLastAggregationMarksRepository extends CrudRepository<RedisLastAggregationMarks, String> {
    List<RedisLastAggregationMarks> findAllBySessionIdAndProductId(Long userId, String productId);

    List<RedisLastAggregationMarks> findAllBySessionIdAndProductIdAndUnitAndChild(Long sessionId, String productId, Integer unit, boolean child);

    List<RedisLastAggregationMarks> findAllBySessionId(Long userId);
}

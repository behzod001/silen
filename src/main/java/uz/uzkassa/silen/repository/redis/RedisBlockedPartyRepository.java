package uz.uzkassa.silen.repository.redis;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.redis.RedisAggregation;
import uz.uzkassa.silen.domain.redis.RedisBlockedParty;

@Repository
public interface RedisBlockedPartyRepository extends CrudRepository<RedisBlockedParty, String> {
}

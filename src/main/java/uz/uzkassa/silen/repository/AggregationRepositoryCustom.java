package uz.uzkassa.silen.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.uzkassa.silen.domain.Aggregation;
import uz.uzkassa.silen.dto.filter.AggregationFilter;
import uz.uzkassa.silen.dto.marking.AggregationStatsDTO;

public interface AggregationRepositoryCustom {
    Page<Aggregation> findAllByFilter(AggregationFilter filter);

    AggregationStatsDTO getStats(AggregationFilter filter);


}

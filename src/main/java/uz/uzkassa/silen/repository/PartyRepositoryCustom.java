package uz.uzkassa.silen.repository;

import org.springframework.data.domain.Page;
import uz.uzkassa.silen.domain.Party;
import uz.uzkassa.silen.dto.filter.MongoFilter;

public interface PartyRepositoryCustom {
    Page<Party> findAllByFilter(MongoFilter filter);
}

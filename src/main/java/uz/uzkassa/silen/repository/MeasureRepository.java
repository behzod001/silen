package uz.uzkassa.silen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.Measure;

import java.util.List;
import java.util.Optional;

/**
 * Created by: Azazello
 * Date: 12/22/2019 8:11 PM
 */

@Repository
public interface MeasureRepository extends JpaRepository<Measure, Long> {
    Optional<Measure> findDistinctTopByNameRuIgnoreCase(String name);

    List<Measure> findAllByOrderByNameRuAsc();
}

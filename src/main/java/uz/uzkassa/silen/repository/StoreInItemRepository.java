package uz.uzkassa.silen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.uzkassa.silen.domain.StoreInItem;

import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 9/22/2023 15:30
 */
public interface StoreInItemRepository extends JpaRepository<StoreInItem, Long> {
    List<StoreInItem> findAllByStoreInId(String id);

    Optional<StoreInItem> findFirstByStoreInIdAndProductId(@NotNull String storeInId, @NotNull String productId);

    Optional<StoreInItem> findFirstByStoreInIdAndProductIdAndSerialId(String storeInId, String productId, String serialId);

    void deleteAllByStoreInId(String storeInId);

    boolean existsByStoreInIdAndProductId(String storeInId, String productId);
}

package uz.uzkassa.silen.repository.mongo.impl;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.dto.filter.MongoFilter;
import uz.uzkassa.silen.enumeration.SortTypeEnum;
import uz.uzkassa.silen.repository.mongo.MarkRepositoryCustom;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;

@Repository
public class MarkRepositoryImpl implements MarkRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    public MarkRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Page<Mark> findAllByFilter(MongoFilter filter) {
        Query query = new Query();
        Criteria criteria = Criteria.where("partyId").is(filter.getPartyId());
        List<Criteria> criterias = new ArrayList<>();
        if (filter.getOrderProductId() != null) {
            criterias.add(Criteria.where("orderProductId").is(filter.getOrderProductId()));
        }/* else {
            criterias.add(Criteria.where("orderProductId").ne(null));
        }*/
        if (filter.getProductId() != null) {
            criterias.add(Criteria.where("productId").is(filter.getProductId()));
        }
        if (filter.getOrganizationId() != null) {
            criterias.add(Criteria.where("organizationId").is(filter.getOrganizationId()));
        }
        if (filter.getUsed() != null) {
            criterias.add(Criteria.where("used").is(filter.getUsed()));
        }
        if (filter.getAggregationStatus() != null) {
            criterias.add(Criteria.where("status").is(filter.getAggregationStatus()));
        }
        if (CollectionUtils.isNotEmpty(criterias)) {
            criteria.andOperator(criterias.toArray(new Criteria[criterias.size()]));
        }
        query.addCriteria(criteria);
        if (StringUtils.isNotEmpty(filter.getSearch())) {
            Criteria searchCriteria = new Criteria();
            searchCriteria.orOperator(
                Criteria.where("printCode").regex(filter.getSearch(), "i"),
                Criteria.where("code").regex(filter.getSearch(), "i"),
                Criteria.where("parentCode").regex(filter.getSearch(), "i")
            );
            query.addCriteria(searchCriteria);
        }
        final Long totalCount = mongoTemplate.count(query, Mark.class);

        Sort.Direction direction = SortTypeEnum.asc.equals(filter.getSortOrder()) ? Sort.Direction.ASC : Sort.Direction.DESC;
        Sort sorting = Sort.by(direction, Objects.toString(filter.getOrderBy(), "id"));
        query.with(sorting);
        query.with(filter.getPageable());

        return new PageImpl<>(mongoTemplate.find(query, Mark.class), filter.getPageable(), totalCount);
    }


    @Override
    public BigDecimal findAllByFilterStats(MongoFilter filter) {
        List<AggregationOperation> matches = new ArrayList<>();
        matches.add(new MatchOperation(Criteria.where("partyId").is(filter.getPartyId())));

        if (filter.getOrderProductId() != null) {
            matches.add(new MatchOperation(Criteria.where("orderProductId").is(filter.getOrganizationId())));
        }/* else {
            matches.add(new MatchOperation(Criteria.where("orderProductId").ne(null)));
        }*/
        if (filter.getProductId() != null) {
            matches.add(new MatchOperation(Criteria.where("productId").is(filter.getProductId())));
        }
        if (filter.getOrganizationId() != null) {
            matches.add(new MatchOperation(Criteria.where("organizationId").is(filter.getOrganizationId())));
        }
        if (filter.getUsed() != null) {
            matches.add(new MatchOperation(Criteria.where("used").is(filter.getUsed())));
        }
        if (filter.getFrom() != null) {
            matches.add(new MatchOperation(Criteria.where("createdDate").gte(filter.getUsed())));
        }
        if (filter.getTo() != null) {
            matches.add(new MatchOperation(Criteria.where("createdDate").lte(filter.getUsed())));
        }
        if (StringUtils.isNotEmpty(filter.getSearch())) {
            Criteria searchCriteria = new Criteria();
            searchCriteria.orOperator(Criteria.where("printCode").regex(filter.getSearch(), "i"), Criteria.where("code").regex(filter.getSearch(), "i"));
            matches.add(new MatchOperation(searchCriteria));
        }

        GroupOperation groupOperation = group("organizationId").sum("quantity").as("quantity");
        matches.add(groupOperation);

        Aggregation aggregate = Aggregation.newAggregation(matches);

        AggregationResults<Mark> orderAggregate = mongoTemplate.aggregate(aggregate, Mark.class, Mark.class);
        BigDecimal quantity = BigDecimal.ZERO;
        if (orderAggregate.getMappedResults().size() > 0) {
            quantity = quantity.add(BigDecimal.valueOf(orderAggregate.getMappedResults().get(0).getQuantity()));
        }

        return quantity;
    }
}

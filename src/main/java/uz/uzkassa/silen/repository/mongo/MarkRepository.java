package uz.uzkassa.silen.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.enumeration.AggregationStatus;
import uz.uzkassa.silen.enumeration.MarkStatus;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface MarkRepository extends MongoRepository<Mark, String>, MarkRepositoryCustom {

    boolean existsByCode(String code);

    Optional<Mark> findFirstByCode(String code);

    List<Mark> findAllByParentCode(String parentCode);

    List<Mark> findAllByParentCodeIn(Set<String> parentCodes);

    List<Mark> findAllByCodeInAndUsedIsTrue(Set<String> printCodes);

    List<Mark> findAllByCodeInAndUsedIsFalse(Set<String> printCodes);

    Long countMarksByPartyIdAndUsedIsFalse(String partyId);

    List<Mark> findAllByPartyIdAndUsedIsFalse(String partyId);

    List<Mark> findAllByCodeIn(Collection<String> printCodes);

    List<Mark> findAllByPartyId(String partyId);

    Optional<Mark> findFirstByPartyId(String partyId);

    List<Mark> findAllByParentCodeAndTuronStatusIsNot(String parentCode, MarkStatus markStatus);

    List<Mark> findAllByCodeInAndStatusIn(Set<String> printCodes, List<AggregationStatus> statuses);

    Long countAllByPartyIdInAndUsedIsFalse(Collection<String> partyIds);

    Long countByParentCode(String code);
}

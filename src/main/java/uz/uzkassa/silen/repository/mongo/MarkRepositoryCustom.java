package uz.uzkassa.silen.repository.mongo;

import org.springframework.data.domain.Page;
import uz.uzkassa.silen.domain.mongo.Mark;
import uz.uzkassa.silen.dto.filter.MongoFilter;

import java.math.BigDecimal;

public interface MarkRepositoryCustom {
    Page<Mark> findAllByFilter(MongoFilter filter);

    BigDecimal findAllByFilterStats(MongoFilter filter);
}

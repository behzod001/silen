package uz.uzkassa.silen.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import uz.uzkassa.silen.domain.mongo.StoreInMark;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 9/23/2023 15:46
 */

@Repository
public interface StoreInMarkRepository extends MongoRepository<StoreInMark, String> {

    Set<StoreInMark> findAllByItemId(Long id);

    List<StoreInMark> findAllByItemIdAndCodeIn(Long itemId, Set<String> codes);

    Optional<StoreInMark> findFirstByItemIdAndCode(Long itemId, String code);
}

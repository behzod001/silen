package uz.uzkassa.silen.repository;

import org.springframework.data.domain.Page;
import uz.uzkassa.silen.domain.StoreIn;
import uz.uzkassa.silen.dto.filter.StoreInFilter;

/**
 * Created by: Abdulaziz Pulatjonov
 * Date: 9/19/2023 11:15
 */
public interface StoreInRepositoryCustom{
    Page<StoreIn> findAllByFilter(StoreInFilter filter);
}

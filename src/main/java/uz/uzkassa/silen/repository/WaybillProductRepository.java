package uz.uzkassa.silen.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.uzkassa.silen.domain.WaybillProduct;

public interface WaybillProductRepository extends JpaRepository<WaybillProduct, String>, WaybillRepositoryCustom {
}

package uz.uzkassa.silen.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicUpdate;
import uz.uzkassa.silen.config.DbConstants;
import uz.uzkassa.silen.dto.invoice.VillageDTO;
import uz.uzkassa.silen.security.SecurityUtils;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.io.Serializable;

/**
 * Created by: Azazello
 * Date: 12/22/2019 3:00 PM
 */

@Entity
@Table(schema = DbConstants.CURRENT_SCHEMA, name = "village")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@DynamicUpdate
public class Village implements Serializable {
    @Id
    private Long id;

    @Column(name = "code")
    private Long code;

    @Column(name = "name")
    private String name;

    @Column(name = "name_ru")
    private String nameRu;

    @Column(name = "name_uz")
    private String nameUz;

    @Column(name = "name_uz_cryl")
    private String nameUzCryl;

    @Column(name = "soato")
    private String soato;

    @Column(name = "region_id")
    private Long regionId;

    @Column(name = "district_id")
    private Long districtId;

    public Village(Long id) {
        this.id = id;
    }

    public String getNameLocale() {
        final String locale = SecurityUtils.getCurrentLocale();
        String name = "";
        switch (locale) {
            case "uz":
                name = this.getNameUz();
                break;
            default:
                name = this.getNameRu();
                break;
        }
        if (name == null || name.trim().length() == 0) {
            name = this.getNameRu();
        }
        return name;
    }

    public VillageDTO toDto() {
        VillageDTO dto = new VillageDTO();
        dto.setId(getId());
        dto.setName(getName());
        dto.setNameUzCyrl(getNameUzCryl());
        dto.setNameUzLatn(getNameUz());
        dto.setNameRu(getNameRu());
        dto.setSoato(getSoato());
        dto.setCode(getCode());
        return dto;
    }
}

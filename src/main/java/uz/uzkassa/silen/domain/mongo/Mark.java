package uz.uzkassa.silen.domain.mongo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import uz.uzkassa.silen.dto.mongo.BlockDTO;
import uz.uzkassa.silen.dto.mongo.MarkDTO;
import uz.uzkassa.silen.enumeration.AggregationStatus;
import uz.uzkassa.silen.enumeration.MarkStatus;
import uz.uzkassa.silen.enumeration.PackageType;
import uz.uzkassa.silen.utils.Utils;

import java.time.LocalDateTime;
import java.util.regex.Matcher;

@ToString
@Getter
@Setter
//@CompoundIndexes(value = {
//    @CompoundIndex(name = "org_par_idx", def = "{'organizationId': 1, 'partyId': 1}")
//})
@Document(collection = "mark")
public class Mark {
    @Id
    private String id;
    @Indexed
    private String code;
    private String printCode;
    private String orderProductId;
    @Indexed
    private String partyId;
    private String productId;
    private String productName;
    @Indexed
    private String parentCode;
    private boolean used;
    private LocalDateTime createdDate;
    @Indexed
    private String organizationId;
    private AggregationStatus status;
    private MarkStatus turonStatus;
    private boolean block;
    private int quantity;
    private String login;
    private LocalDateTime aggregationDate;
    private String description;
    private String reportId;
    private PackageType unit;
    private LocalDateTime productionDate;
    private String utilisationId;
    private LocalDateTime utilisationTime;

    public PackageType getPackageType() {
        return unit != null ? unit : PackageType.BOTTLE;
    }

    public AggregationStatus getStatus() {
        return status == null ? AggregationStatus.DRAFT : status;
    }

    public MarkDTO toDto() {
        MarkDTO dto = new MarkDTO();
        dto.setId(getId());
        dto.setCode(getCode());
        dto.setPrintCode(getPrintCode());
        dto.setOrderProductId(getOrderProductId());
        dto.setPartyId(getPartyId());
        dto.setProductId(getProductId());
        dto.setProductName(getProductName());
        dto.setParentCode(getParentCode());
        dto.setUsed(isUsed());
        dto.setOrganizationId(getOrganizationId());
        dto.setCreatedDate(getCreatedDate());
        if (getStatus() != null) {
            dto.setStatus(getStatus());
            dto.setStatusName(getStatus().getText());
        }
        if (getTuronStatus() != null) {
            dto.setTuronStatus(getTuronStatus());
            dto.setTuronStatusName(getTuronStatus().getText());
        }
        Matcher matcher = Utils.parseMark(getPrintCode());
        if (matcher != null && matcher.find()) {
            dto.setPackUnit(matcher.group(1));
            dto.setGtin(matcher.group(2));
            dto.setSerialNumber(matcher.group(3));
            dto.setOther(matcher.group(4));
        }
        return dto;
    }

    public BlockDTO toBlockDto() {
        BlockDTO dto = new BlockDTO();
        dto.setId(getId());
        dto.setCode(getCode());
        dto.setPrintCode(getPrintCode());
        dto.setOrderProductId(getOrderProductId());
        dto.setPartyId(getPartyId());
        dto.setProductId(getProductId());
        dto.setProductName(getProductName());
        dto.setParentCode(getParentCode());
        dto.setUsed(isUsed());
        dto.setOrganizationId(getOrganizationId());
        dto.setCreatedDate(getCreatedDate());
        if (this.getTuronStatus() != null) {
            dto.setMarkStatus(getTuronStatus());
        }
        if (this.getStatus() != null) {
            dto.setStatus(getStatus());
            dto.setStatusName(this.getStatus().getText());
        }
        dto.setQuantity(getQuantity());
        dto.setUnit(PackageType.BLOCK);
        dto.setUnitName(PackageType.BLOCK.getNameRu());
        dto.setTuronAggregationId(getReportId());
        return dto;
    }

    public BlockDTO toListBlockDto() {
        BlockDTO dto = new BlockDTO();
        dto.setId(getId());
        dto.setProductName(getProductName());
        dto.setPrintCode(getPrintCode());
        dto.setCode(getCode());
        dto.setQuantity(getQuantity());
        dto.setCreatedDate(getCreatedDate());
        dto.setAggregationDate(getAggregationDate());
        dto.setLogin(getLogin());
        if (this.getStatus() != null) {
            dto.setStatus(getStatus());
            dto.setStatusName(this.getStatus().getText());
        }
        dto.setUnit(PackageType.BLOCK);
        dto.setUnitName(PackageType.BLOCK.getNameRu());
        dto.setTuronAggregationId(getReportId());
        return dto;
    }
}

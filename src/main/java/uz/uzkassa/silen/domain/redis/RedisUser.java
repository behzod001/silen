package uz.uzkassa.silen.domain.redis;

import lombok.*;
import org.springframework.data.redis.core.RedisHash;

import jakarta.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@ToString
@RedisHash(value = "User", timeToLive = 28800)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RedisUser implements Serializable {
    @Id
    private String id;
    private LocalDateTime logInDate;
}

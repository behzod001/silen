package uz.uzkassa.silen.domain.redis;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import org.springframework.data.redis.core.RedisHash;

import jakarta.persistence.Id;

@ToString
@RedisHash("blocked_party")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class RedisBlockedParty {
    @Id
    String id;

    public RedisBlockedParty(String id) {
        this.id = id;
    }
}

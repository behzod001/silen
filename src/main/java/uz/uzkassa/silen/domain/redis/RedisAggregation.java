package uz.uzkassa.silen.domain.redis;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import jakarta.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

@ToString
@RedisHash("aggregation")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class RedisAggregation implements Serializable {
    @Id
    Long id;
    @Indexed
    String code;
    Set<String> marks;

    String productId;
    String organizationId;
    boolean sendUtilise;
    boolean offline;
    LocalDateTime createdAt = LocalDateTime.now();
}

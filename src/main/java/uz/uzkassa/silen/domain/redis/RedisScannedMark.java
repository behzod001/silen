package uz.uzkassa.silen.domain.redis;

import lombok.*;
import org.springframework.data.redis.core.RedisHash;

import jakarta.persistence.Id;
import java.io.Serializable;

@ToString
@RedisHash(value = "ScannedMark", timeToLive = 3600)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RedisScannedMark implements Serializable {
    @Id
    private String id;
}

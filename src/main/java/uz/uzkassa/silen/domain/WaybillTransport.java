package uz.uzkassa.silen.domain;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;
import uz.uzkassa.silen.config.DbConstants;
import uz.uzkassa.silen.dto.waybill.PersonDTO;
import uz.uzkassa.silen.dto.waybill.ProductGroupDTO;
import uz.uzkassa.silen.dto.waybill.TransportDTO;
import uz.uzkassa.silen.dto.waybill.TruckDTO;
import uz.uzkassa.silen.enumeration.TrailerType;
import uz.uzkassa.silen.enumeration.TransportType;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(schema = DbConstants.CURRENT_SCHEMA, name = "waybill_transport")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@SQLDelete(sql = "UPDATE waybill_transport SET deleted = 'true' WHERE id=?")
public class WaybillTransport extends BaseOrganizationEntity implements Serializable {

    @Enumerated(EnumType.STRING)
    @Column(name = "transport_type")
    TransportType transportType;

    @Column(name = "driver_pinfl")
    String driverPinfl;
    @Column(name = "driver_full_name")
    String driverFullName;

    @Column(name = "truck_reg_no")
    String truckRegNo;
    @Column(name = "truck_model")
    String truckModel;

    @Column(name = "trailer_reg_no")
    String trailerRegNo;
    @Column(name = "trailer_model")
    String trailerModel;

    @Enumerated(EnumType.STRING)
    @Column(name = "trailer_type")
    TrailerType trailerType;

    @Column(name = "waybill_id", updatable = false, insertable = false)
    String waybillId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "waybill_id")
    Waybill waybill;

    @ElementCollection(targetClass = Carriages.class, fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @Column(name = "carriage", nullable = false)
    @CollectionTable(name = "waybill_carriages", joinColumns = @JoinColumn(name = "waybill_transport_id", referencedColumnName = "id"))
    private List<Carriages> carriages = new ArrayList<>();

    @OneToMany(mappedBy = "waybillTransport", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @SQLRestriction("deleted is not true")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    List<WaybillProductGroups> productGroups = new ArrayList<>();
//
//    public List<Carriages> getCarriages() {
//        if (carriages != null) {
//            carriages = new ArrayList<>();
//        }
//        return carriages;
//    }

    public List<WaybillProductGroups> getProductGroups() {
        if (productGroups == null) {
            productGroups = new ArrayList<>();
        }
        return productGroups;
    }

    public void addProductGroups(WaybillProductGroups productGroups) {
        productGroups.setWaybillTransport(this);
        this.getProductGroups().add(productGroups);
    }

    public TransportDTO toDTO() {
        TransportDTO dto = new TransportDTO();
        dto.setId(getId());
        if (getDriverPinfl() != null) {
            dto.setDriver(new PersonDTO(getDriverPinfl(), getDriverFullName()));
        }
        if(getTruckRegNo()!=null){
            dto.setTruckDTO(new TruckDTO(getTruckRegNo(), getTruckModel()));
        }
        if(getTrailerRegNo()!=null){
            dto.setTrailer(new TruckDTO(getTrailerRegNo(), getTrailerModel()));
        }
        dto.setTrailerType(getTrailerType());
        dto.setTransportType(getTransportType());

        List<TruckDTO> dtoList = new ArrayList<>();
        for (Carriages carriage : getCarriages()) {
            dtoList.add(new TruckDTO(carriage.getRegNo(), carriage.getModel()));
        }
        if (CollectionUtils.isNotEmpty(dtoList)) {
            dto.setCarriages(dtoList);
        }

        if (getProductGroups() != null && CollectionUtils.isNotEmpty(getProductGroups())) {
            List<ProductGroupDTO> collect = new ArrayList<>();
            for (WaybillProductGroups productGroup : getProductGroups()) {
                ProductGroupDTO productGroupDTO = productGroup.toDTO();
                collect.add(productGroupDTO);
            }
            dto.setProductGroups(collect);
        }
        return dto;
    }
}

package uz.uzkassa.silen.domain;

import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@MappedSuperclass
public class BaseOrganizationEntity extends BaseEntity implements Serializable {
    @Column(name = "organization_id")
    private String organizationId;
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_id", updatable = false, insertable = false)
    private Organization organization;
}

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

insert into users (id,first_name, last_name, login, password_hash, created_by, created_date, activated, role) values (uuid_generate_v4(),'Admin', 'Asap', 'a5ap2021', '$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC', 'system', now(), true, 'ADMIN'); --5uprAdm1n$
insert into users (id,first_name, last_name, login, password_hash, created_by, created_date, activated, role) values (uuid_generate_v4(),'Silen', 'Device', '5I1en_dev1ce', '$2a$10$yjl3LQzA/.azSeyEMERlsOwoJOSBULtFMOvmIKsczdS1jiaWEYFqS', 'system', now(), true, 'DEVICE'); --T0P@5eCure

-- Start here
INSERT INTO public.permissions (name, code, position, section, parent_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES ('Permission admin', 'ROLE_ADMIN', null, false, null, 'Superadmin', '2023-10-04 16:09:00.682000', null, null, false);
INSERT INTO public.permissions (name, code, position, section, parent_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES ('Permission user', 'ROLE_USER', null, false, null, 'Superadmin', '2023-10-04 16:09:00.682000', null, null, false);
INSERT INTO public.permissions (name, code, position, section, parent_id, created_by, created_date, last_modified_by, last_modified_date, deleted) VALUES ('Permission device', 'ROLE_DEVICE', null, false, null, 'Superadmin', '2023-10-04 16:09:00.682000', null, null, false);

INSERT INTO public.role_permissions (role, permission_code) VALUES ('ADMIN', 'ROLE_ADMIN');
INSERT INTO public.role_permissions (role, permission_code) VALUES ('ADMIN', 'ROLE_USER');
INSERT INTO public.role_permissions (role, permission_code) VALUES ('DEVICE', 'ROLE_DEVICE');
INSERT INTO public.role_permissions (role, permission_code) VALUES ('DEVICE', 'ROLE_ADMIN');
INSERT INTO public.role_permissions (role, permission_code) VALUES ('DEVICE', 'ROLE_USER');
